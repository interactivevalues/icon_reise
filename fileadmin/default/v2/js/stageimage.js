$(document).ready(function() {
  var p = getJSParams('stageimage.js');
  var slSpeed = p.speed ? parseInt(p.speed) : 500;
  var slIntv = p.intv ? parseInt(p.intv) : 5000;
  var slDelay = p.delay ? parseInt(p.delay) : 2500;
  if( $('#stageImages img').length > 1 ) {
    $("#stageImages").slidesjs({
      effect:{slide:{speed:slSpeed}, fade:{speed:slSpeed}},
      navigation:{active:false, effect:"fade"},
      pagination:{active:true, effect:"fade"},
      play: {
        active:true,
        effect:"fade",
        interval:slIntv,
        auto:true,
        swap:true,
        pauseOnHover:true,
        restartDelay:slDelay
      }
    });
  }
});