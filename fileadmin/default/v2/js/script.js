/*
ALLGEMEINE SCRIPTS für alle SEITEN gültig
*/

$.urlParam = function(name){
  var res = new RegExp('[\\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
  try { var ret = res[1]; } catch(e) { return ret = 0; }
  return ret;
}

function getJSParams (pFile) {
  var params = {};
  var scripts = document.getElementsByTagName('script');
  try {
    for( var i=0; i<scripts.length; i++){
      if( scripts[i].src.indexOf(pFile) >= 0 ){
        var tokens = scripts[i].src.split('?')[1].split('&');
        for(var k=0; k<tokens.length; k++) {
          var tmp = tokens[k].split('=');
          params[tmp[0]] = tmp[1];
        }
        break;
      }
    }
  }
  catch (e) {}
  return params;
}


$(document).ready(function() {
	//	prepare MobielMenu
	$('#nav > li > a').each(function(idx, elm){
		$(this).after('<i class="subMenuOpener fa fa-caret-right" aria-hidden="true" data-id="'+idx+'"></i>');
		$(this).parent().find('ul').attr('id', 'submenu-'+idx);
	});
	$('.subMenuOpener').click(function(){
		if( $('#submenu-'+$(this).attr('data-id')).hasClass('opened') ){
			$('#submenu-'+$(this).attr('data-id')).removeClass('opened');
			$(this).removeClass('fa-caret-down').addClass('fa-caret-right');
		} else {
			$('#nav li ul').removeClass('opened');
			$('.subMenuOpener').removeClass('fa-caret-down').addClass('fa-caret-right');
			$('#submenu-'+$(this).attr('data-id')).addClass('opened');
			$(this).removeClass('fa-caret-right').addClass('fa-caret-down');
		}
	});
	//	enable MobileMenu-Button
	$('#mobileMenu').click(function(){
		//
		$('#nav li ul').removeClass('opened');
		$('.subMenuOpener').removeClass('fa-caret-down').addClass('fa-caret-right');
		//
		if( $(this).hasClass('opened') ){
			//	-> close!
			$(this).removeClass('opened');
			$(this).find('i.fa').removeClass('fa-times').addClass('fa-bars');
			$('#navigationFirstLevelMenu').removeClass('mobileNav');
			$('#langMenu').removeClass('opened');
			$('#searchBox').removeClass('opened');
			$('html, body').removeClass('no-scrollbar');
		} else {
			//	-> open!
			$(this).addClass('opened');
			$(this).find('i.fa').removeClass('fa-bars').addClass('fa-times');
			$('#navigationFirstLevelMenu').addClass('mobileNav');
			$('#langMenu').addClass('opened');
			$('#searchBox').addClass('opened');
			$('html, body').addClass('no-scrollbar');
		}
	});
});