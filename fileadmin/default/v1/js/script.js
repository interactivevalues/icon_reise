/*
ALLGEMEINE SCRIPTS für alle SEITEN gültig
*/
$.urlParam = function(name){
  var res = new RegExp('[\\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
  try { var ret = res[1]; } catch(e) { return ret = 0; }
  return ret;
}

function getJSParams (pFile) {
  var params = {};
  var scripts = document.getElementsByTagName('script');
  try {
    for( var i=0; i<scripts.length; i++){
      if( scripts[i].src.indexOf(pFile) >= 0 ){
        var tokens = scripts[i].src.split('?')[1].split('&');
        for(var k=0; k<tokens.length; k++) {
          var tmp = tokens[k].split('=');
          params[tmp[0]] = tmp[1];
        }
        break;
      }
    }
  }
  catch (e) {}
  return params;
}