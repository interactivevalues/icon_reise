function showDiv (div, wert) {
  document.getElementById(div).style.display=wert;
  if (wert=='block') {
    height = jQuery('body').height();
    width = jQuery('body').width();
    height_window = jQuery(window).height();
    width_window = jQuery(window).width();
    if (height_window>height) height = height_window;
    if (div=='divweiterempfehlen') {
      scrollTop = jQuery(window).scrollTop();
      xtop = String(parseInt(scrollTop - 20 + ((height_window - jQuery('#'+div).height()) / 2))) +'px';
      xleft = String(parseInt(((width_window - jQuery('#'+div).width()) / 2) - 20)) +'px';
      jQuery('#'+div).css('top', xtop);
      jQuery('#'+div).css('left', xleft);
    } else if (div=='divweiterempfehlen_background') {
      jQuery('#'+div).height(height);
      jQuery('#'+div).width(width_window);
    }
  }
}


function buildWeiterempfehlen() {
  var sc_w = document.getElementById('sc_weiterempfehlen').value;
  var id_w = document.getElementById('id_weiterempfehlen').value;
  var xo_w = document.getElementById('xo_weiterempfehlen').value;

  var data = '';
  data += '&empfehlung_send=1';
  if (sc_w!='') data += '&sc='+sc_w;
  if (id_w!='') data += '&id='+id_w;
  if (xo_w!='') data += '&xo='+xo_w;
  data += '&toEmail='+document.getElementById('recipient').value;
  data += '&fromEmail='+document.getElementById('sender').value;
  data += '&nachricht='+document.getElementById('nachricht').value;
  data += '&verify_code='+document.getElementById('verify_code').value;
  data += '&verify_key='+document.getElementById('verify_key').value;

  sendWeiterempfehlen(data);
}
function buildWeiterempfehlen0() {
  var sc_w = document.getElementById('sc_weiterempfehlen').value;
  var id_w = document.getElementById('id_weiterempfehlen').value;
  var xo_w = document.getElementById('xo_weiterempfehlen').value;

  var data = '';
  data += '&empfehlung_send=0';
  if (sc_w!='') data += '&sc='+sc_w;
  if (id_w!='') data += '&id='+id_w;
  if (xo_w!='') data += '&xo='+xo_w;

  sendWeiterempfehlen(data);
}

function sendWeiterempfehlen(data) {
var xmlHttp = null;
// Mozilla, Opera, Safari sowie Internet Explorer (ab v7)
if (typeof XMLHttpRequest != 'undefined') {
    xmlHttp = new XMLHttpRequest();
}
if (!xmlHttp) {
    // Internet Explorer 6 und �lter
    try {
        xmlHttp  = new ActiveXObject("Msxml2.XMLHTTP");
    } catch(e) {
        try {
            xmlHttp  = new ActiveXObject("Microsoft.XMLHTTP");
        } catch(e) {
            xmlHttp  = null;
        }
    }
}
if (xmlHttp) {
    xmlHttp.open('post', 'modules/customized/mpages/empfehlung/show', true);
    xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4) {
            var text = xmlHttp.responseText;
            document.getElementById('divweiterempfehlen').innerHTML = text;
            //document.getElementById('divweiterempfehlen').style.display = 'none';
            //document.getElementById('divweiterempfehlen_msg').style.display = 'block';
        }
    };
    xmlHttp.send(data);
}
}

jQuery(document).ready(function(){
  display = jQuery('#divweiterempfehlen').css('display');
  if (display=='block') {
    showDiv('divweiterempfehlen', 'block');
    showDiv('divweiterempfehlen_background', 'block');
  }
});
