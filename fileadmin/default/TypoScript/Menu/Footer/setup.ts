/*
The FOOTER MENU template.

Configuration for the menu of the first level of pages. It inherits the MAIN
MENU template and has some small modifications on top of it, in this case the
setting of the entryLevel.
*/

# Start with copying our default menu configuration so we don't have to repeat our selves (except in the explanation, didn't I already mention this?) ;-)
menu.footer = COA
menu.footer.50 = < menu.template
menu.footer.50 {
  # Since this is the top-level menu, we start this menu at the root level of the website
  entryLevel = 0
  special = directory
  special.value = {$menu.footer.PID}
  
  1 {
    #wrap = <ul>|</ul>
    NO {
      wrapItemAndSub = | |*| | |*| |
    }
 
    # Copy properties of normal to active state, and then add a CSS class for styling
    ACT < .NO
    ACT {
      ATagParams = class="active"
    }

    # Copy properties of normal to current state, and then add a CSS class for styling
    CUR < .NO
    CUR {
      ATagParams = class="selected"
    }

  }
}

#menu.footer.70 = TEXT
#menu.footer.70.value = Newsletter Anmelden
#menu.footer.70.wrap = <a href="mailto:Claudia.Hois@icon.at?subject=NEWSLETTERANMELDUNG">|</a>

#menu.footer.80 = TEXT
#menu.footer.80.value = SEITE WEITEREMPFEHLEN
#menu.footer.80.wrap = <a href="#" onclick="buildWeiterempfehlen0();showDiv('divweiterempfehlen', 'block');showDiv('divweiterempfehlen_background', 'block');showDiv('divweiterempfehlen_msg', 'none');return false;">|</a>


[globalVar = TSFE:id > 1]

menu.footer.20 = TEXT
menu.footer.20.value = Zur&uuml;ck
menu.footer.20.wrap = <a href="javascript:history.back()" class="back">|</a>

menu.footer.30 = TEXT
menu.footer.30.value = Drucken
menu.footer.30.wrap = <a href="javascript:window.print();">|</a>

[end]


[globalVar = GP:L = 1]
menu.footer.20.value = Back
menu.footer.30.value = Print
#menu.footer.70.value = SUBSCRIBE NEWSLETTER
#menu.footer.80.value = RECOMMEND PAGE

[end]

