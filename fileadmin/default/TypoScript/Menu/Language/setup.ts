/*
The LANGUAGE menu

Generates a list of available languages on a page. If the page has no language
overlay, there is no link available to switch to the language.
*/

menu.language = HMENU
menu.language {
  special = language
  1 = TMENU
  1.NO = 1
  1.NO.stdWrap.cObject = TEXT
}
// Condition to set language according to L POST/GET variable
[globalVar = GP:L = 1]

menu.language.special.value = 0
menu.language.1.NO.stdWrap.cObject.value = {$contentpage.language0}

[else]

menu.language.special.value = 1
menu.language.1.NO.stdWrap.cObject.value = {$contentpage.language1}

[global]



[globalVar = GP:tx_ttaddressicon_pi2|address > 0 , GP:tx_ttaddressicon_pi3|address > 0]

menu.language.addQueryString = 1
menu.language.addQueryString.exclude = L,id,no_cache
menu.language.addQueryString.method = GET

[global]

