/*
The JS template.

Special condition for Frontpage.
*/

#  GENREALL JS-LIBs
page.includeJS {
  jquery = {$filepaths.files.js.jq_main}
  jquery.forceOnTop = 1
  file40 = {$filepaths.files.js.jq_placeholder}
}


[globalVar = TSFE:id=1]

page.includeJS {
  file50 = {$filepaths.files.js.jq_slider}
  file90 = {$filepaths.files.js.scripts}
  file100 = {$stage.images.jsFile}{$stage.images.jsParams}
}

[ELSE]

page.includeJS {
//  file70 = {$filepaths.files.js.jq_fancybox}
  file90 = {$filepaths.files.js.scripts}
}

[END]

page.headerData {
  999999 = TEXT
  999999.value (

  <script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-10168271-1', 'auto');
  ga('send', 'pageview');

</script>
  
  )
}
