/*
The PAGE OBJECT template. 

Tells the PAGE object to use the parsed HTML template from the automaketemplate extension.
*/

# Make the PAGE object
page = PAGE
page {
  # Regular pages always have typeNum = 0
  typeNum = 0

  # Add the icon that will appear in front of the url in the browser
  # This icon will also be used for the bookmark menu in browsers
  shortcutIcon = {$filepaths.images}favicon.ico

  # Add class to bodytag to select which columns will be used in the HTML template
  # Labels for the values used in this field are defined in the TSconfig field of the root page of the website
  bodyTagCObject = CASE
  bodyTagCObject {
    # The value of the CASE object will depend on the value of the layout field in the page records
    key.field = layout

    # Define the default value
    default = TEXT
    default.value = <body>

    # Copy the default value to 0
    0 < .default

    # Add different values for cases 1, 2 and 3
    1 = TEXT
    1.wrap = <body class="|">
    1.value = pageWide

    2 < .1
    2.value = Galerie

    3 < .1
    3.value = frontPage    
  }

  # Add a TEMPLATE object to the page
  # We use the template autoparser extension to easily replace parts of the HTML template by dynamic TypoScript objects
  10 = TEMPLATE
  10 {
    # Use the HTML template from the automake template plugin
    template =< plugin.tx_automaketemplate_pi1

    # Use the <body> subpart
    workOnSubpart = DOCUMENT_BODY

    # Link content and page blocks to id's that have been enabled in the
    # automaketemplate template in the extension_configuration sysfolder
    subparts {
      # Insert menu's from lib-objects into the appropriate subparts
      navigationFirstLevelMenu < menu.firstlevel
      #navigationSecondLevelMenu < menu.secondlevel
      langMenu < menu.language
      #breadcrumb < menu.breadcrumb
      footerRight < menu.footer

      # Insert various TypoScript lib objects into the appropriate subparts of the template
      logo < lib.Logo
      siteTitle < lib.sitetitle
      searchBox < lib.searchbox

      # Insert content as already constructed in TypoScript objects into subparts
      #mainContent < lib.content
      #secondaryContent < lib.contentright
      #navigationContent < lib.contentleft
    }
  }
}

[globalVar = GP:tx_newsicon_pi1|news > 0]
config.defaultGetVars {
        tx_newsicon_pi1 {
                controller=Books
                action=detail
        }
}
[global]

[globalVar = GP:tx_newsicon_pi2|news > 0]
config.defaultGetVars {
        tx_newsicon_pi2 {
                controller=News
                action=detail
        }
}
[global]

[globalVar = GP:tx_ttaddressicon_pi2|address > 0]
config.defaultGetVars {
        tx_ttaddressicon_pi2 {
                controller=Address
                action=detail
        }
}

page.bodyTagCObject = TEXT
page.bodyTagCObject.value = <body class="pageWide">

[globalVar = GP:tx_ttaddressicon_pi3|address > 0]
config.defaultGetVars {
        tx_ttaddressicon_pi3 {
                controller=Address
                action=detail
        }
}

page.bodyTagCObject = TEXT
page.bodyTagCObject.value = <body class="pageWide">

[global]


[globalVar = TSFE:id = 1]

tt_content.stdWrap.innerWrap.cObject.default >

lib.fp = COA
#    BÜHNE
lib.fp.10 < lib.stageImages
#    LINKS
lib.fp.20 < lib.quicklinks
#    TEASER
lib.fp.30 = COA
#    TEXT
lib.fp.40 < lib.contentright

page.10.subparts.content < lib.fp

lib.tagList = COA
lib.tagList.10 = CONTENT
lib.tagList.10.table = tt_content
#lib.tagList.10.select.max = 1
lib.tagList.10.select.orderBy = sorting
lib.tagList.10.select.pidInList = 74
lib.tagList.10.select.where = colPos=0

page.10.subparts.tagList < lib.tagList

[else]

lib.tmpContent = CASE
lib.tmpContent {
  key.field = layout
  
  default = COA
  default.10 = COA
  default.10.10 < lib.pageImage
  default.10.wrap = <div class="mainLeft">|</div>
  default.20 = COA
  default.20.10 < lib.content
  default.20.wrap = <div class="mainMiddle">|</div>
  default.900 = TEXT
  default.900.value = <div class="clear"></div>
  
  0 < .default
  
  1 < .default
  1.20.wrap = <div class="mainMiddleInfobar">|</div>
  1.30 = COA
  1.30.10 < lib.contentright
  1.30.wrap = <div class="mainRight">|</div>
  
  2 < .default
  
}
page.10.subparts.content < lib.tmpContent

#page.10.subparts.contentLeft < lib.pageImage
#page.10.subparts.contentMain < lib.content
#page.10.subparts.contentRight < lib.contentright


[global]