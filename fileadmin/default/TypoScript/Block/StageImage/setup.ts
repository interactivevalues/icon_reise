/*
  smo, r.schmoller@next-linz.com, 2014-04-13

  Bühnen-Bild /
  CType:  next_stageimage

  Typo3 defined content-element on basis of tt_content
  -> configuration provided via TCA!!!
*/

tt_content.next_stageimage = COA
tt_content.next_stageimage {
  10 = FILES
  10 {
    references {
      table = tt_content
      uid.data = uid
      fieldName = image
    }
    renderObj = IMAGE
    renderObj {
      file.import.data = file:current:originalUid // file:current:uid
      file.maxW = {$stage.images.maxW}
      file.maxH = {$stage.images.maxH}
      file.width = {$stage.images.width}
      file.height = {$stage.images.height}
      altText.data = file:current:alternative // field:header
      stdWrap.typolink.if.isTrue.data = file:current:link
      stdWrap.typolink.parameter.data = file:current:link
    }
  }
}

[globalVar = TSFE:id=1]

lib.content {
  stdWrap.wrap = |
}


lib.stageImages = COA
lib.stageImages.10 = COA
lib.stageImages.10.40 < lib.content
lib.stageImages.10.40.select.where = ( CType = "next_stageimage" )
lib.stageImages.10.40.select.max = {$stage.images.max}
lib.stageImages.10.40.wrap = <div id="stageImages">|</div>
lib.stageImages.10.wrap = <div id="stageWrapper"><h1 class="hide">{$stage.images.hiddenText}</h1>|</div>

[end]
