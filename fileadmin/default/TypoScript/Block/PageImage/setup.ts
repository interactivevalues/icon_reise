/*
The PAGEIMAGE-RECURSIVE template. 

shows the PageImage of the current page on the current page
if there isn't any PageImage an the current Page, try to go up an take the PageImage of parent.
do this till hit root-level.
*/

lib.pageImage = IMAGE
lib.pageImage {
  file.import.data = levelmedia: -1,slide
  file.import.listNum = 0
  file.treatIdAsReference = 1
  file.maxW = {$pageImage.maxW}
  file.maxH = {$pageImage.maxH}
  altText.data = page:title
}

