/*
TypoScript template which compiles the contents for the right column on the pages.
*/

lib.contentright = COA
lib.contentright {
  20 = CONTENT
  20.table = tt_content
  20.select.where = colPos = 2
  20.select.orderBy = sorting
}
