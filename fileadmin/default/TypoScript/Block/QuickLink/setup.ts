/*
  smo, r.schmoller@next-linz.com, 2014-04-13

  Quicklink
  CType:  next_quicklink

  Typo3 defined content-element on basis of tt_content
  -> configuration provided via TCA!!!
*/
tt_content.next_quicklink = COA
tt_content.next_quicklink {
  10 = COA
  10.10 = TEXT
  10.10.field = header
  10.10.stdWrap.typolink.parameter.field = header_link
  10.wrap = |###SPLITTER###
  10.wrap.require = header
}

[globalVar = TSFE:id=1]

lib.quicklinks = COA
lib.quicklinks.10 = COA
lib.quicklinks.10.10 < lib.contentleft
lib.quicklinks.10.10.select.where = ( colPos = 1 AND CType = "next_quicklink" )
lib.quicklinks.10.10.select.max = {$stage.quicklinks.max}

lib.quicklinks.10.10.stdWrap.split {
  token = ###SPLITTER###
  cObjNum = 1 |*| 2 |*| 3 || 4
  1.wrap = <li class="first">|</li>
  1.current = 1
  2.wrap = <li>|</li>
  2.current = 1
  3.wrap = <li class="last">|</li>
  3.current = 1
  4.current = 1
}
lib.quicklinks.10.wrap = <ul>|</ul>
lib.quicklinks.wrap = <div class="quicklinks">|</div>

[end]