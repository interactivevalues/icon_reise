/*
The LOGO template. 

sets the correct href on the logo to send the user to front-page
*/

#[globalVar = GP:tx_iconevents_pi1|event > 0]

[globalVar = GP:tx_iconevents_pi1|event > 0, GP:tx_iconevents_pi1|registration > 0]
lib.Logo = COA
lib.Logo.10 = USER
lib.Logo.10 {
#  userFunc = tx_extbase_core_bootstrap->run
  userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
  pluginName = Pi1_logo
  extensionName = IconEvents
  controller = Event
  vendorName = NEXT
  settings =< plugin.tx_iconevents.settings
  settings.onlyLogo = 1
  persistence =< plugin.tx_iconevents.persistence
  view =< plugin.tx_iconevents.view
  action = logo
  switchableControllerActions.Event.1 = logo
}

[else]

lib.Logo = TEXT
lib.Logo.value = <img src="{$contentpage.logo.file}" alt="{$contentpage.logo.altText}" title="{$contentpage.logo.titleText}" width="{$contentpage.logo.width}" height="{$contentpage.logo.height}" border="0" />
lib.Logo.insertData = 1

[global]


[globalVar = GP:L = 1]
lib.Logo.wrap = <a href="/en/">|</a>
[else]
lib.Logo.wrap = <a href="/de/">|</a>
[global]