# Add the following uncommented line, to the root sys_template Constants to include this file
# <INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/TypoScript/constants.ts">


<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/TypoScript/System/CssStyledContent/constants.ts">
#<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/TypoScript/Extension/JqueryColorbox/constants.ts">
#<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/TypoScript/Extension/WtSpamshield/constants.ts">

filepaths {
  # cat=filepaths; type=string; label=HTML Templates: Location of the (X)HTML templates relative to site
  templates = fileadmin/default/v2/

  # cat=filepaths; type=string; label=CSS: Location of the Cascading Style Sheets relative to site
  css = fileadmin/default/v2/css/

  # cat=filepaths; type=string; label=Images: Location of the images relative to site
  images = fileadmin/default/v2/pix/

  # cat=filepaths; type=string; label=Scripts: Location of the Javascript files relative to site
  scripts = fileadmin/default/v2/js/

  # cat=filepaths; type=string; label=HTML Templates for extensions: Location of the (X)HTML templates for extensions
  extensiontemplates = fileadmin/default/v2/extensions/

  files.css.fancybox = {$filepaths.scripts_global}fancybox/jquery.fancybox.css

  files.js.jq_main = {$filepaths.scripts}jquery-1.11.2.min.js
  files.js.jq_fancybox = {$filepaths.scripts}fancybox/jquery.fancybox.pack.js
  files.js.jq_placeholder = {$filepaths.scripts}jquery.placeholder.js
  files.js.jq_slider = {$filepaths.scripts}jquery.slides.min.js
  files.js.scripts = {$filepaths.scripts}script.js
}
  
menu {
  # cat=navigation menus; type=string; label= Top-menu pages: Comma separated list of page id's to be included in top-right menu.
  top.pages = 28
  
  #
  footer.PID = 76
}

plugin.tx_automaketemplate_pi1 {
  # cat=plugin.automaketemplate; type:string; label= HTML template file: Name of the HTML page template file (without path, see contant for filepath.templates)
  templatefile = index.html
}

plugin.tx_indexedsearch {
  # cat=plugin.indexed_search; type=int+; label= Search Page ID: UID of the page which contains the indexed search plugin.
  searchpageID = 17
}

plugin.tx_ttaddressicon {
  settings {
    general.phone = +43 / 732 / 69412
    general.fax = +43 / 732 / 6980
    
    detail.image.width = 160c
    detail.image.height = 214c

    events.events.detailUid = 189
    events.events.storagePid = 397

    publications.books.detailUid = 200
    publications.books.storagePid = 81

    publications.news.detailUid = 199
    publications.news.storagePid = 18

    publications.publications.detailUid = 520
    publications.publications.storagePid = 519
  }
}

plugin.tx_newsicon {
  settings {
    person {
      general.phone = {$plugin.tx_ttaddressicon.settings.general.phone}
      general.fax = {$plugin.tx_ttaddressicon.settings.general.fax}
    }
  }
}

plugin.tx_powermail {
  settings {
    javascript {
      powermailJQuery = 
    }
  }
}

plugin.meta {
  # cat=plugin.meta; type=string; label= Description: Write a short abstract for your website.
  description = ICON Wirtschaftstreuhand GmbH: In der ONLINEBERATUNG erfahren Sie Interessantes &uuml;ber Auslandsentsendungen, unsere 24-Stunden-Garantie, Formulare, Checklisten, News aus dem Steuerrecht, u.v.m.

  # cat=plugin.meta; type=string; label= Keywords: Enter a comma separated list of keywords.
  keywords = ICON, ICON Wirtschaftstreuhand GmbH, Auslandsentsendung, Ausland, Auslandseinsatz, Auslandstätigkeit, DBA, Seminare, Workshops, ICON-Telefonverzeichnis, Stellenangebote, New Economy, ICON-News, I-Business, Aktuelles, Dr. Stefan Bendlinger, Mag. Karl Mitterlehner, Mag. Max Panholzer, Rudolf Ga&szlig;ner, Mag. Rudolf Theinschnack, Dr. Eugen Strimitzer, Mitarbeiterentsendung, Icon wirtschaftsprüfung, linz, umsatzsteuer, entsendung

  # cat=plugin.meta; type=string; label= Robots: Use for instance these codes: Index all pages: "all".  Index no pages: "none". Only this page: "index,nofollow".  Only subpages: "noindex,follow"
  robots = index,follow

  # cat=plugin.meta; type=string; label= Reply-to email
  email =

  # cat=plugin.meta; type=string; label= Author: Enter name of author.
  author = ICON Wirtschaftstreuhand GmbH

  # The meta tags below are not used on this website, if you want to use these, you can configure that in TypoScript template page.meta

  # cat=plugin.meta; type=boolean; label=Always include global.
  includeGlobal = 0

  # cat=plugin.meta; type=options[,Arabic=ar,Chinese=zh,Danish=dk,Dutch=nl,English=en,Finnish=fi,French=fr,German=de,Greek=el,Hebrew=he,Icelandic=is,Italian=it,Japanese=ja,Norwegian=no,Polish=pl,Portuguese=pt,Russian=ru,Spanish=es,Swedish=sv,Turkish=tr,Multi language=mul]; label= Language: Select language of the content.
  language = de

  # cat=plugin.meta; type=string; label= Distribution
  distribution = 

  # cat=plugin.meta; type=options[,General,Mature,14 years,Restricted]; label= Rating
  rating = 
}

styles.content {
  # This defines the maximum width of images inserted in content records of type Images or Text-with-images.
  # There are seperate settings for images floated next to text (..InText)
  imgtext {
    maxW = 651
    maxWInText = 651
    borderThick = 1
    linkWrap.newWindow = 1
  }
  uploads {
    jumpurl_secure = 1
    jumpurl_secure_mimeTypes = pdf=application/pdf, doc=application/msword
    jumpurl = 1
  }
}

contentpage {
  # cat=contentpage; type=int+; label= Footer source PID: Parent ID of the footer record used on content pages.
  footerPID = 20

  # cat=contentpage; type=int+; label= ID of the home page: ID of the home (root) page of the site.
  homeID = 1

  # cat=contentpage; type=int+; label= loginboxPID: ID of the folder containing the login box record (to be shown on multiple pages)
  loginboxPID = 21

  # cat=contentpage; type=int+; label= loginboxUID: UID of the login box record (to be shown on multiple pages)
  loginboxUID = 31

  # cat=contentpage; type=int+; label= loginpageID: UID of the customer login page.
  loginpageID = 28

  # cat=contentpage; type=string; label= language0: name of the default language of this site
  language0 = Deutsch

  # cat=contentpage; type=string; label= language1: name of the second language of this site
  language1 = English
  
  logo.file = fileadmin/default/v2/pix/logo.gif
  logo.width = 156
  logo.height = 156
  logo.altText = ICON Wirtschaftstreuhand GmbH
  logo.titleText = ICON Wirtschaftstreuhand GmbH
}

pageImage {
  maxW = 156
  maxH = 600
}

stage {
  images {
    hiddenText = ICON
    max = 5
    maxW = 940
    maxH = 285
    width = 940
    height = 285
    jsFile = {$filepaths.scripts}stageimage.js
    jsParams =
  }
  quicklinks {
    max = 5
  }
}


# Set the language of meta tag with DC.language to Danish, when in Danish
[globalVar = GP:L = 1]
plugin.meta.language = en
[end]

<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/settings/introduction.ts">
