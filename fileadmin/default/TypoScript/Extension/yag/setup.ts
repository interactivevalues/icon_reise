# YAG CONFIG
plugin.tx_yag.settings.themes.default_original < plugin.tx_yag.settings.themes.default
plugin.tx_yag.settings.themes.default < plugin.tx_yag.settings.themes.lightbox

plugin.tx_yag.settings.themes.default {
  showBreadcrumbs = 0
  albumList {
    showPager = 1
  }
  itemList {
    itemsPerPage = 300
    #itemsPerPage = 10
    columnCount = 100
    #columnCount = 10
    showPager = 0
    #showPager = 1
  }
  item {
    showPermaLink = 0
    lightbox {
      showPermaLink = 0
    }
  }
  resolutionConfigs {
    thumb.width = 180c
    thumb.height = 135c
    medium.maxW = 1024
    medium.maxH = 768
  }
  includeCSS.yag_theme_default = fileadmin/default/v1/css/yag.css
  controller.ItemList.list.template = fileadmin/default/v1/extensions/yag/Templates/Themes/LightBox/ItemList/List.html
}

plugin.tx_yag.view.layoutRootPath = fileadmin/default/v1/extensions/yag/Layouts/
plugin.tx_yag.view.partialRootPath = fileadmin/default/v1/extensions/yag/Partials/
plugin.tx_yag.view.templateRootPath = fileadmin/default/v1/extensions/yag/Templates/
