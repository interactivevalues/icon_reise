/*
The AUTOMAKETEMPLATE template

The automaketemplate parser reads the HTML template files, extracts the markers
for content, and replaces them with the content assigned to these markers.
*/

# Configuring the Auto-Parser for main template:

plugin.tx_automaketemplate_pi1 {
  # Read the template file
  content = FILE
  content.file = {$filepaths.templates}{$plugin.tx_automaketemplate_pi1.templatefile}

  elements {
    BODY.all = 1
    BODY.all.subpartMarker = DOCUMENT_BODY
    HEAD.all = 1
    HEAD.all.subpartMarker = DOCUMENT_HEADER

    DIV.id.site = 1
    # Configure which HTML-tags should be made replacable by subparts
    DIV.id.navigationFirstLevelMenu = 1
    DIV.id.navigationSecondLevelMenu = 1
    DIV.id.topMenu = 1
    DIV.id.logo = 1
    DIV.id.langMenu = 1
    DIV.id.searchBox = 1

    DIV.id.siteTitle = 1
    DIV.id.footer = 1
    DIV.id.footerRight = 1
    DIV.id.tagList = 1
    DIV.id.pageImage = 1

    DIV.id.content = 1
    DIV.id.contentLeft = 1
    DIV.id.contentMain = 1
    DIV.id.contentRight = 1
    DIV.id.navigationontent = 1

    H1.all = 1

    # Remove some tags from HTML head section (because TYPO3 will add these dynamically)
    HEAD.rmTagSections = title
    HEAD.rmSingleTags = meta,link
  }

  # Prefix all relative paths in the HTML template with this value
  #relPathPrefix = {$filepaths.templates}
}
