### RTE configuration GLOBAL

RTE.default {
  
  # LOKALES CSS FILE EINBINDEN / LOKALES IMPORTIERT GLOBALES!
#  contentCSS = fileadmin/css/rte.css
  
  # HINZUFÜGEN UNSERER EIGENEN KLASSEN
  
  proc {
    # tags die erlaubt / verboten sind
    allowTags (
    , table ,tbody, tr, th, td,thead, tfoot, caption
    , h1, h2, h3, h4, h5, h6
    , div
    , p
    , br
    , span
    , ul, ol, li
    , re
    , blockquote
    , strong, em, b, i, u, sub, sup, strike
    , a
    , img
    , nobr, hr, tt, q, cite, abbr, acronym, center
    )
    
    denyTags = font
    
    # br wird nicht zu p konvertiert
    dontConvBRtoParagraph = 1
    
    # tags sind erlaubt außerhalt von p, div
    allowTagsOutside = img,hr
    
    # erlaubte attribute in p, div tags
    keepPDIVattribs = align,class,style,id
    
    # Generelle Einstellungen für den HTML-Parser
    HTMLparser_rte {
      
      # tags die erlaubt/verboten sind
      allowTags < RTE.default.proc.allowTags
      denyTags < RTE.default.proc.denyTags
      
      # entfernt html-kommentare
      removeComments = 1
      
      # tags die nicht übereinstimmen werden nicht entfernt (protect / 1 / 0)
      # Tags, die nicht richtig verschachtelt sind, werden entfernt
      keepNonMatchedTags = 1
    }
    
    
    # Vom RTE in die Datenbank
    entryHTMLparser_db = 1
    entryHTMLparser_db {
      
      # tags die erlaubt/verboten sind
      allowTags < RTE.default.proc.allowTags
      denyTags < RTE.default.proc.denyTags
      
      # Für diese Tags sind keine Attribute erlaubt
      noAttrib = b, i, u, strike, sub, sup, strong, em, quote, blockquote, cite, tt, br, center
      
      # Diese Tags werden entfernt wenn kein Attribut vorhanden ist
      rmTagIfNoAttrib = span,div,font
      
      # htmlSpecialChars = 1
      
      ## align attribute werden erlaubt
      tags {
        p.fixAttrib.align.unset >
        p.allowedAttribs = class,style,align
        
        div.fixAttrib.align.unset >
        
        hr.allowedAttribs = class
        
        # Das <b>-Tag wird durch <strong> ersetzt
        b.remap = strong
        i.remap = em
        
        ## img tags werden erlaubt
        img >
      }
    }
    
    # Von der Datenbank in den RTE
    exitHTMLparser_db = 1
    exitHTMLparser_db {
    }
    
  } #end proc
  
  showTagFreeClasses = 1
  # Tags die nicht eingeführt werden dürfen
  hideTags = font
  
  # Tabellen Optionen in der RTE Toolbar
  hideTableOperationsInToolbar = 0
  keepToggleBordersInToolbar = 1
  
  # prevent P-tags in Table-Cells
  #disableEnterParagraphs = 1
  
  # Tabellen Editierungs-Optionen (cellspacing/ cellpadding / border)
  disableSpacingFieldsetInTableOperations = 1
  disableAlignmentFieldsetInTableOperations=1
  disableColorFieldsetInTableOperations=1
  disableLayoutFieldsetInTableOperations=1
  disableBordersFieldsetInTableOperations=1
}

/*
https://www.typo3.net/forum/thematik/zeige/thema/117185/
*/
RTE.default {
  buttons.image.options.magic.maxWidth = 740
  buttons.image.options.magic.maxHeight = 1000
  buttons.image.options.plain.maxWidth = 740
  buttons.image.options.plain.maxHeight = 1000
}
RTE.buttons.image < RTE.default.buttons.image

/*
RTE.default {
  
  showButtons := addToList(acronym, abbr)

  # LOKALES CSS FILE EINBINDEN / LOKALES IMPORTIERT GLOBALES!
  contentCSS = fileadmin/css/rte.css
  
  # HINZUFÜGEN UNSERER EIGENEN KLASSEN
  
  # Eigenen Klassen müssen hier eingefügt werden
  # allowedClasses = underline
  classesHeadline = underline
  classesTable = tableborder
  classesList = hinterlegt
  classesParagraph = hinterlegt

  # Liste aller Klassen die in die DB geschrieben werden dürfen
  # Eigene Klassen müssen hier angefügt werden!
  allowedClasses (
    external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail,
    author, underline, tableborder, hinterlegt
  )    
  
  # ALLGEMEINE EINSTELLUNGEN RTE
  
  # Einträge im RTE select Feld "Format"
  # zunächst eine Übersicht aller Standard Einträge:
  # address, article, aside, div, footer, header, nav, p,  h1 - h6, pre, blockquote, section,
  # jetzt all das was wir nicht wollen:
  
  buttons.formatblock.removeItems (
    address
    , article
    , aside
    , div
    , footer
    , header
    , nav
    , pre
    , section
  )
  
  ## Eigens Stylesheet (für RTE Ansicht) wird nicht vom RTE EXT Stylesheet überschrieben
  ignoreMainStyleOverride = 1
  
  proc {
    # tags die erlaubt / verboten sind
    allowTags (
    , table ,tbody, tr, th, td,thead, tfoot, caption
    , h1, h2, h3, h4, h5, h6
    , div
    , p
    , br
    , span
    , ul, ol, li
    , re
    , blockquote
    , strong, em, b, i, u, sub, sup, strike
    , a
    , img
    , nobr, hr, tt, q, cite, abbr, acronym, center
    )
    
    denyTags = font
    
    # br wird nicht zu p konvertiert
    dontConvBRtoParagraph = 1
    
    # tags sind erlaubt außerhalt von p, div
    allowTagsOutside = img,hr
    
    # erlaubte attribute in p, div tags
    keepPDIVattribs = align,class,style,id
    
    # Generelle Einstellungen für den HTML-Parser
    HTMLparser_rte {
      
      # tags die erlaubt/verboten sind
      allowTags < RTE.default.proc.allowTags
      denyTags < RTE.default.proc.denyTags
      
      # entfernt html-kommentare
      removeComments = 1
      
      # tags die nicht übereinstimmen werden nicht entfernt (protect / 1 / 0)
      # Tags, die nicht richtig verschachtelt sind, werden entfernt
      keepNonMatchedTags = 1
    }
    
    
    # Vom RTE in die Datenbank
    entryHTMLparser_db = 1
    entryHTMLparser_db {
      
      # tags die erlaubt/verboten sind
      allowTags < RTE.default.proc.allowTags
      denyTags < RTE.default.proc.denyTags
      
      # Für diese Tags sind keine Attribute erlaubt
      noAttrib = b, i, u, strike, sub, sup, strong, em, quote, blockquote, cite, tt, br, center
      
      # Diese Tags werden entfernt wenn kein Attribut vorhanden ist
      rmTagIfNoAttrib = span,div,font
      
      # htmlSpecialChars = 1
      
      ## align attribute werden erlaubt
      tags {
        p.fixAttrib.align.unset >
        p.allowedAttribs = class,style,align
        
        div.fixAttrib.align.unset >
        
        hr.allowedAttribs = class
        
        # Das <b>-Tag wird durch <strong> ersetzt
        b.remap = strong
        i.remap = em
        
        ## img tags werden erlaubt
        img >
      }
    }
    
    # Von der Datenbank in den RTE
    exitHTMLparser_db = 1
    exitHTMLparser_db {
    }
    
  } #end proc
  
  showTagFreeClasses = 1
  # Tags die nicht eingeführt werden dürfen
  hideTags = font
  
  # Tabellen Optionen in der RTE Toolbar
  hideTableOperationsInToolbar = 0
  keepToggleBordersInToolbar = 1
  
  # prevent P-tags in Table-Cells
  #disableEnterParagraphs = 1
  
  # Tabellen Editierungs-Optionen (cellspacing/ cellpadding / border)
  disableSpacingFieldsetInTableOperations = 1
  disableAlignmentFieldsetInTableOperations=1
  disableColorFieldsetInTableOperations=1
  disableLayoutFieldsetInTableOperations=1
  disableBordersFieldsetInTableOperations=1
}

RTE.default.classesParagraph := removeFromList(component-items-ordered, action-items-ordered, action-items, csc-frame-frame1, csc-frame-frame2, name-of-person, detail, important,align-left, align-right, align-justify)
RTE.default.proc.allowedClasses := removeFromList(component-items-ordered, action-items-ordered, action-items, csc-frame-frame1, csc-frame-frame2, name-of-person, detail, important,align-left, align-right, align-justify)

RTE.classes  {
  underline.name = Unterstrichen
  hinterlegt.name = Hinterlegt
  tableborder.name = Tabelle mit Rahmen
  zitatblau.name = Zitat Blau
  zitatgelb.name = Zitat Gelb
  zitatorange.name = Zitat Orange
}

#WESENTLICH FÜR TABELLEN STYLES, ADD CLASS HERE!!!
RTE.default.buttons.blockstyle.tags.table.allowedClasses := addToList(tableborder)
RTE.default.proc.allowedClasses := addToList(tableborder)

*/

