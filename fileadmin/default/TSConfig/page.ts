# Add the following uncommented line, to the website's root page Resources > TypoScript Configuration to include this file
# <INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/TSConfig/page.ts">

#  IMPORT GENERAL RTE-CONFIGURATION
#<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/TSConfig/rte.ts">

# Show only the chosen columns in the page module
# 0=normal, 1=left, 2=right, 3=border
mod.SHARED.colPos_list = 1,0,2

# Show the content element wizard with tabs (for consistency)
mod.wizards.newContentElement.renderMode = tabs

TCEFORM {
  pages {
    layout {
      # Rename the default options for the layout field in table pages
      altLabels.0 = Standard (Content Breit)
      altLabels.1 = Infoleiste Rechts (2 Spalten)
      altLabels.2 = Galerie
      altLabels.3 = Frontpage
    }
    # There is no need for the Alias field in page properties when we use RealURL
    alias.disabled = 1
  }

  tt_content {
    # Remove the 'border' option from selectbox 'column' in content records
    colPos.keepItems = 1,0,2
  }
}

// If an editor creates a page it should be visible to all editors and admins
TCEMAIN {
    // group "all users"
  permissions.groupid = 5

  user = show,edit,delete,new,editcontent
  group = show,edit,delete,new,editcontent
  everybody =
}

# Show only modules "Columns" and "Language", hide "Quickedit" and "Page Information"
mod.web_layout.menu.function {
  1 = 1
  2 = 1
  0 = 0
  3 = 0
}


# Use different views for the news plugin, those entries show up in the news content element configuration
tx_news.templateLayouts {
  normal = Normal
  latest = Latest
  # You can even translate those if you create a custom ll-xml file.
  #custom = fileadmin/Language/news-templates.xml:keyForCustom
}

# Condition for news storage folder
[PIDinRootline = 18]
  mod.web_list {
    # Limit the creation of new records in this sysFolder to these types
    allowedNewTables = tx_news_domain_model_news,tx_news_domain_model_category,sys_note
  }

  # This will open the news singleView page (id 23) when clicking 'preview' for a news record
  tx_news.singlePid = 23


  TCEMAIN {
    # Clear cache of the News page after content of the News folder has changed
    clearCacheCmd = 23,30
    clearCache_pageSiblingChildren = 1
  }
[END]

# Condition for frontend user storage folder
[PIDinRootline = 29]
  mod.web_list {
    # limit the creation of new records in this sysFolder to these types
    allowedNewTables = fe_groups,fe_users,sys_note
  }
[END]
