<?php

if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1',
  array(
    'Skriptum' => 'index, submit, done, fail',
  ),
  // non-cacheable actions
  array(
    'Skriptum' => 'index, submit, done, fail',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1_teaser',
  array(
    'Skriptum' => 'teaser',
  ),
  // non-cacheable actions
  array(
    'Skriptum' => 'teaser',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Csv',
  array(
	'Skriptum' => 'csvFile',
  ),
  // non-cacheable actions
  array(
	'Skriptum' => 'csvFile',
  )
);

/*
	WICHTIG !!!		!!!			!!!			!!!
	
	damit das klappt muß realurl in /typo3conf/PackageStage.php vor dieser extension geladen werden !!!
	-> muss vl. per hand angepasst werden !!!

*/

  // registering contact.vcf for each hierachy of configuration to realurl (meaning to every website in a multisite installation)
$realurl = $TYPO3_CONF_VARS['EXTCONF']['realurl'];
if (is_array($realurl))  {
	foreach ($realurl as $host => $cnf) {
		// we won't do anything with string pointer (e.g. example.org => www.example.org)
		if (!is_array($realurl[$host])) {
			continue;
		}
		if (!isset($realurl[$host]['fileName'])) {
			$realurl[$host]['fileName'] = array();
		}
		$realurl[$host]['fileName']['index']['skriptumanforderungen.csv']['keyValues']['type'] = 789;
	}
	
	$TYPO3_CONF_VARS['EXTCONF']['realurl'] = $realurl;
}

?>
