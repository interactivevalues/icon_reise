// JavaScript Document
/*
	USES:
	http://pbauerochse.github.io/searchable-option-list/examples.html
*/

/*
	PREPARE VARIABLES
*/
var SOL_texts = {};
var SOL_texts_DE = {
	country: {
		noItemsAvailable: 'Land nicht gefunden',
		searchplaceholder: 'Wählen Sie ein Land',
	},
	finanzamt: {
		noItemsAvailable: 'Finanzamt nicht gefunden',
		searchplaceholder: 'Wählen Sie ein Finanzamt',
	},
	finanzamt_btn_set: 'Österreichisches Finanzamt wählen',
	finanzamt_btn_clr: 'Feld leeren',
}
var SOL_texts_EN = {
	country: {
		noItemsAvailable: 'Country not found',
		searchplaceholder: 'Choose a Country',
	},
	finanzamt: {
		noItemsAvailable: 'Tax Office not found',
		searchplaceholder: 'Choose Austrian Tax Office',
	},
	finanzamt_btn_set: 'Choose Austrian Tax Office',
	finanzamt_btn_clr: 'clear field',
}

/*
	FUNCTIONS
*/
function enableFormPrivate(){
	//
	prepareSelect('function');
	//
	$('#country').searchableOptionList({
		maxHeight:'206px',
		texts: SOL_texts.country,
	});
	//
	var mySOL;
	mySOL_act = 1;
	//
	$('#sel-taxOffices').searchableOptionList({
		maxHeight:'206px',
		texts: SOL_texts.finanzamt,
		events: {            
            onChange: function(sol,changedElements) {
//				alert('changedElements: ' + changedElements.attr('value') + ' text: ' + $('#sel-taxOffice option[value="'+changedElements.attr('value')+'"]').text() );
				//
//	alert(mySOL_act + ' act-text: ' + $('#taxOffice'+mySOL_act).attr('value') + ' new-text: ' + $('#sel-taxOffices option[value="'+changedElements.attr('value')+'"]').text());
				//
				$('#taxOffice'+mySOL_act).val( $('#sel-taxOffices option[value="'+changedElements.attr('value')+'"]').text() );
				$('#taxOfficeNr'+mySOL_act).val( changedElements.attr('value') );
				//
				$('#taxOffice-set-'+mySOL_act).addClass('hide');
				$('#taxOffice-clr-'+mySOL_act).removeClass('hide');
				//
				$('#taxOffices div.js-selector').addClass('hide');
            },
			onInitialized: function(sol, items) {
				mySOL = sol;
				$('#taxOffices .sol-current-selection').addClass('hide');
			},
        }
	});

	var i = 1;
	var i_elm = $('#taxOfficeOptions'+i);
	var i_id_set = 'taxOffice-set-'+i;
	var i_id_clr = 'taxOffice-clr-'+i;
	//
	var cls_set = 'button';
	var cls_clr = 'button';
	//
	if( $('#taxOfficeNr'+i).val()!='' ){
		cls_set += ' hide';
	} else {
		cls_clr += ' hide';
	}
	//
	i_elm.html('<button type="button" id="'+i_id_set+'" class="'+cls_set+'" data-id="'+i+'">'+SOL_texts.finanzamt_btn_set+'</button> <button type="button" id="'+i_id_clr+'" class="'+cls_clr+'" data-id="'+i+'">'+SOL_texts.finanzamt_btn_clr+'</button>');
	//
	$('#taxOffice'+i).attr('data-id', i);
	$('#taxOffice'+i).focusin(function(){
		$(this).change(function(){
			var Id = $(this).attr('data-id');
			$('#taxOfficeNr'+Id).val('');
			//
			$('#taxOffice-set-'+Id).removeClass('hide');
			$('#taxOffice-clr-'+Id).addClass('hide');
			$('#taxOffices div.js-selector').addClass('hide');
		});
	});
	$('#taxOffice'+i).focusout(function(){
		$(this).change(function(){});
	});
	//
	$('#'+i_id_set).click(function(){
		//
		$('#taxOffices div.js-selector').removeClass('hide');
		//
		$(this).addClass('hide');
		//
		mySOL.open();
		//
		return false;
	});
	$('#'+i_id_clr).click(function(){
		//	CLEAR FA
		var t_id = $(this).attr('data-id');
		$('#taxOffice'+t_id).val('');
		$('#taxOfficeNr'+t_id).val('');
		//
		$('#taxOffice-set-'+t_id).removeClass('hide');
		$('#taxOffice-clr-'+t_id).addClass('hide');
		//
		$('#taxOffices div.js-selector').addClass('hide');
		//
		return false;
	});

}

function enableFormCompany(){
	//
	$('#comCountry').searchableOptionList({
		maxHeight:'206px',
		texts: SOL_texts.country,
	});
	$('#billCountry').searchableOptionList({
		maxHeight:'206px',
		texts: SOL_texts.country,
	});

	//
	var mySOL;
	mySOL_act = -1;
	//
	$('#sel-taxOffices').searchableOptionList({
		maxHeight:'206px',
		texts: SOL_texts.finanzamt,
		events: {            
            onChange: function(sol,changedElements) {
//				alert('changedElements: ' + changedElements.attr('value') + ' text: ' + $('#sel-taxOffice option[value="'+changedElements.attr('value')+'"]').text() );
				//
//	alert(mySOL_act + ' act-text: ' + $('#taxOffice'+mySOL_act).attr('value') + ' new-text: ' + $('#sel-taxOffices option[value="'+changedElements.attr('value')+'"]').text());
				//
				$('#taxOffices button[data-sol="1"]').removeClass('hide');
				$('#taxOffices button[data-sol="1"]').removeAttr('data-sol');
				//
				$('#taxOffice'+mySOL_act).val( $('#sel-taxOffices option[value="'+changedElements.attr('value')+'"]').text() );
				$('#taxOfficeNr'+mySOL_act).val( changedElements.attr('value') );
				//
				$('#taxOffice-set-'+mySOL_act).addClass('hide');
				$('#taxOffice-clr-'+mySOL_act).removeClass('hide');
				//
				$('#taxOffices div.js-selector').insertAfter('#taxOffice_5');
				$('#taxOffices div.js-selector').addClass('hide');
            },
			onInitialized: function(sol, items) {
				mySOL = sol;
				$('#taxOffices .sol-current-selection').addClass('hide');
			},
        }
	});
	//
	for( var i=0; i<=5; i++ ){
		var i_elm = $('#taxOfficeOptions'+i);
		var i_id_set = 'taxOffice-set-'+i;
		var i_id_clr = 'taxOffice-clr-'+i;
		//
		var cls_set = 'button';
		var cls_clr = 'button';
		//
		if( $('#taxOfficeNr'+i).val()!='' ){
			cls_set += ' hide';
		} else {
			cls_clr += ' hide';
		}
		//
		i_elm.html('<button type="button" id="'+i_id_set+'" class="'+cls_set+'" data-id="'+i+'">'+SOL_texts.finanzamt_btn_set+'</button> <button type="button" id="'+i_id_clr+'" class="'+cls_clr+'" data-id="'+i+'">'+SOL_texts.finanzamt_btn_clr+'</button>');
		//
		$('#taxOffice'+i).attr('data-id', i);
		$('#taxOffice'+i).focusin(function(){
			$(this).change(function(){
				var Id = $(this).attr('data-id');
				$('#taxOfficeNr'+Id).val('');
				//
				$('#taxOffice-set-'+Id).removeClass('hide');
				$('#taxOffice-clr-'+Id).addClass('hide');
				$('#taxOffices div.js-selector').insertAfter('#taxOffice_5');
				$('#taxOffices div.js-selector').addClass('hide');
			});
		});
		$('#taxOffice'+i).focusout(function(){
			$(this).change(function(){});
		});
		//
		$('#'+i_id_set).click(function(){
			//	SET FA
			mySOL_act = $(this).attr('data-id');
			//
			$('#taxOffices button[data-sol="1"]').removeClass('hide');
			$('#taxOffices button[data-sol="1"]').removeAttr('data-sol');
			//
			$('#taxOffices div.js-selector').removeClass('hide');
			$('#taxOffices div.js-selector').insertAfter('#taxOffice_'+mySOL_act +' fieldset.col3');
			//
			$(this).attr('data-sol', '1');
			$(this).addClass('hide');
			//
			mySOL.open();
			//
			return false;
		});
		$('#'+i_id_clr).click(function(){
			//	CLEAR FA
			//
			$('#taxOffices button[data-sol="1"]').removeClass('hide');
			$('#taxOffices button[data-sol="1"]').removeAttr('data-sol');
			//
			var t_id = $(this).attr('data-id');
			$('#taxOffice'+t_id).val('');
			$('#taxOfficeNr'+t_id).val('');
			//
			$('#taxOffice-set-'+t_id).removeClass('hide');
			$('#taxOffice-clr-'+t_id).addClass('hide');
			//
			$('#taxOffices div.js-selector').insertAfter('#taxOffice_5');
			$('#taxOffices div.js-selector').addClass('hide');
			//
			return false;
		});
	}
	
	//
	prepareSelect('billFunction');
	prepareSelect('billEmailFunction');
	prepareSelect('function0');
	prepareSelect('function1');
	prepareSelect('function2');
	prepareSelect('function3');
	prepareSelect('function4');
	prepareSelect('function5');
	//
	$('#switchBillAddr').click(function(){
		if (this.checked) {
			$('#billAddr').removeClass('hide');
		} else {
			$('#billAddr').addClass('hide');
		}
	});
	//
	$('#switchBillEmailMode').click(function(){
		if (this.checked) {
			$('#billEmail').removeClass('hide');
		} else {
			$('#billEmail').addClass('hide');
		}
	});
	//
	var taxMin=1;
	var taxMax=5;
	var taxAct=1;
	//
	for( var i=1; i<=taxMax; i++ ){
		if( $('#taxOfficeUsed'+i).attr('value') == 1 ){
			taxAct = i;
		}
	}
	//
	if( taxAct == taxMax ){
		$('#taxAdd').addClass('hide');
		$('#taxDel').removeClass('hide');
	} else if( taxAct > taxMin && taxAct < taxMax ){
		$('#taxAdd').removeClass('hide');
		$('#taxDel').removeClass('hide');
	}
	//
	$('#taxAdd').click(function(){
		taxAct ++;
		//
		$('#taxOffice_'+taxAct).removeClass('hide');
		$('#taxOfficeUsed'+taxAct).attr('value', 1);
		//
		$('#taxDel').removeClass('hide');
		//
		if( taxAct==taxMax ) { $('#taxAdd').addClass('hide'); } 
		else { $('#taxAdd').removeClass('hide'); }
		//
		return false;
	});
	//
	$('#taxDel').click(function(){
		//
		$('#taxOffice_'+taxAct).addClass('hide');
		$('#taxOfficeUsed'+taxAct).attr('value', '');
		//
		$('#taxOffice'+taxAct).attr('value', '');
		$('#taxOffice'+taxAct).removeClass('f3-form-error');
		$('#taxId'+taxAct).attr('value', '');
		$('#taxId'+taxAct).removeClass('f3-form-error');
		$('#taxUid'+taxAct).attr('value', '');
		$('#taxUid'+taxAct).removeClass('f3-form-error');
		//
		taxAct --;
		//
		$('#taxAdd').removeClass('hide');
		//
		if( taxAct>taxMin ) { $('#taxDel').removeClass('hide'); }
		else { $('#taxDel').addClass('hide'); }
		//
		return false;
	});

	//
	var personMin=0;
	var personMax=5;
	var personAct=0;
	//
	for( var i=0; i<=personMax; i++ ){
		if( $('#used'+i).attr('value') == 1 ){
			personAct ++;
		}
	}
	//
	if( personAct == personMax ){
		$('#personAdd').addClass('hide');
		$('#personDel').removeClass('hide');
	} else if( personAct > personMin && personAct < personMax ){
		$('#personAdd').removeClass('hide');
		$('#personDel').removeClass('hide');
	}
	//
	$('#personAdd').click(function(){
		personAct ++;
		//
		$('#blockPerson_'+personAct).removeClass('hide');
		$('#used'+personAct).attr('value', 1);
		//
		$('#personDel').removeClass('hide');
		//
		if( personAct==personMax) { $('#personAdd').addClass('hide'); } 
		else { $('#personAdd').removeClass('hide'); }
		//
		return false;
	});
	//
	$('#personDel').click(function(){
		//
		$('#blockPerson_'+personAct).addClass('hide');
		$('#used'+personAct).attr('value', 0);
		//
		$('#gender'+personAct).attr('value', '');
		$('#gender'+personAct).removeClass('f3-form-error');
		$('#firstname'+personAct).attr('value', '');
		$('#firstname'+personAct).removeClass('f3-form-error');
		$('#lastname'+personAct).attr('value', '');
		$('#lastname'+personAct).removeClass('f3-form-error');
		$('#function'+personAct).attr('value', '');
		$('#function'+personAct).removeClass('f3-form-error');
		$('#email'+personAct).attr('value', '');
		$('#email'+personAct).removeClass('f3-form-error');
		//
		personAct --;
		//
		$('#personAdd').removeClass('hide');
		//
		if( personAct>personMin) { $('#personDel').removeClass('hide'); }
		else { $('#personDel').addClass('hide'); }
		//
		return false;
	});
}

function prepareSelect( pSelName ) {
	var t_val_act = $('#'+pSelName).attr('value');
	var t_values = sel_values_position;
	//
	t_values = t_values.replace(' value="'+t_val_act+'"', ' value="'+t_val_act+'" selected="selected"'); 
	//
	$('#sel-'+pSelName).html(t_values);
	if( $('#'+pSelName).hasClass('f3-form-error') ){
		$('#sel-'+pSelName).addClass('f3-form-error');
	}
	$('#sel-'+pSelName).change(function() {
		$('#'+pSelName).attr('value', $(this).val() );
	});
}

/*
	INIT
*/
$(document).ready(function() {
	$('#langMenu').addClass('hide');
	//
	SOL_texts = $('#form-lang').val()=='1' ? SOL_texts_EN : SOL_texts_DE;
	//
	var formP = $('#formPrivate');
	var formC = $('#formCompany');
	if( formP.length ){
		enableFormPrivate();
	} else if ( formC.length ) {
		enableFormCompany();
	}
});
