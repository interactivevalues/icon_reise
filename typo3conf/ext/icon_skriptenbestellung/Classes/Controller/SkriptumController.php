<?php
namespace NEXT\IconSkriptenbestellung\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\DebugUtility;

/**
 *
 *
 * @package icon_skriptenbestellung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SkriptumController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * webserviceRepository
	 *
	 * @var \NEXT\IconIgelconnector\Domain\Repository\WebserviceRepository
	 * @inject
	 */
	protected $webserviceRepository;
	
	/**
	 * skriptumRepository
	 *
	 * @var \NEXT\IconSkriptenbestellung\Domain\Repository\SkriptumRepository
	 * @inject
	 */
	protected $skriptumRepository;
	
	/**
	 * anforderungRepository
	 *
	 * @var \NEXT\IconSkriptenbestellung\Domain\Repository\AnforderungRepository
	 * @inject
	 */
	protected $anforderungRepository;

	/**
	 * persistence manager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
	 * @inject
	 */
	protected $persistenceManager;
	

	/**
	 * action index
	 *
	 * @return void
	 */
	public function indexAction() {
		//	-> show Result-Screen
		$LANG =  $GLOBALS['TSFE']->sys_language_uid;
		$LANG_str = $LANG == 1 ? 'EN' : 'DE';
		//
		$uid = $this->settings['skriptumUid'];
		//
		$skript = $this->skriptumRepository->findByUid($uid);
		
		if( $skript!=NULL){

			if( $anforderung==NULL ){
				$anforderung = $this->objectManager->getEmptyObject('\NEXT\IconSkriptenbestellung\Domain\Model\Anforderung');
			}
			//
			$lst_titel = unserialize($this->webserviceRepository->getFileData('titel-plain', $LANG_str));
			$lst_titel_nach = unserialize($this->webserviceRepository->getFileData('titel_nach-plain', $LANG_str));

			$this->view->assignMultiple(array(
				'LANG' => $LANG,
				'uid' => $uid,
				'skript' => $skript,
				'formdata' => $anforderung,
				'lst_titel' => $lst_titel,
				'lst_titel_nach' => $lst_titel_nach,
			));
		} else {

			//	nicht gefunden, dann LEER zurück geben !!!
			return '';
		}
	}


	/**
	 * action teaser
	 *
	 * @return void
	 */
	public function teaserAction() {
		//	-> show Result-Screen
		$LANG =  $GLOBALS['TSFE']->sys_language_uid;
		$LANG_str = $LANG == 1 ? 'EN' : 'DE';
		//
		$uid = $this->settings['skriptumUid'];
		//
		$skript = $this->skriptumRepository->findByUid($uid);
		
		if( $skript!=NULL){
			//
			$this->view->assignMultiple(array(
				'LANG' => $LANG,
				'uid' => $uid,
				'skript' => $skript,
			));
		} else {

			//	nicht gefunden, dann LEER zurück geben !!!
			return '';
		}
	}


	/**
	 * action submit
	 *
	 * @param \NEXT\IconSkriptenbestellung\Domain\Model\Skriptum $skriptum
	 * @param \NEXT\IconSkriptenbestellung\Domain\Model\Anforderung $anforderung
	 * @return void
	 */
	public function submitAction(\NEXT\IconSkriptenbestellung\Domain\Model\Skriptum $skriptum = NULL, \NEXT\IconSkriptenbestellung\Domain\Model\Anforderung $anforderung = NULL) {

	    //
		$LANG =  $GLOBALS['TSFE']->sys_language_uid;
		//
		if( $skriptum==NULL ){
			$this->redirect('index');
		}
		//	-> check formdata
		if( $anforderung==NULL ){
			$this->redirect('index');
		}
	
		//
		$anforderung->setSkriptum( $skriptum->getUid() );
		//
		$this->anforderungRepository->add($anforderung);
		$this->persistenceManager->persistAll();
	
		/*
			PROTOKOLLIERE FORMULAR_DATEN UND ANTWORT
		*/
		$m_charset = $GLOBALS['TSFE']->metaCharset;
		//	get SENDER from settings!
		$m_sender_email = $this->settings['mail']['sender']['email'];
		$m_sender_name = $this->settings['mail']['sender']['name'];
		$m_sender = array( $m_sender_email => $m_sender_name );
		//	get RECIPIENT from settings!
		$m_recipient_email = $this->settings['mail']['recipient']['email'];
		$m_recipient_name = $this->settings['mail']['recipient']['name'];
		//	are there overwrites from MODEL ?
		$m_recipient_email = $skriptum->getMailRecipientEmail() ? $skriptum->getMailRecipientEmail() : $m_recipient_email;
		$m_recipient_name = $skriptum->getMailRecipientName() ? $skriptum->getMailRecipientName() : $m_recipient_name;
		//
		$m_recipient = array( $m_recipient_email => $m_recipient_name );
//		$m_recipient = array( 'r.schmoller@next-linz.com' => 'smo' );
		//	get RECIPIENT_SUBJECT from settings!
		$m_recipient_subject = $this->settings['mail']['recipient']['subject'];
		//	are there overwrites from MODEL ?
		$m_recipient_subject = $skriptum->getMailRecipientSubject() ? $skriptum->getMailRecipientSubject() : $m_recipient_subject;
		//	build MAIL BODY
		$mailBody = '';
//		$mailBody .= 'skriptum-UID: ' . $skriptum->getUid() . PHP_EOL;
//		$mailBody .= 'anfoderung-UID: ' . $anforderung->getUid() . PHP_EOL;
		$mailBody .= '<p><strong>Es wurde das Skriptum "' . $skriptum->getTitle() . '" angefordert: </strong></p>' . PHP_EOL;
		$mailBody .= '<br>' . PHP_EOL;
		$mailBody .= '<table>' . PHP_EOL;
		$mailBody .= '<tr><th align="left">Zeitpunkt:</th><td>' . date('d.m.Y H:i:s') . '</td></tr>' . PHP_EOL;
		$mailBody .= '<tr><th align="left"></th><td></td></tr>' . PHP_EOL;
		$mailBody .= '<tr><th align="left">Titel:</th><td>' . $anforderung->getTitle() . '</td></tr>' . PHP_EOL;
		$mailBody .= '<tr><th align="left">Vorname:</th><td>' . $anforderung->getFirstname() . '</td></tr>' . PHP_EOL;
		$mailBody .= '<tr><th align="left">Nachname:</th><td>' . $anforderung->getLastname() . '</td></tr>' . PHP_EOL;
		$mailBody .= '<tr><th align="left">Nachgest. Titel:</th><td>' . $anforderung->getTitleAfter() . '</td></tr>' . PHP_EOL;
		$mailBody .= '<tr><th align="left">Unternehmen:</th><td>' . $anforderung->getCompany() . '</td></tr>' . PHP_EOL;
		$mailBody .= '<tr><th align="left">E-Mail:</th><td>' . $anforderung->getEmail() . '</td></tr>' . PHP_EOL;
		if( $anforderung->getNewsletter()==1 || $anforderung->getNewsletter()=='1' ){
			$mailBody .= '<tr><th align="left">Newsletter abonieren:</th><td>JA</td></tr>' . PHP_EOL;
		}
		$mailBody .= '</table>' . PHP_EOL;
		$mailBody .= PHP_EOL;
		//
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		$mail
		  ->setTo( $m_recipient )
		  ->setFrom( $m_sender )
		  ->setSubject( $m_recipient_subject )
		  ->setCharset( $m_charset );
		//
		//$mail->setCc($m_ccArray);
		//$mail->setBcc($m_bccArray);
		//      $message->setReturnPath($cObj->cObjGetSingle($conf[$type . '.']['overwrite.']['returnPath'], $conf[$type . '.']['overwrite.']['returnPath.']));
		//      $mail->setReplyTo($replyArray);
		$mail->setBody($mailBody, 'text/html');


		$mail->send();
		$m_result = $mail->isSent();
		
		$error = 0;
		if( !$m_result ){
			//	ERROR SENDING MAIL TO RECIPIENT
			$error = 1;
		} else {
			//	NOW, SEND MAIL TO USER

			//
			$user_email = $anforderung->getEmail();
			$user_name = trim( $anforderung->getFirstname() . ' ' . $anforderung->getLastname() );
			$user_name = $anforderung->getTitle() ? ($anforderung->getTitle() . ' ' . $user_name) : $user_name;
			$user_name = $anforderung->getTitleAfter() ? ($user_name . ', ' . $anforderung->getTitleAfter()) : $user_name;
			//
			$m_recipient = array( $user_email  => $user_name );
			//
			$m_user_subject = $skriptum->getMailUserSubject();
			//
			$m_attachment = '';
			if( $skriptum->getMailAttachment() ){
				if( $skriptum->getMailAttachment()->getOriginalResource() ){
					//	CREATE ATTACHMENT FROM SRC-PATH
					$m_attachment = \Swift_Attachment::fromPath( $skriptum->getMailAttachment()->getOriginalResource()->getPublicUrl() );
//					$m_attachment = \Swift_Attachment::fromPath('/path/to/image.jpg', 'image/jpeg');
					// ADDING THE FILENAME (OPTIONAL !)
					$m_attachment->setFilename( $skriptum->getMailAttachment()->getOriginalResource()->getProperty('name') );
				}
			}

			//
			$m_content = $skriptum->getMailUserText() ? $skriptum->getMailUserText() : 'TEST';
			$m_content = $this->skriptumRepository->prepareMailContent($m_content, $this->settings, $this->configurationManager->getContentObject());
			
			//
			$mailData = array(
/*
				'topic' => $this->settings['mailRegisterTopic'],
				'topic' => 'Anforderung Skriptum',
*/
				'topic' => $skriptum->getTitle(),
				'info' => $m_content,
				'mailFooterLinks' => $this->skriptumRepository->buildMailFooterLinks($this->settings, $this->configurationManager->getContentObject()),
			);
			$mailBody = $this->skriptumRepository->buildMailBody($mailData, $this->settings);
			//
			$mail
			  ->setTo( $m_recipient )
			  ->setFrom( $m_sender )
			  ->setSubject( $m_user_subject )
			  ->setCharset( $m_charset );
			$mail->setBody($mailBody, 'text/html');
			//	ADD ATTACHMENT IF THERE IS ONE
			if( $m_attachment ){
				$mail->attach($m_attachment);
			}
			//	SEND MAIL
			$mail->send();
			$m_result = $mail->isSent();
			//
			if( !$m_result ){
				$error = 1;
			} else {
				//
				//-> olles OK!
			}	
		}
		//
		$actionName = $error == 0 ? 'done' : 'fail';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array( 'skriptum' => $skriptum->getUid() );
		//
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
	}
	
	
	/**
	 * action done
	 *
	 * @param \NEXT\IconSkriptenbestellung\Domain\Model\Skriptum $skriptum
	 * @return void
	 */
	public function doneAction(\NEXT\IconSkriptenbestellung\Domain\Model\Skriptum $skriptum = NULL) {
		$LANG =  $GLOBALS['TSFE']->sys_language_uid;
		//
		$this->view->assignMultiple(array(
			'LANG' => $LANG,
			'skript' => $skriptum,
		));
	}
	
	
	/**
	 * action fail
	 *
	 * @param \NEXT\IconSkriptenbestellung\Domain\Model\Skriptum $skriptum
	 * @return void
	 */
	public function failAction(\NEXT\IconSkriptenbestellung\Domain\Model\Skriptum $skriptum = NULL) {
		$LANG =  $GLOBALS['TSFE']->sys_language_uid;
		//
		$this->view->assignMultiple(array(
			'LANG' => $LANG,
			'skript' => $skriptum,
		));
	}

	/**
	 * action csvFile
	 *
	 * @return void
	 */
	public function csvFileAction() {
		$csv = '';
		// this will execute only in the backend...
		$uidSkriptum = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP("skriptum");
		if( $uidSkriptum > 0 ){
			$skriptum = $this->skriptumRepository->findByUid($uidSkriptum);
			if( $skriptum ){
				$csv = $this->anforderungRepository->buildCsvFile($skriptum, $this->settings);
			}
		} else {
			header('Content-Type: text/plain; charset=utf-8');
			$csv = 'ungültiger Aufruf!;event = ' . $uidEvent;
		}
		//
		return $csv;
	}

}

?>