<?php
namespace NEXT\IconSkriptenbestellung\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_skriptenbestellung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Skriptum extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
	/**
	 * Ttitle
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $title;

	/**
	 * image
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $image;

	/**
	 * info_done
	 *
	 * @var string
	 */
	protected $infoDone;

	/**
	 * info_done_EN
	 *
	 * @var string
	 */
	protected $infoDoneEn;

	/**
	 * mail_attachment
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $mailAttachment;

	/**
	 * mail_recipient_email
	 *
	 * @var string
	 */
	protected $mailRecipientEmail;

	/**
	 * mail_recipient_name
	 *
	 * @var string
	 */
	protected $mailRecipientName;

	/**
	 * mail_recipient_subject
	 *
	 * @var string
	 */
	protected $mailRecipientSubject;

	/**
	 * mail_user_subject
	 *
	 * @var string
	 */
	protected $mailUserSubject;

	/**
	 * mail_user_text
	 *
	 * @var string
	 */
	protected $mailUserText;

	/**
	 * teaser_header
	 *
	 * @var string
	 */
	protected $teaserHeader;

	/**
	 * teaser_text
	 *
	 * @var string
	 */
	protected $teaserText;


	/**
	 * Related Text Link
	 *
	 * @var \string
	 */
//	protected $relatedTextLink;

	/**
	 * tag
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconEvents\Domain\Model\Tag>
	 */
//	protected $tag;

	/**
	 * category
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconEvents\Domain\Model\Category>
	 * @lazy
	 */
//	protected $category;

	/**
	 * __construct
	 *
	 * @return Link
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}
	
	/**
	 * Initializes all ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
//		$this->categories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
//		$this->tag = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}
	
	
	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the image
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
	 */
	public function getImage() {
		return $this->image;
	}
	
	/**
	 * Sets the image
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
	 * @return void
	 */
	public function setImage($image) {
		$this->image = $image;
	}
	
	/**
	 * Returns the infoDone
	 *
	 * @return string $infoDone
	 */
	public function getInfoDone() {
		return $this->infoDone;
	}
	
	/**
	 * Sets the infoDone
	 *
	 * @param string $infoDone
	 * @return void
	 */
	public function setInfoDone($infoDone) {
		$this->infoDone = $infoDone;
	}

	/**
	 * Returns the infoDoneEn
	 *
	 * @return string $infoDoneEn
	 */
	public function getInfoDoneEn() {
		return $this->infoDoneEn;
	}
	
	/**
	 * Sets the infoDoneEn
	 *
	 * @param string $infoDoneEn
	 * @return void
	 */
	public function setInfoDoneEn($infoDoneEn) {
		$this->infoDoneEn = $infoDoneEn;
	}



	/**
	 * Returns the mailAttachment
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $mailAttachment
	 */
	public function getMailAttachment() {
		return $this->mailAttachment;
	}
	
	/**
	 * Sets the mailAttachment
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $mailAttachment
	 * @return void
	 */
	public function setMailAttachment($mailAttachment) {
		$this->mailAttachment = $mailAttachment;
	}
	

		/*
			START:		RECIPIENT
		*/

	/**
	 * Returns the mailRecipientEmail
	 *
	 * @return string $mailRecipientEmail
	 */
	public function getMailRecipientEmail() {
		return $this->mailRecipientEmail;
	}
	
	/**
	 * Sets the mailRecipientEmail
	 *
	 * @param string $mailRecipientEmail
	 * @return void
	 */
	public function setMailRecipientEmail($mailRecipientEmail) {
		$this->mailRecipientEmail = $mailRecipientEmail;
	}

	/**
	 * Returns the mailRecipientName
	 *
	 * @return string $mailRecipientName
	 */
	public function getMailRecipientName() {
		return $this->mailRecipientName;
	}
	
	/**
	 * Sets the mailRecipientName
	 *
	 * @param string $mailRecipientName
	 * @return void
	 */
	public function setMailRecipientName($mailRecipientName) {
		$this->mailRecipientName = $mailRecipientName;
	}

	/**
	 * Returns the mailRecipientSubject
	 *
	 * @return string $mailRecipientSubject
	 */
	public function getMailRecipientSubject() {
		return $this->mailRecipientSubject;
	}
	
	/**
	 * Sets the mailRecipientSubject
	 *
	 * @param string $mailRecipientSubject
	 * @return void
	 */
	public function setMailRecipientSubject($mailRecipientSubject) {
		$this->mailRecipientSubject = $mailRecipientSubject;
	}


		/*
			END:		RECIPIENT
		*/


	/*
		START:		USER
	*/

	/**
	 * Returns the mailUserSubject
	 *
	 * @return string $mailUserSubject
	 */
	public function getMailUserSubject() {
		return $this->mailUserSubject;
	}
	
	/**
	 * Sets the mailUserSubject
	 *
	 * @param string $mailUserSubject
	 * @return void
	 */
	public function setMailUserSubject($mailUserSubject) {
		$this->mailUserSubject = $mailUserSubject;
	}

	/**
	 * Returns the mailUserText
	 *
	 * @return string $mailUserText
	 */
	public function getMailUserText() {
		return $this->mailUserText;
	}
	
	/**
	 * Sets the mailUserText
	 *
	 * @param string $mailUserText
	 * @return void
	 */
	public function setMailUserText($mailUserText) {
		$this->mailUserText = $mailUserText;
	}

	/*
		END:		USER
	*/

	/*
		START:		TEASDER
	*/

	/**
	 * Returns the teaserHeader
	 *
	 * @return string $teaserHeader
	 */
	public function getTeaserHeader() {
		return $this->teaserHeader;
	}
	
	/**
	 * Sets the teaserHeader
	 *
	 * @param string $teaserHeader
	 * @return void
	 */
	public function setTeaserHeader($teaserHeader) {
		$this->teaserHeader = $teaserHeader;
	}

	/**
	 * Returns the teaserText
	 *
	 * @return string $teaserText
	 */
	public function getTeaserText() {
		return $this->teaserText;
	}
	
	/**
	 * Sets the teaserText
	 *
	 * @param string $teaserText
	 * @return void
	 */
	public function SetTeaserText($teaserText) {
		$this->teaserText = $teaserText;
	}


	/*
		END:		TEASER
	*/
	


}
?>