<?php
namespace NEXT\IconSkriptenbestellung\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_skriptenbesetllung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Anforderung extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	// START: -------------- SYSTEM ----------------

	/**
	 * skriptum
	 *
	 * @var integer
	 */
	protected $skriptum;
	
	/**
	 * title
	 *
	 * @var string
	 */
	protected $title;

	/**
	 * firstname
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $firstname;

	/**
	 * lastname
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $lastname;

	/**
	 * titleAfter
	 *
	 * @var string
	 */
	protected $titleAfter;

	/**
	 * company
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $company;

	/**
	 * email
	 *
	 * @var string
	 * @validate EmailAddress, NotEmpty
	 */
	protected $email;

	/**
	 * newsletter
	 *
	 * @var int
	 */
	protected $newsletter = 0;



	/**
	* Sets the skriptum
	*
	* @param integer $skriptum
	* @return void
	*/
	public function setSkriptum($skriptum) {
		$this->skriptum = $skriptum;
	}
	
	/**
	* Returns the skriptum
	*
	* @return integer $skriptum
	*/
	public function getSkriptum() {
		return $this->skriptum;
	}
	
	/**
	* Sets the title
	*
	* @param string $title
	* @return void
	*/
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	* Returns the title
	*
	* @return string $title
	*/
	public function getTitle() {
		return $this->title;
	}

	/**
	* Sets the firstname
	*
	* @param string $firstname
	* @return void
	*/
	public function setFirstname($firstname) {
		$this->firstname = $firstname;
	}
	
	/**
	* Returns the firstname
	*
	* @return string $firstname
	*/
	public function getFirstname() {
		return $this->firstname;
	}

	/**
	* Sets the lastname
	*
	* @param string $lastname
	* @return void
	*/
	public function setLastname($lastname) {
		$this->lastname = $lastname;
	}
	
	/**
	* Returns the lastname
	*
	* @return string $lastname
	*/
	public function getLastname() {
		return $this->lastname;
	}

	/**
	* Sets the titleAfter
	*
	* @param string $titleAfter
	* @return void
	*/
	public function setTitleAfter($titleAfter) {
		$this->titleAfter = $titleAfter;
	}
	
	/**
	* Returns the titleAfter
	*
	* @return string $titleAfter
	*/
	public function getTitleAfter() {
		return $this->titleAfter;
	}

	/**
	* Sets the company
	*
	* @param string $company
	* @return void
	*/
	public function setCompany($company) {
		$this->company = $company;
	}
	
	/**
	* Returns the company
	*
	* @return string $company
	*/
	public function getCompany() {
		return $this->company;
	}

	/**
	* Sets the email
	*
	* @param string $email
	* @return void
	*/
	public function setEmail($email) {
		$this->email = $email;
	}
	
	/**
	* Returns the email
	*
	* @return string $email
	*/
	public function getEmail() {
		return $this->email;
	}

	/**
	* Sets the newsletter
	*
	* @param int $newsletter
	* @return void
	*/
	public function setNewsletter($newsletter) {
		$this->newsletter = $newsletter;
	}
	
	/**
	* Returns the newsletter
	*
	* @return int $newsletter
	*/
	public function getNewsletter() {
		return $this->newsletter;
	}

}
?>