<?php
namespace NEXT\IconSkriptenbestellung\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_skriptenbestellung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SkriptumRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * prepareMailContent
	 *
	 * @param string $text
	 * @param array $settings
	 * @param mixed $cObj
	 * @return string
	 */
	public function prepareMailContent ($text, $settings, $cObj) {
		//
		$text = $cObj->parseFunc($text, array(), '< lib.parseFunc_RTE');
		//
		$srch = array(' href="/', ' href="de/');
		$rplc = array(' href="' . $settings['mail']['footer']['URLPrefix'] . '/');
		//
		$text = str_replace( $srch, $rplc, $text );
		//
		return $text;
	}


  /**
   * build Mail-Body and return string
   * 
   * @param array $mailData
   * @param array $settings
   * @return string
   */
  public function buildMailBody ($mailData, $settings) {
	$mailBody = $this->getMailBody($settings);
	$mailBody = $this->mergeMailBodyData($mailBody, $mailData);
	//
	return $mailBody;
  }
  
  /**
   * builds FOOTER LINKS for Mail
   * 
   * @param array $settings
   * @param mixed $cObj
   * @return string
   */
  public function buildMailFooterLinks ($settings, $cObj) {
	$ATagParams = 'style="color:#FFFFFF; font-size:12px;"';
	$srch = 'href="/';
	$rplc = 'href="' . $settings['mail']['footer']['URLPrefix'] . '/';
	$t = array();
	$contact = $settings['mail']['footer']['contactPage'];
	$imprint = $settings['mail']['footer']['imprintPage'];
	$disclaimer = $settings['mail']['footer']['disclaimerPage'];
	if( $contact ){
		$cfgTypoLink = array( 'parameter' => $contact, 'ATagParams' => $ATagParams );
		$t[] = str_replace( $srch, $rplc, $cObj->typolink('KONTAKT', $cfgTypoLink) );
	}
	if( $imprint ){
		$cfgTypoLink = array( 'parameter' => $imprint, 'ATagParams' => $ATagParams  );
		$t[] = str_replace( $srch, $rplc, $cObj->typolink('IMPRESSUM', $cfgTypoLink) );
	}
	if( $disclaimer ){
		$cfgTypoLink = array( 'parameter' => $disclaimer, 'ATagParams' => $ATagParams  );
		$t[] = str_replace( $srch, $rplc, $cObj->typolink('DISCLAIMER', $cfgTypoLink) );
	}
	//
	$t = join( ' &middot; ', $t);
	//
	return $t;
  }


  /**
   * inserts MailData into MailBody and returns new string
   *
   * @param string $mailBody
   * @param array $mailData
   * @return string
   */
  public function mergeMailBodyData ($mailBody, $mailData) {
	//
	$mailBody = str_replace('###TOPIC###', $mailData['topic'], $mailBody);
	$mailBody = str_replace('###ANREDE###!', $mailData['anrede'], $mailBody);
	$mailBody = str_replace('###IMAGE###', $mailData['mailImage'], $mailBody);
	$mailBody = str_replace('###INFO###', $mailData['info'], $mailBody);
	$mailBody = str_replace('###FOOTER_LINKS###', $mailData['mailFooterLinks'], $mailBody);
	//
	return $mailBody;
  }
  
  
  /**
   * returns contents of template as MailBody
   *
   * @param array $settings
   * @return string
   */
  public function getMailBody ($settings) {
	$fp_file = $settings['mail']['user']['template'];
	$fp = fopen($fp_file, 'r');
	$mailBody = fread($fp, filesize($fp_file));
	fclose($fp);
	//
	return $mailBody;
  }


}
?>