<?php
namespace NEXT\IconSkriptenbestellung\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_skriptenbestellung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AnforderungRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

  /**
   * buildCsvFile
   *
   * @param \NEXT\IconSkriptenbestellung\Domain\Model\Skriptum $skriptum
   * @param \array $settings
   * @return \string
   */
  public function buildCsvFile (\NEXT\IconSkriptenbestellung\Domain\Model\Skriptum $skriptum, $settings) {
	//
	$filename .= 'anforderungen';
	$headers = array('Titel', 'Vorname', 'Nachname', 'Nachgest. Titel', 'Unternehmen', 'E-Mail', 'Newsletter', 'uid', 'Datum/Uhrzeit');
	$delimiter = ';';
	// we use a threshold of 2 MB (2 *1024 * 1024), it's just an example
	$fd = fopen('php://temp/maxmemory:2097152', 'w');
	if($fd === FALSE) {
		die('Failed to open temporary file');
	}
	//
	fputcsv($fd, $headers, $delimiter);
	//
	$uid = $skriptum->getUid();
	if( $uid > 0 ){
		//
		$filename .= '_uid' . $uid;
		//
		$sql = ' SELECT * FROM `tx_iconskriptenbestellung_domain_model_anforderung` ';
		$sql .= ' WHERE `skriptum` = ' . $uid ;
		$sql .= ' ORDER BY `uid` ASC ';
		//
		$res = $GLOBALS['TYPO3_DB']->sql_query($sql);
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$crdate = $row['crdate'];
			$t = array(
				$row['title'],
				$row['firstname'],
				$row['lastname'],
				$row['title_after'],
				$row['company'],
				$row['email'],
				$row['newsletter'],
				$row['uid'],
				date("d.m.Y H:i:s", $row['crdate']),
			);
			fputcsv($fd, $t, $delimiter);
		}
		$GLOBALS['TYPO3_DB']->sql_free_result($res);
	}
	//
	rewind($fd);
	$csv = stream_get_contents($fd);
	fclose($fd);
	//
	$filename .= '.csv';
	$csv = utf8_decode($csv);
	//
//	header('Content-Type: application/csv');
//	header('Content-Type: text/csv; charset=utf-8');
    // tell the browser we want to save it instead of displaying it
    header('Content-Disposition: attachement; filename="'.$filename.'";');
	//
	return $csv;
  }


}
?>