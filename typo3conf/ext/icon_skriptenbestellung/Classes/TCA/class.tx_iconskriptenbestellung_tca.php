<?php

class tx_iconskriptenbestellung_tca {

        public function showLinkCSV ($PA, $fObj) {
                $formField = '';
                $uid = $PA['itemFormElValue'];
                $path = '/skriptumanforderungen.csv?';
				$url_skriptum = $path . 'skriptum=' . $uid;
				$url_folder = $path . 'folder=' . $uid;
                if( is_numeric($uid) && $uid > 0 ){
                        $formField .= '<div>';
                        $formField .= 'aktuellen Stand <a href="'.$url_skriptum.'" target="_blank" style="text-decoration:underline;">downloaden (.csv)</a>';
                        $formField .= '</div>';
                }
                return $formField;
        }
}

?>
