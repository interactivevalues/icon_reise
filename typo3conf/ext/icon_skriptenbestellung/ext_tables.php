<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iconskriptenbestellung_domain_model_skriptum', 'EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_csh_tx_iconskriptenbestellung_domain_model_skriptum.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iconskriptenbestellung_domain_model_skriptum');

//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iconskriptenbestellung_domain_model_anforderung', 'EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_csh_tx_iconskriptenbestellung_domain_model_anforderung.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iconskriptenbestellung_domain_model_anforderung');

?>
