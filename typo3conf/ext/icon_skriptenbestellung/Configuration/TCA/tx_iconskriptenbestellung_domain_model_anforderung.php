<?php
if (!defined ('TYPO3_MODE')) {
  die ('Access denied.');
}

$version7 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('7.0');
$version8 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('8.0');

return array(
	'ctrl' => array(
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'prependAtCopy' => ' - Kopie',
		'delete' => 'deleted',
		'title'  => 'LLL:EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_db.xlf:tx_iconskriptenbestellung_domain_model_anforderung',
		'versioningWS' => false,
		'origUid' => 't3_origuid',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title, firstname, lastname, title_after, company, email',
        'iconfile' => $version7 ? 'EXT:icon_skriptenbestellung/ext_icon.gif' : \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('icon_skriptenbestellung') . 'ext_icon.gif',
		'dividers2tabs' => 1,

		'sortby' => 'sorting',
		'hideAtCopy' => TRUE,
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, type, pre_title, pre_title_big, title, image, link, registration, price, price_special, speaker, text_links, text_rechts,',
	),
	'types' => array(
		'1' => array('showitem' => 
			'sys_language_uid, uid, l10n_parent, l10n_diffsource, hidden, title, image, info_done, info_done_EN, --div--;Mail an User, mail_user_subject,mail_user_text,mail_attachment,--div--;Mail an ICON, mail_recipient_name,mail_recipient_email,mail_recipient_subject,--div--;Teaser,teaser_header,teaser_text,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array(
			'showitem' => '',
		),
	),
	'columns' => array(
		'uid' => array(
			'exclude' => 1,
			'label' => 'Anforderungen',
			'config' => array(
				'type' => 'user',
				'size' => 30,
				'userFunc' => 'EXT:icon_skriptenbestellung/Classes/TCA/class.tx_iconskriptenbestellung_tca.php:tx_iconskriptenbestellung_tca->showLinkCSV',
				'parameters' => array(),
			),
		),
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_iconskriptenbestellung_domain_model_skriptum',
				'foreign_table_where' => 'AND tx_iconskriptenbestellung_domain_model_skriptum.pid=###CURRENT_PID### AND tx_iconskriptenbestellung_domain_model_skriptum.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),

		'skriptum' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_db.xlf:tx_iconskriptenbestellung_domain_model_skriptum.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_db.xlf:tx_iconskriptenbestellung_domain_model_skriptum.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'firstname' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_db.xlf:tx_iconskriptenbestellung_domain_model_skriptum.info_done',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'lastname' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_db.xlf:tx_iconskriptenbestellung_domain_model_skriptum.info_done_EN',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'title_after' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_db.xlf:tx_iconskriptenbestellung_domain_model_skriptum.info_done_EN',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'company' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_db.xlf:tx_iconskriptenbestellung_domain_model_skriptum.info_done_EN',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'email' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_db.xlf:tx_iconskriptenbestellung_domain_model_skriptum.info_done_EN',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'newsletter' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:icon_skriptenbestellung/Resources/Private/Language/locallang_db.xlf:tx_iconskriptenbestellung_domain_model_skriptum.info_done_EN',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

	),
);

?>