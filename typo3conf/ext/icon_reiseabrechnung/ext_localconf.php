<?php

if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1_Manager',
  array(
    'Manager' => 'index, login, logout, edit, save, submit, submitted, seminar, saveSeminar, abrechnen, abrechnenBH, abrechnenLV, abrechnenWV, erledigt, erstattung, view, delete, deleted, showDeleted, mitarbeiter, editMitarbeiter, saveMitarbeiter, newMitarbeiter, saveNewMitarbeiter',
  ),
  // non-cacheable actions
  array(
    'Manager' => 'index, login, logout, edit, save, submit, submitted, seminar, saveSeminar, abrechnen, abrechnenBH, abrechnenLV, abrechnenWV, erledigt, erstattung, view, delete, deleted, showDeleted, mitarbeiter, editMitarbeiter, saveMitarbeiter, newMitarbeiter, saveNewMitarbeiter',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1_User',
  array(
    'User' => 'index, login, logout, edit, save, submit, submitted, view',
  ),
  // non-cacheable actions
  array(
    'User' => 'index, login, logout, edit, save, submit, submitted, view',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1_Chief',
  array(
    'Chief' => 'view, submit, submitted',
  ),
  // non-cacheable actions
  array(
    'Chief' => 'view, submit, submitted',
  )
);

?>

