<?php
namespace NEXT\IconReiseabrechnung\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ChiefController extends \NEXT\IconReiseabrechnung\Controller\BaseController {


	/**
	 * userMode
	 *
	 * @var string
	 */
	protected $userMode = 'CHIEF';


	/**
	 * action view
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
	 */
	public function viewAction(\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
/*
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		//
		$users = $this->userRepository->findAll();
*/

		//
		$person = $this->userRepository->getByPersonnr($reise->getPersonnr(), $mode='USER');

		//
		$reise->prepareVerkehrsmittel();

		//		
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'person' => $person,
			'reise' => $reise,
			'optionsBelegType' => $this->reiseRepository->getOptionsBelegType(),
			'optionsCountries' => $this->reiseRepository->getOptionsCountries(),
			'optionsNaechtigung' => $this->reiseRepository->getOptionsNaechtigung(),
		));
	}


	/**
	 * action submit
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function submitAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
		//
		$mailCFG = array(
			'contentType' => 'text/html',
			'charset' => $GLOBALS['TSFE']->metaCharset,
		);
		
		$mailCFG['fromEmail'] = 'website@icon.at';
		$mailCFG['fromName'] = 'ICON';

		$secretary = $reise->getSecretary();
		$approved = $reise->getSubmit();
		//
		if( $approved ){
			$reise->setStatusAbrechnen();
			$mail_subject = 'approved';
			$t_URL = 'http://www.icon.at/de/reiseabrechnung/sekretariat/?tx_iconreiseabrechnung_pi1_manager[action]=abrechnen';
			$t_URL_TXT = 'Verwaltung für Reiseabrechnung öffnen';
		} else {
			$reise->setStatusSekretariat();
			$mail_subject = 'declined';
			$t_URL = 'http://www.icon.at/de/reiseabrechnung/sekretariat/?tx_iconreiseabrechnung_pi1_manager[action]=edit&tx_iconreiseabrechnung_pi1_manager[reise]=' . $reise->getUid();
			$t_URL_TXT = 'Reiseabrechnung öffnen';
		}	
		//
		$this->reiseRepository->add($reise);
		$this->persistenceManager->persistAll();

		//
		$person = $this->userRepository->getByPersonnr($reise->getPersonnr(), $mode='USER');

		//	send INFO to central USER
		$m_info = '';
		//
		$m_mailData = $mailData;
		$m_mailData['DECISION'] = $approved ? 'freigegeben' : 'abgelehnt';
		$m_mailData['REISE_NR'] = $reise->getReisenr();
		$m_mailData['PERSON_NR'] = $reise->getPersonnr();
		$m_mailData['FIRSTNAME'] = $person->getFirstName();
		$m_mailData['LASTNAME'] = $person->getLastName();
		$m_mailData['INFO'] = nl2br( strip_tags( $reise->getSecretaryInfo() ) );
		$m_mailData['URL'] = $t_URL;
		$m_mailData['URL_TEXT'] = $t_URL_TXT;
		$m_mailBody = $this->mailRepository->buildMailBody($this->userMode, $m_mailData, $this->settings);
		//
		$m_mailCFG = $mailCFG;
		$m_mailCFG['toEmail'] = $this->settings['departments']['1']['secretary']['email'];
		$m_mailCFG['toEmail'] = $this->settings['departments'][$secretary]['secretary']['email'];
		$m_mailCFG['toName'] = $this->settings['departments']['1']['secretary']['name'];
		$m_mailCFG['toName'] = $this->settings['departments'][$secretary]['secretary']['name'];
		$m_mailCFG['subject'] = $this->settings['chief']['mail']['subject'][$mail_subject];
		//
		$m_mailRes = $this->mailRepository->deliverMail($m_mailCFG, $m_mailBody);

		//	-> show Result-Screen
		$actionName = 'submitted';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array(
			'reise' => $reise->getUid(),
		);
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);

	}

	/**
	 * submittedAction
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function submittedAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {

		//		
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'reise' => $reise,
		));

	}
	
}

