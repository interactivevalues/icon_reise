<?php
namespace NEXT\IconReiseabrechnung\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use NEXT\IconReiseabrechnung\Domain\Model\Passenger;

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class BaseController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
  
	/**
	 * loginRepository
	 *
	 * @var \NEXT\IconReiseabrechnung\Domain\Repository\LoginRepository
	 * @inject
	 */
	protected $loginRepository;
	
	/**
	 * userRepository
	 *
	 * @var \NEXT\IconReiseabrechnung\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository;
	
	/**
	 * mailRepository
	 *
	 * @var \NEXT\IconReiseabrechnung\Domain\Repository\MailRepository
	 * @inject
	 */
	protected $mailRepository;
	
	/**
	 * reiseRepository
	 *
	 * @var \NEXT\IconReiseabrechnung\Domain\Repository\ReiseRepository
	 * @inject
	 */
	protected $reiseRepository;
	
	/**
	 * abschnittRepository
	 *
	 * @var \NEXT\IconReiseabrechnung\Domain\Repository\AbschnittRepository
	 * @inject
	 */
	protected $abschnittRepository;
	
	/**
	 * passengerRepository
	 *
	 * @var \NEXT\IconReiseabrechnung\Domain\Repository\PassengerRepository
	 * @inject
	 */
	protected $passengerRepository;
	
	/**
	 * belegRepository
	 *
	 * @var \NEXT\IconReiseabrechnung\Domain\Repository\BelegRepository
	 * @inject
	 */
	protected $belegRepository;
	
	/**
	 * persistence manager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
	 * @inject
	 */
	protected $persistenceManager;

	/**
	 * userMode
	 *
	 * @var string
	 */
	protected $userMode = '--UNDEFINED--';
  	

	/**
	 * action view
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
	 */
	public function viewAction(\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		//
		$person = $this->userRepository->getByPersonnr($reise->getPersonnr(), $mode='USER');

		//
		$reise->prepareVerkehrsmittel();

		//		
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'viewMode' => TRUE,
			'person' => $person,
			'reise' => $reise,
			'optionsBelegType' => $this->reiseRepository->getOptionsBelegType(),
			'optionsCountries' => $this->reiseRepository->getOptionsCountries(),
			'optionsNaechtigung' => $this->reiseRepository->getOptionsNaechtigung(),
		));
	}


	/**
	 * updateReise
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function updateReise (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {

		/*	ABSCHNITTE	------------------------------------------------	*/
		//	UPDATE ABSCHNITTE
		$update_abschnitte = trim( $reise->getAbschnitteUpdate() );
		if( $update_abschnitte ){
			$lines = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('][', $update_abschnitte);
			foreach( $lines as $line ){
				$values = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('|', $line);
				$t_mode = $values[0];
				$t_uid = $values[1];
				if( $t_mode == 'U' ){
					//	UPDATE
					foreach( $reise->getAbschnitte() as $abschnitt ){
						if( $abschnitt->getUid() == $t_uid ){
							$abschnitt->setAbDatetime( $this->abschnittRepository->prepareDatetime($values[2]) );
							$abschnitt->setAbOrt($values[3]);
							$abschnitt->setAbCountry($values[4]);

							$abschnitt->setGuDateTime( $this->abschnittRepository->prepareDatetime($values[5]) );
							$abschnitt->setGuOrt($values[6]);
							$abschnitt->setGuCountry($values[7]);
							$abschnitt->setGu2DateTime( $this->abschnittRepository->prepareDatetime($values[8]) );
							$abschnitt->setGu2Ort($values[9]);
							$abschnitt->setGu2Country($values[10]);
							$abschnitt->setGu3DateTime( $this->abschnittRepository->prepareDatetime($values[11]) );
							$abschnitt->setGu3Ort($values[12]);
							$abschnitt->setGu3Country($values[13]);
							$abschnitt->setGu4DateTime( $this->abschnittRepository->prepareDatetime($values[14]) );
							$abschnitt->setGu4Ort($values[15]);
							$abschnitt->setGu4Country($values[16]);

							$abschnitt->setAnDateTime( $this->abschnittRepository->prepareDatetime($values[17]) );
							$abschnitt->setAnOrt($values[18]);
							$abschnitt->setAnCountry($values[19]);
							$abschnitt->setPrivatehours($values[20]);
							$abschnitt->setMittag($values[21]);
							$abschnitt->setAbend($values[22]);
							$abschnitt->setNacht($values[23]);
						}
					}
				} elseif( $t_mode == 'A' ){
					//	ADD NEW
					$abschnitt = $this->objectManager->getEmptyObject('\NEXT\IconReiseabrechnung\Domain\Model\Abschnitt');
					$abschnitt->setAbDateTime( $this->abschnittRepository->prepareDatetime($values[2]) );
					$abschnitt->setAbOrt($values[3]);
					$abschnitt->setAbCountry($values[4]);

					$abschnitt->setGuDateTime( $this->abschnittRepository->prepareDatetime($values[5]) );
					$abschnitt->setGuOrt($values[6]);
					$abschnitt->setGuCountry($values[7]);
					$abschnitt->setGu2DateTime( $this->abschnittRepository->prepareDatetime($values[8]) );
					$abschnitt->setGu2Ort($values[9]);
					$abschnitt->setGu2Country($values[10]);
					$abschnitt->setGu3DateTime( $this->abschnittRepository->prepareDatetime($values[11]) );
					$abschnitt->setGu3Ort($values[12]);
					$abschnitt->setGu3Country($values[13]);
					$abschnitt->setGu4DateTime( $this->abschnittRepository->prepareDatetime($values[14]) );
					$abschnitt->setGu4Ort($values[15]);
					$abschnitt->setGu4Country($values[16]);

					$abschnitt->setAnDateTime( $this->abschnittRepository->prepareDatetime($values[17]) );
					$abschnitt->setAnOrt($values[18]);
					$abschnitt->setAnCountry($values[19]);
					$abschnitt->setPrivatehours($values[20]);
					$abschnitt->setMittag($values[21]);
					$abschnitt->setAbend($values[22]);
					$abschnitt->setNacht($values[23]);
					$reise->addAbschnitt($abschnitt);

				} elseif( $t_mode == 'D' ){
					//	DELETE
					foreach( $reise->getAbschnitte() as $abschnitt ){
						if( $abschnitt->getUid() == $t_uid ){
							$reise->delAbschnitt($abschnitt);
							$this->abschnittRepository->remove($abschnitt);
						}
					}

				}
			}
		}

		/*	PASSENGERS	------------------------------------------------	*/
		//	UPDATE PASSENGERS
		$update_passengers = trim( $reise->getPassengersUpdate() );
		if( $update_passengers ){
			$lines = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('][', $update_passengers);
			foreach( $lines as $line ){
				$values = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('|', $line);
				$t_mode = $values[0];
				$t_uid = $values[1];
				if( $t_mode == 'U' ){
					//	UPDATE
					foreach( $reise->getPassengers() as $passenger ){
						if( $passenger->getUid() == $t_uid ){
							$passenger->setFirstname($values[2]);
							$passenger->setLastname($values[3]);
                            $passenger->setKmgefahren($values[4]);
						}
					}
				} elseif( $t_mode == 'A' ){
					//	ADD NEW
					$passenger = $this->objectManager->getEmptyObject('\NEXT\IconReiseabrechnung\Domain\Model\Passenger');
					$passenger->setFirstname($values[2]);
					$passenger->setLastname($values[3]);
                    $passenger->setKmgefahren($values[4]);
					$reise->addPassenger($passenger);

				} elseif( $t_mode == 'D' ){
					//	DELETE
					foreach( $reise->getPassengers() as $passenger ){
						if( $passenger->getUid() == $t_uid ){
							$reise->delPassenger($passenger);
							$this->passengerRepository->remove($passenger);
						}
					}

				}
			}
		}
		
		
		/*	BELEGE	------------------------------------------------	*/
		//	UPDATE BELEGE
		$update_belege = trim( $reise->getBelegeUpdate() );
		if( $update_belege ){
			$lines = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('][', $update_belege);
			foreach( $lines as $line ){
				$values = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('|', $line);
				$t_mode = $values[0];
				$t_uid = $values[1];
				if( $t_mode == 'U' ){
					//	UPDATE
					foreach( $reise->getBelege() as $beleg ){
						if( $beleg->getUid() == $t_uid ){
							$beleg->setType($values[2]);
							$beleg->setCountry($values[3]);
							$beleg->setCurrency($values[4]);
							$beleg->setSum($values[5]);
							//
							if( $this->userMode=='MANAGER' ){
								$beleg->setSumEuroBrutto($values[6]);
								$beleg->setSumEuroNetto($values[7]);
								$beleg->setTaxfee($values[8]);
								$beleg->setMitarbeiter($values[9]);
							}
						}
					}

				} elseif( $t_mode == 'A' ){
					//	ADD NEW
					$beleg = $this->objectManager->getEmptyObject('\NEXT\IconReiseabrechnung\Domain\Model\Beleg');
					$beleg->setType($values[2]);
					$beleg->setCountry($values[3]);
					$beleg->setCurrency($values[4]);
					$beleg->setSum($values[5]);
					//
					if( $this->userMode=='USER' ){
						$beleg->setMitarbeiter(1);	//	KENNUNG, das der BELEG FÜR MA ist, somit bei BERECHNUNG FÜR MA gezählt wird !!! 
					} else {
						$beleg->setSumEuroBrutto($values[6]);
						$beleg->setSumEuroNetto($values[7]);
						$beleg->setTaxfee($values[8]);
						$beleg->setMitarbeiter($values[9]);
					}
					//
					$reise->addBeleg($beleg);

				} elseif( $t_mode == 'D' ){
					//	DELETE
					foreach( $reise->getBelege() as $beleg ){
						if( $beleg->getUid() == $t_uid ){
							$reise->delBeleg($beleg);
							$this->belegRepository->remove($beleg);
						}
					}

				}
			}
		}

	}


	/**
	 * makeKostenberechnung
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function makeKostenberechnung (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
		//
		$MB = PHP_EOL . 'BERECHNUNG:';
		$MB .= PHP_EOL;
		$MB .= PHP_EOL . 'Reise-Nr:  ' . $reise->getReisenr();
		$MB .= PHP_EOL . 'Ziel:  ' . $reise->getZiel();
		$MB .= PHP_EOL . 'Personal-Nr:  ' . $reise->getPersonnr();
		$MB .= PHP_EOL;
		
		//	MAKE SOME CALCULATIONS
		$t_sumTaxfree = 0.0;
		$t_sumTax = 0.0;
		$t_sumTotal = 0.0;
		$t_sumIcon = 0.0;
		$t_sumTaggeld = array( 'AT' => array( 'sum' => 0.0, 'mwst' => 0.0 ), 'DE' => array( 'sum' => 0.0, 'mwst' => 0.0 ), 'REST' => array( 'sum' => 0.0, 'mwst' => 0.0 ) );
		$t_sumNachtgeld = array( 'AT' => array( 'sum' => 0.0, 'mwst' => 0.0 ), 'DE' => array( 'sum' => 0.0, 'mwst' => 0.0 ), 'REST' => array( 'sum' => 0.0, 'mwst' => 0.0 ) );
		$t_sumKmgeld = array( 'AT' => 0.0, 'DE' => 0.0, 'REST' => 0.0 );
		$t_kmgeld = 0.0;
		$t_kmgeld_wv = 0.0;
		$t_kmgeldpassagier = 0.0;
		$t_kmgeldpassagier_wv = 0.0;
		$t_belegTaxfree = 0.0;
		$t_belegTax = 0.0;
		$t_TG = 0.0;
/*
		$t_V_x = 0.0;
		$t_NG_x = 0.0;
*/		
		$t_ausland = false;
		
		//	INLAND / AUSLAND?
		if( $t_ausland ){
			//	AUSLANDS-REISE
			
		} else {
			//	INLANDS-REISE
			$t_abschnitte = $reise->getAbschnitte()->toArray();
			$t_abschnitte_count = count($t_abschnitte);
			$MB .= 'Abschnitte: ' . $t_abschnitte_count;
			//
			if( $t_abschnitte_count > 1 ){
				//	MEHRTÄGIG !!!
				$MB .= PHP_EOL . ' -> MEHRTÄGIG !!!';
				$t_first = reset($t_abschnitte);	//	$t_abschnitte[0];
				$t_last = end($t_abschnitte);		//	$t_abschnitte[1];
				//
				$t_first_str = strftime('%Y-%m-%d', $t_first->getAbDateTime());
				$t_last_str = strftime('%Y-%m-%d', $t_last->getAnDateTime());
				$MB .= PHP_EOL . 't_first_str: ' . $t_first_str;
				$MB .= PHP_EOL . 't_last_str: ' . $t_last_str;
				//
				end($t_abschnitte);
				$t_lastItem = key($t_abschnitte);
				reset($t_abschnitte);
				foreach( $t_abschnitte as $k => $t_abschnitt){
					$isLast = $k === $t_lastItem;
					list($t_TG_x, $t_TG_array, $t_NG_array, $t_MB) = $this->makeKostenberechnungTag($t_abschnitt, true, $isLast);
					//
					$t_TG += $t_TG_x;
					//	TAGGELD BUCHHALTUNGS-WERTE
					$t_sumTaggeld['AT']['sum'] = $t_sumTaggeld['AT']['sum'] + $t_TG_array['AT_sum'];
					$t_sumTaggeld['AT']['mwst'] = $t_sumTaggeld['AT']['mwst'] + $t_TG_array['AT_mwst'];
					$t_sumTaggeld['DE']['sum'] = $t_sumTaggeld['DE']['sum'] + $t_TG_array['DE_sum'];
					$t_sumTaggeld['DE']['mwst'] = $t_sumTaggeld['DE']['mwst'] + $t_TG_array['DE_mwst'];
					$t_sumTaggeld['REST']['sum'] = $t_sumTaggeld['REST']['sum'] + $t_TG_array['REST_sum'];
					$t_sumTaggeld['REST']['mwst'] = $t_sumTaggeld['REST']['mwst'] + $t_TG_array['REST_mwst'];
					//	NACHTGELD BUCHHALTUNGS-WERTE
					$t_sumNachtgeld['AT']['sum'] = $t_sumNachtgeld['AT']['sum'] + $t_NG_array['AT_sum'];
					$t_sumNachtgeld['AT']['mwst'] = $t_sumNachtgeld['AT']['mwst'] + $t_NG_array['AT_mwst'];
					$t_sumNachtgeld['DE']['sum'] = $t_sumNachtgeld['DE']['sum'] + $t_NG_array['DE_sum'];
					$t_sumNachtgeld['DE']['mwst'] = $t_sumNachtgeld['DE']['mwst'] + $t_NG_array['DE_mwst'];
					$t_sumNachtgeld['REST']['sum'] = $t_sumNachtgeld['REST']['sum'] + $t_NG_array['REST_sum'];
					$t_sumNachtgeld['REST']['mwst'] = $t_sumNachtgeld['REST']['mwst'] + $t_NG_array['REST_mwst'];
					//
					$MB .= $t_MB;
					$MB .= PHP_EOL;
					$MB .= PHP_EOL . ' TG ges. Reise: ' . $t_TG;
					$MB .= PHP_EOL;
					$MB .= PHP_EOL;
				}
			} else {
				//	EINTÄGIG
				$MB .= PHP_EOL . ' -> EINTÄGIG !!!';

				$t_abschnitt = $t_abschnitte[0];
				//
				$MB .= PHP_EOL . 't_first_str: ' . strftime('%Y-%m %d %H:%M', $t_abschnitt->getAbDateTime());
				$MB .= PHP_EOL . 't_last_str: ' . strftime('%Y-%m-%d %H:%M', $t_abschnitt->getAnDateTime());
				//
				list($t_TG, $t_TG_array, $t_NG_array, $t_MB) = $this->makeKostenberechnungTag($t_abschnitt, false, false);
				//	TAGGELD BUCHHALTUNGS-WERTE
				$t_sumTaggeld['AT']['sum'] = $t_sumTaggeld['AT']['sum'] + $t_TG_array['AT_sum'];
				$t_sumTaggeld['AT']['mwst'] = $t_sumTaggeld['AT']['mwst'] + $t_TG_array['AT_mwst'];
				$t_sumTaggeld['DE']['sum'] = $t_sumTaggeld['DE']['sum'] + $t_TG_array['DE_sum'];
				$t_sumTaggeld['DE']['mwst'] = $t_sumTaggeld['DE']['mwst'] + $t_TG_array['DE_mwst'];
				$t_sumTaggeld['REST']['sum'] = $t_sumTaggeld['REST']['sum'] + $t_TG_array['REST_sum'];
				$t_sumTaggeld['REST']['mwst'] = $t_sumTaggeld['REST']['mwst'] + $t_TG_array['REST_mwst'];
				//	NACHTGELD BUCHHALTUNGS-WERTE
				//	-> ned gezählt, weil eintägig !!!
				
				//
				$MB .= $t_MB;
				$MB .= PHP_EOL;
				$MB .= PHP_EOL . ' TG ges. Reise: ' . $t_TG;
				$MB .= PHP_EOL;
			}
		}

		$MB .= PHP_EOL;
		//	KMGEFAHREN
		if( $reise->getVkmPkwPrivat() ||
			$reise->getVkmPkwDienst() ) {
			$MB .= PHP_EOL . 'VkmPkwPrivat: ' . $reise->getVkmPkwPrivat();
			$MB .= PHP_EOL . 'VkmPkwDienst: ' . $reise->getVkmPkwDienst();
			$MB .= PHP_EOL . 'kmgefahren:   ' . $reise->getKmgefahren();
			if( $reise->getKmgefahren() > 0 ){
				$t_kmgeld = $reise->getKmgefahren() * 0.42;
				//
				$MB .= PHP_EOL . 'Mitreisende: ' . $reise->getPassengers()->count();
				//$t_kmgeldpassagier = $reise->getPassengers()->count() * ($reise->getKmgefahren() * 0.05);

                //GET KM OF PASSENGER AND CALCULATE KMGELD
                $mitreisende = $reise->getPassengers();
                if ($mitreisende) {
                    $cnt = 1;
                    foreach($mitreisende as $passenger) {
                        /** @var Passenger $passenger */
                        $km = $passenger->getKmgefahren();
                        if($km > 0 && $km <= $reise->getKmgefahren()){
                            $t_kmgeldpassagier += $km * 0.05;

                            //$MB .= PHP_EOL . 't_kmgeld:          ' . $t_kmgeld;
                            $MB .= PHP_EOL . 't_kmgeldpassagier-'.$cnt.': ' . $t_kmgeldpassagier;
                            $cnt++;
                        }
                    }
                }
			}
			//
			$MB .= PHP_EOL . 't_kmgeld:          ' . $t_kmgeld;
			$MB .= PHP_EOL . 't_kmgeldpassagier: ' . $t_kmgeldpassagier;
			//
			$t_kmgeld_wv = $t_kmgeld;
			$t_kmgeldpassagier_wv = $t_kmgeldpassagier;
			//
			if( $reise->getVkmPkwDienst() ){
				$t_kmgeld = 0.0;
				$t_kmgeldpassagier = 0.0;
			}
		}
		//
		$MB .= PHP_EOL . 't_kmgeld:          ' . $t_kmgeld;
		$MB .= PHP_EOL . 't_kmgeldpassagier: ' . $t_kmgeldpassagier;
		$MB .= PHP_EOL . 't_kmgeld_wv:          ' . $t_kmgeld_wv;
		$MB .= PHP_EOL . 't_kmgeldpassagier_wv: ' . $t_kmgeldpassagier_wv;

		//	BELEGE
		foreach( $reise->getBelege() as $beleg ){
			if( $beleg->getMitarbeiter() ){
				if( $beleg->getType() == 6 ||
					$beleg->getType() == '6' ||
					$beleg->getType() == 9 ||
					$beleg->getType() == '9' ||
					$beleg->getType() == 6000 ){
					//	STEUERPFLICHTIG
					$t_belegTax += $beleg->getSumEuroBrutto();
				} else {
					//	STEUERFREI
					$t_belegTaxfree += $beleg->getSumEuroBrutto();
				}
			} else {
				$t_sumIcon += $beleg->getSumEuroBrutto();
			}
		}
		//
		$MB .= PHP_EOL;
		$MB .= PHP_EOL . 't_belegTax:     ' . $t_belegTax;
		$MB .= PHP_EOL . 't_belegTaxfree: ' . $t_belegTaxfree;
		
		//	SUM
		$t_sumKmgeld['AT'] = $t_kmgeld + $t_kmgeldpassagier;
		$t_sumTaxfree = $t_TG + $t_kmgeld + $t_kmgeldpassagier + $t_belegTaxfree;
		$t_sumTax = $t_belegTax;
		$t_sumTotal = $t_TG + $t_kmgeld + $t_kmgeldpassagier + $t_belegTaxfree + $t_belegTax;
		$t_sumTotalWv = $reise->getVerrechenbar() ? ( $t_TG + $t_kmgeld_wv + $t_kmgeldpassagier_wv + $t_belegTaxfree + $t_belegTax + $t_sumIcon ) : 0.0;
		//
		$MB .= PHP_EOL;
		$MB .= PHP_EOL . 't_sumTaggeld AT sum:        ' . $t_sumTaggeld['AT']['sum'];
		$MB .= PHP_EOL . 't_sumTaggeld AT mwst:       ' . $t_sumTaggeld['AT']['mwst'];
		$MB .= PHP_EOL . 't_sumTaggeld DE sum:        ' . $t_sumTaggeld['DE']['sum'];
		$MB .= PHP_EOL . 't_sumTaggeld DE mwst:       ' . $t_sumTaggeld['DE']['mwst'];
		$MB .= PHP_EOL . 't_sumTaggeld REST sum:      ' . $t_sumTaggeld['REST']['sum'];
		$MB .= PHP_EOL . 't_sumTaggeld REST mwst:     ' . $t_sumTaggeld['REST']['mwst'];
		$MB .= PHP_EOL . 't_sumNachtgeld AT sum:      ' . $t_sumNachtgeld['AT']['sum'];
		$MB .= PHP_EOL . 't_sumNachtgeld AT mwst:     ' . $t_sumNachtgeld['AT']['mwst'];
		$MB .= PHP_EOL . 't_sumNachtgeld DE sum:      ' . $t_sumNachtgeld['DE']['sum'];
		$MB .= PHP_EOL . 't_sumNachtgeld DE mwst:     ' . $t_sumNachtgeld['DE']['mwst'];
		$MB .= PHP_EOL . 't_sumNachtgeld REST sum:    ' . $t_sumNachtgeld['REST']['sum'];
		$MB .= PHP_EOL . 't_sumNachtgeld REST mwst:   ' . $t_sumNachtgeld['REST']['mwst'];
		$MB .= PHP_EOL . 't_sumKmgeld AT:         ' . $t_sumKmgeld['AT'];
		$MB .= PHP_EOL;
		$MB .= PHP_EOL . 't_sumTaxfree:           ' . $t_sumTaxfree;
		$MB .= PHP_EOL . 't_sumTax:               ' . $t_sumTax;
		$MB .= PHP_EOL . 't_sumTotal:             ' . $t_sumTotal;
		$MB .= PHP_EOL . 't_sumTotalWv:           ' . $t_sumTotalWv;

		//	Kürze float-zahlen
		$t_sumTaggeld['AT']['sum'] = number_format($t_sumTaggeld['AT']['sum'], 2, '.', '' );
		$t_sumTaggeld['AT']['mwst'] = number_format($t_sumTaggeld['AT']['mwst'], 2, '.', '' );
		$t_sumTaggeld['DE']['sum'] = number_format($t_sumTaggeld['DE']['sum'], 2, '.', '' );
		$t_sumTaggeld['DE']['mwst'] = number_format($t_sumTaggeld['DE']['mwst'], 2, '.', '' );
		$t_sumTaggeld['REST']['sum'] = number_format($t_sumTaggeld['REST']['sum'], 2, '.', '' );
		$t_sumTaggeld['REST']['mwst'] = number_format($t_sumTaggeld['REST']['mwst'], 2, '.', '' );
		$t_sumNachtgeld['AT']['sum'] = number_format($t_sumNachtgeld['AT']['sum'], 2, '.', '' );
		$t_sumNachtgeld['AT']['mwst'] = number_format($t_sumNachtgeld['AT']['mwst'], 2, '.', '' );
		$t_sumNachtgeld['DE']['sum'] = number_format($t_sumNachtgeld['DE']['sum'], 2, '.', '' );
		$t_sumNachtgeld['DE']['mwst'] = number_format($t_sumNachtgeld['DE']['mwst'], 2, '.', '' );
		$t_sumNachtgeld['REST']['sum'] = number_format($t_sumNachtgeld['REST']['sum'], 2, '.', '' );
		$t_sumNachtgeld['REST']['mwst'] = number_format($t_sumNachtgeld['REST']['mwst'], 2, '.', '' );
		$t_sumKmgeld['AT'] = number_format($t_sumKmgeld['AT'], 2, '.', '' );
		$t_sumKmgeld['DE'] = number_format($t_sumKmgeld['DE'], 2, '.', '' );
		$t_sumKmgeld['REST'] = number_format($t_sumKmgeld['REST'], 2, '.', '' );
		//
		$reise->setSumTaggeld($t_sumTaggeld);
		$reise->setSumNachtgeld($t_sumNachtgeld);
		$reise->setSumKmgeld($t_sumKmgeld);
		//
		$reise->setSumTaxfree( $t_sumTaxfree );
		$reise->setSumTax( $t_sumTax );
		$reise->setSumTotal( $t_sumTotal );
		$reise->setSumTotalWv( $t_sumTotalWv );

		//
		$t_user = $this->getLoggedInUser();
		if( $t_user->getPersonnr() == '999999' || $t_user->getPersonnr() == '111111' ){
			//
			$mailCFG = array(
				'contentType' => 'text/plain',
				'charset' => $GLOBALS['TSFE']->metaCharset,
			);
			$mailCFG['fromEmail'] = 'website@icon.at';
			$mailCFG['fromName'] = 'ICON';
			$mailCFG['toEmail'] = 'wegererthomas@gmail.com';
			$mailCFG['toName'] = 'smo';
			$mailCFG['subject'] = 'BERECHNUNG: ' . $reise->getReisenr() . ' | ' . $reise->getZiel();

			$this->mailRepository->deliverMail($mailCFG, $MB);
		}

	}

	/*
	 * makeKostenberechnungTag
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Abschnitt $abschnitt
	 * @param $multiDay
	 * @param $lastDay
	 * @return mixed
	 */
	public function makeKostenberechnungTag (\NEXT\IconReiseabrechnung\Domain\Model\Abschnitt $abschnitt, $multiDay, $lastDay ) {
		//
/*
		$TS = array(
			'AT' => 26.40,
			'DE' => 35.30,
			'CH' => 36.80,
			'CZ' =>	31.00,
			'HU' =>	26.60,
			'IT' => 35.80,
		);
		$NS = array(
			'AT' => 15.00,
			'DE' => 27.90,
			'CH' => 32.70,
			'CZ' =>	24.40,
			'HU' =>	26.60,
			'IT' => 27.90,
		);
*/
		//
		$MB = PHP_EOL;
		$t_TG_AT_sum = 0.0;
		$t_TG_AT_mwst = 0.0;
		$t_TG_DE_sum = 0.0;
		$t_TG_DE_mwst = 0.0;
		$t_TG_REST_sum = 0.0;
		$t_TG_REST_mwst = 0.0;
		$t_NG_AT_sum = 0.0;
		$t_NG_AT_mwst = 0.0;
		$t_NG_DE_sum = 0.0;
		$t_NG_DE_mwst = 0.0;
		$t_NG_REST_sum = 0.0;
		$t_NG_REST_mwst = 0.0;

		//
		$isInland = ($abschnitt->getAbCountry()=='AT' && 
					 $abschnitt->getGuCountry()=='AT' && 
					 $abschnitt->getGu2Country()=='AT' && 
					 $abschnitt->getGu3Country()=='AT' && 
					 $abschnitt->getGu4Country()=='AT' &&
					 $abschnitt->getAnCountry()=='AT');
		$MB .= PHP_EOL . ' isInland: ' . ($isInland ? 'JA' : 'NEIN');
		//
//		$t_TS = $TS[ $abschnitt->getAbCountry() ];
//		$t_NS = $NS[ $abschnitt->getAnCountry() ];
		$t_TS = $this->reiseRepository->getTagsatzLand( $abschnitt->getAbCountry() );
		$t_NS = $this->reiseRepository->getNachtsatzLand( $abschnitt->getAnCountry() );
		$MB .= PHP_EOL . ' TS[' . $abschnitt->getAbCountry() . ']: ' . $t_TS;
		$MB .= PHP_EOL . ' NS[' . $abschnitt->getAbCountry() . ']: ' . $t_NS;
		
		//
		$t_RZ_start = $abschnitt->getAbDateTime();
		$t_RZ_end = $abschnitt->getAnDateTime();
		//
		$MB .= PHP_EOL . ' is multiDay: ' . ($multiDay ? 'JA' : 'NEIN');
		$MB .= PHP_EOL . ' is lastDay: ' . ($lastDay ? 'JA' : 'NEIN');
		if( $multiDay && !$lastDay ){
			$t_date = date($t_RZ_end);
			$t_time = mktime( 23, 59, 59, date('n', $t_RZ_end), date('j', $t_RZ_end), date('Y', $t_RZ_end) );
			$t_RZ_end = $t_time;
		}
		//
		$MB .= PHP_EOL . 't_RZ_start: ' . $t_RZ_start . ' | ' . date('Y-m-d H:i', $t_RZ_start);
		$MB .= PHP_EOL . 't_RZ_end:  ' . $t_RZ_end . ' | ' . date('Y-m-d H:i', $t_RZ_end);
		//
		$t_RZ_ges = ($t_RZ_end - $t_RZ_start);
		$MB .= PHP_EOL . 't_RZ_ges:           ' . $t_RZ_ges;
		$MB .= PHP_EOL . 't_RZ_ges (H):       ' . ($t_RZ_ges / 3600);
		//
		$t_RZ_privH = $abschnitt->getPrivatehours();
		$MB .= PHP_EOL . 't_RZ_privH (H):     ' . $t_RZ_privH;
		$t_privH_array = explode(':', $t_RZ_privH);
		$MB .= PHP_EOL . ' t_privH_array: ' . (int)$t_privH_array[0] . ' : ' . (int)$t_privH_array[1];
		$t_RZ_priv = (int)$t_privH_array[0]*3600 + (int)$t_privH_array[1]*60;
		$MB .= PHP_EOL . ' t_RZ_priv:        ' . $t_RZ_priv;
		//
		$t_RZ = $t_RZ_ges - $t_RZ_priv;
		//	umrechnen der sekunden in stunden mit kommastellen
		$t_RZ = $t_RZ < 0 ? 0.0 : ($t_RZ / 3600);
		$MB .= PHP_EOL . 't_RZ (H):           ' . $t_RZ;

		//	volle stunden für Tagsatzberechnung
		$t_RZ_ceil = ceil($t_RZ);
		$MB .= PHP_EOL . 't_RZ_ceil (H):      ' . $t_RZ_ceil;

		$MB .= PHP_EOL . PHP_EOL . 'Startland: ' . $abschnitt->getAbCountry();

		$t_RZ_ausland_0_time = 0;
		$t_RZ_ausland_0 = 0.0;
		if( $abschnitt->getAbCountry() != 'AT' && $abschnitt->getGuOrt() ){
			$t_RZ_ausland_0_time = $abschnitt->getGuDatetime() - $abschnitt->getAbDatetime();
			$t_RZ_ausland_0 = $t_RZ_ausland_0_time / 3600;
		}
		$MB .= PHP_EOL . ' RZ Ausland 0 ['.$abschnitt->getAbCountry().']:   ' . $t_RZ_ausland_0_time . ' | ' . date('Y-m-d H:i', $t_RZ_ausland_0) . ' | ' . $t_RZ_ausland_0;
		$t_RZ_ausland_1_time = 0;
		$t_RZ_ausland_1 = 0.0;
		if( $abschnitt->getGuOrt() && $abschnitt->getGuCountry()!=$abschnitt->getAbCountry() ){
			$t_RZ_ausland_1_time = ($abschnitt->getGu2Ort() ? $abschnitt->getGu2Datetime() : $abschnitt->getAnDateTime()) - $abschnitt->getGuDatetime();
			$t_RZ_ausland_1 = $t_RZ_ausland_1_time / 3600;
		}
		$MB .= PHP_EOL . ' RZ Ausland 1 ['.$abschnitt->getGuCountry().']:   ' . $t_RZ_ausland_1_time . ' | ' . date('Y-m-d H:i', $t_RZ_ausland_1) . ' | ' . $t_RZ_ausland_1;
		$t_RZ_ausland_2_time = 0;
		$t_RZ_ausland_2 = 0.0;
		if( $abschnitt->getGu2Ort() && $abschnitt->getGu2Country()!=$abschnitt->getAbCountry()  ){
			$t_RZ_ausland_2_time = ($abschnitt->getGu3Ort() ? $abschnitt->getGu3Datetime() : $abschnitt->getAnDateTime()) - $abschnitt->getGu2Datetime();
			$t_RZ_ausland_2 = $t_RZ_ausland_2_time / 3600;
		}
		$MB .= PHP_EOL . ' RZ Ausland 2 ['.$abschnitt->getGu2Country().']:   ' . $t_RZ_ausland_2_time . ' | ' . date('Y-m-d H:i', $t_RZ_ausland_2) . ' | ' . $t_RZ_ausland_2;
		$t_RZ_ausland_3_time = 0;
		$t_RZ_ausland_3 = 0.0;
		if( $abschnitt->getGu3Ort() && $abschnitt->getGu3Country()!=$abschnitt->getAbCountry()  ){
			$t_RZ_ausland_3_time = ($abschnitt->getGu4Ort() ? $abschnitt->getGu4Datetime() : $abschnitt->getAnDateTime()) - $abschnitt->getGu3Datetime();
			$t_RZ_ausland_3 = $t_RZ_ausland_3_time / 3600;
		}
		$MB .= PHP_EOL . ' RZ Ausland 3 ['.$abschnitt->getGu3Country().']:   ' . $t_RZ_ausland_3_time . ' | ' . date('Y-m-d H:i', $t_RZ_ausland_3) . ' | ' . $t_RZ_ausland_3;
		$t_RZ_ausland_4_time = 0;
		$t_RZ_ausland_4 = 0.0;
		if( $abschnitt->getGu4Ort() && $abschnitt->getGu4Country()!=$abschnitt->getAbCountry()  ){
			$t_RZ_ausland_4_time = $abschnitt->getAnDateTime() - $abschnitt->getGu4Datetime();
			$t_RZ_ausland_4 = $t_RZ_ausland_4_time / 3600;
		}
		$MB .= PHP_EOL . ' RZ Ausland 4 ['.$abschnitt->getGu4Country().']:   ' . $t_RZ_ausland_4_time . ' | ' . date('Y-m-d H:i', $t_RZ_ausland_4) . ' | ' . $t_RZ_ausland_4;
		//	RZ-GESAMT AUSLAND ?
		$t_RZ_ausland_time = $t_RZ_ausland_0_time + $t_RZ_ausland_1_time + $t_RZ_ausland_2_time + $t_RZ_ausland_3_time + $t_RZ_ausland_4_time;
		$t_RZ_ausland = $t_RZ_ausland_0 + $t_RZ_ausland_1 + $t_RZ_ausland_2 + $t_RZ_ausland_3 + $t_RZ_ausland_4;
		$MB .= PHP_EOL . ' RZ Ausland Ges: ' . $t_RZ_ausland_time . ' | ' . date('Y-m-d H:i', $t_RZ_ausland_time) . ' | ' . $t_RZ_ausland;

		if( $t_RZ_ausland <= 3.0 ){
			//	weniger als 3h im Ausland
			$MB .= PHP_EOL . ' -> weniger als 3h im Ausland! es gilt berechnung für AnCountry !!!';
			if( $t_RZ <= 3.0 ){
				//	wenn weniger als 3 Stunden Reisezeit, dann gibt es kein Taggeld
				//	von 00:01 - 10:00	->	02:01 - 03:00
				$t_TG = 0.0;
			} else if( $t_RZ >= 12 ) {
				//	ab der angebrochenen 12ten Stunde gibt es den max. Tagsatz!
				//	ab 11:01 - 12:00 	->	und alle folgestunen!
				$t_TG = $t_TS;
				if( $abschnitt->getAbCountry() == 'AT' ){
					$t_TG_AT_sum = $t_TG;
					$t_TG_AT_mwst = ($t_TG / 110) * 10;
				} elseif( $abschnitt->getAbCountry() == 'DE' ){
					$t_TG_DE_sum = $t_TG;
					$t_TG_DE_mwst = 0.0;
				} else {
					$t_TG_REST_sum = $t_TG;
					$t_TG_REST_mwst = 0.0;
				}
			} else {
				//	ab der 4ten angefangenen Stunde Reisezeit bis zur vollen 11ten Stunde gibts "Stunden * 12tel Tagsatz" 
				//	zw. 03:01 - 04:00	-> 10:01 - 11:00
				$t_TG = $t_TS / 12 * $t_RZ_ceil;
				if( $abschnitt->getAbCountry() == 'AT' ){
					$t_TG_AT_sum = $t_TG;
					$t_TG_AT_mwst = ($t_TG / 110) * 10;
				} elseif( $abschnitt->getAbCountry() == 'DE' ){
					$t_TG_DE_sum = $t_TG;
					$t_TG_DE_mwst = 0.0;
				} else {
					$t_TG_REST_sum = $t_TG;
					$t_TG_REST_mwst = 0.0;
				}
			}
		} else {
			//	IM Ausland !!!
			$MB .= PHP_EOL . PHP_EOL . ' Berechne Ausland:';

			if( $t_RZ_ceil >= 12 ){
				$t_RZ_ausland_rest = 12.0;
			} else {
				$t_RZ_ausland_rest = $t_RZ_ceil;
			}
			$MB .= PHP_EOL . PHP_EOL . 't_RZ_ausland_rest: ' . $t_RZ_ausland_rest;
			//	zw. >3h und < 12h im Ausland

			$t_TG_ausland_0 = 0.0;
			$t_TG_ausland_0_C = $abschnitt->getAbCountry();
			$t_TS_ausland_0 = $this->reiseRepository->getTagsatzLand( $abschnitt->getAbCountry() );
			if( $t_RZ_ausland_rest > 0 ){
				$t_RZ_ausland_0_ceil = ceil($t_RZ_ausland_0);
				$t_RZ_ausland_0_rest = $t_RZ_ausland_0_ceil > $t_RZ_ausland_rest ? $t_RZ_ausland_rest : $t_RZ_ausland_0_ceil;
				if( $t_RZ_ausland_0_rest >= 12.0 ){
					$t_TG_ausland_0 = $t_TS_ausland_0;
					$t_RZ_ausland_rest -= 12.0;
				} else if( $t_RZ_ausland_0_rest > 3.0 ){
					$t_TG_ausland_0 = $t_TS_ausland_0 / 12 * $t_RZ_ausland_0_rest;
					$t_RZ_ausland_rest -= $t_RZ_ausland_0_rest;
				}
			}
			$MB .= PHP_EOL . 't_TG_ausland_0: ' . $t_TG_ausland_0 . ' / TS['.$abschnitt->getAbCountry().']: ' . $t_TS_ausland_0;
			$MB .= PHP_EOL . 't_RZ_ausland_rest: ' . $t_RZ_ausland_rest;

			$t_TG_ausland_1 = 0.0;
			$t_TG_ausland_1_C = $abschnitt->getGuCountry();
			$t_TS_ausland_1 = $this->reiseRepository->getTagsatzLand( $abschnitt->getGuCountry() );
			if( $t_RZ_ausland_rest > 0 ){
				$t_RZ_ausland_1_ceil = ceil($t_RZ_ausland_1);
				$t_RZ_ausland_1_rest = $t_RZ_ausland_1_ceil > $t_RZ_ausland_rest ? $t_RZ_ausland_rest : $t_RZ_ausland_1_ceil;
				if( $t_RZ_ausland_1_rest >= 12.0 ){
					$t_TG_ausland_1 = $t_TS_ausland_1;
					$t_RZ_ausland_rest -= 12.0;
				} else if( $t_RZ_ausland_1_rest > 3.0 ){
					$t_TG_ausland_1 = $t_TS_ausland_1 / 12 * $t_RZ_ausland_1_rest;
					$t_RZ_ausland_rest -= $t_RZ_ausland_1_rest;
				}
			}
			$MB .= PHP_EOL . 't_TG_ausland_1: ' . $t_TG_ausland_1 . ' / TS['.$abschnitt->getGuCountry().']: ' . $t_TS_ausland_1;
			$MB .= PHP_EOL . 't_RZ_ausland_rest: ' . $t_RZ_ausland_rest;

			$t_TG_ausland_2 = 0.0;
			$t_TG_ausland_2_C = $abschnitt->getGu2Country();
			$t_TS_ausland_2 = $this->reiseRepository->getTagsatzLand( $abschnitt->getGu2Country() );
			if( $t_RZ_ausland_rest > 0 ){
				$t_RZ_ausland_2_ceil = ceil($t_RZ_ausland_2);
				$t_RZ_ausland_2_rest = $t_RZ_ausland_2_ceil > $t_RZ_ausland_rest ? $t_RZ_ausland_rest : $t_RZ_ausland_2_ceil;
				if( $t_RZ_ausland_2_rest >= 12.0 ){
					$t_TG_ausland_2 = $t_TS_ausland_2;
					$t_RZ_ausland_rest -= 12.0;
				} else if( $t_RZ_ausland_2_rest > 3.0 ){
					$t_TG_ausland_2 = $t_TS_ausland_2 / 12 * $t_RZ_ausland_2_rest;
					$t_RZ_ausland_rest -= $t_RZ_ausland_2_rest;
				}
			}
			$MB .= PHP_EOL . 't_TG_ausland_2: ' . $t_TG_ausland_2 . ' / TS['.$abschnitt->getGu2Country().']: ' . $t_TS_ausland_2;
			$MB .= PHP_EOL . 't_RZ_ausland_rest: ' . $t_RZ_ausland_rest;

			$t_TG_ausland_3 = 0.0;
			$t_TG_ausland_3_C = $abschnitt->getGu3Country();
			$t_TS_ausland_3 = $this->reiseRepository->getTagsatzLand( $abschnitt->getGu3Country() );
			if( $t_RZ_ausland_rest > 0 ){
				$t_RZ_ausland_3_ceil = ceil($t_RZ_ausland_3);
				$t_RZ_ausland_3_rest = $t_RZ_ausland_3_ceil > $t_RZ_ausland_rest ? $t_RZ_ausland_rest : $t_RZ_ausland_3_ceil;
				if( $t_RZ_ausland_3 >= 12.0 ){
					$t_TG_ausland_3 = $t_TS_ausland_3;
					$t_RZ_ausland_rest -= 12.0;
				} else if( $t_RZ_ausland_3 > 3.0 ){
					$t_TG_ausland_3 = $t_TS_ausland_3 / 12 * $t_RZ_ausland_3_rest;
					$t_RZ_ausland_rest -= $t_RZ_ausland_3_rest;
				}
			}
			$MB .= PHP_EOL . 't_TG_ausland_3: ' . $t_TG_ausland_3 . ' / TS['.$abschnitt->getGu3Country().']: ' . $t_TS_ausland_3;
			$MB .= PHP_EOL . 't_RZ_ausland_rest: ' . $t_RZ_ausland_rest;

			$t_TG_ausland_4 = 0.0;
			$t_TG_ausland_4_C = $abschnitt->getGu4Country();
			$t_TS_ausland_4 = $this->reiseRepository->getTagsatzLand( $abschnitt->getGu4Country() );
			if( $t_RG_ausland_rest > 0 ){
				$t_RZ_ausland_4_ceil = ceil($t_RZ_ausland_4);
				$t_RZ_ausland_4_rest = $t_RZ_ausland_4_ceil > $t_RZ_ausland_rest ? $t_RZ_ausland_rest : $t_RZ_ausland_4_ceil;
				if( $t_RZ_ausland_4 >= 12.0 ){
					$t_TG_ausland_4 = $t_TS_ausland_4;
					$t_RZ_ausland_rest -= 12.0;
				} else if( $t_RZ_ausland_4 > 3.0 ){
					$t_TG_ausland_4 = $t_TS_ausland_4 / 12 * $t_RZ_ausland_4_ceil;
					$t_RZ_ausland_rest -= $t_RZ_ausland_4_ceil;
				}
			}
			$MB .= PHP_EOL . 't_TG_ausland_4: ' . $t_TG_ausland_4 . ' / TS['.$abschnitt->getGu4Country().']: ' . $t_TS_ausland_4;
			$MB .= PHP_EOL . 't_RZ_ausland_rest: ' . $t_RZ_ausland_rest;
			//
			$t_TG_ausland = $t_TG_ausland_0 + $t_TG_ausland_1 + $t_TG_ausland_2 + $t_TG_ausland_3 + $t_TG_ausland_4;
			$MB .= PHP_EOL . ' TG_ausland: ' . $t_TG_ausland;
			//
			if( $t_TG_ausland_0 > 0 ){
				$t_TG_AT_sum += $t_TG_ausland_0_C == 'AT' ? $t_TG_ausland_0 : 0.0;
				$t_TG_AT_mwst += $t_TG_ausland_0_C == 'AT' ? (($t_TG_ausland_0/110)*10) : 0.0;
				$t_TG_DE_sum += $t_TG_ausland_0_C == 'DE' ? $t_TG_ausland_0 : 0.0;
				$t_TG_REST_sum += ($t_TG_ausland_0_C != 'AT' && $t_TG_ausland_0_C != 'DE') ? $t_TG_ausland_0 : 0.0;
			}
			//
			if( $t_TG_ausland_1 > 0 ){
				$t_TG_AT_sum += $t_TG_ausland_1_C == 'AT' ? $t_TG_ausland_1 : 0.0;
				$t_TG_AT_mwst += $t_TG_ausland_1_C == 'AT' ? (($t_TG_ausland_1/110)*10) : 0.0;
				$t_TG_DE_sum += $t_TG_ausland_1_C == 'DE' ? $t_TG_ausland_1 : 0.0;
				$t_TG_REST_sum += ($t_TG_ausland_1_C != 'AT' && $t_TG_ausland_1_C != 'DE') ? $t_TG_ausland_1 : 0.0;
			}
			//
			if( $t_TG_ausland_2 > 0 ){
				$t_TG_AT_sum += $t_TG_ausland_2_C == 'AT' ? $t_TG_ausland_2 : 0.0;
				$t_TG_AT_mwst += $t_TG_ausland_2_C == 'AT' ? (($t_TG_ausland_2/110)*10) : 0.0;
				$t_TG_DE_sum += $t_TG_ausland_2_C == 'DE' ? $t_TG_ausland_2 : 0.0;
				$t_TG_REST_sum += ($t_TG_ausland_2_C != 'AT' && $t_TG_ausland_2_C != 'DE') ? $t_TG_ausland_2 : 0.0;
			}
			//
			if( $t_TG_ausland_3 > 0 ){
				$t_TG_AT_sum += $t_TG_ausland_3_C == 'AT' ? $t_TG_ausland_3 : 0.0;
				$t_TG_AT_mwst += $t_TG_ausland_3_C == 'AT' ? (($t_TG_ausland_3/110)*10) : 0.0;
				$t_TG_DE_sum += $t_TG_ausland_3_C == 'DE' ? $t_TG_ausland_3 : 0.0;
				$t_TG_REST_sum += ($t_TG_ausland_3_C != 'AT' && $t_TG_ausland_3_C != 'DE') ? $t_TG_ausland_3 : 0.0;
			}
			//
			if( $t_TG_ausland_4 > 0 ){
				$t_TG_AT_sum += $t_TG_ausland_4_C == 'AT' ? $t_TG_ausland_4 : 0.0;
				$t_TG_AT_mwst += $t_TG_ausland_4_C == 'AT' ? (($t_TG_ausland_4/110)*10) : 0.0;
				$t_TG_DE_sum += $t_TG_ausland_4_C == 'DE' ? $t_TG_ausland_4 : 0.0;
				$t_TG_REST_sum += ($t_TG_ausland_4_C != 'AT' && $t_TG_ausland_4_C != 'DE') ? $t_TG_ausland_4 : 0.0;
			}

			//

			$MB .= PHP_EOL . PHP_EOL . 't_RZ_ausland_rest: ' . $t_RZ_ausland_rest;
			$MB .= PHP_EOL . ' is noch was zum auffüllen frei? ' . ($t_RZ_ausland_rest > 0 ? 'JA' : 'NEIN');
			$t_TG_inland = 0;
			if( $t_RZ_ausland_rest > 0 ){
				$MB .= PHP_EOL . PHP_EOL . ' FÜLLE AUF !!! ';
				$t_RZ_gesamt_rest = $t_RZ_ausland_rest < $t_RZ_ceil ? $t_RZ_ausland_rest : $t_RZ_ceil;
				$MB .= PHP_EOL . 't_RZ_gesamt_rest: ' . $t_RZ_gesamt_rest;
//				$t_TG_inland = $TS['AT'] / 12 * $t_RZ_gesamt_rest;
				$t_TG_inland = $this->reiseRepository->getTagsatzLand( 'AT' ) / 12 * $t_RZ_gesamt_rest;
				$t_TG_AT_sum += $t_TG_inland;
				$t_TG_AT_mwst += ($t_TG_inland/110)*10;
			}
			$MB .= PHP_EOL . ' TG_inland: ' . $t_TG_inland;

			//
			$t_TG = $t_TG_ausland + $t_TG_inland;
			$MB .= PHP_EOL . ' t_TG (ausland + inland): ' . $t_TG;

		}
		
		$MB .= PHP_EOL . PHP_EOL . 't_TG:  ' . $t_TG;
		
		//	VERKÜRZUNG MITTAG / ABEND ?
		$t_V_M = 0.0;
		$t_V_A = 0.0;
		$MB .= PHP_EOL . PHP_EOL . 'Mittag:  ' . ($abschnitt->getMittag() ? 'JA' : 'NEIN');
		if( $abschnitt->getMittag() ){
			//	VERKÜRZUNG MITTAG = 50% vom TAGSATZ (generell immer / egal welches Land !!!)
			$t_V_M = $t_TG * 0.5;	
		}
		$MB .= PHP_EOL . 't_V_M:  ' . $t_V_M;
		//
		$MB .= PHP_EOL . PHP_EOL . 'Abend:  ' . ($abschnitt->getAbend() ? 'JA' : 'NEIN');
		if( $abschnitt->getAbend() ){
			//	VERKÜRZUNG ABEND = 50% vom TAGSATZ (generell immer / egal welches Land !!!)
			$t_V_A = $t_TG * 0.5;	
		}
		$MB .= PHP_EOL . 't_V_A:  ' . $t_V_A;
		//	VERKÜRZUNG GESAMT = MITTAG + ABEND, max. jedoch aktuelles Taggeld !!! (ergibt damit später in summe 0 Taggeld!!!)
		$t_V = $t_V_M + $t_V_A;
		$MB .= PHP_EOL . PHP_EOL . 't_V:  ' . $t_V;
		if( $t_V > 0 ){
			$MB .= PHP_EOL . 't_V > 0:  ';
			if( $t_V > $t_TG ){
				$MB .= PHP_EOL . 't_V > t_TG:  ';
				$t_V = $t_TG;
			}
		}
		$MB .= PHP_EOL . 't_V:  ' . $t_V;
		//
		if( $t_V_M > 0 && $t_V_A > 0 ){
			$t_TG_AT_sum = 0.0;
			$t_TG_AT_mwst = 0.0;
			$t_TG_DE_sum = 0.0;
			$t_TG_DE_mwst = 0.0;
			$t_TG_REST_sum = 0.0;
			$t_TG_REST_mwst = 0.0;
		} elseif( $t_V_M > 0 || $t_V_A > 0 ){
			$t_TG_AT_sum *= 0.5;
			$t_TG_AT_mwst *= 0.5;
			$t_TG_DE_sum *= 0.5;
			$t_TG_DE_mwst *= 0.5;
			$t_TG_REST_sum *= 0.5;
			$t_TG_REST_mwst *= 0.5;
		}
		
		//	NÄCHTIGUNG ?
		$t_naechtigungen = $this->reiseRepository->getOptionsNaechtigung();
		$t_NG = 0.0;
		$MB .= PHP_EOL;
		if( $multiDay && !$lastDay ){
			$MB .= PHP_EOL . 'Nächtigung: ' . $t_naechtigungen[$abschnitt->getNacht()];
			if( $abschnitt->getNacht() == 2 || $abschnitt->getNacht() == '2' ){
//				$t_NG = $t_NS;
//				$t_NG = $NS[ $abschnitt->getAnCountry() ];
				$t_NG = $this->reiseRepository->getNachtsatzLand( $abschnitt->getAnCountry() );
				$MB .= PHP_EOL . 't_NG['.$abschnitt->getAnCountry().']: ' . $t_NG;
				//
				if( $abschnitt->getAnCountry() == 'AT' ){
					$t_NG_AT_sum = $t_NG;
					$t_NG_AT_mwst = 1.65;
				} elseif( $abschnitt->getAnCountry() == 'DE' ){
					$t_NG_DE_sum = $t_NG;
					$t_NG_AT_mwst = 0.0;
				} else {
					$t_NG_REST_sum = $t_NG;
					$t_NG_AT_mwst = 0.0;
				}
			}
		} else {
			$MB .= PHP_EOL . 'Nächtigung: ' . 'KEINE, weil eintägig !';
		}
		$MB .= PHP_EOL . PHP_EOL . 't_NG: ' . $t_NG;
		//
		$MB .= PHP_EOL . PHP_EOL;
		$MB .= '  TG AT sum:     ' . $t_TG_AT_sum . PHP_EOL;
		$MB .= '  TG AT mwst:    ' . $t_TG_AT_mwst . PHP_EOL;
		$MB .= '  TG DE sum:     ' . $t_TG_DE_sum . PHP_EOL;
		$MB .= '  TG DE mwst:    ' . $t_TG_DE_mwst . PHP_EOL;
		$MB .= '  TG REST sum:   ' . $t_TG_REST_sum . PHP_EOL;
		$MB .= '  TG REST mwst:  ' . $t_TG_REST_mwst . PHP_EOL;
		$MB .= PHP_EOL;
		$MB .= '  NG AT sum:     ' . $t_NG_AT_sum . PHP_EOL;
		$MB .= '  NG AT mwst:    ' . $t_NG_AT_mwst . PHP_EOL;
		$MB .= '  NG DE sum:     ' . $t_NG_DE_sum . PHP_EOL;
		$MB .= '  NG DE mwst:    ' . $t_NG_DE_mwst . PHP_EOL;
		$MB .= '  NG REST sum:   ' . $t_NG_REST_sum . PHP_EOL;
		$MB .= '  NG REST mwst:  ' . $t_NG_REST_mwst . PHP_EOL;

		//	GESAMT TAGGELD: TG durch Reisezeit minus Verkürzung durch Mittag/Abend plus Nächtigung
		$t = $t_TG - $t_V + $t_NG;
		$MB .= PHP_EOL . PHP_EOL . ' TG des Tages: ' . $t;
		//
		$t_TG_array = array('AT_sum' => $t_TG_AT_sum, 'AT_mwst' => $t_TG_AT_mwst, 'DE_sum' => $t_TG_DE_sum, 'DE_mwst' => $t_TG_DE_mwst, 'REST_sum' => $t_TG_REST_sum, 'REST_mwst' => $t_TG_REST_mwst);
		$t_NG_array = array('AT_sum' => $t_NG_AT_sum, 'AT_mwst' => $t_NG_AT_mwst, 'DE_sum' => $t_NG_DE_sum, 'DE_mwst' => $t_NG_DE_mwst, 'REST_sum' => $t_NG_REST_sum, 'REST_mwst' => $t_NG_REST_mwst);
		//
		return array($t, $t_TG_array, $t_NG_array, $MB);
	}


	/*		LOGIN	----------------------------------------------------		*/


	/*
	 * action login
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Login $logindata
	 * @dontvalidate $logindata
	 * @return void
	 */
	public function loginAction (\NEXT\IconReiseabrechnung\Domain\Model\Login $logindata = NULL) {
		//
		if( $logindata==NULL ){
			$logindata = $this->objectManager->getEmptyObject('\NEXT\IconReiseabrechnung\Domain\Model\Login');
		}
		//
		$user = $this->userRepository->getByPersonnr( $logindata->getLogin(), $this->userMode );
		if( $user ){
//			$this->redirect('edit');
			if( $user->getPersonnr() === $logindata->getLogin() ){
				$this->loginRepository->writeSessionLogin($this->userMode, $user);
				//
				$this->redirect('index');
			}
		}

		//
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => NULL,
		));	
	}

	/*
	 * logout
	 *
	 */
	public function logoutAction () {
		//
		$this->loginRepository->clearSessionLogin( $this->userMode );
		//
		$this->redirect('login');
	}

	/*
	 * checkIsLoggedIn
	 *
	 * @return void
	 */
	public function checkIsLoggedIn () {
		//
		$isLoggedIn = $this->loginRepository->isLoggedIn( $this->userMode );
		//
		if( !$isLoggedIn ){
			$this->redirect('login');
		}
	}
	
	/**
	 * getLoggedInUser
	 * 
	 * @return mixed
	 */
	public function getLoggedInUser () {
		//
		return $this->loginRepository->getLoggedInUser( $this->userMode );
	}
	
	/**
	 * getPaginateSettings
	 *
	 * @return array
	 */
	public function getPaginateSettings () {
		//
		$paginate = array();
		$paginate['addQueryStringMethod'] = $this->settings['paginate']['addQueryStringMethod'];
		$paginate['maximumNumberOfLinks'] = $this->settings['paginate']['maximumNumberOfLinks'];
		$paginate['itemsPerPage'] = $this->settings['paginate']['itemsPerPage'];
		$paginate['insertAbove'] = $this->settings['paginate']['insertAbove'];
		$paginate['insertBelow'] = $this->settings['paginate']['insertBelow'];
		return $paginate;
	}

}

