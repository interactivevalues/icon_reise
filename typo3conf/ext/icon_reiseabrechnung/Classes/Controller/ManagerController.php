<?php
namespace NEXT\IconReiseabrechnung\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\Common\Util\Debug;
use NEXT\IconReiseabrechnung\Domain\Model\User;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ManagerController extends \NEXT\IconReiseabrechnung\Controller\BaseController {


	/**
	 * seminarRepository
	 *
	 * @var \NEXT\IconReiseabrechnung\Domain\Repository\SeminarRepository
	 * @inject
	 */
	protected $seminarRepository;
	

	/**
	 * userMode
	 *
	 * @var string
	 */
	protected $userMode = 'MANAGER';


	/**
	 * action index
	 *
	 * @return void
	 */
	public function indexAction() {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		//
		$paginate = $this->getPaginateSettings();
		//
		$reisen = $this->reiseRepository->findForManager( array('SEK','GL'), $order='DESC' );
		//
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'action' => 'index',
			'paginate' => $paginate,
			'reisen' => $reisen,
		));
	}

    /**
     * action deleted
     *
     * @return void
     */
    public function deletedAction() {
        //	1. CHECK IF USER IS LOGGED IN ?
        $this->checkIsLoggedIn();
        //	2. GET USERDATA FROM SESSION !
        $loggedInUser = $this->getLoggedInUser();

        //
        $paginate = $this->getPaginateSettings();
        //
        $reisen = $this->reiseRepository->findForManagerDeleted( array('SEK','GL','GELÖSCHT'), $order='DESC' );
        //
        $this->view->assignMultiple(array(
            'userMode' => $this->userMode,
            'loggedInUser' => $loggedInUser,
            'action' => 'deleted',
            'paginate' => $paginate,
            'reisen' => $reisen,
        ));
    }
	

	/**
	 * action abrechnen
	 *
	 * @return void
	 */
	public function abrechnenAction() {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		//
		$paginate = $this->getPaginateSettings();
		//
		$reisen = $this->reiseRepository->findForManager( array('ABRECHNEN'), $order='DESC' );
		//
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'action' => 'abrechnen',
			'paginate' => $paginate,
			'reisen' => $reisen,
		));
	}


	/**
	 * action abrechnenBH
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Abrechnen $abrechnen
	 * @return void
   	 */
	public function abrechnenBHAction (\NEXT\IconReiseabrechnung\Domain\Model\Abrechnen $abrechnen = NULL) {
		//
		$file_header = '"Pers.Nr.";"Name";"ReiseNr.";"Reiseziel";"Reisegrund";"Klient / Projekt";"Art Beleg";"Tag Start";"Brutto";"Netto";"abziehbare USt in %";"Land Beleg";"MwSt AT";"MwSt DE";"MwSt Rest-Ausland";"Konto"'.PHP_EOL;
		$file = '';
		//
		if( !is_null($abrechnen) ){
			//
			$ids = $abrechnen->getIds();
			$ids = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', $ids);
			//	DO WE HAVE A ARRAY / MORE THEN 0 ?
			if( count($ids) > 0 ){
				//	FETCH ALL ENTRIES BY IDS
				$reisen = $this->reiseRepository->findByUid_BH($ids);
				if( $reisen ){
					//	REISENR / PERSONNR from FIRST ENTRY
					$t_last_reisenr = $reisen[0]->getReisenr();
					$t_last_personnr = $reisen[0]->getPersonnr();
					//
					foreach( $reisen as $reise ){
						//
						$reise->prepareVerkehrsmittel();
						//	GET FIRST ABSCHNITT FOR DATE
						$t_abschnitte = $reise->getAbschnitte()->toArray();
						$t_first = reset($t_abschnitte);
						//
						//	GET	KONTO by REISEGRUND
						$t_konto = $this->reiseRepository->getKontoReisegrund( $reise->getGrund() );
						//
						$t_personnr = $reise->getPersonnr();
						$t_reisenr = $reise->getReisenr();
						$t_name = $reise->getFirstname() . ' ' . $reise->getLastname();
						$t_ziel = $reise->getZiel();
						//	GET REISEGRUND
						$t_grund = $this->reiseRepository->getReisegrund( $reise->getGrund() );
						//	KLIENTENNR. + PROJEKTNR.
						$t_klientprojekt = $reise->getKlientennr() . ' / ' . $reise->getProjektnr();
						//	START TAG
						$t_starttag = strftime('%d.%m.%Y', $t_first->getAbDateTime() );

						//	BUILD FIELDS OF CSV
						$t_line = '';

						//	CSV:	TAGGELD
						$t_taggeld = $reise->getSumTaggeld();
						$t_taggeld_AT = $t_taggeld['AT'];
						$t_taggeld_DE = $t_taggeld['DE'];
						$t_taggeld_REST = $t_taggeld['REST'];
						//	CSV:	TAGGELD AT
						if( $t_taggeld_AT['sum'] > 0 ){
							//
							$t_type = 'Taggeld (AT)';
							$t_brutto = $t_taggeld_AT['sum'];
							$t_mwst = $t_taggeld_AT['mwst'];
							$t_netto = $t_brutto - $t_mwst;
							$t_taxfee = '10';
							$t_country = 'AT';
							//
							$t_line .= $this->reiseRepository->buildCSVLine_BH($t_personnr, $t_name, $t_reisenr, $t_ziel, $t_grund, $t_klientprojekt, $t_type, $t_starttag, $t_brutto, $t_netto, $t_taxfee, $t_country, $t_mwst, $t_konto);
						}
						//	CSV:	TAGGELD DE
						if( $t_taggeld_DE['sum'] > 0 ){
							//
							$t_type = 'Taggeld (DE)';
							$t_brutto = $t_taggeld_DE['sum'];
							$t_mwst = $t_taggeld_DE['mwst'];
							$t_netto = $t_brutto - $t_mwst;
							$t_taxfee = '0';
							$t_country = 'DE';
							//
							$t_line .= $this->reiseRepository->buildCSVLine_BH($t_personnr, $t_name, $t_reisenr, $t_ziel, $t_grund, $t_klientprojekt, $t_type, $t_starttag, $t_brutto, $t_netto, $t_taxfee, $t_country, $t_mwst, $t_konto);
						}
						//	CSV:	TAGGELD REST
						if( $t_taggeld_REST['sum'] > 0 ){
							//
							$t_type = 'Taggeld (Rest-Ausland)';
							$t_brutto = $t_taggeld_REST['sum'];
							$t_mwst = $t_taggeld_REST['mwst'];
							$t_netto = $t_brutto - $t_mwst;
							$t_taxfee = '0';
							$t_country = 'Rest-Ausland';
							//
							$t_line .= $this->reiseRepository->buildCSVLine_BH($t_personnr, $t_name, $t_reisenr, $t_ziel, $t_grund, $t_klientprojekt, $t_type, $t_starttag, $t_brutto, $t_netto, $t_taxfee, $t_country, $t_mwst, $t_konto);
						}

						//	CSV:	NACHTGELD
						$t_nachtgeld = $reise->getSumNachtgeld();
						$t_nachtgeld_AT = $t_nachtgeld['AT'];
						$t_nachtgeld_DE = $t_nachtgeld['DE'];
						$t_nachtgeld_REST = $t_nachtgeld['REST'];
						//	CSV:	NACHTGELD AT
						if( $t_nachtgeld_AT['sum'] > 0 ){
							//
							$t_type = 'Nachtgeld (AT)';
							$t_brutto = $t_nachtgeld_AT['sum'];
							$t_mwst = $t_nachtgeld_AT['mwst'];
							$t_netto = $t_brutto - $t_mwst;
							$t_taxfee = '10u13';
							$t_country = 'AT';
							//
							$t_line .= $this->reiseRepository->buildCSVLine_BH($t_personnr, $t_name, $t_reisenr, $t_ziel, $t_grund, $t_klientprojekt, $t_type, $t_starttag, $t_brutto, $t_netto, $t_taxfee, $t_country, $t_mwst, $t_konto);
						}
						//	CSV:	NACHTGELD DE
						if( $t_nachtgeld_DE['sum'] > 0 ){
							//
							$t_type = 'Nachtgeld (DE)';
							$t_brutto = $t_nachtgeld_DE['sum'];
							$t_mwst = $t_nachtgeld_DE['mwst'];
							$t_netto = $t_brutto - $t_mwst;
							$t_taxfee = '0';
							$t_country = 'DE';
							//
							$t_line .= $this->reiseRepository->buildCSVLine_BH($t_personnr, $t_name, $t_reisenr, $t_ziel, $t_grund, $t_klientprojekt, $t_type, $t_starttag, $t_brutto, $t_netto, $t_taxfee, $t_country, $t_mwst, $t_konto);
						}
						//	CSV:	NACHTGELD REST
						if( $t_nachtgeld_REST['sum'] > 0 ){
							//
							$t_type = 'Nachtgeld (Rest-Ausland)';
							$t_brutto = $t_nachtgeld_REST['sum'];
							$t_mwst = $t_nachtgeld_REST['mwst'];
							$t_netto = $t_brutto - $t_mwst;
							$t_taxfee = '0';
							$t_country = 'Rest-Ausland';
							//
							$t_line .= $this->reiseRepository->buildCSVLine_BH($t_personnr, $t_name, $t_reisenr, $t_ziel, $t_grund, $t_klientprojekt, $t_type, $t_starttag, $t_brutto, $t_netto, $t_taxfee, $t_country, $t_mwst, $t_konto);
						}

						//	CSV:	KILOMETERGELD
						if( $reise->getKmgefahren() > 0 && $reise->getVkmPkwPrivat() ){
							//
							$t_kmgeld = $reise->getSumKmgeld();
							$t_kmgeld_AT = $t_kmgeld['AT'];
							if( $t_kmgeld_AT == '' ){
								$t_kmgeld_AT = 0.0;
							}
							//
							$t_type = 'Kilometergeld';	// . ' KM: ' . $reise->getKmgefahren() . ($reise->getVkmPkwDienst() ? ' | PKW-Dienst ' : '') . ($reise->getVkmPkwPrivat()  ? ' | PKW-Privat ' : '' );
							$t_brutto = $t_kmgeld_AT;
							$t_netto = $t_brutto;
							$t_taxfee = '0';
							$t_mwst = 0.0;
							$t_country = 'AT';
							//
							$t_line .= $this->reiseRepository->buildCSVLine_BH($t_personnr, $t_name, $t_reisenr, $t_ziel, $t_grund, $t_klientprojekt, $t_type, $t_starttag, $t_brutto, $t_netto, $t_taxfee, $t_country, $t_mwst, $t_konto);
						}

						//
						//	CSV:	ALLE BELEGE
						foreach( $reise->getBelege() as $beleg ){
							//
							if( $beleg->getMitarbeiter() ){
								$t_type = $this->reiseRepository->getBelegType( $beleg->getType() );
								$t_brutto = $beleg->getSumEuroBrutto();
								$t_netto = $beleg->getSumEuroNetto();
								$t_taxfee = $beleg->getTaxfee();
								$t_mwst = $t_brutto - $t_netto;
								$t_country = $beleg->getCountry();
								//
								$t_line .= $this->reiseRepository->buildCSVLine_BH($t_personnr, $t_name, $t_reisenr, $t_ziel, $t_grund, $t_klientprojekt, $t_type, $t_starttag, $t_brutto, $t_netto, $t_taxfee, $t_country, $t_mwst, $t_konto);
							}
						}
						//
						if( $t_line ){
							$file .= $t_line . PHP_EOL;
						}
/*
						//	WHEN NEW PERSONNR, INSERT EMPTY LINE
						if( $reise->getReisenr() != $t_last_reisenr ){
							$t_last_reisenr = $reise->getReisenr();
							$file .= PHP_EOL;
						}
*/
/*
						//	WHEN NEW PERSONNR, INSERT EMPTY LINE
						if( $reise->getPersonnr() != $t_last_personnr ){
							$t_last_personnr = $reise->getPersonnr();
							$file .= PHP_EOL;
						}
*/
						//
						if( $abrechnen->getDotest() != 1 ){
							//	MARK AS EXPORTED
							$reise->setExportBh(1);
							//	CHECK IF REISE IS "ERLEDIGT"
							$updateStatus = $this->checkReiseStatus($reise);
							$reise->setStatus( $updateStatus );
							//	UPDATE
							$this->reiseRepository->update($reise);
						}
					}
				}
				//	COMMIT TO DB !!!
				$this->persistenceManager->persistAll();
			}
		}
		//
		if( $file ){
			//
			$file = $file_header . $file;
			//
			$file = utf8_decode($file);
			//
			$fileName = 'reiseabrechnung_export-BH_'.strftime('%Y%m%dT%H%M%S', time()).'.csv';
			$fileLen = strlen($file);
//			$cType = 'application/force-download';
			$cType = 'text/csv';
			//
			$headers = array(
				'Pragma'                    => 'public', 
				'Expires'                   => 0, 
/*
				'Cache-Control'             => 'public',
*/
				'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
/*
				'Content-Description'       => 'File Transfer',
*/
				'Content-Type'              => $cType,
				'Content-Disposition'       => 'attachment; filename="'. $fileName .'"',
/*
				'Content-Transfer-Encoding' => 'binary', 
*/
				'Content-Length'            => $fileLen         
			);
			//
			foreach($headers as $header => $data) {
				$this->response->setHeader($header, $data);
			}
			//
			$this->response->sendHeaders();
			echo $file;
			exit;
			
		} else {
			//	-> show Abrechnen-Screen
			$actionName = 'abrechnen';
			$controllerName = NULL;
			$extensionName = NULL;
			$arguments = NULL;
			//
			$this->redirect($actionName, $controllerName, $extensioName, $arguments);
		}
	}
	

	/**
	 * action abrechnenLV
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Abrechnen $abrechnen
	 * @return void
   	 */
	public function abrechnenLVAction (\NEXT\IconReiseabrechnung\Domain\Model\Abrechnen $abrechnen = NULL) {
		//
		$file_header = '"Pers.Nr.";"Name";"ReiseNr.";"Reiseziel";"Tag Start";"LSt-frei";"LSt-pflichtig";"Gesamt/Auszahlung"'.PHP_EOL;
		$file = '';
		//
		if( !is_null($abrechnen) ){
			//
			$ids = $abrechnen->getIds();
			$ids = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', $ids);
			//	DO WE HAVE A ARRAY / MORE THEN 0 ?
			if( count($ids) > 0 ){
				//	FETCH ALL ENTRIES BY IDS
				$reisen = $this->reiseRepository->findByUid_LV($ids);
				if( $reisen ){
					//	PERSONNR from FIRST ENTRY
					$t_last_personnr = $reisen[0]->getPersonnr();
					foreach( $reisen as $reise ){
						//	GET FIRST ABSCHNITT FOR DATE
						$t_abschnitte = $reise->getAbschnitte()->toArray();
						$t_first = reset($t_abschnitte);
						//	BUILD FIELDS OF CSV
						$t_array = array();
						$t_array[] = $reise->getPersonnr();
						$t_array[] = $reise->getFirstname() . ' ' . $reise->getLastname();
						$t_array[] = $reise->getReisenr();
						$t_array[] = $reise->getZiel();
						$t_array[] = strftime('%d.%m.%Y', $t_first->getAbDateTime() );
						$t_array[] = number_format($reise->getSumTaxfree(), 2, ',', '' );
						$t_array[] = number_format($reise->getSumTax(), 2, ',', '' );
						$t_array[] = number_format($reise->getSumTotal(), 2, ',', '' );
						//	WHEN NEW PERSONNR, INSERT EMPTY LINE
						if( $reise->getPersonnr() != $t_last_personnr ){
							$t_last_personnr = $reise->getPersonnr();
							$file .= PHP_EOL;
						}
						//	ADD LINE WITH DATA
						$file .= '"' . implode('";"',$t_array) . '"' . PHP_EOL;
						//
						if( $abrechnen->getDotest() != 1 ){
							//	MARK AS EXPORTED
							$reise->setExportLv(1);
							//	CHECK IF REISE IS "ERLEDIGT"
							$updateStatus = $this->checkReiseStatus($reise);
							$reise->setStatus( $updateStatus );
							//	UPDATE
							$this->reiseRepository->update($reise);
						}
					}
				}
				//	COMMIT TO DB !!!
				$this->persistenceManager->persistAll();
			}
		}
		//
		if( $file ){
			//
			$file = $file_header . $file;
			//
			$file = utf8_decode($file);
			//
			$fileName = 'reiseabrechnung_export-LV_'.strftime('%Y%m%dT%H%M%S', time()).'.csv';
			$fileLen = strlen($file);
//			$cType = 'application/force-download';
			$cType = 'text/csv';
			//
			$headers = array(
				'Pragma'                    => 'public', 
				'Expires'                   => 0, 
/*
				'Cache-Control'             => 'public',
*/
				'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
/*
				'Content-Description'       => 'File Transfer',
*/
				'Content-Type'              => $cType,
				'Content-Disposition'       => 'attachment; filename="'. $fileName .'"',
/*
				'Content-Transfer-Encoding' => 'binary', 
*/
				'Content-Length'            => $fileLen         
			);
			//
			foreach($headers as $header => $data) {
				$this->response->setHeader($header, $data);
			}
			//
			$this->response->sendHeaders();
			echo $file;
			exit;
			
		} else {
			//	-> show Abrechnen-Screen
			$actionName = 'abrechnen';
			$controllerName = NULL;
			$extensionName = NULL;
			$arguments = NULL;
			//
			$this->redirect($actionName, $controllerName, $extensioName, $arguments);
		}
	}
	

	/**
	 * action abrechnenWV
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Abrechnen $abrechnen
	 * @return void
   	 */
	public function abrechnenWVAction (\NEXT\IconReiseabrechnung\Domain\Model\Abrechnen $abrechnen = NULL) {
		//
		if ( !is_null($abrechnen) ) {
			//
			$ids = $abrechnen->getIds();
			$ids = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', $ids);
			//
			foreach( $ids as $id ){
				//
				$reise = $this->reiseRepository->findByUid($id);
				if( $reise ){
					$reise->setExportWv(1);
					//	CHECK IF REISE IS "ERLEDIGT"
					$updateStatus = $this->checkReiseStatus($reise);
					$reise->setStatus( $updateStatus );
					//
					$this->reiseRepository->update($reise);
/*
					//	-> show Edit-Screen
					$actionName = 'view';
					$controllerName = NULL;
					$extensionName = NULL;
					$arguments = array(
						'reise' => $reise->getUid(),
					);
					//
					$this->redirect($actionName, $controllerName, $extensioName, $arguments);
*/					
				}
			}
			//
			$this->persistenceManager->persistAll();	
		}
		//	-> show Edit-Screen
		$actionName = 'abrechnen';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = NULL;
		//
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
	}
	

	/**
	 * checkReiseStatus
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return string
	 */
	public function checkReiseStatus(\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise) {
		//
		if( $reise->getVerrechenbar() ){
			$t = $reise->getExportBh() && $reise->getExportLv() && $reise->getExportWv();
		} else {
			$t = $reise->getExportBh() && $reise->getExportLv();
		}
		//
		return $t ? 'ERLEDIGT' : $reise->getStatus();
	}

	/**
	 * action erledigt
	 *
	 * @return void
	 */
	public function erledigtAction() {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();
        $personNr = GeneralUtility::_POST('mitarbeiter') ? GeneralUtility::_POST('mitarbeiter') : GeneralUtility::_GET('mitarbeiter');
        $users = $this->userRepository->findAllUsers( );

		//
		$paginate = $this->getPaginateSettings();
		//
        if ($personNr) {
            $reisen = $this->reiseRepository->findForManager( array('ERLEDIGT'), $order='DESC', $personNr );
        }
        else{
            $reisen = $this->reiseRepository->findForManager( array('ERLEDIGT'), $order='DESC' );
        }

		//
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'action' => 'erledigt',
			'paginate' => $paginate,
			'reisen' => $reisen,
            'users' => $users,
            'personNr' => $personNr
		));
	}
	

	/**
	 * action erstattung
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Erstattung $erstattung
	 * @return void
	 */
	public function erstattungAction(\NEXT\IconReiseabrechnung\Domain\Model\Erstattung $erstattung = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

        $users = $this->userRepository->findAllUsers( );

		//
		$today = date('d.m.Y');
		$results = false;
		$reisen = NULL;
		$seminare = NULL;
		//
		if( !is_null($erstattung) ){
			$personnr = trim($erstattung->getPersonnr());
			if( $personnr != '' ){
//				$reisen = $this->reiseRepository->findForManager( array('ERLEDIGT'), $order='DESC', $personnr=$personnr );
				$reisen = $this->reiseRepository->findForManagerRueckerstattung( $personnr=$personnr, $grund=4 );
				$seminare = $this->seminarRepository->findForManager($personnr=$personnr, $order='DESC');
			}
		} else {
			$erstattung = $this->objectManager->getEmptyObject('\NEXT\IconReiseabrechnung\Domain\Model\Erstattung');
		}
	
		//
		$t_reisen = 0;
		$t_seminare = 0;
		if( $reisen ){
			$t_reisen = $reisen->count();
		}
		if( $seminare ){
			$t_seminare = $seminare->count();
		}
		$results = ($t_reisen > 0 || $t_seminare > 0) ? true : false;

		//
		$seminar = $this->objectManager->getEmptyObject('\NEXT\IconReiseabrechnung\Domain\Model\Seminar');
		$seminar->setPersonnr( $erstattung->getPersonnr() );
		//
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'action' => 'erstattung',
			'erstattung' => $erstattung,
			'today' => $today,
			'results' => $results,
			'seminar' => $seminar,
			'reisen' => $reisen,
			'seminare' => $seminare,
            'users' => $users
		));
	}
	

	/**
	 * action edit
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function editAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		if ( is_null($reise) ) {
			$this->redirect();
		}

		//
		$person = $this->userRepository->getByPersonnr($reise->getPersonnr(), $mode='USER');

		//
		$reise->prepareVerkehrsmittel();

		//		
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'person' => $person,
			'reise' => $reise,
			'optionsBelegType' => $this->reiseRepository->getOptionsBelegType(),
			'optionsCountries' => $this->reiseRepository->getOptionsCountries(),
		));
	}


	/**
	 * saveAction
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function saveAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();
		//	SETTINGS
		$extName = 'tx_iconreiseabrechnung_pi1_manager';
		$uploadPath = $this->settings['manager']['upload']['path'];    			//    'uploads/tx_iconreiseabrechnung/';
		$uploadSizeMax = $this->settings['manager']['upload']['maxSizeMB'];		//    5;
//		$allowedFileTypes = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', 'application/pdf;application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/msword;application/vnd.openxmlformats-officedocument.wordprocessingml.document;image/jpg;image/jpeg;image/pjpeg;');
		$allowedFileTypes = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', $this->settings['manager']['upload']['allowedFileTypes']);		//	'application/pdf;'

		//	REISE-NR
		$t_reisenr = $reise->getReisenr();
		$t_year = substr($reise->getReisenr(), 0, 2);
//		$t_uploadPath = $uploadPath . $t_year . '/';
		$t_uploadPath = $uploadPath;
//		$t_fileName = $t_reisenr . '.pdf';	
		$t_fileName = $t_year . '/' . $t_reisenr . '.pdf';	

		//	UPDATE: Abschnitte / Passagiere / Belege
		$this->updateReise($reise);
		
		//	CHECK, DELETE CURRENT BELEGE.PDF:
		if( $reise->getBelegePdf()==$t_fileName.'.DELETE' ){
			//	remoce File from Storage
			unlink( \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($t_uploadPath) . $t_fileName);
			//	unset BelegePdf
			$reise->setBelegePdf('');
		}

		//	UPDATE UPLOADED BELEGE
		//
		$files = array();
		//
		if( $_FILES[$extName] ){
			$fparam = 'filePDF';
			//    get FILENAME from FORM
			$fileName = trim($_FILES[$extName]['name'][$fparam]);
			//    get FILESIZE from FORM
			$fSize = $_FILES[$extName]['size'][$fparam];
			//    get FILETYPE from FORM
			$fType = trim($_FILES[$extName]['type'][$fparam]);
			//    get TMP-FILENAME to FILE in FORM
			$tmpName = trim($_FILES[$extName]['tmp_name'][$fparam]);
			//
			$isFileName = $fileName != '';
			$isFileSize = $fSize > 0 && $fSize <= ($uploadSizeMax *1024*1024);
			$isFileType = in_array($fType, $allowedFileTypes);
			$isFileTemp = $tmpName !='';
			//
			if( $isFileName && $isFileSize && $isFileType && $isFileTemp ){
				//	...
				$storeFile = TRUE;
				//
				$reise->setBelegePdf('');
				//	check, is destination upload-dir available:
				$t_uploadPathDir = $t_uploadPath . $t_year . '/';
				if( !file_exists( \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($t_uploadPathDir) ) ){
					//	create directory
					if( !mkdir( \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($t_uploadPathDir) ) ){
						//	ERROR: konnte verzeichnis nicht anlegen !!!
						$storeFile = FALSE;
					}
				}
				//
				if( $storeFile ){
					//	
					$newPathName = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($t_uploadPath) . $t_fileName;
					//	move TMP-FILE to destination of newPathName
					\TYPO3\CMS\Core\Utility\GeneralUtility::upload_copy_move($tmpName, $newPathName);
					//	store filename
					$reise->setBelegePdf($t_fileName);
				}
			}
		}
		
		//	UPDATE VERKEHRSMITTEL
		$reise->updateVerkehrsmittel();

		//	UPDATE:	KOSTENBERECHNUNG
		$this->makeKostenberechnung($reise);

 		//
		$do_submit = $reise->getSubmit();

		//
		if( $do_submit ){
			$reise->setStatusGL();
		}
		//
		$this->reiseRepository->add($reise);
		$this->persistenceManager->persistAll();

		//
		//	-> show Edit-Screen
		$actionName = 'edit';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array(
			'reise' => $reise->getUid(),
		);
		//
		if( $do_submit ){
			$actionName = 'submit';
			$arguments['chief'] = $reise->getChief();
		}
		//
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
	}


	/**
	 * submitAction
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @param int $chief
	 * @return void
   	 */
	public function submitAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL, $chief = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

/*
		//
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'users' => $users,
		));
*/
        if(!$chief)
        {
            $chief = $reise->getChief();
        }

        //
		$person = $this->userRepository->getByPersonnr($reise->getPersonnr(), $mode='USER');

		//
		$mailCFG = array(
			'contentType' => 'text/html',
			'charset' => $GLOBALS['TSFE']->metaCharset,
		);
		
		$mailCFG['fromEmail'] = 'website@icon.at';
		$mailCFG['fromName'] = 'ICON';

		//	send INFO to central USER
		$m_info = '';
/*
		$m_info .= 'Teilnehmer <strong>';
		$m_info .= '<a href="mailto:' . $registration->getEmail() . '">' . $registration->getFirstname() . ' ' . $registration->getLastname() . '</a>';
		$m_info .= '</strong> schreibt:<br><br>';
		$m_info .= nl2br( strip_tags( $registration->getMessage() ) );
		$m_info .= '<br>';
*/
		//
		$m_mailData = $mailData;
		$m_mailData['REISE_NR'] = $reise->getReisenr();
		$m_mailData['PERSON_NR'] = $reise->getPersonnr();
		$m_mailData['FIRSTNAME'] = $person->getFirstName();
		$m_mailData['LASTNAME'] = $person->getLastName();
		$m_mailData['URL'] = 'http://www.icon.at/de/reiseabrechnung/geschaeftsleitung/?tx_iconreiseabrechnung_pi1_chief[reise]=' . $reise->getUid();
		$m_mailBody = $this->mailRepository->buildMailBody($this->userMode, $m_mailData, $this->settings);
		//
		$m_mailCFG = $mailCFG;
//		$m_mailCFG['toEmail'] = $this->settings['departments']['1']['chief']['email'];
		$m_mailCFG['toEmail'] = $this->settings['departments'][$chief]['chief']['email'];
//		$m_mailCFG['toName'] = $this->settings['departments']['1']['chief']['name'];
		$m_mailCFG['toName'] = $this->settings['departments'][$chief]['chief']['name'];
		$m_mailCFG['subject'] = $this->settings['manager']['mail']['subject'];
		//
        $m_mailRes = $this->mailRepository->deliverMail($m_mailCFG, $m_mailBody);

		//	-> show Result-Screen
		$actionName = 'submitted';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array(
			'reise' => $reise->getUid(),
		);
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);

	}

	/**
	 * submittedAction
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function submittedAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		//		
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'reise' => $reise,
		));

	}
	

	/**
	 * action seminar
	 *
	 * 
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Seminar $seminar
	 * @return void
	 */
	public function seminarAction (\NEXT\IconReiseabrechnung\Domain\Model\Seminar $seminar = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();
		//
		if ( is_null($seminar) ) {
			/*
				FUNKTIONIERT NUR, WENN DER PATCH EINGEBAUT IST:
				https://forge.typo3.org/issues/55861
				https://review.typo3.org/#/c/27535/2/typo3/sysext/extbase/Classes/Mvc/Controller/AbstractController.php
			*/
			$seminar = $this->objectManager->getEmptyObject('\NEXT\IconReiseabrechnung\Domain\Model\Seminar');
		}

		//
		if( $seminar->getUid() < 1 ){
			//	seams this is a new one!
			//	apply Firstname & Lastname by Personnr!
			$personnr = $seminar->getPersonnr();
			if( $personnr != '' ){
				$ma = $this->userRepository->getByPersonnr($personnr=$personnr);
				if( $ma ){
					$seminar->setFirstname($ma->getFirstname());
					$seminar->setLastname($ma->getLastname());
				}
			}
		}
		//
		$erstattung = $this->objectManager->getEmptyObject('\NEXT\IconReiseabrechnung\Domain\Model\Erstattung');
		$erstattung->setPersonnr( $seminar->getPersonnr() );

		//		
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'users' => $users,
			'action' => 'seminar',
			'seminar' => $seminar,
			'erstattung' => $erstattung,
		));

	}
	
	/**
	 * action saveSeminar
	 *
	 * 
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Seminar $seminar
	 * @return void
   	 */
	public function saveSeminarAction (\NEXT\IconReiseabrechnung\Domain\Model\Seminar $seminar = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		//
		if( $seminar->getUid() < 1 ){
			//	seams this is a new one!
			//	apply Firstname & Lastname by Personnr!
			$personnr = $seminar->getPersonnr();
			$ma = $this->userRepository->getByPersonnr($personnr=$personnr);
			if( $ma ){
				$seminar->setFirstname($ma->getFirstname());
				$seminar->setLastname($ma->getLastname());
			}
		}

		//
		$this->seminarRepository->add($seminar);
		$this->persistenceManager->persistAll();
		
		//	-> show Result-Screen
		$actionName = 'seminar';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array(
			'seminar' => $seminar->getUid(),
		);
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);

	}

    /**
     * action deleteReise
     *
     *
     * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
     * @return void
     */
    public function deleteAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
        //	1. CHECK IF USER IS LOGGED IN ?
        $this->checkIsLoggedIn();
        //	2. GET USERDATA FROM SESSION !
        $loggedInUser = $this->getLoggedInUser();

        $reise->setStatusDeleted();

        $this->reiseRepository->update($reise);
        $this->reiseRepository->remove($reise);

        $this->persistenceManager->persistAll();

        //	-> show Index-Screen
        $actionName = 'index';
        $controllerName = NULL;
        $extensionName = NULL;
        $arguments = array(
            'reise' => $reise->getUid(),
        );
        $this->redirect($actionName, $controllerName, $extensionName, $arguments);

    }

    /**
     * action showDeleteReise
     *
     *
     * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
     * @return void
     */
    public function showDeletedAction()
    {
        //	1. CHECK IF USER IS LOGGED IN ?
        $this->checkIsLoggedIn();
        //	2. GET USERDATA FROM SESSION !
        $loggedInUser = $this->getLoggedInUser();

        $getVars = GeneralUtility::_GET('tx_iconreiseabrechnung_pi1_manager');
        $reiseId = (int)$getVars['reise'];
        if ($reiseId) {
            $reise = $this->reiseRepository->findDeletedById($reiseId);

            $this->view->assignMultiple([
                'userMode' => $this->userMode, 'loggedInUser' => $loggedInUser,
                'reise' => $reise,
                'action' => 'deleted'
            ]);
        }
    }

    /**
     * action mitarbeiterReise
     *
     *
     * @param \NEXT\IconReiseabrechnung\Domain\Model\User $user
     * @return void
     */
    public function mitarbeiterAction(){
        //	1. CHECK IF USER IS LOGGED IN ?
        $this->checkIsLoggedIn();
        //	2. GET USERDATA FROM SESSION !
        $loggedInUser = $this->getLoggedInUser();

        $users = $this->userRepository->findAllUsers( );


        //
        $this->view->assignMultiple(array(
            'userMode' => $this->userMode,
            'loggedInUser' => $loggedInUser,
            'action' => 'mitarbeiter',
            'users' => $users
        ));
    }

    /**
     * action editMitarbeiter
     *
     * @param \NEXT\IconReiseabrechnung\Domain\Model\User $user
     * @return void
     */
    public function editMitarbeiterAction (\NEXT\IconReiseabrechnung\Domain\Model\User $user = NULL) {
        //	1. CHECK IF USER IS LOGGED IN ?
        $this->checkIsLoggedIn();
        //	2. GET USERDATA FROM SESSION !
        $loggedInUser = $this->getLoggedInUser();

        $getVars = GeneralUtility::_GET('tx_iconreiseabrechnung_pi1_manager');
        $userId = (int)$getVars['user'];

        $user = $this->userRepository->findAllById($userId);

        $this->view->assignMultiple(array(
            'userMode' => $this->userMode,
            'loggedInUser' => $loggedInUser,
            'action' => 'mitarbeiter',
            'user' => $user
        ));

    }

    /**
     * @ignorevalidation $user
     * @param \NEXT\IconReiseabrechnung\Domain\Model\User $user
     */
    public function newMitarbeiterAction(\NEXT\IconReiseabrechnung\Domain\Model\User $user = NULL) {
        //	1. CHECK IF USER IS LOGGED IN ?
        $this->checkIsLoggedIn();
        //	2. GET USERDATA FROM SESSION !
        $loggedInUser = $this->getLoggedInUser();

        $this->view->assignMultiple(array(
            'userMode' => $this->userMode,
            'loggedInUser' => $loggedInUser,
            'action' => 'mitarbeiter',
            'user' => $this->objectManager->get(User::class)
        ));
    }

    public function saveNewMitarbeiterAction (\NEXT\IconReiseabrechnung\Domain\Model\User $user = NULL) {
        //	1. CHECK IF USER IS LOGGED IN ?
        $this->checkIsLoggedIn();
        //	2. GET USERDATA FROM SESSION !
        $loggedInUser = $this->getLoggedInUser();

        $this->userRepository->add($user);

        $this->persistenceManager->persistAll();

        //	-> show Mitarbeiter-Screen
        $actionName = 'editMitarbeiter';
        $controllerName = NULL;
        $extensionName = NULL;
        $arguments = array(
            'user' => $user->getUid(),
        );

        $this->redirect($actionName, $controllerName, $extensionName, $arguments);
    }

    /**
     * action saveMitarbeiter
     *
     * @param \NEXT\IconReiseabrechnung\Domain\Model\User $user
     * @return void
     */
    public function saveMitarbeiterAction (\NEXT\IconReiseabrechnung\Domain\Model\User $user = NULL) {
        //	1. CHECK IF USER IS LOGGED IN ?
        $this->checkIsLoggedIn();
        //	2. GET USERDATA FROM SESSION !
        $loggedInUser = $this->getLoggedInUser();

        if ($user === null) {
            $postVars = GeneralUtility::_POST('tx_iconreiseabrechnung_pi1_manager');
            if (isset($postVars['user']['__identity'])) {
                $user = $this->userRepository->findAllById($postVars['user']['__identity']);
                if ($user !== null) {
                    $fieldsToUpdate = $postVars['user'];
                    unset($fieldsToUpdate['__identity']);
                    foreach($fieldsToUpdate as $fieldName => $value) {
                        $setter = 'set' . ucfirst($fieldName);
                        $user->$setter($value);
                    }
                }
            }
        }

        $this->userRepository->update($user);

        $this->persistenceManager->persistAll();

        //	-> show Mitarbeiter-Screen
        $actionName = 'editMitarbeiter';
        $controllerName = NULL;
        $extensionName = NULL;
        $arguments = array(
            'user' => $user->getUid(),
        );

        $this->redirect($actionName, $controllerName, $extensionName, $arguments);

    }
	
}

