<?php
namespace NEXT\IconReiseabrechnung\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class UserController extends \NEXT\IconReiseabrechnung\Controller\BaseController {


	/**
	 * userMode
	 *
	 * @var string
	 */
	protected $userMode = 'USER';


	/**
	 * action index
	 *
	 * @return void
	 */
	public function indexAction() {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		//
		$paginate = $this->getPaginateSettings();
		//
		$reisen = $this->reiseRepository->getByPersonnr( $loggedInUser->getPersonnr(), $order='DESC' );
		//
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'paginate' => $paginate,
			'reisen' => $reisen,
		));
	}


	/**
	 * action edit
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function editAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		if ( is_null($reise) ) {
			/*
				FUNKTIONIERT NUR, WENN DER PATCH EINGEBAUT IST:
				https://forge.typo3.org/issues/55861
				https://review.typo3.org/#/c/27535/2/typo3/sysext/extbase/Classes/Mvc/Controller/AbstractController.php
			*/
			$reise = $this->objectManager->getEmptyObject('\NEXT\IconReiseabrechnung\Domain\Model\Reise');
			$reise->setPersonnr( $loggedInUser->getPersonnr() );
			$reise->setFirstname( $loggedInUser->getFirstname() );
			$reise->setLastname( $loggedInUser->getLastname() );
		}
		//
		$reise->prepareVerkehrsmittel();

		//		
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'reise' => $reise,
			'optionsBelegType' => $this->reiseRepository->getOptionsBelegType(),
			'optionsCountries' => $this->reiseRepository->getOptionsCountries(),
		));
	}


	/**
	 * saveAction
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function saveAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();
		
		//
		if( $reise->getUid() < 1 ){
			$reise->setPersonnr( $loggedInUser->getPersonnr() );
			$reise->setFirstname( $loggedInUser->getFirstname() );
			$reise->setLastname( $loggedInUser->getLastname() );
		}

		//	UPDATE: Abschnitte / Passagiere / Belege
		$this->updateReise($reise);

		//	UPDATE VERKEHRSMITTEL
		$reise->updateVerkehrsmittel();
		
		//	UPDATE:	KOSTENBERECHNUNG
		//	zum testen !!!
//		$this->makeKostenberechnung($reise);

 		//
		$do_submit = $reise->getSubmit();

		//
		if( $do_submit ){
			$abschnitte = $reise->getAbschnitte()->toArray();
			$abschnitt = reset($abschnitte);
			$year = (int)strftime('%Y', $abschnitt->getAbDatetime() );
			$reisenr = $this->reiseRepository->getNewReisenr( $year );

			$reise->setReisenr( $reisenr );

			if( $reisenr > 0){
				$reise->setStatusSekretariat();
				$reise->setReisenr( $reisenr );
				//	UPDATE:	KOSTENBERECHNUNG
//				$this->makeKostenberechnung($reise);
			} else {
				//	es konnte keine Reise-Nr erzeugt werden, damit keine weiterverarbeitung!
				$do_submit = FALSE;
			}

		}
		//
		$this->reiseRepository->add($reise);
		$this->persistenceManager->persistAll();

		//
		//	-> show Edit-Screen
		$actionName = 'edit';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array(
			'reise' => $reise->getUid(),
		);
		//
		if( $do_submit ){
			$actionName = 'submit';
			$arguments['secretary'] = $reise->getSecretary();
		}
		//
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
	}


	/**
	 * submitAction
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @param integer $secretary
	 * @return void
   	 */
	public function submitAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL, $secretary = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

/*
		//		
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'users' => $users,
		));
*/

		//
		$mailCFG = array(
			'contentType' => 'text/html',
			'charset' => $GLOBALS['TSFE']->metaCharset,
		);
		
		$mailCFG['fromEmail'] = 'website@icon.at';
		$mailCFG['fromName'] = 'ICON';

		//	send INFO to central USER
		$m_info = '';
/*
		$m_info .= 'Teilnehmer <strong>';
		$m_info .= '<a href="mailto:' . $registration->getEmail() . '">' . $registration->getFirstname() . ' ' . $registration->getLastname() . '</a>';
		$m_info .= '</strong> schreibt:<br><br>';
		$m_info .= nl2br( strip_tags( $registration->getMessage() ) );
		$m_info .= '<br>';
*/
		//
		$m_mailData = $mailData;
		$m_mailData['REISE_NR'] = $reise->getReisenr();
		$m_mailData['PERSON_NR'] = $loggedInUser->getPersonnr();
		$m_mailData['FIRSTNAME'] = $loggedInUser->getFirstName();
		$m_mailData['LASTNAME'] = $loggedInUser->getLastName();
		$m_mailData['URL'] = 'http://www.icon.at/de/reiseabrechnung/sekretariat/?tx_iconreiseabrechnung_pi1_manager[action]=edit&tx_iconreiseabrechnung_pi1_manager[reise]=' . $reise->getUid();
		$m_mailBody = $this->mailRepository->buildMailBody($this->userMode, $m_mailData, $this->settings);
		//
		$m_mailCFG = $mailCFG;
//		$m_mailCFG['toEmail'] = $this->settings['departments']['1']['secretary']['email'];
		$m_mailCFG['toEmail'] = $this->settings['departments'][$secretary]['secretary']['email'];
//		$m_mailCFG['toName'] = $this->settings['departments']['1']['secretary']['name'];
		$m_mailCFG['toName'] = $this->settings['departments'][$secretary]['secretary']['name'];
		$m_mailCFG['subject'] = $this->settings['user']['mail']['subject'];
		//
		$m_mailRes = $this->mailRepository->deliverMail($m_mailCFG, $m_mailBody);

		//	-> show Result-Screen
		$actionName = 'submitted';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array(
			'reise' => $reise->getUid(),
		);
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);

	}

	/**
	 * submittedAction
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return void
   	 */
	public function submittedAction (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise = NULL) {
		//	1. CHECK IF USER IS LOGGED IN ?
		$this->checkIsLoggedIn();
		//	2. GET USERDATA FROM SESSION !
		$loggedInUser = $this->getLoggedInUser();

		//		
		$this->view->assignMultiple(array(
			'userMode' => $this->userMode,
			'loggedInUser' => $loggedInUser,
			'reise' => $reise,
		));

	}


}

