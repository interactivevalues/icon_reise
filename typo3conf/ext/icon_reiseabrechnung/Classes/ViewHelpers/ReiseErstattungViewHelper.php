<?php
namespace NEXT\IconReiseabrechnung\ViewHelpers;


/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */

class ReiseErstattungViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * render
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @return mixed
	 */
	public function render (\NEXT\IconReiseabrechnung\Domain\Model\Reise $reise=NULL) {
		$t = NULL;
		$t_uid = $reise->getUid();
		//
		$t_aliquote = 0;
		$t_total = $reise->getSumTotal();		//	ist BRUTTO BETRAG
		$t_seminar = 0;
		//
		$t_belege = $reise->getBelege();
		
		//	ALS ERSTES, ZIEHE ALLE BELEGE des MA den BRUTTO-BETRAG AB !!!
		foreach( $t_belege as $beleg ){
			if( $beleg->getMitarbeiter() ){
				$t_total -= $beleg->getSumEuroBrutto();
			}
		}

		//	ALS ZWEITES, RECHNE ALLE BELEGE MA + ICON den NETTO-BETRAG DAZU !!!
		foreach( $t_belege as $beleg ){
			$t_total += $beleg->getSumEuroNetto();
			if( $beleg->getType() == 10 ){
				$t_seminar += $beleg->getSumEuroNetto();
			}
		}
		
		//	ERRECHNE KOSTEN REISE
		$t_reise = $t_total - $t_seminar;
/*
		//
		if( $reise->getGrund() == 4 ||
			$reise->getGrund() == '4' ){
			$t_total = $t_reise + $t_seminar;
		} else {
			$t_total = $t_seminar > 0 ? $t_reise + $t_seminar : 0;
		}
*/
		//
		$t = '
        		<td data-uid="'. $t_uid .'" data-sumTotal="' . $t_total. '">&euro; ' . number_format($t_total, 2, ',', '.') . '</td>
                <td data-uid="'. $t_uid .'" data-sumSeminar="' . $t_seminar . '">&euro; ' . number_format($t_seminar, 2, ',', '.') . '</td>
				<td data-uid="'. $t_uid .'" data-sumReise="' . $t_reise . '">&euro; ' . number_format($t_reise, 2, ',', '.') . '</td>
				<td data-uid="'. $t_uid .'" data-sumAliquote="0">&euro; ' . number_format($t_aliquote, 2, ',', '.') . '</td>
		';
		//
		return $t;
	}

}
