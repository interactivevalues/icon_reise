<?php
namespace NEXT\IconReiseabrechnung\ViewHelpers;


/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */

class ReiseViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * render
	 *
	 * @param string $mode
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise
	 * @param int $reisenr
	 * @param int $grund
	 * @param string $status
	 * @param string $userMode
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Beleg $beleg
	 * @return mixed
	 */
	public function render ($mode='', \NEXT\IconReiseabrechnung\Domain\Model\Reise $reise=NULL, $reisenr=-1, $grund=-1, $status='', $userMode='', \NEXT\IconReiseabrechnung\Domain\Model\Beleg $beleg=NULL) {
		$t = NULL;
		//
		if( $mode == 'reisenr' ){
			if( $reise->getReisenr() > 0 ){
				$t = $reise->getReisenr();
			} else {
				$t = '-------';
			}
		}
		//
		if( $mode == 'grund' ){
			switch($grund){
				case 1: case '1':	$t = 'Besprechung / Interessensvertretung / usw.'; break;
				case 2: case '2':	$t = 'Vortragender (Tax Academy)'; break;
				case 3: case '3':	$t = 'Vortragender (Klienten-Workshop)'; break;
				case 4: case '4':	$t = 'Teilnehmer (Seminar)'; break;
				default: 			$t = ''; break;
			}
		}
		//
		if( $mode == 'showBelegMA'){
			if( $userMode=='USER' ){
				if( $beleg->getMitarbeiter() ){
					return TRUE;
				} else {
					return FALSE;
				}
			} else {
				return TRUE;
			}
		}
		//
		if( $mode == 'showSumMA'){
			if( $userMode=='USER' ){
				if( $reise->getStatus()=='ABRECHNEN' || $reise->getStatus()=='ERLEDIGT' ){
					return trim($this->renderChildren());
				} else {
					return '---,--';				
				}
			} else {
				return trim($this->renderChildren());
			}
		}
		//
		if( $mode == 'hasVerkehrsmittel' ){
			if( $reise->getVkmBahn() ||
				$reise->getVkmFlug() ||
				$reise->getVkmMietauto() ||
				$reise->getVkmMitreise() ||
				$reise->getVkmPkwDienst() ||
				$reise->getVkmPkwPrivat() ||
				$reise->getVkmSonstigeOeffentliche() 
				){
				return TRUE;
			} else {
				return FALSE;
			}
		}
		//
		if( $mode == 'hasPassengers' ){
			return $reise->getPassengers()->count() ? TRUE : FALSE;
		}
		//
		if( $mode == 'hasBelege' ){
			return $reise->getBelege()->count() ? TRUE : FALSE;
		}
		//
		return $t;
	}

}
