<?php
namespace NEXT\IconReiseabrechnung\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Abschnitt extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
	/**
	 * abDattime
	 *
	 * @var integer
	 */
	protected $abDatetime;

	/**
	 * abOrt
	 *
	 * @var string
	 */
	protected $abOrt;

	/**
	 * abCountry
	 *
	 * @var string
	 */
	protected $abCountry;

	/**
	 * guDatetime
	 *
	 * @var integer
	 */
	protected $guDatetime;

	/**
	 * guOrt
	 *
	 * @var string
	 */
	protected $guOrt;

	/**
	 * guCountry
	 *
	 * @var string
	 */
	protected $guCountry;	

	/**
	 * gu2Datetime
	 *
	 * @var integer
	 */
	protected $gu2Datetime;

	/**
	 * gu2Ort
	 *
	 * @var string
	 */
	protected $gu2Ort;

	/**
	 * gu2Country
	 *
	 * @var string
	 */
	protected $gu2Country;	

	/**
	 * gu3Datetime
	 *
	 * @var integer
	 */
	protected $gu3Datetime;

	/**
	 * guOrt
	 *
	 * @var string
	 */
	protected $gu3Ort;

	/**
	 * gu3Country
	 *
	 * @var string
	 */
	protected $gu3Country;	

	/**
	 * gu4Datetime
	 *
	 * @var integer
	 */
	protected $gu4Datetime;

	/**
	 * gu4Ort
	 *
	 * @var string
	 */
	protected $gu4Ort;

	/**
	 * gu4Country
	 *
	 * @var string
	 */
	protected $gu4Country;	

	/**
	 * anDatetime
	 *
	 * @var integer
	 */
	protected $anDatetime;

	/**
	 * anOrt
	 *
	 * @var string
	 */
	protected $anOrt;

	/**
	 * anCountry
	 *
	 * @var string
	 */
	protected $anCountry;	

	/**
	 * privatehours
	 *
	 * @var string
	 */
	protected $privatehours;	

	/**
	 * mittag
	 *
	 * @var boolean
	 */
	protected $mittag;	

	/**
	 * abend
	 *
	 * @var boolean
	 */
	protected $abend;	

	/**
	 * nacht
	 *
	 * @var integer
	 */
	protected $nacht;	


	
	/**
	 * Sets the abDatetime
	 *
	 * @param integer $abDatetime
	 * @return void
	 */
	public function setAbDatetime($abDatetime) {
		$this->abDatetime = $abDatetime;
	}
	
	/**
	 * Returns the abDatetime
	 *
	 * @return integer $abDatetime
	 */
	public function getAbDatetime() {
		return $this->abDatetime;
	}
	

	/**
	 * Sets the abOrt
	 *
	 * @param string $abOrt
	 * @return void
	 */
	public function setAbOrt($abOrt) {
		$this->abOrt = $abOrt;
	}
	
	/**
	 * Returns the abOrt
	 *
	 * @return string $abOrt
	 */
	public function getAbOrt() {
		return $this->abOrt;
	}
	
	/**
	 * Sets the abCountry
	 *
	 * @param string $abCountry
	 * @return void
	 */
	public function setAbCountry($abCountry) {
		$this->abCountry = $abCountry;
	}
	
	/**
	 * Returns the abCountry
	 *
	 * @return string $abCountry
	 */
	public function getAbCountry() {
		return $this->abCountry;
	}
	

	/**
	 * Sets the guDatetime
	 *
	 * @param integer $guDatetime
	 * @return void
	 */
	public function setGuDatetime($guDatetime) {
		$this->guDatetime = $guDatetime;
	}
	
	/**
	 * Returns the guDatetime
	 *
	 * @return integer $guDatetime
	 */
	public function getGuDatetime() {
		return $this->guDatetime;
	}
	
	/**
	 * Sets the guOrt
	 *
	 * @param string $guOrt
	 * @return void
	 */
	public function setGuOrt($guOrt) {
		$this->guOrt = $guOrt;
	}
	
	/**
	 * Returns the guOrt
	 *
	 * @return string $guOrt
	 */
	public function getGuOrt() {
		return $this->guOrt;
	}
	
	/**
	 * Sets the guCountry
	 *
	 * @param string $guCountry
	 * @return void
	 */
	public function setGuCountry($guCountry) {
		$this->guCountry = $guCountry;
	}
	
	/**
	 * Returns the guCountry
	 *
	 * @return string $guCountry
	 */
	public function getGuCountry() {
		return $this->guCountry;
	}
	

	/**
	 * Sets the gu2Datetime
	 *
	 * @param integer $gu2Datetime
	 * @return void
	 */
	public function setGu2Datetime($gu2Datetime) {
		$this->gu2Datetime = $gu2Datetime;
	}
	
	/**
	 * Returns the gu2Datetime
	 *
	 * @return integer $gu2Datetime
	 */
	public function getGu2Datetime() {
		return $this->gu2Datetime;
	}
	
	/**
	 * Sets the gu2Ort
	 *
	 * @param string $gu2Ort
	 * @return void
	 */
	public function setGu2Ort($gu2Ort) {
		$this->gu2Ort = $gu2Ort;
	}
	
	/**
	 * Returns the gu2Ort
	 *
	 * @return string $gu2Ort
	 */
	public function getGu2Ort() {
		return $this->gu2Ort;
	}
	
	/**
	 * Sets the gu2Country
	 *
	 * @param string $gu2Country
	 * @return void
	 */
	public function setGu2Country($gu2Country) {
		$this->gu2Country = $gu2Country;
	}
	
	/**
	 * Returns the gu2Country
	 *
	 * @return string $gu2Country
	 */
	public function getGu2Country() {
		return $this->gu2Country;
	}


	/**
	 * Sets the gu3Datetime
	 *
	 * @param integer $gu3Datetime
	 * @return void
	 */
	public function setGu3Datetime($gu3Datetime) {
		$this->gu3Datetime = $gu3Datetime;
	}
	
	/**
	 * Returns the gu3Datetime
	 *
	 * @return integer $gu3Datetime
	 */
	public function getGu3Datetime() {
		return $this->gu3Datetime;
	}
	
	/**
	 * Sets the gu3Ort
	 *
	 * @param string $gu3Ort
	 * @return void
	 */
	public function setGu3Ort($gu3Ort) {
		$this->gu3Ort = $gu3Ort;
	}
	
	/**
	 * Returns the gu3Ort
	 *
	 * @return string $gu3Ort
	 */
	public function getGu3Ort() {
		return $this->gu3Ort;
	}
	
	/**
	 * Sets the gu3Country
	 *
	 * @param string $gu3Country
	 * @return void
	 */
	public function setGu3Country($gu3Country) {
		$this->gu3Country = $gu3Country;
	}
	
	/**
	 * Returns the gu3Country
	 *
	 * @return string $gu3Country
	 */
	public function getGu3Country() {
		return $this->gu3Country;
	}


	/**
	 * Sets the gu4Datetime
	 *
	 * @param integer $gu4Datetime
	 * @return void
	 */
	public function setGu4Datetime($gu4Datetime) {
		$this->gu4Datetime = $gu4Datetime;
	}
	
	/**
	 * Returns the gu4Datetime
	 *
	 * @return integer $gu4Datetime
	 */
	public function getGu4Datetime() {
		return $this->gu4Datetime;
	}
	
	/**
	 * Sets the gu4Ort
	 *
	 * @param string $gu4Ort
	 * @return void
	 */
	public function setGu4Ort($gu4Ort) {
		$this->gu4Ort = $gu4Ort;
	}
	
	/**
	 * Returns the gu4Ort
	 *
	 * @return string $gu4Ort
	 */
	public function getGu4Ort() {
		return $this->gu4Ort;
	}
	
	/**
	 * Sets the gu4Country
	 *
	 * @param string $gu4Country
	 * @return void
	 */
	public function setGu4Country($gu4Country) {
		$this->gu4Country = $gu4Country;
	}
	
	/**
	 * Returns the gu4Country
	 *
	 * @return string $gu4Country
	 */
	public function getGu4Country() {
		return $this->gu4Country;
	}


	/**
	 * Sets the anDatetime
	 *
	 * @param integer $anDatetime
	 * @return void
	 */
	public function setAnDatetime($anDatetime) {
		$this->anDatetime = $anDatetime;
	}
	
	/**
	 * Returns the anDatetime
	 *
	 * @return integer $anDatetime
	 */
	public function getAnDatetime() {
		return $this->anDatetime;
	}
	
	/**
	 * Sets the anOrt
	 *
	 * @param string $anOrt
	 * @return void
	 */
	public function setAnOrt($anOrt) {
		$this->anOrt = $anOrt;
	}
	
	/**
	 * Returns the anOrt
	 *
	 * @return string $anOrt
	 */
	public function getAnOrt() {
		return $this->anOrt;
	}
	
	/**
	 * Sets the anCountry
	 *
	 * @param string $anCountry
	 * @return void
	 */
	public function setAnCountry($anCountry) {
		$this->anCountry = $anCountry;
	}
	
	/**
	 * Returns the anCountry
	 *
	 * @return string $anCountry
	 */
	public function getAnCountry() {
		return $this->anCountry;
	}
	

	/**
	 * Sets the privatehours
	 *
	 * @param string $privatehours
	 * @return void
	 */
	public function setPrivatehours($privatehours) {
		$this->privatehours = $privatehours;
	}
	
	/**
	 * Returns the privatehours
	 *
	 * @return string $privatehours
	 */
	public function getPrivatehours() {
		return $this->privatehours;
	}
	

	/**
	 * Sets the mittag
	 *
	 * @param boolean $mittag
	 * @return void
	 */
	public function setMittag($mittag) {
		$this->mittag = $mittag;
	}
	
	/**
	 * Returns the mittag
	 *
	 * @return boolean $mittag
	 */
	public function getMittag() {
		return $this->mittag;
	}
	

	/**
	 * Sets the abend
	 *
	 * @param boolean $abend
	 * @return void
	 */
	public function setAbend($abend) {
		$this->abend = $abend;
	}
	
	/**
	 * Returns the abend
	 *
	 * @return boolean $abend
	 */
	public function getAbend() {
		return $this->abend;
	}
	

	/**
	 * Sets the nacht
	 *
	 * @param integer $nacht
	 * @return void
	 */
	public function setNacht($nacht) {
		$this->nacht = $nacht;
	}
	
	/**
	 * Returns the nacht
	 *
	 * @return integer $nacht
	 */
	public function getNacht() {
		return $this->nacht;
	}
	
}
?>