<?php
namespace NEXT\IconReiseabrechnung\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Reise extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * abschnitte
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconReiseabrechnung\Domain\Model\Abschnitt>
	 * @lazy
	 */
	protected $abschnitte;
	
	/**
	 * passengers
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconReiseabrechnung\Domain\Model\Passenger>
	 * @lazy
	 */
	protected $passengers;
	
	/**
	 * belege
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconReiseabrechnung\Domain\Model\Beleg>
	 * @lazy
	 */
	protected $belege;
	

	/**
	 * Initialize categories and media relation
	 *
	 * @return \NEXT\IconReiseabrechnung\Domain\Model\Reise
	 */
	public function __construct() {
		$this->abschnitte = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->passengers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->belege = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}


	
	/*		START:	REISE	---------------------------------------------		*/

	/**
	 * personnr
	 *
	 * @var string
	 */
	protected $personnr;

	/**
	 * firstname
	 *
	 * @var string
	 */
	protected $firstname;

	/**
	 * lastname
	 *
	 * @var string
	 */
	protected $lastname;

	/**
	 * status
	 *
	 * @var string
	 */
	protected $status = 'MA';

	/**
	 * reisenr
	 *
	 * @var integer
	 */
	protected $reisenr;

	/**
	 * grund
	 *
	 * @var integer
	 */
	protected $grund = 1;
	
	/**
	 * ziel
	 *
	 * @var string
	 */
	protected $ziel;
	
	/**
	 * firma
	 *
	 * @var string
	 */
	protected $firma;
	
	/**
	 * kontaktperson
	 *
	 * @var string
	 */
	protected $kontaktperson;
	
	/**
	 * thema
	 *
	 * @var string
	 */
	protected $thema;
	
	/**
	 * klientennr
	 *
	 * @var string
	 */
	protected $klientennr;
	
	/**
	 * projektnr
	 *
	 * @var string
	 */
	protected $projektnr;
	
	/**
	 * verrechenbar
	 *
	 * @var integer
	 */
	protected $verrechenbar = 1;

	/**
	 * kmgefahren
	 *
	 * @var float
	 */
	protected $kmgefahren;
	

	/**
	 * sumTaxfree
	 *
	 * @var float
	 */
	protected $sumTaxfree;
	
	/**
	 * sumTax
	 *
	 * @var float
	 */
	protected $sumTax;
	
	/**
	 * sumTotal
	 *
	 * @var float
	 */
	protected $sumTotal;
	
	/**
	 * sumTotalWv
	 *
	 * @var float
	 */
	protected $sumTotalWv;
	
	/**
	 * sumTaggeld
	 *
	 * @var string
	 */
	protected $sumTaggeld;
	
	/**
	 * sumNachtgeld
	 *
	 * @var string
	 */
	protected $sumNachtgeld;
	
	/**
	 * sumKmgeld
	 *
	 * @var string
	 */
	protected $sumKmgeld;
	

	/**
	 * exportBh
	 *
	 * @var integer
	 */
	protected $exportBh;
	
	/**
	 * exportLv
	 *
	 * @var integer
	 */
	protected $exportLv;
	
	/**
	 * exportWv
	 *
	 * @var integer
	 */
	protected $exportWv;


    /** delete_reason
    *
    * @var string
    */
    protected $deleteReason;

    /** @var bool  */
    protected $deleted = false;
	
	/**
	 * Sets the personnr
	 *
	 * @param string $personnr
	 * @return void
	 */
	public function setPersonnr($personnr) {
		$this->personnr = $personnr;
	}
	
	/**
	 * Returns the personnr
	 *
	 * @return string $personnr
	 */
	public function getPersonnr() {
		return $this->personnr;
	}

	/**
	 * Sets the firstname
	 *
	 * @param string $firstname
	 * @return void
	 */
	public function setFirstname($firstname) {
		$this->firstname = $firstname;
	}
	
	/**
	 * Returns the firstname
	 *
	 * @return string $firstname
	 */
	public function getFirstname() {
		return $this->firstname;
	}
	
	/**
	 * Sets the lastname
	 *
	 * @param string $lastname
	 * @return void
	 */
	public function setLastname($lastname) {
		$this->lastname = $lastname;
	}
	
	/**
	 * Returns the lastname
	 *
	 * @return string $lastname
	 */
	public function getLastname() {
		return $this->lastname;
	}

	/**
	 * Sets the status
	 *
	 * @param string $status
	 * @return void
	 */
	public function setStatus($status) {
		$this->status = $status;
	}
	
	/**
	 * Returns the status
	 *
	 * @return string $status
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * Sets the reisenr
	 *
	 * @param string $reisenr
	 * @return void
	 */
	public function setReisenr($reisenr) {
		$this->reisenr = $reisenr;
	}
	
	/**
	 * Returns the reisenr
	 *
	 * @return string $reisenr
	 */
	public function getReisenr() {
		return $this->reisenr;
	}

	/**
	 * Sets the grund
	 *
	 * @param integer $grund
	 * @return void
	 */
	public function setGrund($grund) {
		$this->grund = $grund;
	}
	
	/**
	 * Returns the grund
	 *
	 * @return integer $grund
	 */
	public function getGrund() {
		return $this->grund;
	}

	/**
	 * Sets the ziel
	 *
	 * @param string $ziel
	 * @return void
	 */
	public function setZiel($ziel) {
		$this->ziel = $ziel;
	}
	
	/**
	 * Returns the ziel
	 *
	 * @return string $ziel
	 */
	public function getZiel() {
		return $this->ziel;
	}

	/**
	 * Sets the firma
	 *
	 * @param string $firma
	 * @return void
	 */
	public function setFirma($firma) {
		$this->firma = $firma;
	}
	
	/**
	 * Returns the firma
	 *
	 * @return string $firma
	 */
	public function getFirma() {
		return $this->firma;
	}

	/**
	 * Sets the kontaktperson
	 *
	 * @param string $kontaktperson
	 * @return void
	 */
	public function setKontaktperson($kontaktperson) {
		$this->kontaktperson = $kontaktperson;
	}
	
	/**
	 * Returns the kontaktperson
	 *
	 * @return string $kontaktperson
	 */
	public function getKontaktperson() {
		return $this->kontaktperson;
	}

	/**
	 * Sets the thema
	 *
	 * @param string $thema
	 * @return void
	 */
	public function setThema($thema) {
		$this->thema = $thema;
	}
	
	/**
	 * Returns the thema
	 *
	 * @return string $thema
	 */
	public function getThema() {
		return $this->thema;
	}

	/**
	 * Sets the klientennr
	 *
	 * @param string $klientennr
	 * @return void
	 */
	public function setKlientennr($klientennr) {
		$this->klientennr = $klientennr;
	}
	
	/**
	 * Returns the klientennr
	 *
	 * @return string $klientennr
	 */
	public function getKlientennr() {
		return $this->klientennr;
	}

	/**
	 * Sets the projektnr
	 *
	 * @param string $projektnr
	 * @return void
	 */
	public function setProjektnr($projektnr) {
		$this->projektnr = $projektnr;
	}
	
	/**
	 * Returns the projektnr
	 *
	 * @return string $projektnr
	 */
	public function getProjektnr() {
		return $this->projektnr;
	}

	/**
	 * Sets the verrechenbar
	 *
	 * @param integer $verrechenbar
	 * @return void
	 */
	public function setVerrechenbar($verrechenbar) {
		$this->verrechenbar = $verrechenbar;
	}
	
	/**
	 * Returns the verrechenbar
	 *
	 * @return integer $verrechenbar
	 */
	public function getVerrechenbar() {
		return $this->verrechenbar;
	}

	/**
	 * Sets the kmgefahren
	 *
	 * @param float $kmgefahren
	 * @return void
	 */
	public function setKmgefahren($kmgefahren) {
		$this->kmgefahren = $kmgefahren;
	}
	
	/**
	 * Returns the kmgefahren
	 *
	 * @return float $kmgefahren
	 */
	public function getKmgefahren() {
		return $this->kmgefahren;
	}

	/**
	 * Sets the sumTaxfree
	 *
	 * @param float $sumTaxfree
	 * @return void
	 */
	public function setSumTaxfree($sumTaxfree) {
		$this->sumTaxfree = $sumTaxfree;
	}
	
	/**
	 * Returns the sumTaxfree
	 *
	 * @return float $sumTaxfree
	 */
	public function getSumTaxfree() {
		return $this->sumTaxfree;
	}

	/**
	 * Sets the sumTax
	 *
	 * @param float $sumTax
	 * @return void
	 */
	public function setSumTax($sumTax) {
		$this->sumTax = $sumTax;
	}
	
	/**
	 * Returns the sumTax
	 *
	 * @return float $sumTax
	 */
	public function getSumTax() {
		return $this->sumTax;
	}

	/**
	 * Sets the sumTotal
	 *
	 * @param float $sumTotal
	 * @return void
	 */
	public function setSumTotal($sumTotal) {
		$this->sumTotal = $sumTotal;
	}
	
	/**
	 * Returns the sumTotal
	 *
	 * @return float $sumTotal
	 */
	public function getSumTotal() {
		return $this->sumTotal;
	}

	/**
	 * Sets the sumTotalWv
	 *
	 * @param float $sumTotalWv
	 * @return void
	 */
	public function setSumTotalWv($sumTotalWv) {
		$this->sumTotalWv = $sumTotalWv;
	}
	
	/**
	 * Returns the sumTotalWv
	 *
	 * @return float $sumTotalWv
	 */
	public function getSumTotalWv() {
		return $this->sumTotalWv;
	}


	/**
	 * Sets the sumTaggeld
	 *
	 * @param array $sumTaggeld
	 * @return void
	 */
	public function setSumTaggeld($sumTaggeld) {
		$this->sumTaggeld = serialize($sumTaggeld);
	}
	
	/**
	 * Returns the sumTaggeld
	 *
	 * @return array $sumTaggeld
	 */
	public function getSumTaggeld() {
		return unserialize($this->sumTaggeld);
	}

	/**
	 * Sets the sumNachtgeld
	 *
	 * @param array $sumNachtgeld
	 * @return void
	 */
	public function setSumNachtgeld($sumNachtgeld) {
		$this->sumNachtgeld = serialize($sumNachtgeld);
	}
	
	/**
	 * Returns the sumNachtgeld
	 *
	 * @return array $sumNachtgeld
	 */
	public function getSumNachtgeld() {
		return unserialize($this->sumNachtgeld);
	}

	/**
	 * Sets the sumKmgeld
	 *
	 * @param array $sumKmgeld
	 * @return void
	 */
	public function setSumKmgeld($sumKmgeld) {
		$this->sumKmgeld = serialize($sumKmgeld);
	}
	
	/**
	 * Returns the sumKmgeld
	 *
	 * @return array $sumKmgeld
	 */
	public function getSumKmgeld() {
		return unserialize($this->sumKmgeld);
	}


	/**
	 * Sets the exportBh
	 *
	 * @param integer $exportBh
	 * @return void
	 */
	public function setExportBh($exportBh) {
		$this->exportBh = $exportBh;
	}
	
	/**
	 * Returns the exportBh
	 *
	 * @return integer $exportBh
	 */
	public function getExportBh() {
		return $this->exportBh;
	}

	/**
	 * Sets the exportLv
	 *
	 * @param integer $exportLv
	 * @return void
	 */
	public function setExportLv($exportLv) {
		$this->exportLv = $exportLv;
	}
	
	/**
	 * Returns the exportLv
	 *
	 * @return integer $exportLv
	 */
	public function getExportLv() {
		return $this->exportLv;
	}

	/**
	 * Sets the exportWv
	 *
	 * @param integer $exportWv
	 * @return void
	 */
	public function setExportWv($exportWv) {
		$this->exportWv = $exportWv;
	}
	
	/**
	 * Returns the exportWv
	 *
	 * @return integer $exportWv
	 */
	public function getExportWv() {
		return $this->exportWv;
	}


	/*		END:	REISE	---------------------------------------------		*/


	/*		START:	VERKEHRSMITTEL --------------------------------------		*/

	/**
	 * verkehrsmittel
	 *
	 * @var string
	 */
	protected $verkehrsmittel;

	/**
	 * vkmBahn
	 *
	 * @var boolean
	 */
	protected $vkmBahn;

	/**
	 * vkmFlug
	 *
	 * @var boolean
	 */
	protected $vkmFlug;

	/**
	 * vkmMietauto
	 *
	 * @var boolean
	 */
	protected $vkmMietauto;

	/**
	 * vkmMitreise
	 *
	 * @var boolean
	 */
	protected $vkmMitreise;

	/**
	 * vkmPkwDienst
	 *
	 * @var boolean
	 */
	protected $vkmPkwDienst;

	/**
	 * vkmPkwPrivat
	 *
	 * @var boolean
	 */
	protected $vkmPkwPrivat;

	/**
	 * vkmSonstigeOeffentliche
	 *
	 * @var boolean
	 */
	protected $vkmSonstigeOeffentliche;

	/**
	 * Sets the verkehrsmittel
	 *
	 * @param array $verkehrsmittel
	 * @return void
	 */
	public function setVerkehrsmittel($verkehrsmittel) {
		$this->verkehrsmittel = serialize($verkehrsmittel);
	}
	
	/**
	 * Returns the verkehrsmittel
	 *
	 * @return array $verkehrsmittel
	 */
	public function getVerkehrsmittel() {
		return unserialize($this->verkehrsmittel);
	}

	/**
	 * Sets the vkmBahn
	 *
	 * @param boolean $vkmBahn
	 * @return void
	 */
	public function setVkmBahn($vkmBahn) {
		$this->vkmBahn = $vkmBahn;
	}
	
	/**
	 * Returns the vkmBahn
	 *
	 * @return boolean $vkmBahn
	 */
	public function getVkmBahn() {
		return $this->vkmBahn;
	}

	/**
	 * Sets the vkmFlug
	 *
	 * @param boolean $vkmFlug
	 * @return void
	 */
	public function setVkmFlug($vkmFlug) {
		$this->vkmFlug = $vkmFlug;
	}
	
	/**
	 * Returns the vkmFlug
	 *
	 * @return boolean $vkmFlug
	 */
	public function getVkmFlug() {
		return $this->vkmFlug;
	}

	/**
	 * Sets the vkmMietauto
	 *
	 * @param boolean $vkmMietauto
	 * @return void
	 */
	public function setVkmMietauto($vkmMietauto) {
		$this->vkmMietauto = $vkmMietauto;
	}
	
	/**
	 * Returns the vkmMietauto
	 *
	 * @return boolean $vkmMietauto
	 */
	public function getVkmMietauto() {
		return $this->vkmMietauto;
	}

	/**
	 * Sets the vkmMitreise
	 *
	 * @param boolean $vkmMitreise
	 * @return void
	 */
	public function setVkmMitreise($vkmMitreise) {
		$this->vkmMitreise = $vkmMitreise;
	}
	
	/**
	 * Returns the vkmMitreise
	 *
	 * @return boolean $vkmMitreise
	 */
	public function getVkmMitreise() {
		return $this->vkmMitreise;
	}

	/**
	 * Sets the vkmPkwDienst
	 *
	 * @param boolean $vkmPkwDienst
	 * @return void
	 */
	public function setVkmPkwDienst($vkmPkwDienst) {
		$this->vkmPkwDienst = $vkmPkwDienst;
	}
	
	/**
	 * Returns the vkmPkwDienst
	 *
	 * @return boolean $vkmPkwDienst
	 */
	public function getVkmPkwDienst() {
		return $this->vkmPkwDienst;
	}

	/**
	 * Sets the vkmPkwPrivat
	 *
	 * @param boolean $vkmPkwPrivat
	 * @return void
	 */
	public function setVkmPkwPrivat($vkmPkwPrivat) {
		$this->vkmPkwPrivat = $vkmPkwPrivat;
	}
	
	/**
	 * Returns the vkmPkwPrivat
	 *
	 * @return boolean $vkmPkwPrivat
	 */
	public function getVkmPkwPrivat() {
		return $this->vkmPkwPrivat;
	}

	/**
	 * Sets the vkmSonstigeOeffentliche
	 *
	 * @param boolean $vkmSonstigeOeffentliche
	 * @return void
	 */
	public function setVkmSonstigeOeffentliche($vkmSonstigeOeffentliche) {
		$this->vkmSonstigeOeffentliche = $vkmSonstigeOeffentliche;
	}
	
	/**
	 * Returns the vkmSonstigeOeffentliche
	 *
	 * @return boolean $vkmSonstigeOeffentliche
	 */
	public function getVkmSonstigeOeffentliche() {
		return $this->vkmSonstigeOeffentliche;
	}


	/**
	 * prepare Verkehrsmittel
	 *
	 * @return void
	 */
	public function prepareVerkehrsmittel() {
		$t2 = '';
		$t = $this->getVerkehrsmittel();
		//
		if( is_array($t) ){
			foreach ($t as $vkm => $v) {
				$t2 .= $v;
				switch($v) {
					case 'bahn':				$this->setVkmBahn(TRUE); break;
					case 'flug':				$this->setVkmFlug(TRUE); break;
					case 'mietauto':			$this->setVkmMietauto(TRUE); break;
					case 'mitreise':			$this->setVkmMitreise(TRUE); break;
					case 'pkwdienst':			$this->setVkmPkwDienst(TRUE); break;
					case 'pkwprivat':			$this->setVkmPkwPrivat(TRUE); break;
					case 'sonstigeoeffentlich':	$this->setVkmSonstigeOeffentliche(TRUE); break;
				}
			}
		}		
	}
	
	/**
	 * update verkehrsmittel
	 *
	 * @return void
	 */
	public function updateVerkehrsmittel() {
		$t = array();
//		$t[] = 'smo';
		//
		if( $this->getVkmBahn() ){
			$t[] = 'bahn';
		}
		if( $this->getVkmFlug() ){
			$t[] = 'flug';
		}
		if( $this->getVkmMietauto() ){
			$t[] = 'mietauto';
		}
		if( $this->getVkmMitreise() ){
			$t[] = 'mitreise';
		}
		if( $this->getVkmPkwDienst() ){
			$t[] = 'pkwdienst';
		}
		if( $this->getVkmPkwPrivat() ){
			$t[] = 'pkwprivat';
		}
		if( $this->getVkmSonstigeOeffentliche() ){
			$t[] = 'sonstigeoeffentlich';
		}
		//	
		$this->setVerkehrsmittel($t);
	}

	/*		END:	VERKEHRSMITTEL --------------------------------------		*/
	

	/*		START:	ABSCHNITTE	-----------------------------------------		*/

	/**
	 * abschnitteUpdate
	 *
	 * @var string
	 */
	protected $abschnitteUpdate = '';


	/**
	 * Sets the abschnitteUpdate
	 *
	 * @param string $abschnitteUpdate
	 * @return void
	 */
	public function setAbschnitteUpdate($abschnitteUpdate) {
		$this->abschnitteUpdate = $abschnitteUpdate;
	}
	
	/**
	 * Returns the abschnitteUpdate
	 *
	 * @return string $abschnitteUpdate
	 */
	public function getAbschnitteUpdate() {
		return $this->abschnitteUpdate;
	}



	/**
	 * get abschnitte
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconReiseabrechnung\Domain\Model\Abschnitt>
	 */
	public function getAbschnitte() {
		return $this->abschnitte;
	}
	
	/**
	 * add abschnitt
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Abschnitt
	 * @return void
	 */
	public function addAbschnitt(\NEXT\IconReiseabrechnung\Domain\Model\Abschnitt $abschnitt) {
		$this->abschnitte->attach($abschnitt);
	}	

	/**
	 * del abschnitt
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Abschnitt
	 * @return void
	 */
	public function delAbschnitt(\NEXT\IconReiseabrechnung\Domain\Model\Abschnitt $abschnitt) {
		$this->abschnitte->detach($abschnitt);
	}	

	/*		END:	ABSCHNITTE	-----------------------------------------		*/
	

	/*		START:	PASSENGERS	-----------------------------------------		*/

	/**
	 * passengersUpdate
	 *
	 * @var string
	 */
	protected $passengersUpdate = '';


	/**
	 * Returns the passengersUpdate
	 *
	 * @return string $passengersUpdate
	 */
	public function getPassengersUpdate() {
		return $this->passengersUpdate;
	}

	/**
	 * Sets the passengersUpdate
	 *
	 * @param string $passengersUpdate
	 * @return void
	 */
	public function setPassengersUpdate($passengersUpdate) {
		$this->passengersUpdate = $passengersUpdate;
	}


	/**
	 * get passengers
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconReiseabrechnung\Domain\Model\Passenger>
	 */
	public function getPassengers() {
		return $this->passengers;
	}
	
	/**
	 * add passenger
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Passenger
	 * @return void
	 */
	public function addPassenger(\NEXT\IconReiseabrechnung\Domain\Model\Passenger $passenger) {
		$this->passengers->attach($passenger);
	}	

	/**
	 * del passenger
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Passenger
	 * @return void
	 */
	public function delPassenger(\NEXT\IconReiseabrechnung\Domain\Model\Passenger $passenger) {
		$this->passengers->detach($passenger);
	}	

	/*		END:	PASSENGERS	-----------------------------------------		*/
	
	
	/*		START:	BELEGE	---------------------------------------------		*/

	/**
	 * belegePdf
	 *
	 * @var string
	 */
	protected $belegePdf = '';

	/**
	 * belegeUpdate
	 *
	 * @var string
	 */
	protected $belegeUpdate = '';


	/**
	 * Sets the belegePdf
	 *
	 * @param string $belegePdf
	 * @return void
	 */
	public function setBelegePdf($belegePdf) {
		$this->belegePdf = $belegePdf;
	}
	
	/**
	 * Returns the belegePdf
	 *
	 * @return string $belegePdf
	 */
	public function getBelegePdf() {
		return $this->belegePdf;
	}

	/**
	 * Sets the belegeUpdate
	 *
	 * @param string $belegeUpdate
	 * @return void
	 */
	public function setBelegeUpdate($belegeUpdate) {
		$this->belegeUpdate = $belegeUpdate;
	}
	
	/**
	 * Returns the belegeUpdate
	 *
	 * @return string $belegeUpdate
	 */
	public function getBelegeUpdate() {
		return $this->belegeUpdate;
	}


	/**
	 * get belege
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconReiseabrechnung\Domain\Model\beleg>
	 */
	public function getBelege() {
		return $this->belege;
	}
	
	/**
	 * add beleg
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Beleg
	 * @return void
	 */
	public function addBeleg(\NEXT\IconReiseabrechnung\Domain\Model\Beleg $beleg) {
		$this->belege->attach($beleg);
	}	

	/**
	 * del beleg
	 *
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\Beleg
	 * @return void
	 */
	public function delBeleg(\NEXT\IconReiseabrechnung\Domain\Model\Beleg $beleg) {
		$this->belege->detach($beleg);
	}	

	/*		END:	BELEG	---------------------------------------------		*/
	
	
	/*		START:	STATUS	---------------------------------------------		*/
	
	/**
	 * setStatusMitarbeiter
	 *
	 * @return void
	 */
	public function setStatusMitarbeiter () {
		$this->setStatus('MA');
	}
	
	/**
	 * setStatusSekretariat
	 *
	 * @return void
	 */
	public function setStatusSekretariat() {
		$this->setStatus('SEK');
	}
	
	/**
	 * setStatusGL
	 *
	 * @return void
	 */
	public function setStatusGL() {
		$this->setStatus('GL');
	}

	/**
	 * setStatusAbrechnen
	 *
	 * @return void
	 */
	public function setStatusAbrechnen() {
		$this->setStatus('ABRECHNEN');
	}

	/**
	 * setStatusErledigt
	 *
	 * @return void
	 */
	public function setStatusErledigt() {
		$this->setStatus('ERLEDIGT');
	}

    /**
     * setStatusDeleted
     *
     * @return void
     */
    public function setStatusDeleted() {
        $this->setStatus('GELÖSCHT');
    }

	
	/*		END:	STATUS	---------------------------------------------		*/


	/*		START:	SECRETARY / CHIEF	---------------------------------		*/

	/**
	 * secretary
	 *
	 * @var integer
	 */
	protected $secretary;
	
	/**
	 * Sets the secretary
	 *
	 * @param integer $secretary
	 * @return void
	 */
	public function setSecretary($secretary) {
		$this->secretary = $secretary;
	}
	
	/**
	 * Returns the secretary
	 *
	 * @return integer $secretary
	 */
	public function getSecretary() {
		return $this->secretary;
	}


	/**
	 * secretaryInfo
	 *
	 * @var string
	 */
	protected $secretaryInfo;
	
	/**
	 * Sets the secretaryInfo
	 *
	 * @param string $secretaryInfo
	 * @return void
	 */
	public function setSecretaryInfo($secretaryInfo) {
		$this->secretaryInfo = $secretaryInfo;
	}
	
	/**
	 * Returns the secretaryInfo
	 *
	 * @return string $secretaryInfo
	 */
	public function getSecretaryInfo() {
		return $this->secretaryInfo;
	}
	
	
	/**
	 * chief
	 *
	 * @var integer
	 */
	protected $chief;
	
	/**
	 * Sets the chief
	 *
	 * @param integer $chief
	 * @return void
	 */
	public function setChief($chief) {
		$this->chief = $chief;
	}
	
	/**
	 * Returns the chief
	 *
	 * @return integer $chief
	 */
	public function getChief() {
		return $this->chief;
	}
	
	/*		END:	SECRETARY / CHIEF	---------------------------------		*/	


	/*		START:	SUBMIT	---------------------------------------------		*/

	/**
	 * submit
	 *
	 * @var integer
	 */
	protected $submit;
	
	/**
	 * Sets the submit
	 *
	 * @param integer $submit
	 * @return void
	 */
	public function setSubmit ($submit) {
		$this->submit = $submit;
	}
	
	/**
	 * Returns the submit
	 *
	 * @return integer $submit
	 */
	public function getSubmit() {
		return $this->submit;
	}

    /**
     * @return string
     */
    public function getDeleteReason()
    {
        return $this->deleteReason;
    }

    /**
     * @param string $deleteReason
     */
    public function setDeleteReason($deleteReason)
    {
        $this->deleteReason = $deleteReason;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }


	
	/*		END:	SUBMIT	---------------------------------------------		*/


}
?>