<?php
namespace NEXT\IconReiseabrechnung\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Beleg extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
	/**
	 * personnr
	 *
	 * @var string
	 */
	protected $personnr;

	/**
	 * type
	 *
	 * @var integer
	 */
	protected $type;

	/**
	 * country
	 *
	 * @var string
	 */
	protected $country;

	/**
	 * currency
	 *
	 * @var string
	 */
	protected $currency;

	/**
	 * sum
	 *
	 * @var float
	 */
	protected $sum;

	/**
	 * sumEuroBrutto
	 *
	 * @var float
	 */
	protected $sumEuroBrutto;

	/**
	 * sumEuroNetto
	 *
	 * @var float
	 */
	protected $sumEuroNetto;

	/**
	 * taxfee
	 *
	 * @var float
	 */
	protected $taxfee;

	/**
	 * mitarbeiter
	 *
	 * @var boolean
	 */
	protected $mitarbeiter;	

	
	/**
	 * Sets the personnr
	 *
	 * @param string $personnr
	 * @return void
	 */
	public function setPersonnr($personnr) {
		$this->personnr = $personnr;
	}
	
	/**
	 * Returns the personnr
	 *
	 * @return string $personnr
	 */
	public function getPersonnr() {
		return $this->personnr;
	}
	
	/**
	 * Sets the type
	 *
	 * @param integer $type
	 * @return void
	 */
	public function setType($type) {
		$this->type = $type;
	}
	
	/**
	 * Returns the type
	 *
	 * @return integer $type
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * Sets the country
	 *
	 * @param string $country
	 * @return void
	 */
	public function setCountry($country) {
		$this->country = $country;
	}
	
	/**
	 * Returns the country
	 *
	 * @return string $country
	 */
	public function getCountry() {
		return $this->country;
	}
	
	/**
	 * Sets the currency
	 *
	 * @param string $currency
	 * @return void
	 */
	public function setCurrency($currency) {
		$this->currency = $currency;
	}
	
	/**
	 * Returns the currency
	 *
	 * @return string $currency
	 */
	public function getCurrency() {
		return $this->currency;
	}
	
	/**
	 * Sets the sum
	 *
	 * @param float $sum
	 * @return void
	 */
	public function setSum($sum) {
		$this->sum = $sum;
	}
	
	/**
	 * Returns the sum
	 *
	 * @return float $sum
	 */
	public function getSum() {
		return $this->sum;
	}
	
	/**
	 * Sets the sumEuroBrutto
	 *
	 * @param float $sumEuroBrutto
	 * @return void
	 */
	public function setSumEuroBrutto($sumEuroBrutto) {
		$this->sumEuroBrutto = $sumEuroBrutto;
	}
	
	/**
	 * Returns the sumEuroBrutto
	 *
	 * @return float $sumEuroBrutto
	 */
	public function getSumEuroBrutto() {
		return $this->sumEuroBrutto;
	}
	
	/**
	 * Sets the sumEuroNetto
	 *
	 * @param float $sumEuroNetto
	 * @return void
	 */
	public function setSumEuroNetto($sumEuroNetto) {
		$this->sumEuroNetto = $sumEuroNetto;
	}
	
	/**
	 * Returns the sumEuroNetto
	 *
	 * @return float $sumEuroNetto
	 */
	public function getSumEuroNetto() {
		return $this->sumEuroNetto;
	}
	
	/**
	 * Sets the taxfee
	 *
	 * @param float $taxfee
	 * @return void
	 */
	public function setTaxfee($taxfee) {
		$this->taxfee = $taxfee;
	}
	
	/**
	 * Returns the taxfee
	 *
	 * @return float $taxfee
	 */
	public function getTaxfee() {
		return $this->taxfee;
	}
	
	/**
	 * Sets the mitarbeiter
	 *
	 * @param boolean $mitarbeiter
	 * @return void
	 */
	public function setMitarbeiter($mitarbeiter) {
		$this->mitarbeiter = $mitarbeiter;
	}
	
	/**
	 * Returns the mitarbeiter
	 *
	 * @return boolean $mitarbeiter
	 */
	public function getMitarbeiter() {
		return $this->mitarbeiter;
	}
		
}
?>