<?php
namespace NEXT\IconReiseabrechnung\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class User extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
  /**
   * manager
   *
   * @var integer
   */
  protected $manager;

  /**
   * personnr
   *
   * @var string
   */
  protected $personnr;

  /**
   * gender
   *
   * @var string
   */
  protected $gender;

  /**
   * title
   *
   * @var string
   */
  protected $title;
  
  /**
   * firstname
   *
   * @var string
   * @validate NotEmpty
   */
  protected $firstname;

  /** @var int  */
  protected $deleted = 0;

  /**
   * lastname
   *
   * @var string
   * @validate NotEmpty
   */
  protected $lastname;

  /**
   * title_after
   *
   * @var string
   */
  protected $titleAfter;

  /**
   * email
   *
   * @var string
   * @validate EmailAddress
   */
  protected $email;


  /**
   * Sets the manager
   *
   * @param string $manager
   * @return void
   */
  public function setManager($manager) {
    $this->manager = $manager;
  }

  /**
   * Returns the manager
   *
   * @return string $manager
   */
  public function getManager() {
    return $this->manager;
  }

  /**
   * Sets the personnr
   *
   * @param string $personnr
   * @return void
   */
  public function setPersonnr($personnr) {
    $this->personnr = $personnr;
  }

  /**
   * Returns the personnr
   *
   * @return string $personnr
   */
  public function getPersonnr() {
    return $this->personnr;
  }

  /**
   * Sets the gender
   *
   * @param string $gender
   * @return void
   */
  public function setGender($gender) {
    $this->gender = $gender;
  }

  /**
   * Returns the gender
   *
   * @return string $gender
   */
  public function getGender() {
    return $this->gender;
  }

  /**
   * Sets the title
   *
   * @param string $title
   * @return void
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Returns the title
   *
   * @return string $title
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Sets the firstname
   *
   * @param string $firstname
   * @return void
   */
  public function setFirstname($firstname) {
    $this->firstname = $firstname;
  }

  /**
   * Returns the firstname
   *
   * @return string $firstname
   */
  public function getFirstname() {
    return $this->firstname;
  }

  /**
   * Sets the lastname
   *
   * @param string $lastname
   * @return void
   */
  public function setLastname($lastname) {
    $this->lastname = $lastname;
  }

  /**
   * Returns the lastname
   *
   * @return string $lastname
   */
  public function getLastname() {
    return $this->lastname;
  }

  /**
   * Sets the titleAfter
   *
   * @param string $titleAfter
   * @return void
   */
  public function setTitleAfter($titleAfter) {
    $this->titleAfter = $titleAfter;
  }

  /**
   * Returns the titleAfter
   *
   * @return string $titleAfter
   */
  public function getTitleAfter() {
    return $this->titleAfter;
  }

  /**
   * Sets the email
   *
   * @param string $email
   * @return void
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * Returns the email
   *
   * @return string $email
   */
  public function getEmail() {
    return $this->email;
  }
  
  /**
   * builds a formal mail Salute
   *
   * @return string
   */
  public function getBriefAnrede () {
	$t = '';
	if( strtolower($this->getGender()) == 'herr' ){
		$t .= 'Sehr geehrter Herr ';
	} elseif( strtolower($this->getGender()) == 'frau' ){
		$t .= 'Sehr geehrte Frau ';
	} else {
		$t .= 'Sehr geehrte(r) ';
	}
	if( $this->getTitle() ){ $t .= $this->getTitle() . ' '; }
	if( $this->getLastname() ){ $t .= $this->getLastname(); }
	return $t;
  }

  /**
   * builds a complete Formal Salute
   *
   * @return string
   */
  public function getVollAnrede () {
	  $t = '';
	  if( $this->getGender() ){ $t .= $this->getGender() . ', '; }
	  if( $this->getTitle() ){ $t .= $this->getTitle() . ' '; }
	  if( $this->getFirstname() ){ $t .= $this->getFirstname() . ' '; }
	  if( $this->getLastname() ){ $t .= $this->getLastname(); }
	  if( $this->getTitleAfter() ){ $t .= ', ' . $this->getTitleAfter(); }
	  return $t;
  }

    /**
     * @return int
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param int $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    public function getSelectLabel() {
        return sprintf('%s %s - %s %s', $this->getFirstname(), $this->getLastname(), $this->getPersonnr(), ($this->deleted ? ' - (AUS)' : ''));
    }

}
?>