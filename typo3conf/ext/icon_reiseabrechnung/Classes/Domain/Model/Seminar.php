<?php
namespace NEXT\IconReiseabrechnung\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Seminar extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
  /**
   * personnr
   *
   * @var string
   */
  protected $personnr;

	/**
	 * firstname
	 *
	 * @var string
	 */
	protected $firstname;

	/**
	 * lastname
	 *
	 * @var string
	 */
	protected $lastname;

  /**
   * title
   *
   * @var string
   */
  protected $title;
  
  /**
   * datetime
   *
   * @var integer
   */
  protected $datetime;

  /**
   * place
   *
   * @var string
   */
  protected $place;

  /**
   * butto
   *
   * @var float
   */
  protected $brutto;

  /**
   * netto
   *
   * @var float
   */
  protected $netto;


  /**
   * Sets the personnr
   *
   * @param string $personnr
   * @return void
   */
  public function setPersonnr($personnr) {
    $this->personnr = $personnr;
  }

  /**
   * Returns the personnr
   *
   * @return string $personnr
   */
  public function getPersonnr() {
    return $this->personnr;
  }

	/**
	 * Sets the firstname
	 *
	 * @param string $firstname
	 * @return void
	 */
	public function setFirstname($firstname) {
		$this->firstname = $firstname;
	}
	
	/**
	 * Returns the firstname
	 *
	 * @return string $firstname
	 */
	public function getFirstname() {
		return $this->firstname;
	}
	
	/**
	 * Sets the lastname
	 *
	 * @param string $lastname
	 * @return void
	 */
	public function setLastname($lastname) {
		$this->lastname = $lastname;
	}
	
	/**
	 * Returns the lastname
	 *
	 * @return string $lastname
	 */
	public function getLastname() {
		return $this->lastname;
	}

  /**
   * Sets the title
   *
   * @param string $title
   * @return void
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Returns the title
   *
   * @return string $title
   */
  public function getTitle() {
    return $this->title;
  }


  /**
   * Sets the datetime
   *
   * @param integer $datetime
   * @return void
   */
  public function setDatetime($datetime) {
    $this->datetime = $datetime;
  }

  /**
   * Returns the datetime
   *
   * @return integer $datetime
   */
  public function getDatetime() {
    return $this->datetime;
  }

  /**
   * Sets the place
   *
   * @param string $place
   * @return void
   */
  public function setPlace($place) {
    $this->place = $place;
  }

  /**
   * Returns the place
   *
   * @return string $place
   */
  public function getPlace() {
    return $this->place;
  }

  /**
   * Sets the brutto
   *
   * @param float $brutto
   * @return void
   */
  public function setBrutto($brutto) {
    $this->brutto = $brutto;
  }

  /**
   * Returns the brutto
   *
   * @return float $brutto
   */
  public function getBrutto() {
    return $this->brutto;
  }

  /**
   * Sets the netto
   *
   * @param float $netto
   * @return void
   */
  public function setNetto($netto) {
    $this->netto = $netto;
  }

  /**
   * Returns the netto
   *
   * @return float $netto
   */
  public function getNetto() {
    return $this->netto;
  }

}
?>