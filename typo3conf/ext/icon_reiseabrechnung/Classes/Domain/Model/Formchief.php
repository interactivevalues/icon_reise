<?php
namespace NEXT\IconReiseabrechnung\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Formchief extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

  /**
   * submit
   *
   * @var integer
   */
  protected $submit;
  
  /**
   * Sets the submit
   *
   * @param integer $submit
   * @return void
   */
  public function setSubmit ($submit) {
    $this->submit = $submit;
  }

  /**
   * Returns the submit
   *
   * @return integer $submit
   */
  public function getSubmit() {
    return $this->submit;
  }


  /**
   * secretary
   *
   * @var integer
   */
  protected $secretary;
  
  /**
   * Sets the secretary
   *
   * @param integer $secretary
   * @return void
   */
  public function setSecretary($secretary) {
    $this->secretary = $secretary;
  }

  /**
   * Returns the secretary
   *
   * @return integer $secretary
   */
  public function getSecretary() {
    return $this->secretary;
  }


  /**
   * secretaryInfo
   *
   * @var string
   */
  protected $secretaryInfo;
  
  /**
   * Sets the secretaryInfo
   *
   * @param string $secretaryInfo
   * @return void
   */
  public function setSecretaryInfo($secretaryInfo) {
    $this->secretaryInfo = $secretaryInfo;
  }

  /**
   * Returns the secretaryInfo
   *
   * @return string $secretaryInfo
   */
  public function getSecretaryInfo() {
    return $this->secretaryInfo;
  }

}
?>