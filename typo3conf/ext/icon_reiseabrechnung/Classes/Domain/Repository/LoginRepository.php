<?php
namespace NEXT\IconReiseabrechnung\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class LoginRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/*
	 * isLoggedIn
	 *
	 * @param string $mode
	 * @return boolean
	 */
	public function isLoggedIn ( $mode = '' ) {
		//
		$t = $this->getSessionKeyBase($mode);
		//
		if( $GLOBALS['TSFE']->fe_user->getKey('ses', $t.'Status') ){
			$loginStatus = $GLOBALS['TSFE']->fe_user->getKey('ses', $t.'Status');
			if( $loginStatus===true ){
				//
				return true;
			}
		}
/*
		if( $GLOBALS['TSFE']->fe_user->getKey('ses', 'iconReiseLoginStatus') &&
			$GLOBALS['TSFE']->fe_user->getKey('user', 'iconReiseLoginUser') ){
			$loginStatus = $GLOBALS['TSFE']->fe_user->getKey('ses', 'iconReiseLoginStatus');
			$loginUser = unserialize( $GLOBALS['TSFE']->fe_user->getKey('user', 'iconReiseLoginUser') );
			if( $loginStatus===true && $loginUser ){
				//
				return true;
			}
		}
*/
		//
		return false;
	}
	
	
	/**
	 * getLoggedInUser
	 *
	 * @param string $mode
	 * @return mixed
	 */
	public function getLoggedInUser ( $mode='' ) {
		//
		$t = $this->getSessionKeyBase($mode);
		//
		return unserialize( $GLOBALS['TSFE']->fe_user->getKey('ses', $t.'User') );
	} 
	
	/*
	 * writeLoginSession
	 *
	 * @param string $mode
	 * @param \NEXT\IconReiseabrechnung\Domain\Model\User $user
	 * @return void
	 */
	public function writeSessionLogin ( $mode='', $user ) {
		//
		$t = $this->getSessionKeyBase($mode);
		//
		$GLOBALS['TSFE']->fe_user->setKey('ses', $t.'Status', true);
		$GLOBALS['TSFE']->fe_user->setKey('ses', $t.'User', serialize($user));
		$GLOBALS['TSFE']->fe_user->storeSessionData();		
	}
	
	
	/*
	 * clearLoginSession
	 * 
	 * @param srting $mode
	 * @return void
	 */
	public function clearSessionLogin ( $mode='' ) {
		//
		$t = $this->getSessionKeyBase($mode);
		//
		$GLOBALS['TSFE']->fe_user->setKey('ses', $t.'Status', NULL);
		$GLOBALS['TSFE']->fe_user->setKey('ses', $t.'User', NULL);
		$GLOBALS['TSFE']->fe_user->storeSessionData();		
	}
	
	/**
	 * getSessionKeyBase
	 *
	 * @param string $mode
	 * @return string
	 */
	private function getSessionKeyBase ( $mode='' ) {
		//
		return 'iconReiseLogin-' . $mode;
	}
}
?>