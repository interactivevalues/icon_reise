<?php
namespace NEXT\IconReiseabrechnung\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use NEXT\IconReiseabrechnung\Domain\Model\Reise;

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ReiseRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	// Example for repository wide settings
	public function initializeObject() {
		/** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
		$querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		// go for $defaultQuerySettings = $this->createQuery()->getQuerySettings(); if you want to make use of the TS persistence.storagePid with defaultQuerySettings(), see #51529 for details
		
		// don't add the pid constraint
		$querySettings->setRespectStoragePage(FALSE);
		// set the storagePids to respect
//		$querySettings->setStoragePageIds(array(1, 26, 989));
		
		// don't add fields from enablecolumns constraint
		// this function is deprecated!
//		$querySettings->setRespectEnableFields(FALSE);
		
		// define the enablecolumn fields to be ignored
		// if nothing else is given, all enableFields are ignored
//		$querySettings->setIgnoreEnableFields(TRUE);       
		// define single fields to be ignored
//		$querySettings->setEnableFieldsToBeIgnored(array('disabled','starttime'));
		
		// add deleted rows to the result
//		$querySettings->setIncludeDeleted(TRUE);
		
		// don't add sys_language_uid constraint
//		$querySettings->setRespectSysLanguage(FALSE);
		
		// perform translation to dedicated language
//		$querySettings->setSysLanguageUid(42);
		//
		$this->setDefaultQuerySettings($querySettings);
	}


	/**
	 * findByUid_BH
	 *
	 * 
	 * @param array $ids
	 * @return mixed
	 */
	public function findByUid_BH( $ids ) {
		//
		$status = array('ABRECHNEN');
		//
		$query = $this->createQuery();
		//
		$query->matching(
			$query->logicalAnd(
				$query->equals('status', $status),
				$query->equals('export_bh', 0),
				$query->in('uid', $ids)
			)
		);
		//
		$t = array( 
			'personnr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
/*
			'startdatum' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
*/
		);
/*
		$t = 					array( 'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING );
*/
		$query->setOrderings( $t );
		//
		return $query->execute();
	}


	/**
	 * findByUid_LV
	 *
	 * 
	 * @param array $ids
	 * @return mixed
	 */
	public function findByUid_LV( $ids ) {
		//
		$status = array('ABRECHNEN');
		//
		$query = $this->createQuery();
		//
		$query->matching(
			$query->logicalAnd(
				$query->equals('status', $status),
				$query->equals('export_lv', 0),
				$query->in('uid', $ids)
			)
		);
		//
		$t = array( 
			'personnr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
/*
			'startdatum' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
*/
		);
/*
		$t = 					array( 'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING );
*/
		$query->setOrderings( $t );
		//
		return $query->execute();
	}


	/**
	 * findForManager
	 *
	 * 
	 * @param array $status
	 * @param string $order
	 * @param string $personnr
	 * @return mixed
	 */
	public function findForManager( $status, $order='', $personnr='' ) {
		$query = $this->createQuery();
		//
		if( $personnr!='' ){
			$query->matching(
				$query->logicalAnd(
					$query->in('status', $status),
					$query->equals('personnr', $personnr)
				)
			);
		} else {
			$query->matching(
				$query->in('status', $status)
			);
		}
		//
		if( $order!='' ){
			if( $order=='DESC' ){
				$t = 					array( 'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING );
			} else {
				$t = 					array( 'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING );
			}
			$query->setOrderings( $t );
		}
		//
		return $query->execute();
	}

    public function findForManagerDeleted( $status, $order='', $personnr='' ) {
        $query = $this->createQuery();

    #    $query->getQuerySettings()->setEnableFieldsToBeIgnored(['deleted']);
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setIncludeDeleted(true);
        //
        if( $personnr!='' ){
            $query->matching(
                $query->logicalAnd(
                    $query->in('status', $status),
                    $query->equals('personnr', $personnr),
                    $query->equals('deleted', 1)
                )
            );
        } else {
            $query->matching(
                $query->logicalAnd(
                    $query->in('status', $status),
                    $query->equals('deleted', 1)
                )
            );
        }
        //
        if( $order!='' ){
            if( $order=='DESC' ){
                $t = 					array( 'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING );
            } else {
                $t = 					array( 'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING );
            }
            $query->setOrderings( $t );
        }
        //
        return $query->execute();
    }


    /**
     * @param int $id
     * @return Reise
     */
    public function findDeletedById($id) {
        $query = $this->createQuery();


        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setIncludeDeleted(true);
        $query->matching(
            $query->logicalAnd(
                $query->equals('uid', $id)
            )
        );
        return $query->execute()->getFirst();
    }


	/**
	 * findForManagerRueckerstattung
	 *
	 * 
	 * @param string $personnr
	 * @param integer $grund
	 * @return mixed
	 */
	public function findForManagerRueckerstattung( $personnr='', $grund='' ) {
		$query = $this->createQuery();
		//
		$status = array('ERLEDIGT');
		$order = 'DESC';
		//
		$query->matching(
			$query->logicalAnd(
				$query->in('status', $status),
				$query->equals('personnr', $personnr),
				$query->equals('grund', $grund)
			)
		);
		//
		if( $order!='' ){
			if( $order=='DESC' ){
				$t = 					array( 'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING );
			} else {
				$t = 					array( 'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING );
			}
			$query->setOrderings( $t );
		}
		//
		return $query->execute();
	}


	// Example for a function setup changing query settings
	/**
	 * getByPersonnr
	 *
	 * @param string $personnr
	 * @param string $order
	 * @param string $status
	 */
	public function getByPersonnr( $personnr, $order='', $status='' ) {
		$query = $this->createQuery();
		//
		if( $status!='' ){
			$query->matching(
				$query->logicalAnd(
					$query->equals('personnr', $personnr),
					$query->equals('status', $status)
				)
			);
		} else {
			$query->matching(
				$query->equals('personnr', $personnr)
			);
		}

		//
		if( $order=='DESC' ){
			$query->setOrderings(
				array(
					'uid' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
				)
			);
		}

		//
		return $query->execute();
	}


	/**
	 * getNewReisenr
	 *
	 * @param integer year
	 * return integer
	 */
	public function getNewReisenr( $year=0 ) {
		//
		if( $year < 2011 || $year > 2100 ){
			//	somit werden bestimmte JAHRE fix ausgeschloßen!
			return 0;
		}
		//	reduziere auf zweistellige zahl / -2000
		$year -= 2000;
		//	definieren START - ENDE - BEREICH
		$nr_S = $year * 100000;
		$nr_E = ($year+1) * 100000;
		//	initiere QUERY
		$query = $this->createQuery();
		//	definiren QUERY - SUCHE
		$query->matching(
			$query->logicalAnd(
				$query->greaterThan('reisenr', $nr_S),
				$query->lessThan('reisenr', $nr_E)
			)
		);
		//	definiere QUERY - SORTIERUNG
		$query->setOrderings(
			array(
				'reisenr' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
			)
		);
		//	führe QUERY aus und HOLE ERSTES ERGEBNIS ZURÜCK
		$reise = $query->execute()->getFirst();
		//
		if( $reise ){
			//	LESE REISNR von erstem ERGEBNIS
			$nr = $reise->getReisenr();
			//	ERHÖHRE UM + 1
			$nr += 1; 
		} else {
			//	KEIN ERGEBNIS, SOMIT STARTE BEI 1
			$nr = $nr_S + 1;
		}
		//	LIEFERE NEUE REISENR ZURÜCK
		return $nr;		
	}

	
	/**
	 * buildCSVLine_BH
	 *
	 * return string
	 */
	public function buildCSVLine_BH ($personnr, $name, $reisenr, $ziel, $grund, $klientprojekt, $type, $starttag, $brutto, $netto, $taxfee, $country, $mwst, $konto) {
		//
		$t_array = array();
		//
		$t_array[] = $personnr;
		$t_array[] = $name;
		$t_array[] = $reisenr;
		$t_array[] = $ziel;
		$t_array[] = $grund;
		$t_array[] = $klientprojekt;
		$t_array[] = $type;
		$t_array[] = $starttag;
		$t_array[] = number_format($brutto, 2, ',', '' );
		$t_array[] = number_format($netto, 2, ',', '' );
		$t_array[] = $taxfee;
		$t_array[] = $country;
		$t_array[] = number_format( ( $country=='AT' ? $mwst : 0.00 ), 2, ',', '' );
		$t_array[] = number_format(( $country=='DE' ? $mwst : 0.00 ), 2, ',', '' );
		$t_array[] = number_format( ( ($country!='AT' && $country!='DE') ? $mwst : 0.00 ), 2, ',', '' );
		$t_array[] = $konto;
		//	BUILD LINE
		$csv = '"' . implode('";"',$t_array) . '"' . PHP_EOL;
		//
		return $csv;
	}
	
	
	/**
	 * getReisegrund
	 *
	 * param integer id
	 * return string
	 */
	public function getReisegrund ( $id ) {
		$t = $this->getOptionsReisegrund();
		return $t[$id];
	}


	/**
	 * getOptionsReisegrund
	 *
	 * return
	 */
	public function getOptionsReisegrund () {
		$t = array();
		$t[1] = 'Besprechung / Interessensvertretung / usw.';
		$t[2] = 'Vortragender (Tax Academy)';
		$t[3] = 'Vortragender (Klienten-Workshop)';
		$t[4] = 'Teilnehmer (Seminar)';
		return $t;		
	}
	

	/**
	 * getKontoReisegrund
	 *
	 * param integer grund
	 * return string
	 */
	public function getKontoReisegrund ( $grund ) {
		//
		switch($grund){
			case 1: case '1':	$t = '7330'; break;		//	'Besprechung / Interessensvertretung / usw.'; break;
			case 2: case '2':	$t = '7660'; break;		//	'Vortragender (Tax Academy)'; break;
			case 3: case '3':	$t = '7660'; break;		//	'Vortragender (Klienten-Workshop)'; break;
			case 4: case '4':	$t = '6710'; break; 	//	'Teilnehmer (Seminar)'; break;
			default: 			$t = ''; break;
		}
		return $t;
	}


	/**
	 * getBelegType
	 *
	 * param integer id
	 * return string
	 */
	public function getBelegType ( $id ) {
		$t = $this->getOptionsBelegType();
		return $t[$id];
	}


	/**
	 * getOptionsBelegType
	 *
	 * return array
	 */
	public function getOptionsBelegType () {
		$t = array();
		$t['1'] = 'Bahnticket [steuerfrei]';
		$t['13'] = 'Bahnticket Businesscard [steuerfrei]';
		$t['2'] = 'Benzin (nicht für Privat-PKW) [steuerfrei]';
		$t['3'] = 'Bewirtung [steuerfrei]';
		$t['4'] = 'Flugticket [steuerfrei]';
		$t['5'] = 'Hotel [steuerfrei]';
		$t['6'] = 'Maut [steuerpflichtig]';
		$t['7'] = 'Mietwagen [steuerfrei]';
		$t['8'] = 'öffentliche Verkehrsmittel [steuerfrei]';
		$t['9'] = 'Parkgebühr [steuerpflichtig]';
		$t['10'] = 'Seminargebühr [steuerfrei]';
		$t['11'] = 'Taxi [steuerfrei]';
		$t['12'] = 'Umbuchungsgebühr [steuerfrei]';
		$t['20'] = 'Sonstiger Aufwand [steuerfrei]';
		return $t;
	}


	/**
	 * getOptionsNaechtigung
	 *
	 * return array
	 */
	public function getOptionsNaechtigung () {
		$t = array();
		$t['0'] = 'Keine';
		$t['1'] = 'Beleg';
		$t['2'] = 'private Übernachtung';
		$t['3'] = 'durch ICON';
		$t['4'] = 'durch Klient';
		return $t;
	}
	

	/**
	 * getOptionsCountries
	 *
	 * return array
	 */
	public function getOptionsCountries () {
		//
		$countries = $this->getCountryData();
		$t = array();
		foreach( $countries as $k => $v){
			$t[$k] = $v['label'];
		}
		//
		return $t;
	}


	/**
	 * getTagsatzLand
	 *
	 * @param string country
	 * return float
	 */
	public function getTagsatzLand ( $country ){
		$t = $this->getCountryData();
		return $t[$country]['TS'];
	}
	

	/**
	 * getNachtsatzLand
	 *
	 * @param string country
	 * return float
	 */
	public function getNachtsatzLand ( $country ){
		$t = $this->getCountryData();
		return $t[$country]['NS'];
	}
	

	/**
	 * getCountryData
	 *
	 * return array
	 */
	public function getCountryData () {
		//
		$t = array();
		//
		$t['AT'] = 					array( 'label' => "Österreich",							'TS' => 26.40,	'NS' => 15.00 );
		$t['DE'] = 					array( 'label' => "Deutschland",						'TS' => 35.30,	'NS' => 27.90 );
		$t['DE-Grenze'] = 			array( 'label' => "Deutschland - Grenzorte",			'TS' => 30.70,	'NS' => 18.10 );
		$t['CZ'] = 					array( 'label' => "Tschechien",							'TS' => 31.00,	'NS' => 24.40 );
		$t['CZ-Moldau'] = 			array( 'label' => "Tschechien - Moldau",				'TS' => 36.80,	'NS' => 31.00 );
		$t['CZ-Grenze'] = 			array( 'label' => "Tschechien - Grenzorte",				'TS' => 27.90,	'NS' => 15.90 );
		$t['SK'] = 					array( 'label' => "Slowakei",							'TS' => 27.90,	'NS' => 15.90 );
		$t['SK-Grenze'] = 			array( 'label' => "Slowakei - Pressburg",				'TS' => 31.00,	'NS' => 24.40 );
		$t['HU'] = 					array( 'label' => "Ungarn",								'TS' => 26.60,	'NS' => 26.60 );
		$t['HU-Budapet'] = 			array( 'label' => "Ungarn - Budapest",					'TS' => 31.00,	'NS' => 26.60 );
		$t['HU-Grenze'] = 			array( 'label' => "Ungarn - Grenzorte",					'TS' => 26.60,	'NS' => 18.10 );
		$t['SI'] = 					array( 'label' => "Slowenien",							'TS' => 31.00,	'NS' => 23.30 );
		$t['SI-Grenze'] = 			array( 'label' => "Slowenien - Grenzorte",				'TS' => 27.90,	'NS' => 15.90 );
		$t['IT'] = 					array( 'label' => "Italien",							'TS' => 35.80,	'NS' => 27.90 );
		$t['TI-RomMailand'] =		array( 'label' => "Italien - Rom und Mailand",			'TS' => 40.60,	'NS' => 36.40 );
		$t['IT-Grenze'] =			array( 'label' => "Italien - Grenzorte",				'TS' => 30.70,	'NS' => 18.10 );
		$t['LI'] = 					array( 'label' => "Liechtenstein",						'TS' => 30.70,	'NS' => 18.10 );
		$t['CH'] = 					array( 'label' => "Schweiz",							'TS' => 36.80,	'NS' => 32.70 );
		$t['CH-Grenze'] = 			array( 'label' => "Schweiz - Grenzorte",				'TS' => 30.70,	'NS' => 18.10 );
	
		$t['AF'] = 					array( 'label' => "Afghanistan",						'TS' => 31.80,	'NS' => 27.70 );
		$t['EG'] = 					array( 'label' => "Ägypten",							'TS' => 37.90,	'NS' => 41.40 );
		$t['AL'] = 					array( 'label' => "Albanien",							'TS' => 27.90,	'NS' => 20.90 );
		$t['DZ'] = 					array( 'label' => "Algerien",							'TS' => 41.40,	'NS' => 27.00 );
		$t['AO'] = 					array( 'label' => "Angola",								'TS' => 43.60,	'NS' => 41.40 );
		$t['AR'] = 					array( 'label' => "Argentinien",						'TS' => 33.10,	'NS' => 47.30 );
		$t['AM'] = 					array( 'label' => "Armenien",							'TS' => 36.80,	'NS' => 31.00 );
		$t['AZ'] = 					array( 'label' => "Aserbaidschan",						'TS' => 36.80,	'NS' => 31.00 );
		$t['ET'] = 					array( 'label' => "Äthiopien",							'TS' => 37.90,	'NS' => 41.40 );
		$t['AU'] = 					array( 'label' => "Australien",							'TS' => 47.30,	'NS' => 39.90 );
		$t['BS'] = 					array( 'label' => "Bahamas",							'TS' => 48.00,	'NS' => 30.50 );
		$t['BH'] = 					array( 'label' => "Bahrain",							'TS' => 54.10,	'NS' => 37.50 );
		$t['BD'] = 					array( 'label' => "Bangladesch",						'TS' => 31.80,	'NS' => 34.20 );
		$t['BB'] = 					array( 'label' => "Barbados",							'TS' => 51.00,	'NS' => 43.60 );
		$t['BY'] = 					array( 'label' => "Belarus",							'TS' => 36.80,	'NS' => 31.00 );
		$t['BE'] = 					array( 'label' => "Belgien",							'TS' => 35.30,	'NS' => 22.70 );
		$t['BE-Bruessel'] = 		array( 'label' => "Belgien - Brüssel",					'TS' => 41.40,	'NS' => 32.00 );
		$t['BJ'] = 					array( 'label' => "Benin",								'TS' => 36.20,	'NS' => 26.60 );
		$t['BO'] = 					array( 'label' => "Bolivien",							'TS' => 26.60,	'NS' => 25.10 );
		$t['BA'] = 					array( 'label' => "Bosnien und Herzegowina",			'TS' => 31.00,	'NS' => 23.30 );
		$t['BR'] = 					array( 'label' => "Brasilien",							'TS' => 33.10,	'NS' => 36.40 );
		$t['BN'] = 					array( 'label' => "Brunei",								'TS' => 33.10,	'NS' => 42.10 );
		$t['BG'] = 					array( 'label' => "Bulgarien",							'TS' => 31.00,	'NS' => 22.70 );
		$t['BF'] = 					array( 'label' => "Burkina Faso",						'TS' => 39.20,	'NS' => 21.10 );
		$t['BI'] = 					array( 'label' => "Burundi",							'TS' => 37.90,	'NS' => 37.90 );
		$t['CL'] = 					array( 'label' => "Chile",								'TS' => 37.50,	'NS' => 36.40 );
		$t['CN'] = 					array( 'label' => "China",								'TS' => 35.10,	'NS' => 30.50 );
		$t['CR'] = 					array( 'label' => "Costa Rica",							'TS' => 31.80,	'NS' => 31.80 );
		$t['CI'] = 					array( 'label' => "Côte d'Ivoire",						'TS' => 39.20,	'NS' => 32.00 );
		$t['DK'] = 					array( 'label' => "Dänemark",							'TS' => 41.40,	'NS' => 41.40 );
		$t['CD'] = 					array( 'label' => "Demokratische Rep. Kongo",			'TS' => 47.30,	'NS' => 33.10 );
		$t['DO'] = 					array( 'label' => "Dominikanische Republik",			'TS' => 39.20,	'NS' => 43.60 );
		$t['DJ'] = 					array( 'label' => "Dschibuti",							'TS' => 45.80,	'NS' => 47.30 );
		$t['EC'] = 					array( 'label' => "Ecuador",							'TS' => 26.60,	'NS' => 21.60 );
		$t['SV'] = 					array( 'label' => "El Salvador",						'TS' => 31.80,	'NS' => 26.20 );
		$t['EE'] = 					array( 'label' => "Estland",							'TS' => 36.80,	'NS' => 31.00 );
		$t['FI'] = 					array( 'label' => "Finnland",							'TS' => 41.40,	'NS' => 41.40 );
		$t['FR'] = 					array( 'label' => "Frankreich",							'TS' => 32.70,	'NS' => 24.00 );
		$t['FR-ParisStrassburg'] =	array( 'label' => "Frankreich - Paris und Straßburg",	'TS' => 35.80,	'NS' => 32.70 );
		$t['GA'] = 					array( 'label' => "Gabun",								'TS' => 45.80,	'NS' => 39.90 );
		$t['GM'] = 					array( 'label' => "Gambia",								'TS' => 43.60,	'NS' => 30.10 );
		$t['GE'] = 					array( 'label' => "Georgien",							'TS' => 36.80,	'NS' => 31.00 );
		$t['GH'] = 					array( 'label' => "Ghana",								'TS' => 43.60,	'NS' => 30.10 );
		$t['GR'] = 					array( 'label' => "Griechenland",						'TS' => 28.60,	'NS' => 23.30 );
		$t['GB'] = 					array( 'label' => "Großbritannien und Nordirland",		'TS' => 36.80,	'NS' => 36.40 );
		$t['GB-London'] = 			array( 'label' => "Großbritannien - London",			'TS' => 41.40,	'NS' => 41.40 );
		$t['GT'] = 					array( 'label' => "Guatemala",							'TS' => 31.80,	'NS' => 31.80 );
		$t['GN'] = 					array( 'label' => "Guinea",								'TS' => 43.60,	'NS' => 30.10 );
		$t['GY'] = 					array( 'label' => "Guyana",								'TS' => 39.20,	'NS' => 34.20 );
		$t['HT'] = 					array( 'label' => "Haiti",								'TS' => 39.20,	'NS' => 27.70 );
		$t['HN'] = 					array( 'label' => "Honduras",							'TS' => 31.80,	'NS' => 27.00 );
		$t['HK'] = 					array( 'label' => "Hongkong",							'TS' => 46.40,	'NS' => 37.90 );
		$t['IN'] = 					array( 'label' => "Indien",								'TS' => 31.80,	'NS' => 39.90 );
		$t['ID'] = 					array( 'label' => "Indonesien",							'TS' => 39.20,	'NS' => 32.00 );
		$t['IQ'] = 					array( 'label' => "Irak",								'TS' => 54.10,	'NS' => 36.40 );
		$t['IR'] = 					array( 'label' => "Iran",								'TS' => 37.10,	'NS' => 29.00 );
		$t['IE'] = 					array( 'label' => "Irland",								'TS' => 36.80,	'NS' => 33.10 );
		$t['IS'] = 					array( 'label' => "Island",								'TS' => 37.90,	'NS' => 31.40 );
		$t['IL'] = 					array( 'label' => "Israel",								'TS' => 37.10,	'NS' => 32.50 );
		$t['JM'] = 					array( 'label' => "Jamaika",							'TS' => 47.10,	'NS' => 47.10 );
		$t['JP'] = 					array( 'label' => "Japan",								'TS' => 65.60,	'NS' => 42.90 );
		$t['YE'] = 					array( 'label' => "Jemen",								'TS' => 54.10,	'NS' => 37.50 );
		$t['JO'] = 					array( 'label' => "Jordanien",							'TS' => 37.10,	'NS' => 32.50 );
		$t['YU'] = 					array( 'label' => "Jugoslawien",						'TS' => 31.00,	'NS' => 23.30 );
		$t['KH'] = 					array( 'label' => "Kambodscha",							'TS' => 31.40,	'NS' => 31.40 );
		$t['CM'] = 					array( 'label' => "Kamerun",							'TS' => 45.80,	'NS' => 25.30 );
		$t['CA'] = 					array( 'label' => "Kanada",								'TS' => 41.00,	'NS' => 34.20 );
		$t['CV'] = 					array( 'label' => "Kap Verde",							'TS' => 27.90,	'NS' => 19.60 );
		$t['KZ'] = 					array( 'label' => "Kasachstan",							'TS' => 36.80,	'NS' => 31.00 );
		$t['QA'] = 					array( 'label' => "Katar",								'TS' => 54.10,	'NS' => 37.50 );
		$t['KE'] = 					array( 'label' => "Kenia",								'TS' => 34.90,	'NS' => 32.00 );
		$t['KG'] = 					array( 'label' => "Kirgisistan",						'TS' => 36.80,	'NS' => 31.00 );
		$t['CO'] = 					array( 'label' => "Kolumbien",							'TS' => 33.10,	'NS' => 35.10 );
		$t['KP'] = 					array( 'label' => "Korea. Demokratische Volksrepublik",	'TS' => 32.50,	'NS' => 32.50 );
		$t['KR'] = 					array( 'label' => "Korea. Republik",					'TS' => 45.30,	'NS' => 32.50 );
		$t['HR'] = 					array( 'label' => "Kroatien",							'TS' => 31.00,	'NS' => 23.30 );
		$t['CU'] = 					array( 'label' => "Kuba",								'TS' => 54.10,	'NS' => 27.70 );
		$t['KW'] = 					array( 'label' => "Kuwait",								'TS' => 54.10,	'NS' => 37.50 );
		$t['LA'] = 					array( 'label' => "Laos",								'TS' => 31.40,	'NS' => 31.40 );
		$t['LV'] = 					array( 'label' => "Lettland",							'TS' => 36.80,	'NS' => 31.00 );
		$t['LB'] = 					array( 'label' => "Libanon",							'TS' => 31.80,	'NS' => 35.10 );
		$t['LR'] = 					array( 'label' => "Liberia",							'TS' => 39.20,	'NS' => 41.40 );
		$t['LY'] = 					array( 'label' => "Libyen",								'TS' => 43.60,	'NS' => 36.40 );
		$t['LT'] = 					array( 'label' => "Litauen",							'TS' => 36.80,	'NS' => 31.00 );
		$t['LU'] = 					array( 'label' => "Luxemburg",							'TS' => 35.30,	'NS' => 22.70 );
		$t['MG'] = 					array( 'label' => "Madagaskar",							'TS' => 36.40,	'NS' => 36.40 );
		$t['MW'] = 					array( 'label' => "Malawi",								'TS' => 32.70,	'NS' => 32.70 );
		$t['MY'] = 					array( 'label' => "Malaysia",							'TS' => 43.60,	'NS' => 45.10 );
		$t['ML'] = 					array( 'label' => "Mali",								'TS' => 39.20,	'NS' => 31.20 );
		$t['MT'] = 					array( 'label' => "Malta",								'TS' => 30.10,	'NS' => 30.10 );
		$t['MA'] = 					array( 'label' => "Marokko",							'TS' => 32.70,	'NS' => 21.80 );
		$t['MR'] = 					array( 'label' => "Mauretanien",						'TS' => 33.80,	'NS' => 31.20 );
		$t['MU'] = 					array( 'label' => "Mauritius",							'TS' => 36.40,	'NS' => 36.40 );
		$t['MX'] = 					array( 'label' => "Mexiko",								'TS' => 41.00,	'NS' => 36.40 );
		$t['MN'] = 					array( 'label' => "Mongolei",							'TS' => 29.40,	'NS' => 29.40 );
		$t['MZ'] = 					array( 'label' => "Mosambik",							'TS' => 43.60,	'NS' => 41.40 );
		$t['MM'] = 					array( 'label' => "Myanmar",							'TS' => 29.40,	'NS' => 29.40 );
		$t['NA'] = 					array( 'label' => "Namibia",							'TS' => 34.90,	'NS' => 34.00 );
		$t['NP'] = 					array( 'label' => "Nepal",								'TS' => 31.80,	'NS' => 34.20 );
		$t['NZ'] = 					array( 'label' => "Neuseeland",							'TS' => 32.50,	'NS' => 36.40 );
		$t['NI'] = 					array( 'label' => "Nicaragua",							'TS' => 31.80,	'NS' => 36.40 );
		$t['NL'] = 					array( 'label' => "Niederlande",						'TS' => 35.30,	'NS' => 27.90 );
		$t['AN'] = 					array( 'label' => "Niederländische Antillen",			'TS' => 43.60,	'NS' => 27.70 );
		$t['NE'] = 					array( 'label' => "Niger",								'TS' => 39.20,	'NS' => 21.10 );
		$t['NG'] = 					array( 'label' => "Nigeria",							'TS' => 39.20,	'NS' => 34.20 );
		$t['NO'] = 					array( 'label' => "Norwegen",							'TS' => 42.90,	'NS' => 41.40 );
		$t['OM'] = 					array( 'label' => "Oman",								'TS' => 54.10,	'NS' => 37.50 );
		$t['PK'] = 					array( 'label' => "Pakistan",							'TS' => 27.70,	'NS' => 25.10 );
		$t['PA'] = 					array( 'label' => "Panama",								'TS' => 43.60,	'NS' => 36.40 );
		$t['PY'] = 					array( 'label' => "Paraguay",							'TS' => 33.10,	'NS' => 25.10 );
		$t['PE'] = 					array( 'label' => "Peru",								'TS' => 33.10,	'NS' => 25.10 );
		$t['PH'] = 					array( 'label' => "Philippinen",						'TS' => 32.50,	'NS' => 32.50 );
		$t['PL'] = 					array( 'label' => "Polen",								'TS' => 32.70,	'NS' => 25.10 );
		$t['PT'] = 					array( 'label' => "Portugal",							'TS' => 27.90,	'NS' => 22.70 );
		$t['CG'] = 					array( 'label' => "Republik Kongo",						'TS' => 39.20,	'NS' => 26.80 );
		$t['RW'] = 					array( 'label' => "Ruanda",								'TS' => 37.90,	'NS' => 37.90 );
		$t['RO'] = 					array( 'label' => "Rumänien",							'TS' => 36.80,	'NS' => 27.30 );
		$t['RU'] = 					array( 'label' => "Russische Föderation",				'TS' => 36.80,	'NS' => 31.00 );
		$t['RU-Moskau'] = 			array( 'label' => "Russische Föderation - Moskau",		'TS' => 40.60,	'NS' => 31.00 );
		$t['ZM'] = 					array( 'label' => "Sambia",								'TS' => 37.10,	'NS' => 34.00 );
		$t['SA'] = 					array( 'label' => "Saudi-Arabien",						'TS' => 54.10,	'NS' => 37.50 );
		$t['SE'] = 					array( 'label' => "Schweden",							'TS' => 42.90,	'NS' => 41.40 );
		$t['SN'] = 					array( 'label' => "Senegal",							'TS' => 49.30,	'NS' => 31.20 );
		$t['SC'] = 					array( 'label' => "Seychellen",							'TS' => 36.40,	'NS' => 36.40 );
		$t['SL'] = 					array( 'label' => "Sierra Leone",						'TS' => 43.60,	'NS' => 34.20 );
		$t['ZW'] = 					array( 'label' => "Simbabwe",							'TS' => 37.10,	'NS' => 34.00 );
		$t['SG'] = 					array( 'label' => "Singapur",							'TS' => 43.60,	'NS' => 44.70 );
		$t['SO'] = 					array( 'label' => "Somalia",							'TS' => 32.70,	'NS' => 29.00 );
		$t['ES'] = 					array( 'label' => "Spanien",							'TS' => 34.20,	'NS' => 30.50 );
		$t['LK'] = 					array( 'label' => "Sri Lanka",							'TS' => 31.80,	'NS' => 32.70 );
		$t['ZA'] = 					array( 'label' => "Südafrika",							'TS' => 34.90,	'NS' => 34.00 );
		$t['SD'] = 					array( 'label' => "Sudan",								'TS' => 43.60,	'NS' => 41.40 );
		$t['SR'] = 					array( 'label' => "Suriname",							'TS' => 39.20,	'NS' => 25.10 );
		$t['SY'] = 					array( 'label' => "Syrien",								'TS' => 32.70,	'NS' => 29.00 );
		$t['TJ'] = 					array( 'label' => "Tadschikistan",						'TS' => 36.80,	'NS' => 31.00 );
		$t['TW'] = 					array( 'label' => "Taiwan",								'TS' => 39.20,	'NS' => 37.50 );
		$t['TZ'] = 					array( 'label' => "Tansania",							'TS' => 43.60,	'NS' => 32.00 );
		$t['TH'] = 					array( 'label' => "Thailand",							'TS' => 39.20,	'NS' => 42.10 );
		$t['TG'] = 					array( 'label' => "Togo",								'TS' => 36.20,	'NS' => 26.60 );
		$t['TT'] = 					array( 'label' => "Trinidad und Tobago",				'TS' => 51.00,	'NS' => 43.60 );
		$t['TD'] = 					array( 'label' => "Tschad",								'TS' => 36.20,	'NS' => 26.60 );
		$t['TN'] = 					array( 'label' => "Tunesien",							'TS' => 36.20,	'NS' => 29.20 );
		$t['TR'] = 					array( 'label' => "Türkei",								'TS' => 31.00,	'NS' => 36.40 );
		$t['TM'] = 					array( 'label' => "Turkmenistan",						'TS' => 36.80,	'NS' => 31.00 );
		$t['UG'] = 					array( 'label' => "Uganda",								'TS' => 41.40,	'NS' => 32.00 );
		$t['UA'] = 					array( 'label' => "Ukraine",							'TS' => 36.80,	'NS' => 31.00 );
		$t['UY'] = 					array( 'label' => "Uruguay",							'TS' => 33.10,	'NS' => 25.10 );
		$t['US'] = 					array( 'label' => "USA",								'TS' => 52.30,	'NS' => 42.90 );
		$t['US-NYWA'] = 			array( 'label' => "USA - New York und Washington",		'TS' => 65.40,	'NS' => 51.00 );		
		$t['UZ'] = 					array( 'label' => "Usbekistan",							'TS' => 36.80,	'NS' => 31.00 );
		$t['VE'] = 					array( 'label' => "Venezuela",							'TS' => 39.20,	'NS' => 35.10 );
		$t['AE'] = 					array( 'label' => "Vereinigte Arabische Emirate",		'TS' => 54.10,	'NS' => 37.50 );
		$t['VN'] = 					array( 'label' => "Vietnam",							'TS' => 31.40,	'NS' => 31.40 );
		$t['CF'] = 					array( 'label' => "Zentralafrikanische Republik",		'TS' => 39.20,	'NS' => 29.00 );
		$t['CY'] = 					array( 'label' => "Zypern",								'TS' => 28.60,	'NS' => 30.50 );
		//
		return $t;
		
	}

}
?>