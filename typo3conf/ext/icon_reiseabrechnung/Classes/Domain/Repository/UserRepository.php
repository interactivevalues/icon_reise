<?php
namespace NEXT\IconReiseabrechnung\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use NEXT\IconReiseabrechnung\Domain\Model\User;

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class UserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	// Example for repository wide settings
	public function initializeObject() {
		/** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
		$querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		// go for $defaultQuerySettings = $this->createQuery()->getQuerySettings(); if you want to make use of the TS persistence.storagePid with defaultQuerySettings(), see #51529 for details
		
		// don't add the pid constraint
		$querySettings->setRespectStoragePage(FALSE);
		// set the storagePids to respect
//		$querySettings->setStoragePageIds(array(1, 26, 989));
		
		// don't add fields from enablecolumns constraint
		// this function is deprecated!
//		$querySettings->setRespectEnableFields(FALSE);
		
		// define the enablecolumn fields to be ignored
		// if nothing else is given, all enableFields are ignored
//		$querySettings->setIgnoreEnableFields(TRUE);       
		// define single fields to be ignored
//		$querySettings->setEnableFieldsToBeIgnored(array('disabled','starttime'));
		
		// add deleted rows to the result
//		$querySettings->setIncludeDeleted(TRUE);
		
		// don't add sys_language_uid constraint
//		$querySettings->setRespectSysLanguage(FALSE);
		
		// perform translation to dedicated language
//		$querySettings->setSysLanguageUid(42);
		//
		$this->setDefaultQuerySettings($querySettings);
	}

	// Example for a function setup changing query settings
	/**
	 * getByPersonnr
	 *
	 * @param string $personnr
	 * @mode string $mode
	 */
	public function getByPersonnr( $personnr, $mode='' ) {
		$query = $this->createQuery();
		//
		$manager = $mode=='MANAGER' ? 1 : 0;
		//
		$query->matching(
			$query->logicalAnd(
				$query->equals('personnr', $personnr),
				$query->equals('manager', $manager)
			)
		);
		//
		return $query->execute()->getFirst();
	}
    /**
     * findAllUsersAction
     *
     * @param \NEXT\IconReiseabrechnung\Domain\Model\User $user
     * @return void
     */
    public function findAllUsers() {
        $query = $this->createQuery();

        #    $query->getQuerySettings()->setEnableFieldsToBeIgnored(['deleted']);
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setIncludeDeleted(true);
        //

        //
            $t = array( 'lastname' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING );
            $query->setOrderings( $t );
        //
        return $query->execute();
    }


    /**
     * @param int $id
     * @return User
     */
    public function findAllById($id) {
        $query = $this->createQuery();


        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setIncludeDeleted(true);
        $query->matching(
            $query->logicalAnd(
                $query->equals('uid', $id)
            )
        );
        return $query->execute()->getFirst();
    }

}
?>