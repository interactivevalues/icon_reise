<?php
namespace NEXT\IconReiseabrechnung\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AbschnittRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	
	/**
	 * prepareDatetime
	 *
	 * @var string $value
	 * return integer
	 */	
	public function prepareDatetime ( $value ) {
		if( strlen($value) > 0 ){
			list($date, $time) = explode(' ', $value);
			list($dd, $dm, $dy) = explode('.', $date);
			list($th, $tm) = explode(':', $time);
			$date = mktime($th, $tm, 0, $dm , $dd, $dy);
		} else {
			$date = 0;
		}
//		return 12345;
		return $date;
	}

}
?>