<?php
namespace NEXT\IconReiseabrechnung\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SeminarRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	// Example for repository wide settings
	public function initializeObject() {
		/** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
		$querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		// go for $defaultQuerySettings = $this->createQuery()->getQuerySettings(); if you want to make use of the TS persistence.storagePid with defaultQuerySettings(), see #51529 for details
		
		// don't add the pid constraint
		$querySettings->setRespectStoragePage(FALSE);
		// set the storagePids to respect
//		$querySettings->setStoragePageIds(array(1, 26, 989));
		
		// don't add fields from enablecolumns constraint
		// this function is deprecated!
//		$querySettings->setRespectEnableFields(FALSE);
		
		// define the enablecolumn fields to be ignored
		// if nothing else is given, all enableFields are ignored
//		$querySettings->setIgnoreEnableFields(TRUE);       
		// define single fields to be ignored
//		$querySettings->setEnableFieldsToBeIgnored(array('disabled','starttime'));
		
		// add deleted rows to the result
//		$querySettings->setIncludeDeleted(TRUE);
		
		// don't add sys_language_uid constraint
//		$querySettings->setRespectSysLanguage(FALSE);
		
		// perform translation to dedicated language
//		$querySettings->setSysLanguageUid(42);
		//
		$this->setDefaultQuerySettings($querySettings);
	}

	/**
	 * findForManager
	 *
	 * 
	 * @param string $personnr
	 * @param string $order
	 * @return mixed
	 */
	public function findForManager( $personnr='', $order='' ) {
		$query = $this->createQuery();
		//
		if( $personnr!='' ){
			$query->matching(
				$query->equals('personnr', $personnr)
			);
		}
		//
		if( $order!='' ){
			if( $order=='DESC' ){
				$t = 					array( 'datetime' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING );
			} else {
				$t = 					array( 'datetime' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING );
			}
			$query->setOrderings( $t );
		}
		//
		return $query->execute();
	}

}
?>