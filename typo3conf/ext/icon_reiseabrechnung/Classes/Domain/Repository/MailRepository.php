<?php
namespace NEXT\IconReiseabrechnung\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_reiseabrechnung
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class MailRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * sends Email to user 
	 *
	 * @param array @mailCFG
	 * @param string $mailBody
	 * @return
	 */
	public function deliverMail ($mailCFG, $mailBody) {
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		$mail->setTo(array( $mailCFG['toEmail'] => $mailCFG['toName'] ))
			->setFrom(array( $mailCFG['fromEmail'] => $mailCFG['fromName'] ))
			->setSubject( $mailCFG['subject'] )
			->setCharset( $mailCFG['charset'] );
		//
		//$mail->setCc($m_ccArray);
		//$mail->setBcc($m_bcc);
		//$message->setReturnPath($cObj->cObjGetSingle($conf[$type . '.']['overwrite.']['returnPath'], $conf[$type . '.']['overwrite.']['returnPath.']));
		//$mail->setReplyTo($replyArray);
		$mail->setBody($mailBody, $mailCFG['contentType']);
		$mail->send();
		return $mail->isSent();
	}


	/**
	 * build Mail-Body and return string
	 * 
	 * @param string $mode
	 * @param array $mailData
	 * @param array $settings
	 * @return string
	 */
	public function buildMailBody ($mode, $mailData, $settings) {
		$mailBody = $this->getMailBody($mode, $settings);
		$mailBody = $this->mergeMailBodyData($mailBody, $mailData);
		//
		return $mailBody;
	}

	/**
	 * inserts MailData into MailBody and returns new string
	 *
	 * @param string $mailBody
	 * @param array $mailData
	 * @return string
	 */
	public function mergeMailBodyData ($mailBody, $mailData) {

		//	USER:
		$mailBody = str_replace('###REISE_NR###', $mailData['REISE_NR'], $mailBody);
		$mailBody = str_replace('###PERSON_NR###', $mailData['PERSON_NR'], $mailBody);
		$mailBody = str_replace('###FIRSTNAME###', $mailData['FIRSTNAME'], $mailBody);
		$mailBody = str_replace('###LASTNAME###', $mailData['LASTNAME'], $mailBody);
		$mailBody = str_replace('###URL###', $mailData['URL'], $mailBody);

		//	MANAGER:
		
		//	CHIEF:
		$mailBody = str_replace('###DECISION###', $mailData['DECISION'], $mailBody);
		$mailBody = str_replace('###INFO###', $mailData['INFO'], $mailBody);
		$mailBody = str_replace('###URL_TEXT###', $mailData['URL_TEXT'], $mailBody);
		
		//
		return $mailBody;
	}
	
	/**
	 * returns contents of template as MailBody
	 *
	 * @param string $mode
	 * @param array $settings
	 * @return string
	 */
	public function getMailBody ($mode, $settings) {
		if( $mode=='USER' ){
			$fp_file = $settings['user']['mail']['template'];
		} elseif ( $mode=='MANAGER' ){
			$fp_file = $settings['manager']['mail']['template'];
		} elseif ( $mode=='CHIEF' ){
			$fp_file = $settings['chief']['mail']['template'];
		}
		$fp = fopen($fp_file, 'r');
		$mailBody = fread($fp, filesize($fp_file));
		fclose($fp);
		//
		return $mailBody;
	}
	

}
?>