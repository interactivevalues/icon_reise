#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_reise'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_reise (
	
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	
	personnr varchar(10) DEFAULT '' NOT NULL,
	firstname varchar(255) DEFAULT '' NOT NULL,
	lastname varchar(255) DEFAULT '' NOT NULL,
	status varchar(255) DEFAULT 'MA' NOT NULL,
	reisenr int(11) DEFAULT '0' NOT NULL,
	grund int(11) DEFAULT '0' NOT NULL,
	ziel varchar(255) DEFAULT '' NOT NULL,
	firma varchar(255) DEFAULT '' NOT NULL,
	kontaktperson varchar(255) DEFAULT '' NOT NULL,
	thema varchar(255) DEFAULT '' NOT NULL,
	klientennr varchar(255) DEFAULT '' NOT NULL,
	projektnr varchar(255) DEFAULT '' NOT NULL,
	verrechenbar tinyint(4) DEFAULT '0' NOT NULL,
	verkehrsmittel varchar(255) DEFAULT '' NOT NULL,
	kmgefahren float DEFAULT '0' NOT NULL,
	abschnitte int(11) DEFAULT '0' NOT NULL,
	passengers int(11) DEFAULT '0' NOT NULL,
	belege int(11) DEFAULT '0' NOT NULL,
	belege_pdf varchar(255) DEFAULT '' NOT NULL,
	
	sum_taxfree float DEFAULT '0' NOT NULL,
	sum_tax float DEFAULT '0' NOT NULL,
	sum_total float DEFAULT '0' NOT NULL,
	sum_total_wv float DEFAULT '0' NOT NULL,

	sum_taggeld varchar(255) DEFAULT '' NOT NULL,
	sum_nachtgeld varchar(255) DEFAULT '' NOT NULL,
	sum_kmgeld varchar(255) DEFAULT '' NOT NULL,

	export_bh tinyint(4) DEFAULT '0' NOT NULL,
	export_lv tinyint(4) DEFAULT '0' NOT NULL,
	export_wv tinyint(4) DEFAULT '0' NOT NULL,

  delete_reason varchar(255) DEFAULT '' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),

);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_user'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_user (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,

	personnr varchar(10) DEFAULT '' NOT NULL,
	gender varchar(4) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	firstname varchar(255) DEFAULT '' NOT NULL,
	lastname varchar(255) DEFAULT '' NOT NULL,
	title_after varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	manager tinyint(4) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY personnr (personnr)
);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_reise_user_mm'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_reise_user_mm (
	uid_local int(11) DEFAULT '0' NOT NULL,
	uid_foreign int(11) DEFAULT '0' NOT NULL,
	tablenames varchar(30) DEFAULT '' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_abschnitt'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_abschnitt (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,

	ab_datetime int(11) DEFAULT '0' NOT NULL,
	ab_ort varchar(255) DEFAULT '' NOT NULL,
	ab_country varchar(255) DEFAULT '' NOT NULL,
	
	gu_datetime int(11) DEFAULT '0' NOT NULL,
	gu_ort varchar(255) DEFAULT '' NOT NULL,
	gu_country varchar(255) DEFAULT '' NOT NULL,
	
	gu2_datetime int(11) DEFAULT '0' NOT NULL,
	gu2_ort varchar(255) DEFAULT '' NOT NULL,
	gu2_country varchar(255) DEFAULT '' NOT NULL,
	
	gu3_datetime int(11) DEFAULT '0' NOT NULL,
	gu3_ort varchar(255) DEFAULT '' NOT NULL,
	gu3_country varchar(255) DEFAULT '' NOT NULL,
	
	gu4_datetime int(11) DEFAULT '0' NOT NULL,
	gu4_ort varchar(255) DEFAULT '' NOT NULL,
	gu4_country varchar(255) DEFAULT '' NOT NULL,
	
	an_datetime int(11) DEFAULT '0' NOT NULL,
	an_ort varchar(255) DEFAULT '' NOT NULL,
	an_country varchar(255) DEFAULT '' NOT NULL,

	privatehours varchar(5) DEFAULT '00:00' NOT NULL,
	
	mittag tinyint(4) DEFAULT '0' NOT NULL,
	abend tinyint(4) DEFAULT '0' NOT NULL,
	
	nacht tinyint(4) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_reise_abschnitt_mm'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_reise_abschnitt_mm (
	uid_local int(11) DEFAULT '0' NOT NULL,
	uid_foreign int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign),
);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_seminar'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_seminar (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,

	personnr varchar(10) DEFAULT '' NOT NULL,
	firstname varchar(255) DEFAULT '' NOT NULL,
	lastname varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	datetime int(11) DEFAULT '0' NOT NULL,
	place varchar(255) DEFAULT '' NOT NULL,
	brutto float DEFAULT '0' NOT NULL,
	netto float DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY personnr (personnr)
);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_seminar_user_mm'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_seminar_user_mm (
	uid_local int(11) DEFAULT '0' NOT NULL,
	uid_foreign int(11) DEFAULT '0' NOT NULL,
	tablenames varchar(30) DEFAULT '' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_passenger'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_passenger (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,

	personnr varchar(10) DEFAULT '' NOT NULL,
	firstname varchar(255) DEFAULT '' NOT NULL,
	lastname varchar(255) DEFAULT '' NOT NULL,
	kmgefahren varchar(255) DEFAULT '' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY personnr (personnr)
);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_reise_passenger_mm'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_reise_passenger_mm (
	uid_local int(11) DEFAULT '0' NOT NULL,
	uid_foreign int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_beleg'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_beleg (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,

	personnr varchar(10) DEFAULT '' NOT NULL,
	type int(11) DEFAULT '0' NOT NULL,
	country varchar(255) DEFAULT '' NOT NULL,
	currency varchar(255) DEFAULT '' NOT NULL,
	sum float DEFAULT '0' NOT NULL,
	sum_euro_brutto float DEFAULT '0' NOT NULL,
	sum_euro_netto float DEFAULT '0' NOT NULL,
	taxfee float DEFAULT '0' NOT NULL,
	mitarbeiter tinyint(4) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
);

#
# Table structure for table 'tx_iconreiseabrechnung_domain_model_reise_beleg_mm'
#
CREATE TABLE tx_iconreiseabrechnung_domain_model_reise_beleg_mm (
	uid_local int(11) DEFAULT '0' NOT NULL,
	uid_foreign int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign),
);
