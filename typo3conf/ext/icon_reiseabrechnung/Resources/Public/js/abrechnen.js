// JavaScript Document

/*
	MAIN
*/
$(document).ready(function() {

	/*
		CHECKBOXEN:		BUCHHALTUNG
	*/

	$("#selectAllBH").click(function(){
		if($("#selectAllBH").is(":checked")){
            $(".CB-BH").each(function( index ) {
                if(!$(this).hasClass("disabled")){
                    $(this).prop("checked",true);
				}
            });
            $('#btn-BH').removeAttr('disabled').removeClass('disabled');
		}
		else{
            $(".CB-BH").each(function( index ) {
                if (!$(this).hasClass("disabled")) {
                    $(this).removeProp("checked");
                }
            });
            $('#btn-BH').attr('disabled', 'disabled').addClass('disabled');
		}
	})
    $("#selectAllLV").click(function(){
        if($("#selectAllLV").is(":checked")){
            $(".CB-LV").each(function( index ) {
                if(!$(this).hasClass("disabled")){
                    $(this).prop("checked",true);
                }
			});
            $('#btn-LV').removeAttr('disabled').removeClass('disabled');
        }
        else{
            $(".CB-LV").each(function( index ) {
                if(!$(this).hasClass("disabled")){
                    $(this).removeProp("checked");
                }
            });
            $('#btn-LV').attr('disabled', 'disabled').addClass('disabled');
        }
    })


	$('input[type=checkbox][name^="BH-"]').change(function(){
//console.log(' CB-WV clicked! id: ' + $(this).prop('id') );
		//
		var n = $( 'input[type=checkbox][name^="BH-"]:enabled:checked' ).length;
//console.log('n: ' + n);
		if( n > 0 ){
			$('#btn-BH').removeProp('disabled').removeClass('disabled');
		} else {
			$('#btn-BH').prop('disabled', 'disabled').addClass('disabled');
		}
	});

	/*
		BUTTON:		ABRECHNEN BUCHHALTUNG
	*/
	$('#btn-BH').click( function(){
		//
console.log('#btn-BH.click();');
		var t_ids = '';
		var t_dotest = $('#dotest-BH').prop('checked');
console.log( 'is Test-Mode: ' + t_dotest );
		$( 'input[type=checkbox][name^="BH-"]:enabled:checked' ).each(function(index, elem){
			t_ids += $(this).data('id') + ';';
			//
			if( !t_dotest ){
				$(this).prop('disabled', 'disabled').prop('readonly', 'readonly');
			}
		});
console.log('ids-BH: ' + t_ids);
		if( t_dotest ){
			t_ids = $('#ids-BH').val() + ';' + t_ids;
		}
		$('#ids-BH').val(t_ids);
		//
console.log('#abrechnenBH-form.submit();');
		$('#abrechnenBH-form').submit();
	});


	/*
		CHECKBOXEN:		LOHNVERRECHNUNG
	*/
	$('input[type=checkbox][name^="LV-"]').change(function(){
//console.log(' CB-WV clicked! id: ' + $(this).prop('id') );
		//
		var n = $( 'input[type=checkbox][name^="LV-"]:enabled:checked' ).length;
//console.log('n: ' + n);
		if( n > 0 ){
			$('#btn-LV').removeProp('disabled').removeClass('disabled');
		} else {
			$('#btn-LV').prop('disabled', 'disabled').addClass('disabled');
		}
	});

	/*
		BUTTON:		ABRECHNEN LOHNVERRECHNUNG
	*/
	$('#btn-LV').click( function(){
		//
console.log('#btn-LV.click();');
		var t_ids = '';
		$( 'input[type="checkbox"][name^="LV-"]:enabled:checked' ).each(function(index, elem){
			t_ids += $(this).data('id') + ';';
			//
			var t_dotest = $('#dotest-LV').prop('checked');
console.log( 'is Test-Mode: ' + t_dotest );
			if( !t_dotest ){
				$(this).prop('disabled', 'disabled').prop('readonly', 'readonly');
			}
		});
console.log('ids-LV: ' + t_ids);
		$('#ids-LV').val(t_ids);
		//
console.log('#abrechnenLV-form.submit();');
		$('#abrechnenLV-form').submit();
	});


	/*
		CHECKBOXEN:	WEITERVERRECHNUNG
	*/
	$('input[type=checkbox][name^="WV-"]').change(function(){
//console.log(' CB-WV clicked! id: ' + $(this).prop('id') );
		//
		var n = $( 'input[type=checkbox][name^="WV-"]:enabled:checked' ).length;
//console.log('n: ' + n);
		if( n > 0 ){
			$('#btn-WV').removeProp('disabled').removeClass('disabled');
		} else {
			$('#btn-WV').prop('disabled', 'disabled').addClass('disabled');
		}
	});

	/*
		BUTTON:		ABRECHNEN WEITERERRECHNUNG
	*/
	$('#btn-WV').click( function(){
		//
console.log('#btn-WV.clicke();');
		var t_ids = '';
		$( 'input[type=checkbox][name^="WV-"]:enabled:checked' ).each(function(index, elem){
			t_ids += $(this).data('id') + ';';
		});
console.log('ids-WV: ' + t_ids);
		$('#ids-WV').val(t_ids);
		//
console.log('#abrechnenWV-form.submit();');
		$('#abrechnenWV-form').submit();
	});
	
} );
