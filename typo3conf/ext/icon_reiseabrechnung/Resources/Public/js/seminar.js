// JavaScript Document

/*
	MAIN
*/
$(document).ready(function() {
	//
	$('#startdatetime').change(function() {
//		console.log('val: ' + $(this).val() );
		var dateParts = $(this).val().split('.');
		var t_date = new Date(dateParts[2], parseInt(dateParts[1], 10) - 1, dateParts[0]);
		$('#datetime').val(t_date.getTime()/1000); 
	});
	$('#startdatetime').datepicker({
		dateFormat: "dd.mm.yy",
		firstDay: 1,
/*
		onSelect: function(dateText, inst){
			console.log('S: onSelect: ' + dateText + ' ' + inst);
		},
*/
	});

	$('#seminar-form').submit(function(event){
		var e_brutto = $('#brutto');
		e_brutto.val( (e_brutto.val()).replace(',', '.') );
		var e_netto = $('#netto');
		e_netto.val( (e_netto.val()).replace(',', '.') );
	});

	//	INIT VALIDATION FEATURES
	initValidation();
	
} );

function initValidation () {
	//
	var config = {
		// reference to elements and the validation rules that should be applied
		validate : {
			'#title' : {
				validation : 'length',
				length : 'min2',
				'_data-sanitize' : 'trim',
			},
			'#ort' : {
				validation : 'length',
				length : 'min2',
				'_data-sanitize': 'trim',
			},
			'#startdatetime' : {
				validation : 'length',
				length : 'min10',
				'_data-sanitize': 'trim',
			},
			'#brutto' : {
				validation : 'required',
				allowing : 'float',
				'_data-sanitize' : 'numberFormat',
				'_data-sanitize-number-format' : '0.00',
				'_data-sanitize-number-delimiter-decimal' : ',',
			},
			'#netto' : {
				validation : 'required',
				allowing : 'float',
				'_data-sanitize' : 'numberFormat',
				'_data-sanitize-number-format' : '0.00',
				'_data-sanitize-number-delimiter-decimal' : ',',
			},
		},
	};
	//

	numeral.language('de', {
		delimiters: {
			thousands: '.',
			decimal: ','
		},
		abbreviations: {
			thousand: 'k',
			million: 'm',
			billion: 'b',
			trillion: 't'
		},
/*
		ordinal : function (number) {
			return number === 1 ? 'er' : 'ème';
		},
*/
		currency: {
			symbol: '€'
		}
	});
	// switch between locales
	numeral.language('de');
	//
	$.validate({
		form : '#seminar-form',
  		modules : 'jsconf, html5, date, security, sanitize',
		showHelpOnFocus : false,
		addSuggestions : false,
		borderColorOnError : '#F00',
/*
		decimalSeparator : ',',
*/
		scrollToTopOnError : true,
		onModulesLoaded : function() {
console.log('onModulesLoaded!');
			$.setupValidation(config);
		},
		onError : function($form) {
alert('Validation of form '+$form.attr('id')+' failed!');
		},
		onSuccess : function($form) {
alert('The form '+$form.attr('id')+' is valid!');
			return false; // Will stop the submission of the form
		},
		onValidate : function($form) {
console.log('onValidate! ' + $form.attr('id') );
			return {
				element : $('#some-input'),
				message : 'This input has an invalid value for some reason'
			}
		},
		onElementValidate : function(valid, $el, $form, errorMess) {
console.log('Input ' +$el.attr('id')+ ' is ' + ( valid ? 'VALID':'NOT VALID') + '  MSG: ' + errorMess );
		},

	});

}