// JavaScript Document

/*
	prepFormmode
*/
function prepFormMode ( pVal, pInit ) {
console.log('prepFormMode: pVal: ' + pVal );
	if( pVal == 1 || pVal=='1' ){
		$('#fs-firma').show();
		$('#fs-kontaktperson').show();
		$('#label-kontaktperson-1').show();
		$('#label-kontaktperson-4').hide();
		$('#fs-thema').show();
		$('#label-thema-1').show();
		$('#label-thema-3').hide();
		$('#label-thema-4').hide();
		$('#fs-klientennr').show();
		$('#fs-projektnr').show();
		$('#fs-verrechenbar').show();
		if( !pInit ){
			$('#verrechenbar-1').prop('checked', true);
		}
	} else if( pVal==2 || pVal=='2' ) {
		$('#fs-firma').hide();
		$('#fs-kontaktperson').hide();
		$('#fs-thema').show();
		$('#label-thema-1').hide();
		$('#label-thema-3').show();
		$('#label-thema-4').hide();
		$('#fs-klientennr').hide();
		$('#fs-projektnr').hide();
		$('#fs-verrechenbar').hide();
		if( !pInit ){
			$('#verrechenbar-0').prop('checked', true);
		}
	} else if( pVal==3 || pVal=='3' ) {
		$('#fs-firma').show();
		$('#fs-kontaktperson').show();
		$('#label-kontaktperson-1').show();
		$('#label-kontaktperson-4').hide();
		$('#fs-thema').show();
		$('#label-thema-1').hide();
		$('#label-thema-3').show();
		$('#label-thema-4').hide();
		$('#fs-klientennr').show();
		$('#fs-projektnr').show();
		$('#fs-verrechenbar').show();
		if( !pInit ){
			$('#verrechenbar-1').prop('checked', true);
		}
	} else if( pVal==4 || pVal=='4' ) {
		$('#fs-firma').hide();
		$('#fs-kontaktperson').show();
		$('#label-kontaktperson-1').hide();
		$('#label-kontaktperson-4').show();
		$('#fs-thema').show();
		$('#label-thema-1').hide();
		$('#label-thema-3').hide();
		$('#label-thema-4').show();
		$('#fs-klientennr').hide();
		$('#fs-projektnr').hide();
		$('#fs-verrechenbar').hide();
		if( !pInit ){
			$('#verrechenbar-0').prop('checked', true);
		}
	}
}
/*
	prepKMgefahrenPassagiere
*/
function prepKMgefahrenPassagiere () {
console.log('prepKMgefahrenPassagiere()');
	if( $('#verkehrsmittel-pkw-privat').prop('checked') ||
		$('#verkehrsmittel-pkw-dienst').prop('checked') ){
		$('#block-kmgefahren').show();
		$('#block-passengers').show();
	} else {
		$('#block-kmgefahren').hide();
		$('#block-passengers').hide();
	}
}

/*
	ABSCHNITT:	removeGrenzübertritt
*/
function delGrenzuebertritt (elm) {
	var t_id = $(elm).data('id');
console.log('abschnitt['+t_id+'] -> del Grenzübertritt');
	var t_count = $('#grenzuebertritt-buttons-' + t_id).data('count');
console.log('abschnitt['+t_id+'] count: ' + t_count);
	t_block_id = '#block-grenzuebertritt-'+t_count+'-' + t_id;
console.log(' zeige block id = ' + t_block_id);
	//
	$(t_block_id).addClass('hide');
	tt_count = t_count == 1 ? '' : t_count;
	$('#abschnitt-gu'+tt_count+'Uhrzeit-'+t_id).val('');
	$('#abschnitt-gu'+tt_count+'Ort-'+t_id).val('');
	$('#abschnitt-gu'+tt_count+'Country-'+t_id).val('AT');
	//
	t_count --;
console.log('abschnitt['+t_id+'] count: ' + t_count);
	$('#grenzuebertritt-buttons-' + t_id).data('count', t_count);
	//
	if( t_count < 4 ){
		$('#add-GU-'+t_id).show();
	}
	if( t_count < 1 ){
		$(elm).hide();
	}
}
/*
	ABSCHNITT:	addGrenzübertritt
*/
function addGrenzuebertritt (elm) {
	var t_id = $(elm).data('id');
console.log('abschnitt['+t_id+'] -> add Grenzübertritt');
	var t_count = $('#grenzuebertritt-buttons-' + t_id).data('count');
console.log('abschnitt['+t_id+'] count: ' + t_count);
	t_count ++;
console.log('abschnitt['+t_id+'] count: ' + t_count);
	t_block_id = '#block-grenzuebertritt-'+t_count+'-' + t_id;
console.log(' zeige block id = ' + t_block_id);
	//
	$(t_block_id).removeClass('hide');
	//
	$('#grenzuebertritt-buttons-' + t_id).data('count', t_count);
	//
	if( t_count > 0 ){
		$('#del-GU-'+t_id).show();
	}
	if( t_count > 3 ){
		$(elm).hide();
	}
}

/*
	ABSCHNITT:	addAbschnitt
*/
function addAbschnitt () {
	//
	abschnitte_count ++;
	//
	var t_id = 'new' + abschnitte_count;
	//
	var t = $('#EMPTY-abschnitt').html();
	t = t.replace(/NEW/g, t_id);
//alert('add Abschnitt! Nr.: ' + abschnitte_count + ' HTML: ' + t);
	$('#block-abschnitte-new').before(t);
	//
	$('#del-GU-'+t_id).hide();
	$('#del-GU-'+t_id).click(function(){
		delGrenzuebertritt($(this));	
	});
	$('#add-GU-'+t_id).click(function(){
		addGrenzuebertritt($(this));
	});
	//
//	initValidation();
	//
	return t_id;
}

/*
	checkAbschnittStartEnd
*/
function checkAbschnittStartEnd ( part ) {
	//
console.log('checkAbschnittStartEnd:');
	var t_elms;
	var d_S = $('#startdatetime').val();
	var d_E = $('#enddatetime').val();
console.log('  ->  startdatetime: ' + d_S);
console.log('  ->  enddatetime:   ' + d_E);
console.log(' isMoreDays: ' + isMoreDays);
	//
	if( isMoreDays ){
		//		=>		MEHRTÄGIG		!!!
		var t_S = $('#startdatetime').val().split('.');
console.log('t_S: ' + t_S);
		var t_S_m = parseInt(t_S[1]) - 1;
console.log('t_S_m: ' + t_S_m);
//		var t_d_S = new Date(t_S[2], t_S[1], t_S[0], 12, 0, 0);
		var t_d_S = new Date(t_S[2], t_S_m, t_S[0], 12, 0, 0);
console.log(t_d_S);
//		t_d_S.setMonth( t_d_S.getMonth() - 1);
console.log(t_d_S);
		//
		var t_E = $('#enddatetime').val().split('.');
console.log('t_E: ' + t_E);
		var t_E_m = parseInt(t_E[1]) - 1;
console.log('t_E_m: ' + t_E_m);
//		var t_d_E = new Date(t_E[2], t_E[1], t_E[0], 12, 0, 0);
		var t_d_E = new Date(t_E[2], t_E_m, t_E[0], 12, 0, 0);
console.log(t_d_E);
//		t_d_E.setMonth( t_d_E.getMonth() - 1);
console.log(t_d_E);
		//
		t_d_between = new Date(t_d_E.getTime() - t_d_S.getTime());
console.log('t_d_between: ' + t_d_between);
		//
		if( t_d_between.getDate() > maxDays ){
			t_d_E = new Date( t_d_S.getTime() );
			t_d_E.setDate( t_d_E.getDate() + maxDays );
		}
		//	hier muss timestamp prüfung her für < >
		var days = t_d_between.getDate();
		t_elms = $('fieldset > div[id^="abschnitt-"]');
		var act = t_elms.length;
console.log(' days: ' + days);
console.log('  act: ' + act);
		if( act < days ){
			for( var i=act; i<days; i++ ){
				//
				var t_id = addAbschnitt();
			}
		} else if( act > days ){
			//
			for( var i=act; i>days; i-- ){
console.log('i: ' + i);
				t_elms = $('fieldset > div[id^="abschnitt-"]');
				var t_id = t_elms.last().data('id');
				if( t_elms.last().data('type') != 'new' ){
					var t_del = $('#abschnitteUpdate').data('del');
					t_del = t_del == undefined ? '' : t_del;
					t_del += 'D|' + t_id + '][';
					$('#abschnitteUpdate').data('del', t_del );
				}
console.log(' remove: ' + t_id);
				$('#abschnitt-'+t_id).remove();
console.log('removed!');
			}
		}
		//
		t_elms = $('fieldset > div[id^="abschnitt-"]');
		var t_id_S = t_elms.first().data('id');
		var t_id_E = t_elms.last().data('id');
		t_elms.each(function(index, elemt){
			var t_d = new Date(t_d_S.getTime());
			t_d.setDate( t_d.getDate() + index );
			var t_id = $(this).data('id');
			var t_d_i = t_d.getDate() + '.' + (t_d.getMonth()+1) + '.' + t_d.getFullYear();
			//
			$(this).data('date', t_d_i);
			//
			$('#abschnitt-abDatetime-'+t_id).val(t_d_i);
			$('#abschnitt-guDatetime-'+t_id).val(t_d_i);
			$('#abschnitt-anDatetime-'+t_id).val(t_d_i);
			$('#abschnitt-'+t_id+' fieldset.block-naechtigung').show();
			$(this).find('legend').html( (index+1) + '. Tag: ' + t_d_i);			
console.log(' t_id: ' + t_id + ' t_id_S: ' + t_id_S + ' t_id_E: ' + t_id_E);
			if( t_id == t_id_S ){
				$('#abschnitt-abUhrzeit-'+t_id).prop('readonly', false);
				$('#abschnitt-anUhrzeit-'+t_id).val('23:59');
				$('#abschnitt-anUhrzeit-'+t_id).prop('readonly', true);
			} else if( t_id == t_id_E ){
				$('#abschnitt-abUhrzeit-'+t_id).val('00:00');
				$('#abschnitt-abUhrzeit-'+t_id).prop('readonly', true);
				$('#abschnitt-anUhrzeit-'+t_id).val('');
				$('#abschnitt-anUhrzeit-'+t_id).prop('readonly', false);
			} else {
				$('#abschnitt-abUhrzeit-'+t_id).val('00:00');
				$('#abschnitt-abUhrzeit-'+t_id).prop('readonly', true);
				$('#abschnitt-anUhrzeit-'+t_id).val('23:59');
				$('#abschnitt-anUhrzeit-'+t_id).prop('readonly', true);
			}
		});

	} else {
		//		=>		EINTÄGIG		!!!
		t_elms = $('fieldset > div[id^="abschnitt-"]');
		var act = t_elms.length;
console.log('  act: ' + act);
		if( act > 1 ){
			for( var i=act; i>1; i-- ){
console.log('i: ' + i);
				t_elms = $('fieldset > div[id^="abschnitt-"]');
				var t_id = t_elms.last().data('id');
				if( t_elms.last().data('type') != 'new' ){
					var t_del = $('#abschnitteUpdate').data('del');
					t_del = t_del == undefined ? '' : t_del;
					t_del += 'D|' + t_id + '][';
					$('#abschnitteUpdate').data('del', t_del );
				}
console.log(' remove: ' + t_id);
				$('#abschnitt-'+t_id).remove();
console.log('removed!');
			}
		}
		//	Abschnitte anpassen
		t_elms = $('fieldset > div[id^="abschnitt-"');
		t_elms.each( function(index) {
			$(this).data('date', d_S);
console.log(' index: ' + index);
			//
			var t_id = $(this).data('id');
			$('#abschnitt-abDatetime-'+t_id).val(d_S);
			$('#abschnitt-guDatetime-'+t_id).val(d_S);
			$('#abschnitt-anDatetime-'+t_id).val(d_S);
			$('#abschnitt-abUhrzeit-'+t_id).prop('readonly', true);
			$('#abschnitt-anUhrzeit-'+t_id).prop('readonly', true);
			$('#abschnitt-'+t_id+' fieldset.block-naechtigung').show();
		});
	}
	//
//	var t_elems = $('fieldset > div[id^="abschnitt-"]');
	var id_S = t_elms.first().data('id');
	var id_E = t_elms.last().data('id');
	//
	$('#abschnitt-abUhrzeit-'+id_S).prop('readonly', false);
	$('#abschnitt-anUhrzeit-'+id_E).prop('readonly', false);	
	$('#abschnitt-'+id_E+' fieldset.block-naechtigung').hide();
}

/*
	setAbschnittMoreDays
*/
function setAbschnittMoreDays ( moreDays ){
	//
console.log('setAbschnittMoreDays: ' + moreDays);
	isMoreDays = moreDays;
	//
	var t_elms;
	var t = $('#startdatetime').val().split('.');
	var t_d_S = new Date(t[2], t[1], t[0], 8, 0, 0);
console.log(t_d_S);
	t_d_S.setMonth( t_d_S.getMonth() - 1);
	//
	if( isMoreDays ){
		//
		var t_d_E = new Date(t_d_S);
		t_d_E.setDate( t_d_E.getDate() + 1);
console.log(t_d_E);
		//
console.log('t_d_S.getTime(): ' + t_d_S.getTime() );
console.log('t_d_E.getTime(): ' + t_d_E.getTime() );
		var t_d_between = new Date(t_d_E.getTime() - t_d_S.getTime());
console.log(t_d_between);
		//
		var d_E = t_d_E.getDate() + '.' + (t_d_E.getMonth()+1) + '.' + t_d_E.getFullYear();
console.log('d_E: ' + d_E);
		$('#enddatetime').val(d_E);
		$('#date-abschnitt-end').show();
		//
		//
		var days = t_d_between.getDate();
		var t_elms = $('fieldset > div[id^="abschnitt-"]');
		var act = t_elms.length;
console.log(' days: ' + days);
console.log('  act: ' + act);
		if( act < days ){
			for( var i=act; i<days; i++ ){
				//
				var t_id = addAbschnitt();
			}
		} else if( act > days ){
			//
			for( var i=act; i>days; i-- ){
				t_elms.last().remove();
			}
			
		}
		//
		t_elms = $('fieldset > div[id^="abschnitt-"]');
		var t_id_S = t_elms.first().data('id');
		var t_id_E = t_elms.last().data('id');
		t_elms.each(function(index, elemt){
			var t_d = new Date(t_d_S.getTime());
			t_d.setDate( t_d.getDate() + index );
			var t_id = $(this).data('id');
			var t_d_i = t_d.getDate() + '.' + (t_d.getMonth()+1) + '.' + t_d.getFullYear();
			//
			$(this).data('date', t_d_i);
			//
			$('#abschnitt-abDatetime-'+t_id).val(t_d_i);
			$('#abschnitt-guDatetime-'+t_id).val(t_d_i);
			$('#abschnitt-anDatetime-'+t_id).val(t_d_i);
			$('#abschnitt-'+t_id+' fieldset.block-naechtigung').show();
			$(this).find('legend').html( (index+1) + '. Tag: ' + t_d_i);	
			//
console.log(' t_id: ' + t_id + ' t_id_S: ' + t_id_S + ' t_id_E: ' + t_id_E);
			if( t_id == t_id_S ){
				$('#abschnitt-abUhrzeit-'+t_id).prop('readonly', false);
				$('#abschnitt-anUhrzeit-'+t_id).val('23:59');
				$('#abschnitt-anUhrzeit-'+t_id).prop('readonly', true);
			} else if( t_id == t_id_E ){
				$('#abschnitt-abUhrzeit-'+t_id).val('00:00');
				$('#abschnitt-abUhrzeit-'+t_id).prop('readonly', true);
				$('#abschnitt-anUhrzeit-'+t_id).prop('readonly', false);
			} else {
				$('#abschnitt-abUhrzeit-'+t_id).val('00:00');
				$('#abschnitt-abUhrzeit-'+t_id).prop('readonly', true);
				$('#abschnitt-anUhrzeit-'+t_id).val('23:59');
				$('#abschnitt-anUhrzeit-'+t_id).prop('readonly', true);
			}
		});
		
	} else {
		//
		$('#date-abschnitt-end').hide();
		//
		t_elms = $('fieldset > div[id^="abschnitt-"]');
		var act = t_elms.length;
console.log('  act: ' + act);
		for( var i=act; i>1; i-- ){
console.log('i: ' + i);
			t_elms = $('fieldset > div[id^="abschnitt-"]');
			var t_id = t_elms.last().data('id');
			if( t_elms.last().data('type') != 'new' ){
				var t_del = $('#abschnitteUpdate').data('del');
				t_del = t_del == undefined ? '' : t_del;
				t_del += 'D|' + t_id + '][';
				$('#abschnitteUpdate').data('del', t_del );
			}
console.log(' remove: ' + t_id);
			$('#abschnitt-'+t_id).remove();
console.log('removed!');
		}
		//
		t_elms = $('fieldset > div[id^="abschnitt-"]');
		var t_id_S = t_elms.first().data('id');
		var t_id_E = t_elms.last().data('id');
		t_elms.each(function(index, elemt){
			var t_d = new Date(t_d_S.getTime());
//			t_d.setDate( t_d.getDate() + index );
			var t_id = $(this).data('id');
			var t_d_i = t_d.getDate() + '.' + (t_d.getMonth()+1) + '.' + t_d.getFullYear();
			//
			$(this).data('date', t_d_i);
			//
			$('#abschnitt-abDatetime-'+t_id).val(t_d_i);
			$('#abschnitt-guDatetime-'+t_id).val(t_d_i);
			$('#abschnitt-anDatetime-'+t_id).val(t_d_i);
			$('#abschnitt-'+t_id+' fieldset.block-naechtigung').show();
			$(this).find('legend').html('Reisetag:');
//console.log(' t_id: ' + t_id + ' t_id_S: ' + t_id_S + ' t_id_E: ' + t_id_E);
			//
//			$('#abschnitt-abUhrzeit-'+t_id).val('00:00');
			$('#abschnitt-abUhrzeit-'+t_id).prop('readonly', false);
			$('#abschnitt-anUhrzeit-'+t_id).val('');
			$('#abschnitt-anUhrzeit-'+t_id).prop('readonly', false);
		});
	}
	//
//	var t_elems = $('fieldset > div[id^="abschnitt-"]');
	var id_S = t_elms.first().data('id');
	var id_E = t_elms.last().data('id');
console.log('id_S: ' + id_S);
console.log('id_E: ' + id_E);
	//
	$('#abschnitt-abUhrzeit-'+id_S).prop('readonly', false);
	$('#abschnitt-anUhrzeit-'+id_E).prop('readonly', false);	
	$('#abschnitt-'+id_E+' fieldset.block-naechtigung').hide();
}



function doFormSubmit () {

		var doSubmit = true;
		var doSubmit = !$('#test-submit').prop('checked');
//		var doSubmit = false;
console.log("Handler for .submit() called." );
		/*	ABSCHNITTE	----------------------------	*/
		var t_abschnitte = '';
		//	UPDATE ABSCHNITTE
		$('fieldset > div[id^="abschnitt-"').each( function(index) {
			var id = $(this).data('id');
console.log('abschnitt: uid: ' + id);
			var t_date = $(this).data('date');
console.log('abschnitt: date: ' + t_date);
			var abD = $('#abschnitt-abDatetime-' + id);
			var abD_act = abD.val();
			var abD_orig = abD.data('value');
			var abT_act = $('#abschnitt-abUhrzeit-' + id).val();
			var abO = $('#abschnitt-abOrt-' + id);
			var abO_act = abO.val();
			var abO_orig = abO.data('value');
			var abC = $('#abschnitt-abCountry-' + id);
			var abC_act = abC.val();
			var abC_orig = abC.data('value');

			var gu1D = $('#abschnitt-guDatetime-' + id);
			var gu1D_act = gu1D.val();
			var gu1D_orig = gu1D.data('value');
			var gu1T_act = $('#abschnitt-guUhrzeit-' + id).val();
			var gu1O = $('#abschnitt-guOrt-' + id);
			var gu1O_act = gu1O.val();
			var gu1O_orig = gu1O.data('value');
			var gu1C = $('#abschnitt-guCountry-' + id);
			var gu1C_act = gu1C.val();
			var gu1C_orig = gu1C.data('value');

			var gu2D = $('#abschnitt-gu2Datetime-' + id);
			var gu2D_act = gu2D.val();
			var gu2D_orig = gu2D.data('value');
			var gu2T_act = $('#abschnitt-gu2Uhrzeit-' + id).val();
			var gu2O = $('#abschnitt-gu2Ort-' + id);
			var gu2O_act = gu2O.val();
			var gu2O_orig = gu2O.data('value');
			var gu2C = $('#abschnitt-gu2Country-' + id);
			var gu2C_act = gu2C.val();
			var gu2C_orig = gu2C.data('value');

			var gu3D = $('#abschnitt-gu3Datetime-' + id);
			var gu3D_act = gu3D.val();
			var gu3D_orig = gu3D.data('value');
			var gu3T_act = $('#abschnitt-gu3Uhrzeit-' + id).val();
			var gu3O = $('#abschnitt-gu3Ort-' + id);
			var gu3O_act = gu3O.val();
			var gu3O_orig = gu3O.data('value');
			var gu3C = $('#abschnitt-gu3Country-' + id);
			var gu3C_act = gu3C.val();
			var gu3C_orig = gu3C.data('value');3

			var gu4D = $('#abschnitt-gu4Datetime-' + id);
			var gu4D_act = gu4D.val();
			var gu4D_orig = gu4D.data('value');
			var gu4T_act = $('#abschnitt-gu4Uhrzeit-' + id).val();
			var gu4O = $('#abschnitt-gu4Ort-' + id);
			var gu4O_act = gu4O.val();
			var gu4O_orig = gu4O.data('value');
			var gu4C = $('#abschnitt-gu4Country-' + id);
			var gu4C_act = gu4C.val();
			var gu4C_orig = gu4C.data('value');

			var anD = $('#abschnitt-anDatetime-' + id);
			var anD_act = anD.val();
			var anD_orig = anD.data('value');
			var anT_act = $('#abschnitt-anUhrzeit-' + id).val();
			var anO = $('#abschnitt-anOrt-' + id);
			var anO_act = anO.val();
			var anO_orig = anO.data('value');
			var anC = $('#abschnitt-anCountry-' + id);
			var anC_act = anC.val();
			var anC_orig = anC.data('value');
			var ph = $('#abschnitt-privatehours-' + id);
			var ph_act = ph.val();
			var ph_orig = ph.data('value');
			var mittag = $('#abschnitt-mittag-' + id);
			var mittag_act = mittag.prop('checked') ? '1' : '';
			var mittag_orig = mittag.data('checked');
			var abend = $('#abschnitt-abend-' + id);
			var abend_act = abend.prop('checked') ? '1' : '';
			var abend_orig = abend.data('checked');
			var nacht_act = $('input[name="abschnitt-nacht-' + id+'"]:checked').val();
			var nacht_orig = $('input[name="abschnitt-nacht-' + id+'"]:checked').data('checked');
			//
			abD_act = t_date + ' ' + abT_act;
console.log('abD_act: ' + abD_act);
			gu1D_act = gu1T_act != '' ? t_date + ' ' + gu1T_act : '';
console.log('gu1D_act: ' + gu1D_act);
			gu2D_act = gu2T_act != '' ? t_date + ' ' + gu2T_act : '';
console.log('gu2D_act: ' + gu2D_act);
			gu3D_act = gu3T_act != '' ? t_date + ' ' + gu3T_act : '';
console.log('gu3D_act: ' + gu3D_act);
			gu4D_act = gu4T_act != '' ? t_date + ' ' + gu4T_act : '';
console.log('gu4D_act: ' + gu4D_act);
			anD_act = t_date + ' ' + anT_act;
console.log('anD_act: ' + abD_act);

//alert( $('input[name="abschnitt-nacht-' + id+'"]:checked').val() + ' ' + $('input[name="abschnitt-nacht-' + id+'"]:checked').data('checked') );
			//
			t_abs = $(this).data('type') == 'new' ? 'A' : 'U';
			t_abs += '|' + id + '|'+abD_act+'|'+abO_act+'|'+abC_act+'|';
			t_abs += gu1D_act+'|'+gu1O_act+'|'+gu1C_act+'|';
			t_abs += gu2D_act+'|'+gu2O_act+'|'+gu2C_act+'|';
			t_abs += gu3D_act+'|'+gu3O_act+'|'+gu3C_act+'|';
			t_abs += gu4D_act+'|'+gu4O_act+'|'+gu4C_act+'|';
			t_abs += anD_act+'|'+anO_act+'|'+anC_act+'|'+ph_act+'|'+mittag_act+'|'+abend_act+'|'+nacht_act + '][';
console.log('t_abs: ' + t_abs);
			t_abschnitte += t_abs;

/*
			if( abD_act!=abD_orig || abO_act!=abO_orig || abC_act!=abC_orig ||
				guD_act!=guD_orig || guO_act!=guO_orig || guC_act!=guC_orig ||
				anD_act!=anD_orig || anO_act!=anO_orig || anC_act!=anC_orig ||
				ph_act!=ph_orig || mittag_act!=mittag_orig || abend_act!=abend_orig || nacht_act!=nacht_orig 
				){
				t_abs = 'U|' + id + '|'+abD_act+'|'+abO_act+'|'+abC_act+'|'+guD_act+'|'+guO_act+'|'+guC_act+'|'+anD_act+'|'+anO_act+'|'+anC_act+'|'+ph_act+'|'+mittag_act+'|'+abend_act+'|'+nacht_act + '][';
console.log('t_abs: ' + t_abs);
				t_abschnitte += t_abs;
//				t_abschnitte += 'U|' + id + '|'+abD_act+'|'+abO_act+'|'+abC_act+'|'+guD_act+'|'+guO_act+'|'+guC_act+'|'+anD_act+'|'+anO_act+'|'+anC_act+'|'+ph_act+'|'+mittag_act+'|'+abend_act+'|'+nacht_act + '][';
			}
*/
		});
		//
		t_del = $('#abschnitteUpdate').data('del');
		if( t_del != undefined ){
			t_abschnitte += t_del;
		}
		//
		$('#abschnitteUpdate').val(t_abschnitte);
//		$('#abschnitteUpdate').val( $('#abschnitteUpdate').val() + t_abschnitte);

		/*	PASSENGERS	----------------------------	*/
		var t_passengers = '';
		//	UPDATE PASSENGERS
		$('div[id^="passenger-"').each( function(index) {
			var id = $(this).data('id');
console.log('passenger: uid: ' + id);
			var fn = $('#passenger-fn-'+id);
			var fn_act = fn.val();
			var fn_orig = fn.data('value');
			var ln = $('#passenger-ln-'+id);
			var ln_act = ln.val();
			var ln_orig = ln.data('value');
            var km = $('#passenger-km-'+id);
            var km_act = km.val();
            var km_orig = km.data('value');
			//
			if( fn_act!=fn_orig || ln_act!=ln_orig  || km_act!=km_orig ){
				t_passengers += 'U|' + id + '|' + fn_act + '|' + ln_act + '|' + km_act + '][';
			}
		});

		//	ADD PASSENGERS
		for( var i=1; i<=passengers_count; i++ ){
			var fn = $('#block-passengers-new #add-passenger-fn-' + i).val();
			var ln = $('#block-passengers-new #add-passenger-ln-' + i).val();
            var km = $('#block-passengers-new #add-passenger-km-' + i).val();
			//
			var t_fl = $.trim(fn+'|'+ln+'|'+km);
			if( t_fl != '|' && t_fl != 'undefined|undefined' ){
				t_passengers += 'A|0|' + t_fl + '][';
			}
		}
		//
		t_del = $('#passengersUpdate').data('del');
		if( t_del != undefined ){
			t_passengers += t_del;
		}
		//
		$('#passengersUpdate').val(t_passengers);
//		$('#passengersUpdate').val( $('#passengersUpdate').val() + t_passengers);

		/*	BELEGE	----------------------------	*/
		var t_belege = '';
		//	UPDATE BELEGE
		$('div[id^="beleg-"').each( function(index) {
			var id = $(this).data('id');
//		alert('passenger: uid: ' + id);
			var e_type = $('#beleg-type-'+id);
			var t_type_act = e_type.val();
			var t_type_orig = e_type.data('value');
			var e_country = $('#beleg-country-'+id);
			var t_country_act = e_country.val();
			var t_country_orig = e_country.data('value');
			var e_currency = $('#beleg-currency-'+id);
			var t_currency_act = e_currency.val();
			var t_currency_orig = e_currency.data('value');
			var e_sum = $('#beleg-sum-'+id);
			var t_sum_act = e_sum.val();
			var t_sum_orig = e_sum.data('value');
			//
			var e_sumEuroB = $('#beleg-sumEuroBrutto-'+id);
			var t_sumEuroB_act = e_sumEuroB.val();
			var t_sumEuroB_orig = e_sumEuroB.data('value');
			var e_sumEuroN = $('#beleg-sumEuroNetto-'+id);
			var t_sumEuroN_act = e_sumEuroN.val();
			var t_sumEuroN_orig = e_sumEuroN.data('value');
			var e_taxfee = $('#beleg-taxfee-'+id);
			var t_taxfee_act = e_taxfee.val();
			var t_taxfee_orig = e_taxfee.data('value');
			//
			var t_mitarbeiter_act = $('input[name="beleg-mitarbeiter-'+id+'"]:checked').val();
			var t_mitarbeiter_orig = $('input[name="beleg-mitarbeiter-'+id+'"]:checked').data('checked');

			t_sum_act = t_sum_act.replace(',', '.');
			//
			if( userMode=='MANAGER' ){
				t_sumEuroB_act = t_sumEuroB_act.replace(',', '.');
				t_sumEuroN_act = t_sumEuroN_act.replace(',', '.');
				t_taxfee_act = t_taxfee_act.replace(',', '.');
				//
				if( t_type_act!=t_type_orig || t_country_act!=t_country_orig || t_currency_act!=t_currency_orig || t_sum_act !=t_sum_orig || t_sumEuroB_act!=t_sumEuroB_orig || t_sumEuroN_act!=t_sumEuroN_orig || t_taxfee_act!=t_taxfee_orig || t_mitarbeiter_act!=t_mitarbeiter_orig ){
					t_belege += 'U|' + id + '|' + t_type_act + '|' + t_country_act + '|' + t_currency_act + '|' + t_sum_act + '|' + t_sumEuroB_act + '|' + t_sumEuroN_act + '|' + t_taxfee_act + '|' + t_mitarbeiter_act  + '][';
				}
			} else {
				if( t_type_act!=t_type_orig || t_country_act!=t_country_orig || t_currency_act!=t_currency_orig || t_sum_act !=t_sum_orig ){
					t_belege += 'U|' + id + '|' + t_type_act + '|' + t_country_act + '|' + t_currency_act + '|' + t_sum_act + '][';
				}
			}
		});

		//	ADD BELEGE
console.log(' belege_count: ' + belege_count);
		for( var i=1; i<=belege_count; i++ ){
console.log(' vorhanden: #add-beleg-'+i + ' ??');
			if( $('#block-belege-new #add-beleg-'+i).data('id') == i ){
console.log(' -> YES !!!');
				var t_type = $('#block-belege-new #add-beleg-type-' + i).val();
				var t_country = $('#block-belege-new #add-beleg-country-' + i).val();
				var t_currency = $('#block-belege-new #add-beleg-currency-' + i).val();
				var t_sum = $('#block-belege-new #add-beleg-sum-' + i).val();
			
				var t_sumEuroB = $('#block-belege-new #add-beleg-sumEuroBrutto-' + i).val();
				var t_sumEuroN = $('#block-belege-new #add-beleg-sumEuroNetto-' + i).val();
				var t_taxfee = $('#block-belege-new #add-beleg-taxfee-' + i).val();
				var t_mitarbeiter = $('input[name="add-beleg-mitarbeiter-' + i+'"]:checked').val();
				//
				t_sum = t_sum.replace(',', '.');
				//	
				if( userMode=='MANAGER' ){
					t_sumEuroB = t_sumEuroB.replace(',', '.');
					t_sumEuroN = t_sumEuroN.replace(',', '.');
					t_taxfee = t_taxfee.replace(',', '.');
					//
					var t_beleg = $.trim(t_type+'|'+t_country+'|'+t_currency+'|'+t_sum+'|'+t_sumEuroB+'|'+t_sumEuroN+'|'+t_taxfee+'|'+t_mitarbeiter);
					if( t_beleg != '||||||0' && t_beleg != 'undefined|undefined|undefined|undefined|undefined|undefined|undefined|0' ){
						t_belege += 'A|0|' + t_beleg + '][';
					}
				} else {
					var t_beleg = $.trim(t_type+'|'+t_country+'|'+t_currency+'|'+t_sum);
					if( t_beleg != '|||' && t_beleg != 'undefined|undefined|undefined|undefined' ){
						t_belege += 'A|0|' + t_beleg + '][';
					}
				}
			}
		}
		//
		t_del = $('#belegeUpdate').data('del');
		if( t_del != undefined ){
			t_belege += t_del;
		}
		//
		$('#belegeUpdate').val(t_belege);
//		$('#belegeUpdate').val( $('#belegeUpdate').val() + t_belege);

		//	OUTPUT CONSOLE:
console.log('Update Abschnitte: ' + $('#abschnitteUpdate').val() );
console.log('Update Passengers: ' + $('#passengersUpdate').val() );
console.log('Update Belege: ' + $('#belegeUpdate').val() );

		//
		return doSubmit;
//		event.preventDefault();	-> doesn't submit Form

}

var userMode = '';
var isNew = true;
var isMoreDays = false;
var maxDays = 14;
var abschnitte_count = 0;
var passengers_count = 0;
var belege_count = 0;

/*
	MAIN
*/
$(document).ready(function() {
	//
	//	
	if( $('input[name$="[reise][__identity]"]').val() > 0 ){
		isNew = false;
	}
console.log('isNew: ' + isNew);
	//	
	userMode = $('#userMode').val();
console.log('userMode: ' + userMode);

	/*
		FORM - SUBMIT
	*/
	$('#trip-form').submit(function( event ) {
		return doFormSubmit();
	});

	/*
		FORM - MODE
	*/
	prepFormMode( $('input[name$="[reise][grund]"]:checked').val(), true );
	$('input[type=radio][name$="[reise][grund]"]').change(function () {
		prepFormMode( $(this).val() );
	});
	prepKMgefahrenPassagiere();
	$('#verkehrsmittel-pkw-dienst').click(function () {
		prepKMgefahrenPassagiere();
	});
	$('#verkehrsmittel-pkw-privat').click(function () {
		prepKMgefahrenPassagiere();
	});

	/*
		ABSCHNITTE:		ZEITRAUM
	*/
	if( isNew ){
		//	GET CURRENT DAY	08:00 - 18:00
		var t_S = '08:00';
		var t_E = '18:00';
		//
		var d = new Date();
		var d_S = d.getDate() + '.' + (d.getMonth()+1) + '.' + d.getFullYear();
		var dt_S = d_S + ' ' + t_S;
		var d_E = d.getDate() + '.' + (d.getMonth()+1) + '.' + d.getFullYear();
		var dt_E = d_E + ' ' + t_E;
		$('#startdatetime').val(d_S);
		$('#enddatetime').val(d_E);
		//	-> CREATE START AND END-ABSCHNITT
		var t_id = addAbschnitt();
		$('#abschnitt-'+t_id).data('date', d_S);
		$('#abschnitt-abDatetime-'+t_id).val(dt_S);
		$('#abschnitt-abUhrzeit-'+t_id).val(t_S);
		$('#abschnitt-anDatetime-'+t_id).val(dt_E);
//		$('#abschnitt-anUhrzeit-'+t_id).val(t_E);
		$('#abschnitt-'+t_id+' fieldset.block-naechtigung').hide();
		//
		$('#date-abschnitt-end').hide();
	} else {
		var t_elems = $('fieldset > div[id^="abschnitt-"]');
		//
		var id_S = t_elems.first().data('id');
		var id_E = t_elems.last().data('id');
		var d_S = $('#abschnitt-'+id_S).data('date');
		var dt_S = $('#abschnitt-abDatetime-'+id_S).val();
		var d_E = $('#abschnitt-'+id_E).data('date');
		var dt_E = $('#abschnitt-anDatetime-'+id_E).val();
		//
		$('#startdatetime').val(d_S);
		$('#enddatetime').val(d_E);
		//
		$('fieldset > div[id^="abschnitt-"]').each(function(index, elemt){
			var t_id = $(this).data('id');
			var t_date = $(this).data('date');
			//
			$(this).find('legend').html( (index+1) + '. Tag: ' + t_date);
			//
			$('#abschnitt-abUhrzeit-'+t_id).prop('readonly', true);
			$('#abschnitt-anUhrzeit-'+t_id).prop('readonly', true);
		});
		//
		$('#abschnitt-abUhrzeit-'+id_S).prop('readonly', false);
		$('#abschnitt-anUhrzeit-'+id_E).prop('readonly', false);	
		//
		$('#abschnitt-'+id_E+' fieldset.block-naechtigung').hide();
		//
		if( d_S == d_E ){
			isMoreDays = false;
			$('#date-abschnitt-end').hide();
			$('#moreDays').prop('checked', false);
			$('#abschnitt-'+id_S+' fieldset legend').html('Reisetag');
		} else {
			isMoreDays = true;
			$('#date-abschnitt-end').show();
			$('#moreDays').prop('checked', true);
		}

		/*
			ABSCHNITTE:		CHECK GRENZUEBERTRITTE
		*/
		$('fieldset > div[id^="abschnitt-"]').each(function(index, elemt){
			var t_id = $(this).data('id');
	console.log('prepare Abschnitt Grenzübertritt !');
			//
			for( var i=4; i>=1; i-- ){
	console.log(' i: ' + i);
				var tt_i = i==1 ? '' : i;
				var t_U = $('#abschnitt-gu'+tt_i+'Uhrzeit-'+t_id).val();
				var t_O = $('#abschnitt-gu'+tt_i+'Ort-'+t_id).val();
				var t_C = $('#abschnitt-gu'+tt_i+'Country-'+t_id).val();
	console.log(' U: ' + t_U + ' O: ' + t_O + ' C: ' + t_C);
				if( t_U=='' && t_O=='' && t_C=='AT' ){
					$('#block-grenzuebertritt-'+i+'-'+t_id).addClass('hide');
				} else {
					break;
				}
			}
	console.log(' abschnitt['+t_id+'] count: ' + i);
			$('#grenzuebertritt-buttons-'+t_id).data('count', i);
			//
			$('#del-GU-' + t_id).click(function(){
				delGrenzuebertritt($(this));
			});
			$('#add-GU-' + t_id).click(function(){
				addGrenzuebertritt($(this));
			});
			//
			if( i==4 ){
				$('#add-GU-' + t_id).hide();
			}
			if( i<1 ){
				$('#del-GU-' + t_id).hide();
			}
		});

	}
console.log('isMoreDays: ' + isMoreDays);
	
	/*
		ABSCHNITTE:		CHECKBOX moreDays
	`*/
	$('#moreDays').change(function () {
		setAbschnittMoreDays( $(this).prop('checked') );
	});

	/*
		ABSCNITTE:		START - ENDE
	`*/
	$('#startdatetime').change(function() { checkAbschnittStartEnd('start'); });
	$('#enddatetime').change(function() { checkAbschnittStartEnd('end'); });
	$('#startdatetime').datepicker({
		dateFormat: "dd.mm.yy",
		firstDay: 1,
/*
		onSelect: function(dateText, inst){
			console.log('S: onSelect: ' + dateText + ' ' + inst);
		},
*/
	});
	$('#enddatetime').datepicker({
		dateFormat: "dd.mm.yy",
		firstDay: 1,
/*
		onSelect: function(dateText, inst){
			console.log('E: onSelect: ' + dateText + ' ' + inst);
		},
*/
	});

	/*
		PASSENGERS:		REMOVE-BUTTON
	*/
	$('button[id^="del-passenger-"').click( function() {
		var id = $(this).data('id');
console.log('id: ' + id);
console.log('data-type: ' + $('#passenger-'+id).data('type') );
		if( $('#passenger-'+id).data('type') != 'new' ){
			var t_del = $('#passengersUpdate').data('del');
			t_del = t_del == undefined ? '' : t_del;
			t_del += 'D|' + id + '][';
			$('#passengersUpdate').data('del', t_del );
console.log('#passengersUpdate.data-del: ' + $('#passengersUpdate').data('del') );
		}
		$('#passenger-'+id).remove();
//		$('#passengersUpdate').val( $('#passengersUpdate').val() + 'D|' + id + '][' );
		//
//		initValidation();
	});

	/*
		PASSENGERGS:	ADD - BUTTON
	*/
	$('#button-passenger').click( function() {
		//
		passengers_count ++;
		//
		var t = $('#add-passenger-empty').html();
		t = t.replace(/NEW/g, passengers_count);
//		alert('add Passenger! Nr.: ' + passengers_count + ' HTML: ' + t);
		$('#block-passengers-new').append( t );
		//
		$('#rem-passenger-'+passengers_count).click( function() {
			//
			var id = $(this).data('id');
			$('#add-passenger-'+id).remove();
		} );
		//
//		initValidation();
//		$.validate();
	} );


	/*
		BELEGE:		REMOVE-BUTTON
	*/
	$('button[id^="del-beleg-"').click( function() {
		var id = $(this).data('id');
console.log('id: ' + id);
console.log('data-type: ' + $('#beleg-'+id).data('type') );
		if( $('#beleg-'+id).data('type') != 'new' ){
			var t_del = $('#belegeUpdate').data('del');
			t_del = t_del == undefined ? '' : t_del;
			t_del += 'D|' + id + '][';
			$('#belegeUpdate').data('del', t_del );
console.log('#belegeUpdate.data-del: ' + $('#belegeUpdate').data('del') );
		}		
		$('#beleg-'+id).remove();		
//		$('#belegeUpdate').val( $('#belegeUpdate').val() + 'D|' + id + '][' );
		//
//		initValidation();
	});

	/*
		BELEGE:	ADD - BUTTON
	*/
	$('#button-beleg').click( function() {
		//
		belege_count ++;
		//
		var t = $('#add-beleg-empty').html();
		t = t.replace('id="add-beleg-type-NEW" ', 'id="add-beleg-type-NEW" data-validation="required" ');
		t = t.replace(/NEW/g, belege_count);
//		alert('add Beleg! Nr.: ' + belege_count + ' HTML: ' + t);
		$('#block-belege-new').append( t );
		//
		$('#rem-beleg-'+belege_count).click( function() {
			//
			var id = $(this).data('id');
			$('#add-beleg-'+id).remove();
		} );
		//
//		initValidation();
	} );
	
	/*
		BELEGE:	REMOVE BELEGE PDF - BUTON
	*/
	$('#del-belegepdf').click(function(){
		var elm = $('#belegePdf');
		if( !elm.val().endsWith(".DELETE") ){
			elm.val( elm.val() + '.DELETE' );
			$('#block-showBelegePdf').remove();
		}
	});
	

	/*
		EINREICHUNG: E-MAIL-Selektor
	*/
	$('input[type=radio][name$="[reise][secretary]"]').change(function(){
		if( $(this).val() ){
			$('#einreichung-submit').removeAttr('disabled').removeClass('disabled');
		}
	});
	$('input[type=radio][name$="[reise][chief]"]').change(function(){
		if( $(this).val() ){
			$('#einreichung-submit').removeAttr('disabled').removeClass('disabled');
		}
	});

	/*
		BUTTONS für EINREICHUNG
	*/
	$('#einreichung-submit').click( function(){
		$('#einreichung-checked').val(1);
		$('#trip-form').submit();
	});

	/*
		REISE LÖSCHEN FÜR SEK
	*/
	$("#button-delete").click(function(){
		$("#delete-reise-container").show();
		$(this).hide();
	})

    $("#delete_reason").keyup(function(e){
    	$delValue = $(this).val();
        $nrWords = countWords($delValue);
        if($nrWords >= 5){
        	$("#button-delete-final").removeAttr('disabled').removeClass('disabled');
		}
    });
	
	//	INIT VALIDATION FEATURES
	initValidation();
	
} );


function countWords(what){
	return what.replace(/\w+/g,"x").replace(/[^x]+/g,"").length;
}

function initValidation () {
	//
	var config = {
		// ordinary validation config
/* */
		form : '#trip-form',
/* */
		// reference to elements and the validation rules that should be applied
		validate : {
			'#ziel' : {
				validation : 'length',
				length : 'min2',
				'_data-sanitize' : 'trim',
			},
			'#firma' : {
				validation : 'length',
				length : 'min2',
				'_data-sanitize': 'trim',
			},
			'#thema' : {
				validation : 'length',
				length : 'min2',
				'_data-sanitize': 'trim',
			},
			'#klientennr' : {
				validation : 'length',
				length : 'min1',
				'_data-sanitize': 'trim',
			},
			'#projektnr' : {
				validation : 'length',
				length : 'min1',
				'_data-sanitize': 'trim',
			},
/* */
			'#kmgefahren' : {
				validation : 'required',
				allowing : 'float',
				'_data-sanitize' : 'numberFormat',
				'_data-sanitize-number-format' : '0',
				'_data-sanitize-number-delimiter-decimal' : ',',
			},
/* */			
		},
/* */
		showHelpOnFocus : false,
		addSuggestions : false,
		borderColorOnError : '#F00',
		scrollToTopOnError : true,
/*
		decimalSeparator : ',',
*/	
/* */
	};
	//

	numeral.language('de', {
		delimiters: {
			thousands: '.',
			decimal: ','
		},
		abbreviations: {
			thousand: 'k',
			million: 'm',
			billion: 'b',
			trillion: 't'
		},
		/*
		ordinal : function (number) {
			return number === 1 ? 'er' : 'ème';
		},
		*/
		currency: {
			symbol: '€'
		}
	});
	// switch between locales
	numeral.language('de');
	//
	$.validate({
		form:'#trip-form',
  		modules : 'jsconf, html5, date, security, sanitize',
		showHelpOnFocus : false,
		addSuggestions : false,
		borderColorOnError : '#F00',
/*
		decimalSeparator : ',',
*/
		scrollToTopOnError : true,
		onModulesLoaded : function() {
console.log('onModulesLoaded!');
			$.setupValidation(config);
		},
		onError : function($form) {
			alert('Validation of form '+$form.attr('id')+' failed!');
		},
		onSuccess : function($form) {
			alert('The form '+$form.attr('id')+' is valid!');
			return false; // Will stop the submission of the form
		},
		onValidate : function($form) {
console.log('onValidate! ' + $form.attr('id') );
			//
//			doFormSubmit();
			//
			return {
				element : $('#some-input'),
				message : 'This input has an invalid value for some reason'
			}
		},
		onElementValidate : function(valid, $el, $form, errorMess) {
console.log('Input ' +$el.attr('id')+ ' is ' + ( valid ? 'VALID':'NOT VALID') + '  MSG: ' + errorMess );
		},

	});
}