// JavaScript Document

/*
	MAIN
*/
(function( $ ) {
	$.sortByDate = function( elements, order ) {
		var arr = [];
		elements.each(function() {
			var obj = {},
				$el = $(this),
/*
				time = $el.find( "time" ).text(),
				date = new Date( $.trim( time ) ),
				timestamp = date.getTime();
*/
				datetime = $el.data( "datetime" );
				
				obj.html = $el[0].outerHTML;
//				obj.time = timestamp;
				obj.time = datetime;
				
				arr.push( obj );
		});
		
		var sorted = arr.sort(function( a, b ) {
			if( order == "ASC" ) {
				return a.time > b.time;
			} else {
				return b.time > a.time;
			}
			
		});
		
		return sorted;
	};
	
	$(function() {
		var $content = $("table tbody"),
			$elements = $("table tbody tr");
		var elements = $.sortByDate( $elements, "DESC" );
		var html = "";
		for( var i = 0; i < elements.length; ++i ) {
			html += elements[i].html;
		}
		$content[0].innerHTML = html;
		$content.css('display', 'table-row-group');
	});
	
})( jQuery );

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return parts.join(",");
}

$(document).ready(function() {
	//
	$('#austrittsdate').change(function() {
//		console.log('val: ' + $(this).val() );
	});
	$('#austrittsdate').datepicker({
		dateFormat: "dd.mm.yy",
		firstDay: 1,
/*
		onSelect: function(dateText, inst){
			console.log('S: onSelect: ' + dateText + ' ' + inst);
		},
*/
	});
	
	$('#austritt-btn').click(function(){
console.log('austritt-btn.click();');
		//
		var t_maxMonths = 48;
		var t_sumRueck = 0;
		//
		var austrittsDatum = $('#austrittsdate').val();
console.log('austritts-datum: ' + austrittsDatum);
		//
		var t_dateParts = austrittsDatum.split('.');
		//	RECHNE MIT EXAKTEM DATUM: 	Y + M + D
		var t_ausDateOrig = new Date(t_dateParts[2], parseInt(t_dateParts[1], 10) - 1, t_dateParts[0]);
		//	RECHNE MIT 1. des MONATS:	Y + M
		var t_ausDate = new Date(t_dateParts[2], parseInt(t_dateParts[1], 10) - 1, 1);
console.log('t_ausDate: ' + t_ausDate);
//		t_date.setMonth( t_date.getMonth()+1 );
//console.log('t_date: ' + t_date);
		var t_ausDatetime = t_ausDate.getTime();
console.log('t_ausDatetime: ' + t_ausDatetime);
		//
		$('tr[data-datetime]').each(function(index){
			var t_trUid = $(this).data('uid');
			var t_trDatetime = parseInt($(this).data('datetime'))*1000;
console.log('tr: ' + index + ' uid: ' + t_trUid + ' datetime: ' + t_trDatetime);
			//
			var t_trDate = new Date(t_trDatetime);
console.log('t_trDate: ' + t_trDate);
//			t_trDate = new Date(t_trDate.getFullYear(), t_trDate.getMonth(), 1);
//console.log('t_trDate: ' + t_trDate);
			//
			var t_diffY, t_diffM;
			var t_aliquote = 0.0;
			var t_trDateY = t_trDate.getFullYear();
			var t_ausDateY = t_ausDate.getFullYear();
console.log('t_ausDateY: ' + t_ausDateY);
console.log('t_trDateY: ' + t_trDateY);
			if( t_trDate < t_ausDateOrig ){
				if( t_trDateY == t_ausDateY ){
console.log('t_trDateY == t_ausDateY');
					//	sind im gleichen Jahr, also nur die Monate nehmen !!!
					t_diffY = 0;
					t_diffM = t_ausDate.getMonth() - t_trDate.getMonth() + 1;
				} else {
					//	
					t_diffY = t_ausDateY - t_trDateY;
console.log('t_diffY: ' + t_diffY);
					t_diffM = t_diffY * 12;
console.log('t_diffM: ' + t_diffM);
					//	reduziere um vergangene Monate des Jahres des Termins
					//	Bsp:	15.04.2017	=>	15.3.2017	=>	getMonth() = 3	=>	-3	-> weil Jan, Feb und März ned zählen!!!
					//	Bsp:	15.12.2017	=>	12.11.2017	=>	getMonth() = 11	=>	-11	-> weil Jan bis Nov ned zählen!!!
					t_diffM -= t_trDate.getMonth();
					//	ergänze um Monate des aktuellen Jahres!
					//	Bsp:	7.05.2017	=>	7.4.2017	=>	getMonth() = 4	=>	+4 + 1	->	weil Jan bis inkl. Mai zählen!!!
					t_diffM += t_ausDate.getMonth() + 1;
				}
				//
console.log('t_diffM: ' + t_diffM);
				if( t_diffM <= t_maxMonths ){
					var t_sumEl = $(this).find('td[data-sumtotal]');
					var t_sum = t_sumEl.data('sumtotal');
					//
					t_sum = t_sum/t_maxMonths * (t_maxMonths - t_diffM);
					t_sum = Number(Math.round(t_sum+'e2')+'e-2');		 // 1.01
					//
					t_sumRueck += t_sum;
					//
					t_aliquote = t_sum;
console.log('t_aliquote: ' + t_aliquote + ' | M: ' + (t_maxMonths-t_diffM) );
//					t_aliquote += ' | M: ' + (t_maxMonths-t_diffM);
				}
			}
			//
			t_aliquote = t_aliquote.toFixed(2);
			t_aliquote = numberWithCommas(t_aliquote);
			t_aliquote = '&euro; ' + t_aliquote;
			$(this).find('td[data-sumaliquote]').html(t_aliquote);
		});
		//
		t_sumRueck = Number(Math.round(t_sumRueck+'e2')+'e-2');		 // 1.01
		t_sumRueck = t_sumRueck.toFixed(2);
		$('#sum-rueck').html(numberWithCommas(t_sumRueck));
	});
});
