<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "icon_reiseabrechnung".
 *
 * 2016-12-12, r.schmoller@next-linz.com
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'ICON - Reiseabrechnung',
  'description' => '',
  'category' => 'plugin',
  'author' => 'smo',
  'author_email' => 'r.schmoller@next-linz.com',
  'author_company' => '',
  'shy' => '',
  'priority' => '',
  'module' => '',
  'state' => 'stable',
  'internal' => '',
  'uploadfolder' => 1,
  'createDirs' => '1',
  'modify_tables' => '',
  'clearCacheOnLoad' => 1,
  'lockType' => '',
  'version' => '0.0.2',
  'constraints' => array (
    'depends' => array(
      'typo3' => '7.6',
      'fluid' => '7.6',
      'extbase' => '7.6',
    ),
    'conflicts' => array (
    ),
    'suggests' => array (
    ),
  ),
);

?>