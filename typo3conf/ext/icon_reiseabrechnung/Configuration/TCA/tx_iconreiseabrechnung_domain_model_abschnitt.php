<?php
defined ('TYPO3_MODE') or die ('Access denied.');

$_EXTKEY = 'icon_reiseabrechnung';

$version7 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('7.0');
$version8 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('8.0');

return array(
	'ctrl' => array(
		'title'     => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xml:tx_iconreiseabrechnung_domain_model_abschnitt',
		'label'     => 'title',
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'default_sortby' => 'ORDER BY crdate',
		'sortby' => 'sorting',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'treeParentField' => '',
		'searchFields' => 'uid,title',
		'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/tx_iconevents_domain_model_registration.gif',
	),
	'feInterface' => $TCA['tx_iconreiseabrechnung_domain_model_abschnitt']['feInterface'],
	'interface' => array(
		'showRecordFieldList' => 'sorting,hidden,personnr,reisenr,'
	),
	'columns' => array(
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			),
		),
	
		'sorting' => array(
			'label' => 'sorting',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
	
		'ab_datetime' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'ab_ort' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'ab_country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),

		'gu_datetime' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gu_ort' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gu_country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),

		'gu2_datetime' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gu2_ort' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gu2_country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),

		'gu3_datetime' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gu3_ort' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gu3_country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),

		'gu4_datetime' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gu4_ort' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gu4_country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),

		'an_datetime' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'an_ort' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'an_country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),


		'privatehours' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.firstname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'mittag' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.firstname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'abend' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.lastname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'nacht' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title_after',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),

	),
	'types' => array(
		0 => array(
			'showitem' =>	'title',
		),
	),
	'palettes' => array(
		'paletteCore' => array(
			'showitem' => 'hidden, ',
			'canNotCollapse' => TRUE,
		),
	)
);

?>