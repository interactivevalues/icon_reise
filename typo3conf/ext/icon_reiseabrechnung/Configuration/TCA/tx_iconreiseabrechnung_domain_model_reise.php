<?php
defined ('TYPO3_MODE') or die ('Access denied.');

$_EXTKEY = 'icon_reiseabrechnung';

$version7 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('7.0');
$version8 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('8.0');

return array(
	'ctrl' => array(
		'title'  => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event',
		'label' => 'title',
		'prependAtCopy' => ' - Kopie',
		'hideAtCopy' => TRUE,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'useColumnsForDefaultValues' => 'type',
		
		'searchFields' => 'title,',
		'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/tx_iconevents_domain_model_registration.gif',
	),
	'feInterface' => $TCA['tx_iconreiseabrechnung_domain_model_reise']['feInterface'],
	'interface' => array(
		'showRecordFieldList' => 'sorting,hidden,personnr,reisenr,'
	),
	'columns' => array(
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			),
		),
	
		'sorting' => array(
			'label' => 'sorting',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
        'deleted' => array(
            'label' => 'deleted',
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
	
		'personnr' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'firstname' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'lastname' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'status' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'reisenr' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'grund' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.firstname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'ziel' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.lastname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'firma' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title_after',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'kontaktperson' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'thema' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'klientennr' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'projektnr' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'verrechenbar' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'verkehrsmittel' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'kmgefahren' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'abschnitte' => array(
			'exclude' => 1,
			'label' => 'Abschnitte (max. 20)',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'tx_iconreiseabrechnung_domain_model_abschnitt',
				'foreign_table' => 'tx_iconreiseabrechnung_domain_model_abschnitt',
				'size' => 20,
				'minitems' => 0,
				'maxitems' => 20,
				'MM' => 'tx_iconreiseabrechnung_domain_model_reise_abschnitt_mm',
				'wizards' => array(
				),
			)
		),
		'passengers' => array(
			'exclude' => 1,
			'label' => 'Passengers (max. 10)',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'tx_iconreiseabrechnung_domain_model_passenger',
				'foreign_table' => 'tx_iconreiseabrechnung_domain_model_passenger',
				'size' => 10,
				'minitems' => 0,
				'maxitems' => 10,
				'MM' => 'tx_iconreiseabrechnung_domain_model_reise_passenger_mm',
				'wizards' => array(
				),
			)
		),
		'belege' => array(
			'exclude' => 1,
			'label' => 'Belege (max. 99)',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'tx_iconreiseabrechnung_domain_model_beleg',
				'foreign_table' => 'tx_iconreiseabrechnung_domain_model_beleg',
				'size' => 10,
				'minitems' => 0,
				'maxitems' => 99,
				'MM' => 'tx_iconreiseabrechnung_domain_model_reise_beleg_mm',
				'wizards' => array(
				),
			)
		),
		'belege_pdf' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum_taxfree' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum_tax' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum_total' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum_total_wv' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum_taggeld' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum_nachtgeld' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum_kmgeld' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'export_bh' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'export_lv' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'export_wv' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),

        'delete_reason' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.deleted_reason',
			'config' => array(
				'type' => 'input',
				'size' => 30,
			)
		),
	),
	'types' => array(
		0 => array(
			'showitem' => 'title, ',
		),
	),
	'palettes' => array(
		'paletteCore' => array(
			'showitem' => 'hidden,  ',
			'canNotCollapse' => TRUE,
		),
	)
);

?>