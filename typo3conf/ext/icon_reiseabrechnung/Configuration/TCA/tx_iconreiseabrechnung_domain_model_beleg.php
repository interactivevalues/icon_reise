<?php
defined ('TYPO3_MODE') or die ('Access denied.');

$_EXTKEY = 'icon_reiseabrechnung';

$version7 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('7.0');
$version8 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('8.0');

return array(
	'ctrl' => array(
		'title'     => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xml:tx_iconreiseabrechnung_domain_model_beleg',
		'label'     => 'title',
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'default_sortby' => 'ORDER BY crdate',
		'sortby' => 'sorting',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'treeParentField' => '',
		'searchFields' => 'uid,title',
		'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/tx_iconevents_domain_model_registration.gif',
	),
	'feInterface' => $TCA['tx_iconreiseabrechnung_domain_model_beleg']['feInterface'],
	'interface' => array(
		'showRecordFieldList' => 'sorting,l10n_parent,l10n_diffsource,hidden,personnr,title,datetime,place,brutto,netto,'
	),
	'columns' => array(
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			),
		),
	
		'sorting' => array(
			'label' => 'sorting',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
	
		'personnr' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'type' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.firstname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'currency' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.lastname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title_after',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum_euro_brutto' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'sum_euro_netto' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'taxfee' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'mitarbeiter' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
	),
	'types' => array(
		0 => array(
			'showitem' => 'title, ',
		),
	),
	'palettes' => array(
		'paletteCore' => array(
			'showitem' => 'hidden,  ',
			'canNotCollapse' => TRUE,
		),
	)
);

?>