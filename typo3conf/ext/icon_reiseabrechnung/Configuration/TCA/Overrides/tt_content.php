<?php
defined ('TYPO3_MODE') or die ('Access denied.');

$_EXTKEY = 'icon_reiseabrechnung';
$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);


/*
  INCLUDE PLUGINS
*/
  //  Pi1_Manager
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
  $_EXTKEY,
  'Pi1_Manager',
  'ICON - Reiseabrechnung (Sekretariat)'
);

  //  Pi1_User
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
  $_EXTKEY,
  'Pi1_User',
  'ICON - Reiseabrechnung (Mitarbeiter)'
);

  //  Pi1_Chief
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
  $_EXTKEY,
  'Pi1_Chief',
  'ICON - Reiseabrechnung (Geschäftsleitung)'
);

$pluginSignature = strtolower($extensionName) . '_pi1_manager';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,recursive,select_key,pages';

$pluginSignature = strtolower($extensionName) . '_pi1_user';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,recursive,select_key,pages';

$pluginSignature = strtolower($extensionName) . '_pi1_chief';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,recursive,select_key,pages';

?>