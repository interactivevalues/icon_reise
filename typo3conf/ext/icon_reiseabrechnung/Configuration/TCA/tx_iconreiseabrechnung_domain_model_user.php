<?php
defined ('TYPO3_MODE') or die ('Access denied.');

$_EXTKEY = 'icon_reiseabrechnung';

$version7 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('7.0');
$version8 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('8.0');

return array(
	'ctrl' => array(
		'title'     => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xml:tx_iconevents_domain_model_registration',
		'label'     => 'title',
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
	#	'languageField'            => '',
	#	'transOrigPointerField'    => 'l10n_parent',
	#	'transOrigDiffSourceField' => 'l10n_diffsource',
		'default_sortby' => 'ORDER BY crdate',
		'sortby' => 'sorting',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'treeParentField' => '',
		'searchFields' => 'uid,title',
		'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/tx_iconevents_domain_model_registration.gif',
	),
	#'feInterface' => $TCA['tx_iconreiseabrechnung_domain_model_user']['feInterface'],
	'interface' => array(
		'showRecordFieldList' => 'sorting,l10n_parent,l10n_diffsource,hidden,personnr,gender,title,firstname,lastname,title_after,email,'
	),
	'columns' => array(
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'sorting' => array(
			'label' => 'sorting',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			),
		),
        'deleted' => array(
            'label' => 'deleted',
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'l10n_parent' => array(
			'config' => array(
				'type' => 'passthrough'
			)
/*
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_iconevents_domain_model_registration',
				'foreign_table_where' => 'AND tx_iconevents_domain_model_registration.pid=###CURRENT_PID### AND tx_iconevents_domain_model_registration.sys_language_uid IN (-1,0)',
			)
*/
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'manager' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'personnr' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gender' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'firstname' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.firstname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'lastname' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.lastname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'title_after' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.title_after',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'email' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_registration.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
	),
	'types' => array(
		0 => array(
			'showitem' => 'title,',
		),
	),
	'palettes' => array(
		'paletteCore' => array(
			'showitem' => 'hidden,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource,',
			'canNotCollapse' => TRUE,
		),
		'paletteAccess' => array(
			'showitem' => 'starttime;LLL:EXT:cms/locallang_ttc.xml:starttime_formlabel, endtime;LLL:EXT:cms/locallang_ttc.xml:endtime_formlabel, ',
			'canNotCollapse' => TRUE,
		),
	)
);

?>