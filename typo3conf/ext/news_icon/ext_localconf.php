<?php

if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1',
  array(
    'Books' => 'list,detail',
  ),
  // non-cacheable actions
  array(
    'Books' => 'list',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi2',
  array(
    'News' => 'list,detail',
  ),
  // non-cacheable actions
  array(
    'News' => 'list',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi2_Teaser',
  array(
    'News' => 'teaser',
  ),
  // non-cacheable actions
  array(
    'News' => 'teaser',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi3',
  array(
    'Publikation' => 'list,detail',
  ),
  // non-cacheable actions
  array(
    'Publikation' => 'list',
  )
);

/*
	WICHTIG !!!		!!!			!!!			!!!
	
	damit das klappt muß realurl in /typo3conf/PackageStage.php vor dieser extension geladen werden !!!
	-> muss vl. per hand angepasst werden !!!

*/

/*
	1:	adding config for RealURL entry-access
*/
$realurl = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'];
if (is_array($realurl))  {
	//	APPLY fixedPostVars:
	if( !isset($realurl['_DEFAULT']['fixedPostVars']) ) { $realurl['_DEFAULT']['fixedPostVars'] = array(); }
	
	$realurl['_DEFAULT']['fixedPostVars']['bookConfiguration'] = array(
		array(
			'GETvar' => 'tx_newsicon_pi1[news]',
			'lookUpTable' => array(
				'table' => 'tx_news_domain_model_news',
				'id_field' => 'uid',
				'alias_field' => "CONCAT( FROM_UNIXTIME( datetime, '%Y-%m-%d' ), '-', title)",
				'addWhereClause' => ' AND NOT deleted ',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-'
				),
/*
				'languageGetVar' => 'L',
				'languageExceptionUids' => '',
				'languageField' => 'sys_language_uid',
				'transOrigPointerField' => 'l10n_parent',
*/
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			)
		),
		array(
			'GETvar' => 'tx_newsicon_pi1[action]',
/**/
			'valueMap' => array(
				'detail' => 'detail',
			),
/**/
			'noMatch' => 'bypass'
		),
		array(
			'GETvar' => 'tx_newsicon_pi1[controller]',
/*
			'valueMap' => array(
				'Books' => '',
			),
*/
			'noMatch' => 'bypass'
		),
	);
	$realurl['_DEFAULT']['fixedPostVars']['publicationConfiguration'] = array(
		array(
			'GETvar' => 'tx_newsicon_pi2[controller]',
/*
			'valueMap' => array(
				'News' => '',
			),
*/
			'noMatch' => 'bypass'
		),
		array(
			'GETvar' => 'tx_newsicon_pi2[action]',
			'valueMap' => array(
				'detail' => 'detail',
			),
			'noMatch' => 'bypass'
		),
		array(
			'GETvar' => 'tx_newsicon_pi2[news]',
			'lookUpTable' => array(
				'table' => 'tx_news_domain_model_news',
				'id_field' => 'uid',
				'alias_field' => "title",
/*
				'alias_field' => "CONCAT( FROM_UNIXTIME( datetime, '%Y-%m-%d' ), '-', title)",
*/
				'addWhereClause' => ' AND NOT deleted ',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-'
				),
/*
				'languageGetVar' => 'L',
				'languageExceptionUids' => '',
				'languageField' => 'sys_language_uid',
				'transOrigPointerField' => 'l10n_parent',
*/
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			)
		),
	);
	$realurl['_DEFAULT']['fixedPostVars']['publication2Configuration'] = array(
		array(
			'GETvar' => 'tx_newsicon_pi3[controller]',
/*
			'valueMap' => array(
				'News' => '',
			),
*/
			'noMatch' => 'bypass'
		),
		array(
			'GETvar' => 'tx_newsicon_pi3[action]',
			'valueMap' => array(
				'detail' => 'detail',
			),
			'noMatch' => 'bypass'
		),
		array(
			'GETvar' => 'tx_newsicon_pi3[news]',
			'lookUpTable' => array(
				'table' => 'tx_news_domain_model_news',
				'id_field' => 'uid',
				'alias_field' => "title",
/*
				'alias_field' => "CONCAT( FROM_UNIXTIME( datetime, '%Y-%m-%d' ), '-', title)",
*/
				'addWhereClause' => ' AND NOT deleted ',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-'
				),
/*
				'languageGetVar' => 'L',
				'languageExceptionUids' => '',
				'languageField' => 'sys_language_uid',
				'transOrigPointerField' => 'l10n_parent',
*/
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			)
		),
	);
//	$realurl['_DEFAULT']['fixedPostVars']['199'] = 'publicationConfiguration';
//	$realurl['_DEFAULT']['fixedPostVars']['200'] = 'bookConfiguration';


	if( !isset($realurl['_DEFAULT']['postVarSets']) ) { $realurl['_DEFAULT']['postVarSets'] = array(); }
	if( !isset($realurl['_DEFAULT']['postVarSets']['_DEFAULT']) ) { $realurl['_DEFAULT']['postVarSets']['_DEFAULT'] = array(); }

	//	BOOKS
	$realurl['_DEFAULT']['postVarSets']['_DEFAULT']['book'] = array(
		array(
			'GETvar' => 'tx_newsicon_pi1[controller]',
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_newsicon_pi1[action]',
			'valueMap' => array(
				'detail' => 'detail',
			),
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_newsicon_pi1[news]',
			'lookUpTable' => array(
				'table' => 'tx_news_domain_model_news',
				'id_field' => 'uid',
				'alias_field' => "CONCAT( FROM_UNIXTIME( datetime, '%Y-%m-%d' ), '-', title)",
				'addWhereClause' => ' AND NOT deleted',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-',
				),
				'languageGetVar' => 'L',
				'languageExceptionUids' => '',
				'languageField' => 'sys_language_uid',
				'transOrigPointerField' => 'l10n_parent',
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			),
		),
	);

	//	NEWS
	$realurl['_DEFAULT']['postVarSets']['_DEFAULT']['news'] = array(
		array(
			'GETvar' => 'tx_newsicon_pi2[controller]',
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_newsicon_pi2[action]',
			'valueMap' => array(
				'detail' => 'detail',
			),
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_newsicon_pi2[news]',
			'lookUpTable' => array(
				'table' => 'tx_news_domain_model_news',
				'id_field' => 'uid',
				'alias_field' => "CONCAT( FROM_UNIXTIME( datetime, '%Y-%m-%d' ), '-', title)",
				'addWhereClause' => ' AND NOT deleted',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-',
				),
				'languageGetVar' => 'L',
				'languageExceptionUids' => '',
				'languageField' => 'sys_language_uid',
				'transOrigPointerField' => 'l10n_parent',
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			),
		),
	);

	//	PUBLIKATIONEN
	$realurl['_DEFAULT']['postVarSets']['_DEFAULT']['publikation'] = array(
		array(
			'GETvar' => 'tx_newsicon_pi3[controller]',
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_newsicon_pi3[action]',
			'valueMap' => array(
				'detail' => 'detail',
			),
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_newsicon_pi3[news]',
			'lookUpTable' => array(
				'table' => 'tx_news_domain_model_news',
				'id_field' => 'uid',
				'alias_field' => "CONCAT( FROM_UNIXTIME( datetime, '%Y-%m-%d' ), '-', title)",
				'addWhereClause' => ' AND NOT deleted',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-',
				),
				'languageGetVar' => 'L',
				'languageExceptionUids' => '',
				'languageField' => 'sys_language_uid',
				'transOrigPointerField' => 'l10n_parent',
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			),
		),
	);

	//	reapply changes to TYPO3_CONF_VARS
	$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = $realurl;
}

?>