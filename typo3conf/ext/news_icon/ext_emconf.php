<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "news_icon"
 *
 * generated 2014-04-14
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
  'title' => 'ICON News-Erweiterung',
  'description' => 'Erweiterung für news-extension',
  'category' => 'plugin',
  'author' => 'smo',
  'author_email' => 'r.schmoller@next-linz.com',
  'author_company' => 'next e-Marketing GmbH.',
  'shy' => '',
  'priority' => '',
  'module' => '',
  'state' => 'stable',
  'internal' => '',
  'uploadfolder' => '1',
  'createDirs' => '',
  'modify_tables' => '',
  'clearCacheOnLoad' => 1,
  'lockType' => '',
  'version' => '0.0.8',
  'constraints' => array(
    'depends' => array(
      'typo3' => '7.6',
      'fluid' => '7.6',
      'extbase' => '7.6',
      'icon_erweiterungen' => '0.0.2',
    ),
    'conflicts' => array(
    ),
    'suggests' => array(
    ),
  ),
);

?>
