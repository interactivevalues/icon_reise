<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/*
	VERFASSER:

	-> nachlesen bzgl. dyn. Anpassungen
	http://labor.99grad.de/2016/11/26/typo3-tca-default-werte-per-tsconfig-setzen/
*/
$maxVerfasser = 10;
$maxVerfasser = ( preg_replace( '/(.*)(id=)([0-9]*)(.*)/i', '\\3', $GLOBALS['_GET']['returnUrl'] ) == 81 ? 4 : $maxVerfasser);
$maxVerfasser = ( preg_replace( '/(.*)(id=)([0-9]*)(.*)/i', '\\3', $GLOBALS['_GET']['returnUrl'] ) == 519 ? 6 : $maxVerfasser);

$fields = array(
	'addresses' => array(
		'exclude' => 1,
		'label' => 'Verfasser (max. '.$maxVerfasser.')',
		'config' => array(
			'type' => 'group',
			'internal_type' => 'db',
			'allowed' => 'tt_address',
			'foreign_table' => 'tt_address',
			'size' => $maxVerfasser,
			'minitems' => 0,
			'maxitems' => $maxVerfasser,
			'MM' => 'tx_news_domain_model_news_ttaddress_mm',
			'wizards' => array(
			),
			'behaviour' => array(
				'allowLanguageSynchronization' => true,
			),
		)
	),
	'pub_type' => array(
		'exclude' => 1,
		'label' => 'Heft / Medium',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'behaviour' => array(
				'allowLanguageSynchronization' => true,
			),
		)
	),
	'pub_pages' => array(
		'exclude' => 1,
		'label' => 'Seite(n)',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'behaviour' => array(
				'allowLanguageSynchronization' => true,
			),
		)
	),
	'pub_url' => array(
		'exclude' => 1,
		'label' => 'Verlinkung',
		'config' => array(
			'type' => 'input',
			'renderType' => 'inputLink',
			'size' => 30,
			'eval'     => 'trim',
			'behaviour' => array(
				'allowLanguageSynchronization' => true,
			),
		)
	),
);

// Add new fields to tx_news_domain_model_news:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news', $fields);

// Make fields visible in the TCEforms:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tx_news_domain_model_news', 
	' --div--;Verfasser, addresses, 
	 --div--;Publikation, pub_type, pub_pages, pub_url, '
);

?>