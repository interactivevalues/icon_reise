<?php
defined('TYPO3_MODE') or die();

$_EXTKEY = 'news_icon';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'ICON News');
