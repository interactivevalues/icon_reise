plugin.tx_newsicon {
	view {
		templateRootPath = {$plugin.tx_newsicon.view.templateRootPath}
		partialRootPath = {$plugin.tx_newsicon.view.partialRootPath}
		layoutRootPath = {$plugin.tx_newsicon.view.layoutRootPath}
	}
	settings {
		person {
			detail {
				pageUid = {$plugin.tx_newsicon.settings.person.detail.pageUid}
				extensionName = {$plugin.tx_newsicon.settings.person.detail.extensionName}
				pluginName = {$plugin.tx_newsicon.settings.person.detail.pluginName}
				controller = {$plugin.tx_newsicon.settings.person.detail.controller}
				action = {$plugin.tx_newsicon.settings.person.detail.action}
			}
			image {
				width = {$plugin.tx_newsicon.settings.person.image.width}
				height = {$plugin.tx_newsicon.settings.person.image.height}
			}
			general {
				phone = {$plugin.tx_newsicon.settings.person.general.phone}
				fax = {$plugin.tx_newsicon.settings.person.general.fax}
			}
		}
	}
}
