config.tx_extbase{
  persistence {
    classes {

      NEXT\NewsIcon\Domain\Model\News {
        mapping {
          tableName = tx_news_domain_model_news
        }
      }

      NEXT\NewsIcon\Domain\Model\Category {
        mapping {
          tableName = sys_category
          columns {
            parent.mapOnProperty = parentcategory
			sorting.mapOnProperty = sorting
          }
        }
      }

      NEXT\NewsIcon\Domain\Model\Address {
        mapping {
          tableName = tt_address
        }
      }
	  
    }
  }
}



/*

config.tx_extbase{
    persistence{
        classes{

            Tx_News_Domain_Model_News {
                subclasses {
                    Tx_NewsIcon_News = NEXT\NewsIcon\Domain\Model\News
          
              }
            }
            NEXT\NewsIcon\Domain\Model\News {
                mapping {
                    tableName = tx_news_domain_model_news
                    recordType = Tx_NewsIcon_News
                }
            }
            
      }
    }
}

*/