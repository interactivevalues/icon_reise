<?php
namespace NEXT\NewsIcon\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package news_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PublikationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

  /**
   * newsRepository
   *
   * @var \NEXT\NewsIcon\Domain\Repository\NewsRepository
   * @inject
   */
  protected $newsRepository;

  /**
   * addressRepository
   *
   * @var \NEXT\NewsIcon\Domain\Repository\AddressRepository
   * @inject
   */
  protected $addressRepository;
  
  
  /**
   * action list
   *
   * @param \NEXT\NewsIcon\Domain\Model\Search $searchdata
   * @dontvalidate $searchdata
   * @return void
   */
  public function listAction(\NEXT\NewsIcon\Domain\Model\Search $searchdata = NULL) {
    $paginate = array();
/*
    $paginate['addQueryStringMethod'] = $this->settings['paginate']['addQueryStringMethod'];
    $paginate['maximumNumberOfLinks'] = $this->settings['paginate']['maximumNumberOfLinks'];
    $paginate['itemsPerPage'] = $this->settings['paginate']['itemsPerPage'];
    $paginate['insertAbove'] = $this->settings['paginate']['insertAbove'];
    $paginate['insertBelow'] = $this->settings['paginate']['insertBelow'];
*/
	//
	$pid = $this->settings['storagePid'];
	if( $searchdata==NULL ){
		$searchdata = $this->objectManager->getEmptyObject('\NEXT\NewsIcon\Domain\Model\Search');
		//
		if( $this->request->hasArgument('author') ){
			$author = $this->request->getArgument('author');
			$searchdata->setAuthor($author);
		}
	}

//    $authors = $this->newsRepository->getAuthors($pid);
	$authors = $this->addressRepository->getRelatedToNews($pid);
    $years = $this->newsRepository->getYears($pid);
    $yearmonths = $this->newsRepository->getYearMonths($pid);
    $categories = $this->newsRepository->getCategories($pid);
    //
    //
    $result = $this->newsRepository->searchNewsNew($pid, $searchdata );
//    $result = $this->newsRepository->searchNews($pid, $searchdata );
    
    $this->view->assign('searchdata', $searchdata);
    $this->view->assign('authors', $authors);
    $this->view->assign('years', $years);
    $this->view->assign('yearmonths', $yearmonths);
    $this->view->assign('categories', $categories);
    $this->view->assign('news', $result);
  }

  /**
   * action detail
   *
   * @param \NEXT\NewsIcon\Domain\Model\News $news news item
   * @return void
   */
  public function detailAction(\NEXT\NewsIcon\Domain\Model\News $news = NULL) {
	//	CHANGE PAGE - TITLE
	$page_title = trim( $news->getTitle() );
	$GLOBALS['TSFE']->page['title'] = $page_title;
	$GLOBALS['TSFE']->indexedDocTitle = $page_title;
	//
    $this->view->assign('newsItem', $news);
  }

  /**
   * action teaser
   *
   * @param \NEXT\NewsIcon\Domain\Model\News $news news item
   * @return void
   */
  public function teaserAction(\NEXT\NewsIcon\Domain\Model\News $news = NULL) {
    $pid = (int)$this->settings['storagePid'];
	$offset = 0;
	$limit = (int)$this->settings['limit'];
	//
	$searchdata = $this->objectManager->getEmptyObject('\NEXT\NewsIcon\Domain\Model\Search');
	//
    $result = $this->newsRepository->searchNews($pid, $searchdata, $offset, $limit);
    $this->view->assign('news', $result);
  }

}