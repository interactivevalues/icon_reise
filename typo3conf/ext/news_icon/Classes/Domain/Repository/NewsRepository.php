<?php
namespace NEXT\NewsIcon\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package news_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
/*
class NewsRepository extends \Tx_News_Domain_Repository_NewsRepository {
	*/
class NewsRepository extends \GeorgRinger\News\Domain\Repository\NewsRepository {

  /**
   * searchNews
   *
   * @param \integer $pid
   * @param \NEXT\NewsIcon\Domain\Model\Search $searchdata
   * @param \integer $offset
   * @param \integer $limit
   * @return
   */
  public function searchNews($pid, $searchdata, $offset=NULL, $limit=NULL) {
    //
    $onlyPast = TRUE;
    //
    $query = $this->createQuery();
    $query->getQuerySettings()->setRespectStoragePage(FALSE);
    
    $constraints = array();
    if( $pid > 0 ){
      $constraints[] = $query->equals('pid', $pid);
    }
    if( $onlyPast ){
      $constraints[] = $query->lessThan('datetime', $GLOBALS['EXEC_TIME']);
    }
    
    if( $searchdata->getPhrase() ){
      $constraints[] = $query->like('title', '%' . $searchdata->getPhrase() . '%');
    }
    if( $searchdata->getAuthor() ){
      $constraints[] = $query->equals('author', $searchdata->getAuthor());
    }
    if( $searchdata->getYear() > 0 ){
      $t_year = $searchdata->getYear();
      $t_from = mktime(0, 0, 0, 1, 1, $t_year);
      $t_to = mktime(0, 0, 0, 1, 1, $t_year+1 );
      $constraints[] = $query->logicalAnd(
        $query->greaterThanOrEqual('datetime', $t_from ),
        $query->lessThan('datetime', $t_to)
      );
    }
    if( $searchdata->getYearmonth() > '' ){
      $t_ym = explode('/', $searchdata->getYearmonth());
      $t_y = (int) $t_ym[0];
      $t_m = (int) $t_ym[1];
      $t_from = mktime(0, 0, 0, $t_m, 1, $t_y);
      if( $t_m < 12 ){
        $t_to = mktime(0, 0, 0, $t_m+1, 1, $t_y);
      } else {
        $t_to = mktime(0, 0, 0, 1, 1, $t_y+1 );
      }
      $constraints[] = $query->logicalAnd(
        $query->greaterThanOrEqual('datetime', $t_from ),
        $query->lessThan('datetime', $t_to)
      );
    }
/*
    if( $searchdata->getCategory() > 0 ){
      $constraints[] = $query->contains('categories', $searchdata->getCategory() );
    }
*/
	$category = $searchdata->getCategory();
	if( $category ){
		$t_constraints = array();
		$categories = explode(',', $category);
		foreach($categories as $categoryUid){
			$t_constraints[] = $query->contains('categories', $categoryUid );
		}
		$constraints[] = $query->logicalOr($t_constraints);
	}
	$tag = $searchdata->getTag();
	if( $tag ){
		$t_constraints = array();
		$tags = explode(',', $tag);
		foreach($tags as $tagUid){
			$t_constraints[] = $query->contains('tags', $tagUid );
		}
		$constraints[] = $query->logicalOr($t_constraints);
	}

    if (!empty($constraints)) {
      $query->matching(
        $query->logicalAnd($constraints)
      );
    }
    
    $query->setOrderings(
      array(
        'datetime' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
        'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
      )
    );
	if( $offset >= 0 && $limit >= 1 ){
	    $query->setOffset($offset);
    	$query->setLimit($limit);
	}
	
    return $query->execute();
  }


  /**
   * searchNewsNew
   *
   * @param \integer $pid
   * @param \NEXT\NewsIcon\Domain\Model\Search $searchdata
   * @param \integer $offset
   * @param \integer $limit
   * @return
   */
  public function searchNewsNew($pid, $searchdata, $offset=NULL, $limit=NULL) {
    //
    $onlyPast = TRUE;
    //
    $query = $this->createQuery();
    $query->getQuerySettings()->setRespectStoragePage(FALSE);
    
    $constraints = array();
    if( $pid > 0 ){
      $constraints[] = $query->equals('pid', $pid);
    }
    if( $onlyPast ){
      $constraints[] = $query->lessThan('datetime', $GLOBALS['EXEC_TIME']);
    }
    
    if( $searchdata->getPhrase() ){
      $constraints[] = $query->like('title', '%' . $searchdata->getPhrase() . '%');
    }
/*
    if( $searchdata->getAuthor() ){
      $constraints[] = $query->equals('author', $searchdata->getAuthor());
    }
*/
    if( $searchdata->getAuthor() > 0 ){
      $constraints[] = $query->contains('addresses', $searchdata->getAuthor() );
    }

    if( $searchdata->getYear() > 0 ){
      $t_year = $searchdata->getYear();
      $t_from = mktime(0, 0, 0, 1, 1, $t_year);
      $t_to = mktime(0, 0, 0, 1, 1, $t_year+1 );
      $constraints[] = $query->logicalAnd(
        $query->greaterThanOrEqual('datetime', $t_from ),
        $query->lessThan('datetime', $t_to)
      );
    }
    if( $searchdata->getYearmonth() > '' ){
      $t_ym = explode('/', $searchdata->getYearmonth());
      $t_y = (int) $t_ym[0];
      $t_m = (int) $t_ym[1];
      $t_from = mktime(0, 0, 0, $t_m, 1, $t_y);
      if( $t_m < 12 ){
        $t_to = mktime(0, 0, 0, $t_m+1, 1, $t_y);
      } else {
        $t_to = mktime(0, 0, 0, 1, 1, $t_y+1 );
      }
      $constraints[] = $query->logicalAnd(
        $query->greaterThanOrEqual('datetime', $t_from ),
        $query->lessThan('datetime', $t_to)
      );
    }
    if( $searchdata->getCategory() > 0 ){
      $constraints[] = $query->contains('categories', $searchdata->getCategory() );
    }
	$tag = $searchdata->getTag();
	if( $tag ){
		$t_constraints = array();
		$tags = explode(',', $tag);
		foreach($tags as $tagUid){
			$t_constraints[] = $query->contains('tags', $tagUid );
		}
		$constraints[] = $query->logicalOr($t_constraints);
	}

    if (!empty($constraints)) {
      $query->matching(
        $query->logicalAnd($constraints)
      );
    }
    
    $query->setOrderings(
      array(
        'datetime' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
        'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
      )
    );
	if( $offset >= 0 && $limit >= 1 ){
	    $query->setOffset($offset);
    	$query->setLimit($limit);
	}
	
    return $query->execute();
  }



  public function getAuthors ($pid) {
    $t = array();
    //
    $sql = 'SELECT author FROM tx_news_domain_model_news ';
    if( $pid > 0 ){
      $sql .= ' WHERE pid = ' . $pid . ' ';
    }
    $sql .= ' GROUP BY author ORDER BY author ASC ';

    $res = $GLOBALS['TYPO3_DB']->sql_query($sql);
    while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
      $author = trim($row['author']);
      if( $author ){
        $t[$author] = $author;
      }
    }
    $GLOBALS['TYPO3_DB']->sql_free_result($res);
    //
    return $t;    
  }

  public function getYears ($pid) {
    $t = array();
    //
    $yearFirst = $this->getYearOfFirstNews($pid);
    if( $yearFirst ){
      $yearNow = intval(date("Y"));
      for( $y=$yearNow; $y>=$yearFirst; $y-- ){
        $t[$y] = $y;
      }
    }
    //
    return $t;
  }

  public function getYearMonths ($pid) {
    $t = array();
    $m_names = array(1 => 'Jänner', 2 => 'Februar', 3 => 'März', 4 => 'April', 5 => 'Mai', 6 => 'Juni', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Dezember');
    //
    $yearFirst = $this->getYearOfFirstNews($pid);
    if( $yearFirst ){
      $yearNow = intval(date("Y"));
      $monthNow = intval(date("m"));
      for( $y=$yearNow; $y>=$yearFirst; $y-- ){
        for( $m=12; $m>0; $m--){
          if( ($y==$yearNow && $m<=$monthNow) || $y < $yearNow ){
            $t[$y.'/'.$m] = $y . ' ' . $m_names[$m];
          }
        }
      }
    }
    //
    return $t;
  }
  
  public function getYearOfFirstNews ($pid) {
    $query = $this->createQuery();
    $query->getQuerySettings()->setRespectStoragePage(FALSE);
    
    $constraints = array();
    if( $pid > 0 ){
      $constraints[] = $query->equals('pid', $pid);
    }

    if (!empty($constraints)) {
      $query->matching(
        $query->logicalAnd($constraints)
      );
    }
    
    $query->setOrderings( array('datetime' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING ));
    $query->setOffset(0);
    $query->setLimit(1);

    $res = $query->execute()->getFirst();
    if( $res ){
      return $res->getDatetime()->format('Y');
    }
    
    return NULL;    
  }

  public function getCategories ($pid) {
    $t = array();
    //
    $sql = 'SELECT uid, title FROM sys_category ';
    if( $pid > 0 ){
      $sql .= ' WHERE pid = ' . $pid . ' ';
    }
    $sql .= ' ORDER BY title ASC ';

    $res = $GLOBALS['TYPO3_DB']->sql_query($sql);
    while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
      $t[$row['uid']] = $row['title'];
    }
    $GLOBALS['TYPO3_DB']->sql_free_result($res);
    //
    return $t;    
  }

}
?>