<?php
namespace NEXT\NewsIcon\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package news_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AddressRepository extends \TYPO3\CMS\Extbase\Persistence\Repository  {

	/**
	 * getRelatedToNews
	 *
	 * @param \integer $pid
	 * return 
	 */
	public function getRelatedToNews ($pid) {
		$t = array();
		//
		$sql = "SELECT TA.uid, TA.title, TA.first_name, TA.last_name, TA.title_after 
				FROM tt_address AS TA, tx_news_domain_model_news_ttaddress_mm AS MM, tx_news_domain_model_news AS TN 
				WHERE TA.uid = MM.uid_foreign
				AND MM.uid_local = TN.uid 
				AND TN.deleted = 0 AND TN.hidden = 0 AND TA.ausgetretten = 0";
		if( $pid > 0 ){
			$sql .= " AND TN.pid = " . $pid;
		}
		$sql .= " GROUP BY TA.uid, TA.last_name, TA.first_name ";
		$sql .= " ORDER BY TA.last_name, TA.first_name ";
		
		$res = $GLOBALS['TYPO3_DB']->sql_query($sql);
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$r_name = $row['title'] . ' ' . $row['first_name'] . ' ' . $row['last_name'];
			if( $row['title_after'] != '' ){
				$r_name .= ', ' . $row['title_after'];
			}
			$t[$row['uid']] = trim($r_name);
		}
		$GLOBALS['TYPO3_DB']->sql_free_result($res);
		//
		return $t;    
	}

}
?>