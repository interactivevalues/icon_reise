<?php
namespace NEXT\NewsIcon\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package news_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Search extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

  /**
   * Phrase
   *
   * @var \string
   */
  protected $phrase;
  
  /**
   * Author
   *
   * @var \string
   */
  protected $author;

  /**
   * Year
   *
   * @var \integer
   */
  protected $year;

  /**
   * Yearmonth
   *
   * @var \string
   */
  protected $yearmonth;
  
  /**
   * Category
   *
   * @var \string
   */
  protected $category;

  /**
   * Tag
   *
   * @var \string
   */
  protected $tag;
  

  /**
   * Sets the phrase
   *
   * @param \string $phrase
   * @return void
   */
  public function setPhrase($phrase) {
    $this->phrase = trim($phrase);
  }

  /**
   * Returns the phrase
   *
   * @return \string $phrase
   */
  public function getPhrase() {
    return $this->phrase;
  }

  /**
   * Sets the author
   *
   * @param \string $author
   * @return void
   */
  public function setAuthor($author) {
    $this->author = trim($author);
  }

  /**
   * Returns the author
   *
   * @return \string $author
   */
  public function getAuthor() {
    return $this->author;
  }

  /**
   * Sets the year
   *
   * @param \integer $year
   * @return void
   */
  public function setYear($year) {
    $this->year = $year;
  }

  /**
   * Returns the year
   *
   * @return \integer $year
   */
  public function getYear() {
    return $this->year;
  }

  /**
   * Sets the yearmonth
   *
   * @param \string $yearmonth
   * @return void
   */
  public function setYearmonth($yearmonth) {
    $this->yearmonth = $yearmonth;
  }

  /**
   * Returns the yearmonth
   *
   * @return \string $yearmonth
   */
  public function getYearmonth() {
    return $this->yearmonth;
  }

  /**
   * Sets the category
   *
   * @param \string $category
   * @return void
   */
  public function setCategory($category) {
    $this->category = $category;
  }

  /**
   * Returns the category
   *
   * @return \string $category
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * Sets the tag
   *
   * @param \string $tag
   * @return void
   */
  public function setTag($tag) {
    $this->tag = $tag;
  }

  /**
   * Returns the tag
   *
   * @return \string $tag
   */
  public function getTag() {
    return $this->tag;
  }

}
?>