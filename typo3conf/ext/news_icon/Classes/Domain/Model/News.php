<?php
namespace NEXT\NewsIcon\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * News model
 *
 * @package news_icon
 * @subpackage news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
/*
class News extends \Tx_News_Domain_Model_News {
*/
class News extends \GeorgRinger\News\Domain\Model\News {
	/**
	 * pub_type
	 *
	 * @var string
	 */
	protected $pubType;

	/**
	 * pub_pages
	 *
	 * @var string
	 */
	protected $pubPages;

	/**
	 * pub_url
	 *
	 * @var string
	 */
	protected $pubUrl;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\NewsIcon\Domain\Model\Category>
	 * @lazy
	 */
	protected $categories;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\NewsIcon\Domain\Model\Address>
	 * @lazy
	 */
	protected $addresses;

	/**
	 * Initialize categories and media relation
	 *
	 * @return \NEXT\NewsIcon\Domain\Model\News
	 */
	public function __construct() {
		$this->categories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->contentElements = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->relatedFiles = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->relatedLinks = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->media = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->falMedia = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->falRelatedFiles = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->addresses = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Get addresses
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\NewsIcon\Domain\Model\Address>
	 */
	public function getAddresses() {
		return $this->addresses;
	}

	/**
	 * Get categoriesSorted
	 *
	 * @return \array
	 */
	public function getCategoriesSorted() {
		$cS = array();
		
		foreach( $this->categories as $category ){
			$cS[ $category->getTitle() ] = $category;
		}
		//
		sort($cS);
		//
		return $cS;
	}

	/**
	 * Sets the pubType
	 *
	 * @param string $pubType
	 * @return void
	*/
	public function setPubType($pubType) {
		$this->pubType = $pubType;
	}
	
	/**
	 * Returns the pubType
	 *
	 * @return string $pubType
	 */
	public function getPubType() {
		return $this->pubType;
	}

	/**
	 * Sets the pubPages
	 *
	 * @param string $pubPages
	 * @return void
	*/
	public function setPubPages($pubPages) {
		$this->pubPages = $pubPages;
	}
	
	/**
	 * Returns the pubPages
	 *
	 * @return string $pubPages
	 */
	public function getPubPages() {
		return $this->pubPages;
	}

	/**
	 * Sets the pubUrl
	 *
	 * @param string $pubUrl
	 * @return void
	*/
	public function setPubUrl($pubUrl) {
		$this->pubUrl = $pubUrl;
	}
	
	/**
	 * Returns the pubUrl
	 *
	 * @return string $pubUrl
	 */
	public function getPubUrl() {
		return $this->pubUrl;
	}


}
?>
