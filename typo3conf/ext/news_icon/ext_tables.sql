#
# Table structure for table 'news_icon'
#
CREATE TABLE tx_news_domain_model_news (
	addresses int(11) DEFAULT '0' NOT NULL,
	pub_type varchar(255) DEFAULT '' NOT NULL,
	pub_pages varchar(255) DEFAULT '' NOT NULL,
	pub_url varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_news_domain_model_news_ttaddress_mm'
#
CREATE TABLE tx_news_domain_model_news_ttaddress_mm (
	uid_local int(11) DEFAULT '0' NOT NULL,
	uid_foreign int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);
