<?php
namespace NEXT\IconNeuklientenanlage\Validation\Validator;

/***************************************************************
* Copyright notice
*
* (c) 2015 robert`smo´ schmoller <r.schmoller@next-linz.com>
*
* All rights reserved
*
* This script is part of the TYPO3 project. The TYPO3 project is
* free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* The GNU General Public License can be found at
* http://www.gnu.org/copyleft/gpl.html.
*
* This script is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
* Formdata validator
*
* @package icon_neuklientenanlage
* @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
*
*/
class FormCompanyValidator extends \NEXT\IconNeuklientenanlage\Validation\Validator\FormValidator {
//class FormValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator {

	/**
	 * Returns TRUE, if the given property ($value) is a valid.
	 *
	 * Otherwise, it is FALSE.
	 *
	 * @param mixed $formdata The value that should be validated
	 * @return boolean TRUE if the value is valid, FALSE if an error occured
	 * @api
	 */
	protected function isValid($formdata) {
		$success = TRUE;
		//	validate Company data
		$success = $this->validateCompany($formdata, $success);
		//	validate Billing Address
		$success = $this->validateBillAddress($formdata, $success);
		//	validate Billing Email Address
		$success = $this->validateBillEmailAddress($formdata, $success);
		//	validate TAX-OFFICES
//		$success = $this->validateTaxOffices($formdata, $success);
		//	validate PERSON 0 - 5
		$success = $this->validatePersons($formdata, $success);
		//	validate Fileuploads
		$success = $this->validateFileuploads($succcess);
		//
		return $success;	
	}
	
	/**
	 * validates comName, comStreet, comStreetNr, comZIP, comCity, comCountry
	 *
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateCompany ($formdata, $success){
		$isCompany = strlen($formdata->getComName()) > 1;
		$isStreet = strlen($formdata->getComStreet()) > 1;
		$isStreetNr = strlen($formdata->getComStreetNr()) > 0;
		$isZIP = strlen($formdata->getComZip()) > 3;
		$isCity = strlen($formdata->getComCity()) > 1;
		$isCountry = strlen($formdata->getComCountry()) > 1;
		//
		if( !$isCompany ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Firmenwortlaut nicht ausgefüllt!', time());
			$this->result->forProperty('comName')->addError($error);
			$success = FALSE;
		}
		if( !$isStreet ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Straße nicht ausgefüllt!', time());
			$this->result->forProperty('comStreet')->addError($error);
			$success = FALSE;
		}
		if( !$isStreetNr ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Hausnummer nicht ausgefüllt!', time());
			$this->result->forProperty('comStreetNr')->addError($error);
			$success = FALSE;
		}
		if( !$isZIP ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('PLZ nicht ausgefüllt!', time());
			$this->result->forProperty('comZip')->addError($error);
			$success = FALSE;
		}
		if( !$isCity ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Stadt nicht ausgefüllt!', time());
			$this->result->forProperty('comCity')->addError($error);
			$success = FALSE;
		}
		if( !$isCountry ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Land nicht ausgefüllt!', time());
			$this->result->forProperty('comCountry')->addError($error);
			$success = FALSE;
		}
/*
		//
		$error = new \TYPO3\CMS\Extbase\Validation\Error('Company', time());
		$this->result->forProperty('comResponse')->addError($error);
*/
		//
		return $success;
	}
	
	/**
	 * validates billCompanyname, billGender, billFunction, billStreet, billStreetNr, billZIP, billCity, billCountry
	 *
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateBillAddress ($formdata, $success){
		//	check if billAddr <> 0 / ist set -> then validate Data
		if( $formdata->getBillAddr() ){
			//
			$isCompany = strlen($formdata->getBillCompanyname()) > 1;
//			$isGender = strlen($formdata->getBillGender()) > 1;
//			$isFunction = strlen($formdata->getBillFunction()) > 1;
			$isStreet = strlen($formdata->getBillStreet()) > 1;
			$isStreetNr = strlen($formdata->getBillStreetNr()) > 0;
			$isZIP = strlen($formdata->getBillZip()) > 3;
			$isCity = strlen($formdata->getBillCity()) > 1;
			$isCountry = strlen($formdata->getBillCountry()) > 1;
			//
/*
			if( !$isCompany ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Firmenwortlaut nicht ausgefüllt!', time());
				$this->result->forProperty('billCompanyname')->addError($error);
				$success = FALSE;
			}
			if( !$isGender ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Anrede nicht gewählt!', time());
				$this->result->forProperty('billGender')->addError($error);
				$success = FALSE;
			}
			if( !$isFunction ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Position / Funktion nicht ausgefüllt!', time());
				$this->result->forProperty('billFunction')->addError($error);
				$success = FALSE;
			}
*/
			if( !$isStreet ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Straße nicht gewählt!', time());
				$this->result->forProperty('billStreet')->addError($error);
				$success = FALSE;
			}
			if( !$isStreetNr ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Hausnummer nicht ausgefüllt!', time());
				$this->result->forProperty('billStreetNr')->addError($error);
				$success = FALSE;
			}
			if( !$isZIP ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('PLZ nicht ausgefüllt!', time());
				$this->result->forProperty('billZip')->addError($error);
				$success = FALSE;
			}
			if( !$isCity ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Stadt nicht ausgefüllt!', time());
				$this->result->forProperty('billCity')->addError($error);
				$success = FALSE;
			}
			if( !$isCountry ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Land nicht ausgefüllt!', time());
				$this->result->forProperty('billCountry')->addError($error);
				$success = FALSE;
			}
		}
		//
		return $success;
	}
	
	/**
	 * validates billEmailGender, billEmailFirstname, billEmailLastname, billEmailEmail
	 *
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateBillEmailAddress ($formdata, $success){
		//	check if billAddr <> 0 / ist set -> then validate Data
		if( $formdata->getBillEmailMode() ){
			//
//			$isGender = strlen($formdata->getBillEmailGender()) > 1;
//			$isFN = strlen($formdata->getBillEmailFirstname()) > 1;
//			$isLN = strlen($formdata->getBillEmailLastname()) > 1;
//			$isFunction = strlen($formdata->getBillEmailFunction()) > 1;
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getBillEmailEmail());
			//
/*
			if( !$isGender ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Anrede nicht gewählt!', time());
				$this->result->forProperty('billEmailGender')->addError($error);
				$success = FALSE;
			}
			if( !$isFN ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('billEmailFirstname')->addError($error);
				$success = FALSE;
			}
			if( !$isLN ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Nachname nicht gewählt!', time());
				$this->result->forProperty('billEmailLastname')->addError($error);
				$success = FALSE;
			}
			if( !$isFunction ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Position / Funktion nicht ausgefüllt!', time());
				$this->result->forProperty('billEmailFunction')->addError($error);
				$success = FALSE;
			}
*/
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('billEmailEmail')->addError($error);
				$success = FALSE;
			}
		}
		//
		return $success;
	}

	/*
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateTaxOffices ($formdata, $success){
		//
		if( $formdata->getTaxOfficeUsed1() ){
			$isTO = strlen($formdata->getTaxOffice1()) > 1;
			$isId = strlen($formdata->getTaxId1()) > 1;
			$isUid = strlen($formdata->getTaxUid1()) > 1;
			//
			if(!$isTO || !$isId || !$isUid ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('1. Finanzamt nicht korret angegeben!', time());
				$this->result->forProperty('taxOfficeUsed1')->addError($error);
				$success = FALSE;
				//
				if( !$isTO ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Finanzamt nicht angegeben!', time());
					$this->result->forProperty('taxOffice1')->addError($error);
				}
				if( !$isId ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Steuernummer nicht angegeben!', time());
					$this->result->forProperty('taxId1')->addError($error);
				}
				if( !$isUid ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('UID-Nr. nicht angegeben!', time());
					$this->result->forProperty('taxUid1')->addError($error);
				}
			}
		}
		if( $formdata->getTaxOfficeUsed2() ){
			$isTO = strlen($formdata->getTaxOffice2()) > 1;
			$isId = strlen($formdata->getTaxId2()) > 1;
			$isUid = strlen($formdata->getTaxUid2()) > 1;
			//
			if(!$isTO || !$isId || !$isUid ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('2. Finanzamt nicht korret angegeben!', time());
				$this->result->forProperty('taxOfficeUsed2')->addError($error);
				$success = FALSE;
				//
				if( !$isTO ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Finanzamt nicht angegeben!', time());
					$this->result->forProperty('taxOffice2')->addError($error);
				}
				if( !$isId ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Steuernummer nicht angegeben!', time());
					$this->result->forProperty('taxId2')->addError($error);
				}
				if( !$isUid ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('UID-Nr. nicht angegeben!', time());
					$this->result->forProperty('taxUid2')->addError($error);
				}
			}
		}
		if( $formdata->getTaxOfficeUsed3() ){
			$isTO = strlen($formdata->getTaxOffice3()) > 1;
			$isId = strlen($formdata->getTaxId3()) > 1;
			$isUid = strlen($formdata->getTaxUid3()) > 1;
			//
			if(!$isTO || !$isId || !$isUid ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('3. Finanzamt nicht korret angegeben!', time());
				$this->result->forProperty('taxOfficeUsed3')->addError($error);
				$success = FALSE;
				//
				if( !$isTO ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Finanzamt nicht angegeben!', time());
					$this->result->forProperty('taxOffice3')->addError($error);
				}
				if( !$isId ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Steuernummer nicht angegeben!', time());
					$this->result->forProperty('taxId3')->addError($error);
				}
				if( !$isUid ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('UID-Nr. nicht angegeben!', time());
					$this->result->forProperty('taxUid3')->addError($error);
				}
			}
		}
		if( $formdata->getTaxOfficeUsed4() ){
			$isTO = strlen($formdata->getTaxOffice4()) > 1;
			$isId = strlen($formdata->getTaxId4()) > 1;
			$isUid = strlen($formdata->getTaxUid4()) > 1;
			//
			if(!$isTO || !$isId || !$isUid ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('4. Finanzamt nicht korret angegeben!', time());
				$this->result->forProperty('taxOfficeUsed4')->addError($error);
				$success = FALSE;
				//
				if( !$isTO ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Finanzamt nicht angegeben!', time());
					$this->result->forProperty('taxOffice4')->addError($error);
				}
				if( !$isId ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Steuernummer nicht angegeben!', time());
					$this->result->forProperty('taxId4')->addError($error);
				}
				if( !$isUid ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('UID-Nr. nicht angegeben!', time());
					$this->result->forProperty('taxUid4')->addError($error);
				}
			}
		}
		if( $formdata->getTaxOfficeUsed5() ){
			$isTO = strlen($formdata->getTaxOffice5()) > 1;
			$isId = strlen($formdata->getTaxId5()) > 1;
			$isUid = strlen($formdata->getTaxUid5()) > 1;
			//
			if(!$isTO || !$isId || !$isUid ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('5. Finanzamt nicht korret angegeben!', time());
				$this->result->forProperty('taxOfficeUsed5')->addError($error);
				$success = FALSE;
				//
				if( !$isTO ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Finanzamt nicht angegeben!', time());
					$this->result->forProperty('taxOffice5')->addError($error);
				}
				if( !$isId ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Steuernummer nicht angegeben!', time());
					$this->result->forProperty('taxId5')->addError($error);
				}
				if( !$isUid ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('UID-Nr. nicht angegeben!', time());
					$this->result->forProperty('taxUid5')->addError($error);
				}
			}
		}
		return $success;
	}

	/*
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validatePersons ($formdata, $success){
		//
		$nr = 0;
		$TN = 'Person 0: ';
		if( strlen($formdata->getGender0()) < 1 ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Anrede nicht gewählt!', time());
			$this->result->forProperty('gender' . $nr)->addError($error);
			$success = FALSE;
		}
		if( strlen($formdata->getFirstname0()) < 1 ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
			$this->result->forProperty('firstname' . $nr)->addError($error);
			$success = FALSE;
		}
		if( strlen($formdata->getLastname0()) < 1 ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
			$this->result->forProperty('lastname' . $nr)->addError($error);
			$success = FALSE;
		}
		if( strlen($formdata->getFunction0()) < 1 ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Fuktion nicht gewählt!', time());
			$this->result->forProperty('function' . $nr)->addError($error);
			$success = FALSE;
		}
		$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail0());
		if( !$isEmail ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
			$this->result->forProperty('email' . $nr)->addError($error);
			$success = FALSE;
		}
		//
		if( $formdata->getUsed1() ){
			$nr = 1;
			$TN = 'Person 1: ';
			if( strlen($formdata->getGender1()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Anrede nicht gewählt!', time());
				$this->result->forProperty('gender' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFirstname1()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname1()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFunction1()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Fuktion nicht gewählt!', time());
				$this->result->forProperty('function' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail1());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $formdata->getUsed2() ){
			$nr = 2;
			$TN = 'Person 2: ';
			if( strlen($formdata->getGender2()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Anrede nicht gewählt!', time());
				$this->result->forProperty('gender' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFirstname2()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname2()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFunction2()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Fuktion nicht gewählt!', time());
				$this->result->forProperty('function' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail2());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $formdata->getUsed3() ){
			$nr = 3;
			$TN = 'Person 3: ';
			if( strlen($formdata->getGender3()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Anrede nicht gewählt!', time());
				$this->result->forProperty('gender' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFirstname3()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname3()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFunction3()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Fuktion nicht gewählt!', time());
				$this->result->forProperty('function' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail3());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $formdata->getUsed4() ){
			$nr = 4;
			$TN = 'Person 4: ';
			if( strlen($formdata->getGender4()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Anrede nicht gewählt!', time());
				$this->result->forProperty('gender' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFirstname4()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname4()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFunction4()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Fuktion nicht gewählt!', time());
				$this->result->forProperty('function' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail4());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $formdata->getUsed5() ){
			$nr = 5;
			$TN = 'Person 5: ';
			if( strlen($formdata->getGender5()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Anrede nicht gewählt!', time());
				$this->result->forProperty('gender' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFirstname5()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname5()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getFunction5()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Fuktion nicht gewählt!', time());
				$this->result->forProperty('function' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail5());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		//
		return $success;
	}
}
?>