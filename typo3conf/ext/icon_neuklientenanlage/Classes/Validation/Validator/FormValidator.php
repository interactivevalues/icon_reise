<?php
namespace NEXT\IconNeuklientenanlage\Validation\Validator;

/***************************************************************
* Copyright notice
*
* (c) 2016 robert`smo´ schmoller <r.schmoller@next-linz.com>
*
* All rights reserved
*
* This script is part of the TYPO3 project. The TYPO3 project is
* free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* The GNU General Public License can be found at
* http://www.gnu.org/copyleft/gpl.html.
*
* This script is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
* Formdata validator
*
* @package icon_neuklientenanlage
* @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
*
*/
class FormValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator {

/*
	/ * *
	 * @ p a r a m \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
	 * @  r e t u rn void
	 * /
	public function injectTypoScript(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager) {
		$typoScriptSetup = $configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT
		);
		$this->settings = $typoScriptSetup['plugin.']['tx_powermail.']['settings.']['setup.'];
	}
*/

	/**
	 * Returns TRUE, if the given property ($value) is a valid.
	 *
	 * Otherwise, it is FALSE.
	 *
	 * @param mixed $formdata The value that should be validated
	 * @return boolean TRUE if the value is valid, FALSE if an error occured
	 * @api
	 */

	protected function isValid($formdata) {
		return TRUE;
	}


	/**
	 * Returs FALSE if not valid 
	 * 
	 * Otherwise, it returns param unchanged !!!
	 *
	 * @param boolean $valid
	 * @return boolean
	 */
	protected function validateFileuploads ($valid) {
		//
		$extName = 'tx_iconneuklientenanlage_pi1';
		$uploadPath = $this->settings['upload']['path'];    //    'uploads/tx_nextgenesungswunsch/';
		$uploadPath = 'uploads/';
		$uploadSizeMax = $this->settings['upload']['maxSizeMB'];    //    2 MB;
		$uploadSizeMax = 2;
		$allowedFileTypes = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', 'application/pdf;application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/msword;application/vnd.openxmlformats-officedocument.wordprocessingml.document;image/jpg;image/jpeg;image/pjpeg;');
		//
		if( $_FILES[$extName] ){
			for( $f=1; $f<=5; $f++ ){
				$fparam = 'file' . $f;
				//    get FILENAME from FORM
				$fileName = trim($_FILES[$extName]['name'][$fparam]);
				//    get FILESIZE from FORM
				$fSize = $_FILES[$extName]['size'][$fparam];
				//    get FILETYPE from FORM
				$fType = trim($_FILES[$extName]['type'][$fparam]);
				//    get TMP-FILENAME to FILE in FORM
				$tmpName = trim($_FILES[$extName]['tmp_name'][$fparam]);
				//
				$isFileName = $fileName != '';
				$isFileSize = $fSize > 0 && $fSize <= ($uploadSizeMax *1024*1024);
				$isFileType = in_array($fType, $allowedFileTypes);
				$isFileTemp = $tmpName !='';
				//
				if( $isFileName && $isFileTemp ){
					if( !$isFileSize || !$isFileType){
						$err_msg = array();
						//
						if( !$isFileSize ){ $err_msg[] = 'Datenmenge zu groß!'; }
						if( !$isFileType ){ $err_msg[] = 'Falscher Dateityp!'; }
						//
						$err_msg = ' [' . implode(' | ', $err_msg) . ']';
						//
						$error = new \TYPO3\CMS\Extbase\Validation\Error('Datei (' . $f . ') "' . $fileName . '" ungültig!'.$err_msg, time());
						$this->result->forProperty('uploads')->addError($error);
						$valid = FALSE;
					}
				}
			}
		}
		//
		return $valid;
	}

}
?>