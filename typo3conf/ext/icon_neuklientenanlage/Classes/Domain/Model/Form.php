<?php
namespace NEXT\IconNeuklientenanlage\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_neuklientenanlage
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Form extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	// START: -------------- SYSTEM ----------------

  /**
   * process
   *
   * @var \string
   */
  protected $process;

  /**
   * interested
   *
   * @var \string
   */
  protected $interested;


  /**
   * Sets the process
   *
   * @param \integer $process
   * @return void
   */
  public function setProcess($process) {
    $this->process = $process;
  }

  /**
   * Returns the process
   *
   * @return \integer $process
   */
  public function getProcess() {
    return $this->process;
  }

  /**
   * Sets the interested
   *
   * @param \integer $interested
   * @return void
   */
  public function setInterested($interested) {
    $this->interested = $interested;
  }

  /**
   * Returns the interested
   *
   * @return \integer $interested
   */
  public function getInterested() {
    return $this->interested;
  }

	// END: -------------- SYSTEM ----------------
	

	// START: -------------- ADDITIONAL ----------------

  /**
   * additional
   *
   * @var \string
   */
  protected $additional;


  /**
   * Sets the additional
   *
   * @param \string $additional
   * @return void
   */
  public function setAdditional($additional) {
    $this->additional = $additional;
  }

  /**
   * Returns the additional
   *
   * @return \string $additional
   */
  public function getAdditional() {
    return $this->additional;
  }

	// END: -------------- ADDITIONAL ----------------

	
	// START: -------------- TAX-OFFICE - 1 ----------------

  /**
   * taxOfficeUsed1
   *
   * @var \boolean
   */
  protected $taxOfficeUsed1;

  /**
   * taxOfficeNr1
   *
   * @var \string
   */
  protected $taxOfficeNr1;

  /**
   * taxOffice1
   *
   * @var \string
   */
  protected $taxOffice1;

  /**
   * taxId1
   *
   * @var \string
   */
  protected $taxId1;

  /**
   * taxUid1
   *
   * @var \string
   */
  protected $taxUid1;


  /**
   * Sets the taxOfficeUsed1
   *
   * @param \boolean $taxOfficeUsed1
   * @return void
   */
  public function setTaxOfficeUsed1($taxOfficeUsed1) {
    $this->taxOfficeUsed1 = $taxOfficeUsed1;
  }

  /**
   * Returns the taxOfficeUsed1
   *
   * @return \boolean $taxOfficeUsed1
   */
  public function getTaxOfficeUsed1() {
    return $this->taxOfficeUsed1;
  }

  /**
   * Sets the taxOfficeNr1
   *
   * @param \string $taxOfficeNr1
   * @return void
   */
  public function setTaxOfficeNr1($taxOfficeNr1) {
    $this->taxOfficeNr1 = $taxOfficeNr1;
  }

  /**
   * Returns the taxOfficeNr1
   *
   * @return \string $taxOfficeNr1
   */
  public function getTaxOfficeNr1() {
    return $this->taxOfficeNr1;
  }

  /**
   * Sets the taxOffice1
   *
   * @param \string $taxOffice1
   * @return void
   */
  public function setTaxOffice1($taxOffice1) {
    $this->taxOffice1 = $taxOffice1;
  }

  /**
   * Returns the taxOffice1
   *
   * @return \string $taxOffice1
   */
  public function getTaxOffice1() {
    return $this->taxOffice1;
  }

  /**
   * Sets the taxId1
   *
   * @param \string $taxId1
   * @return void
   */
  public function setTaxId1($taxId1) {
    $this->taxId1 = $taxId1;
  }

  /**
   * Returns the taxId1
   *
   * @return \string $taxId1
   */
  public function getTaxId1() {
    return $this->taxId1;
  }

  /**
   * Sets the taxUid1
   *
   * @param \string $taxUid1
   * @return void
   */
  public function setTaxUid1($taxUid1) {
    $this->taxUid1 = $taxUid1;
  }

  /**
   * Returns the taxUid1
   *
   * @return \string $taxUid1
   */
  public function getTaxUid1() {
    return $this->taxUid1;
  }

	// END: -------------- TAX-OFFICE - 1 ----------------
	
	// START: -------------- UPLOADS ----------------
	
  /**
   * uploads
   *
   * @var \integer
   */
  protected $uploads;


  /**
   * Sets the uploads
   *
   * @param \integer $uploads
   * @return void
   */
  public function setUploads($uploads) {
    $this->uploads = $uploads;
  }

  /**
   * Returns the uploads
   *
   * @return \integer $uploads
   */
  public function getUploads() {
    return $this->uploads;
  }



	// END: -------------- UPLOADS ----------------


	// START: -------------- ATTENTIVELY ----------------

  /**
   * attentivelyWebsite
   *
   * @var \integer
   */
  protected $attentivelyWebsite;

  /**
   * attentivelyTaxAcademy
   *
   * @var \integer
   */
  protected $attentivelyTaxAcademy;

  /**
   * attentivelyInserat
   *
   * @var \integer
   */
  protected $attentivelyInserat;

  /**
   * attentivelyEmpfehlung
   *
   * @var \integer
   */
  protected $attentivelyEmpfehlung;

   /**
   * attentivelyEmpfehlungInfo
   *
   * @var \string
   */
  protected $attentivelyEmpfehlungInfo;

  /**
   * attentivelySearchengine
   *
   * @var \integer
   */
  protected $attentivelySearchengine;

  /**
   * attentivelySonstiges
   *
   * @var \integer
   */
  protected $attentivelySonstiges;

  /**
   * attentivelySonstigesInfo
   *
   * @var \string
   */
  protected $attentivelySonstigesInfo;


  /**
   * Sets the attentivelyWebsite
   *
   * @param \integer $attentivelyWebsite
   * @return void
   */
  public function setAttentivelyWebsite($attentivelyWebsite) {
    $this->attentivelyWebsite = $attentivelyWebsite;
  }

  /**
   * Returns the attentivelyWebsite
   *
   * @return \integer $attentivelyWebsite
   */
  public function getAttentivelyWebsite() {
    return $this->attentivelyWebsite;
  }

  /**
   * Sets the attentivelyTaxAcademy
   *
   * @param \integer $attentivelyTaxAcademy
   * @return void
   */
  public function setAttentivelyTaxAcademy($attentivelyTaxAcademy) {
    $this->attentivelyTaxAcademy = $attentivelyTaxAcademy;
  }

  /**
   * Returns the attentivelyTaxAcademy
   *
   * @return \integer $attentivelyTaxAcademy
   */
  public function getAttentivelyTaxAcademy() {
    return $this->attentivelyTaxAcademy;
  }

  /**
   * Sets the attentivelyInserat
   *
   * @param \integer $attentivelyInserat
   * @return void
   */
  public function setAttentivelyInserat($attentivelyInserat) {
    $this->attentivelyInserat = $attentivelyInserat;
  }

  /**
   * Returns the attentivelyInserat
   *
   * @return \integer $attentivelyInserat
   */
  public function getAttentivelyInserat() {
    return $this->attentivelyInserat;
  }

  /**
   * Sets the attentivelyEmpfehlung
   *
   * @param \integer $attentivelyEmpfehlung
   * @return void
   */
  public function setAttentivelyEmpfehlung($attentivelyEmpfehlung) {
    $this->attentivelyEmpfehlung = $attentivelyEmpfehlung;
  }

  /**
   * Returns the attentivelyEmpfehlung
   *
   * @return \integer $attentivelyEmpfehlung
   */
  public function getAttentivelyEmpfehlung() {
    return $this->attentivelyEmpfehlung;
  }
 
  /**
   * Sets the attentivelyEmpfehlungInfo
   *
   * @param \string $attentivelyEmpfehlungInfo
   * @return void
   */
  public function setAttentivelyEmpfehlungInfo($attentivelyEmpfehlungInfo) {
    $this->attentivelyEmpfehlungInfo = $attentivelyEmpfehlungInfo;
  }

  /**
   * Returns the attentivelyEmpfehlungInfo
   *
   * @return \string $attentivelyEmpfehlungInfo
   */
  public function getAttentivelyEmpfehlungInfo() {
    return $this->attentivelyEmpfehlungInfo;
  }

  /**
   * Sets the attentivelySearchengine
   *
   * @param \integer $attentivelySearchengine
   * @return void
   */
  public function setAttentivelySearchengine($attentivelySearchengine) {
    $this->attentivelySearchengine = $attentivelySearchengine;
  }

  /**
   * Returns the attentivelySearchengine
   *
   * @return \integer $attentivelySearchengine
   */
  public function getAttentivelySearchengine() {
    return $this->attentivelySearchengine;
  }

  /**
   * Sets the attentivelySonstiges
   *
   * @param \integer $attentivelySonstiges
   * @return void
   */
  public function setAttentivelySonstiges($attentivelySonstiges) {
    $this->attentivelySonstiges = $attentivelySonstiges;
  }

  /**
   * Returns the attentivelySonstiges
   *
   * @return \integer $attentivelySonstiges
   */
  public function getAttentivelySonstiges() {
    return $this->attentivelySonstiges;
  }

  /**
   * Sets the attentivelySonstigesInfo
   *
   * @param \string $attentivelySonstigesInfo
   * @return void
   */
  public function setAttentivelySonstigesInfo($attentivelySonstigesInfo) {
    $this->attentivelySonstigesInfo = $attentivelySonstigesInfo;
  }

  /**
   * Returns the attentivelySonstigesInfo
   *
   * @return \string $attentivelySonstigesInfo
   */
  public function getAttentivelySonstigesInfo() {
    return $this->attentivelySonstigesInfo;
  }

	// END: -------------- ATTENTIVELY ----------------
}
?>