<?php
namespace NEXT\IconNeuklientenanlage\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_neuklientenanlage
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FormPrivate extends \NEXT\IconNeuklientenanlage\Domain\Model\Form {

	// START: -------------- PERSON ----------------

  /**
   * gender
   *
   * @var \string
   */
  protected $gender;

  /**
   * title
   *
   * @var \string
   */
  protected $title;
  
  /**
   * firstname
   *
   * @var \string
   * @ v alidate NotEmpty
   */
  protected $firstname;

  /**
   * lastname
   *
   * @var \string
   * @ v alidate NotEmpty
   */
  protected $lastname;

  /**
   * titleAfter
   *
   * @var \string
   */
  protected $titleAfter;

  /**
   * function
   *
   * @var \string
   */
  protected $function;

  /**
   * email
   *
   * @var \string
   * @- validate EmailAddress
   */
  protected $email;

  /**
   * phone
   *
   * @var \string
   */
  protected $phone;

  /**
   * phoneCc
   *
   * @var \string
   */
  protected $phoneCc;

  /**
   * phoneRc
   *
   * @var \string
   */
  protected $phoneRc;

  /**
   * phoneNr
   *
   * @var \string
   */
  protected $phoneNr;

  /**
   * phoneEx
   *
   * @var \string
   */
  protected $phoneEx;

  /**
   * fax
   *
   * @var \string
   */
  protected $fax;

  /**
   * faxCc
   *
   * @var \string
   */
  protected $faxCc;

  /**
   * faxRc
   *
   * @var \string
   */
  protected $faxRc;

  /**
   * faxNr
   *
   * @var \string
   */
  protected $faxNr;

  /**
   * faxEx
   *
   * @var \string
   */
  protected $faxEx;

  /**
   * mobile
   *
   * @var \string
   */
  protected $mobile;

  /**
   * mobileCc
   *
   * @var \string
   */
  protected $mobileCc;

  /**
   * mobileRc
   *
   * @var \string
   */
  protected $mobileRc;

  /**
   * mobileNr
   *
   * @var \string
   */
  protected $mobileNr;

  /**
   * mobileEx
   *
   * @var \string
   */
  protected $mobileEx;

  /**
   * country
   *
   * @var \string
   */
  protected $country;


  /**
   * Sets the gender
   *
   * @param \string $gender
   * @return void
   */
  public function setGender($gender) {
    $this->gender = $gender;
  }

  /**
   * Returns the gender
   *
   * @return \string $gender
   */
  public function getGender() {
    return $this->gender;
  }

  /**
   * Sets the title
   *
   * @param \string $title
   * @return void
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Returns the title
   *
   * @return \string $title
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Sets the firstname
   *
   * @param \string $firstname
   * @return void
   */
  public function setFirstname($firstname) {
    $this->firstname = $firstname;
  }

  /**
   * Returns the firstname
   *
   * @return \string $firstname
   */
  public function getFirstname() {
    return $this->firstname;
  }

  /**
   * Sets the lastname
   *
   * @param \string $lastname
   * @return void
   */
  public function setLastname($lastname) {
    $this->lastname = $lastname;
  }

  /**
   * Returns the lastname
   *
   * @return \string $lastname
   */
  public function getLastname() {
    return $this->lastname;
  }

  /**
   * Sets the titleAfter
   *
   * @param \string $titleAfter
   * @return void
   */
  public function setTitleAfter($titleAfter) {
    $this->titleAfter = $titleAfter;
  }

  /**
   * Returns the titleAfter
   *
   * @return \string $titleAfter
   */
  public function getTitleAfter() {
    return $this->titleAfter;
  }

  /**
   * Sets the function
   *
   * @param \string $function
   * @return void
   */
  public function setFunction($function) {
    $this->function = $function;
  }

  /**
   * Returns the function
   *
   * @return \string $function
   */
  public function getFunction() {
    return $this->function;
  }

  /**
   * Sets the email
   *
   * @param \string $email
   * @return void
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * Returns the email
   *
   * @return \string $email
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Sets the phone
   *
   * @param \string $phone
   * @return void
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * Returns the phone
   *
   * @return \string $phone
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * Sets the phoneCc
   *
   * @param \string $phoneCc
   * @return void
   */
  public function setPhoneCc($phoneCc) {
    $this->phoneCc = $phoneCc;
  }

  /**
   * Returns the phoneCc
   *
   * @return \string $phoneCc
   */
  public function getPhoneCc() {
    return $this->phoneCc;
  }

  /**
   * Sets the phoneRc
   *
   * @param \string $phoneRc
   * @return void
   */
  public function setPhoneRc($phoneRc) {
    $this->phoneRc = $phoneRc;
  }

  /**
   * Returns the phoneRc
   *
   * @return \string $phoneRc
   */
  public function getPhoneRc() {
    return $this->phoneRc;
  }

  /**
   * Sets the phoneNr
   *
   * @param \string $phoneNr
   * @return void
   */
  public function setPhoneNr($phoneNr) {
    $this->phoneNr = $phoneNr;
  }

  /**
   * Returns the phoneNr
   *
   * @return \string $phoneNr
   */
  public function getPhoneNr() {
    return $this->phoneNr;
  }

  /**
   * Sets the phoneEx
   *
   * @param \string $phoneEx
   * @return void
   */
  public function setPhoneEx($phoneEx) {
    $this->phoneEx = $phoneEx;
  }

  /**
   * Returns the phoneEx
   *
   * @return \string $phoneEx
   */
  public function getPhoneEx() {
    return $this->phoneEx;
  }

  /**
   * Sets the fax
   *
   * @param \string $fax
   * @return void
   */
  public function setFax($fax) {
    $this->fax = $fax;
  }

  /**
   * Returns the fax
   *
   * @return \string $fax
   */
  public function getFax() {
    return $this->fax;
  }

  /**
   * Sets the faxCc
   *
   * @param \string $faxCc
   * @return void
   */
  public function setFaxCc($faxCc) {
    $this->faxCc = $faxCc;
  }

  /**
   * Returns the faxCc
   *
   * @return \string $faxCc
   */
  public function getFaxCc() {
    return $this->faxCc;
  }

  /**
   * Sets the faxRc
   *
   * @param \string $faxRc
   * @return void
   */
  public function setFaxRc($faxRc) {
    $this->faxRc = $faxRc;
  }

  /**
   * Returns the faxRc
   *
   * @return \string $faxRc
   */
  public function getFaxRc() {
    return $this->faxRc;
  }

  /**
   * Sets the faxNr
   *
   * @param \string $faxNr
   * @return void
   */
  public function setFaxNr($faxNr) {
    $this->faxNr = $faxNr;
  }

  /**
   * Returns the faxNr
   *
   * @return \string $faxNr
   */
  public function getFaxNr() {
    return $this->faxNr;
  }

  /**
   * Sets the faxEx
   *
   * @param \string $faxEx
   * @return void
   */
  public function setFaxEx($faxEx) {
    $this->faxEx = $faxEx;
  }

  /**
   * Returns the faxEx
   *
   * @return \string $faxEx
   */
  public function getFaxEx() {
    return $this->faxEx;
  }

  /**
   * Sets the mobile
   *
   * @param \string $mobile
   * @return void
   */
  public function setMobile($mobile) {
    $this->mobile = $mobile;
  }

  /**
   * Returns the mobile
   *
   * @return \string $mobile
   */
  public function getMobile() {
    return $this->mobile;
  }

  /**
   * Sets the mobileCc
   *
   * @param \string $mobileCc
   * @return void
   */
  public function setMobileCc($mobileCc) {
    $this->mobileCc = $mobileCc;
  }

  /**
   * Returns the mobileCc
   *
   * @return \string $mobileCc
   */
  public function getMobileCc() {
    return $this->mobileCc;
  }

  /**
   * Sets the mobileRc
   *
   * @param \string $mobileRc
   * @return void
   */
  public function setMobileRc($mobileRc) {
    $this->mobileRc = $mobileRc;
  }

  /**
   * Returns the mobileRc
   *
   * @return \string $mobileRc
   */
  public function getMobileRc() {
    return $this->mobileRc;
  }

  /**
   * Sets the mobileNr
   *
   * @param \string $mobileNr
   * @return void
   */
  public function setMobileNr($mobileNr) {
    $this->mobileNr = $mobileNr;
  }

  /**
   * Returns the mobileNr
   *
   * @return \string $mobileNr
   */
  public function getMobileNr() {
    return $this->mobileNr;
  }

  /**
   * Sets the mobileEx
   *
   * @param \string $mobileEx
   * @return void
   */
  public function setMobileEx($mobileEx) {
    $this->mobileEx = $mobileEx;
  }

  /**
   * Returns the mobileEx
   *
   * @return \string $mobileEx
   */
  public function getMobileEx() {
    return $this->mobileEx;
  }

  /**
   * Sets the country
   *
   * @param \string $country
   * @return void
   */
  public function setCountry($country) {
    $this->country = $country;
  }

  /**
   * Returns the country
   *
   * @return \string $country
   */
  public function getCountry() {
    return $this->country;
  }

	// END: -------------- PERSON ----------------


	// START: -------------- ADDRESS ----------------

  /**
   * street
   *
   * @var \string
   */
  protected $street;

  /**
   * streetNr
   *
   * @var \string
   */
  protected $streetNr;

  /**
   * zip
   *
   * @var \string
   */
  protected $zip;

  /**
   * city
   *
   * @var \string
   */
  protected $city;


  /**
   * Sets the street
   *
   * @param \string $street
   * @return void
   */
  public function setStreet($street) {
    $this->street = $street;
  }

  /**
   * Returns the street
   *
   * @return \string $street
   */
  public function getStreet() {
    return $this->street;
  }

  /**
   * Sets the streetNr
   *
   * @param \string $streetNr
   * @return void
   */
  public function setStreetNr($streetNr) {
    $this->streetNr = $streetNr;
  }

  /**
   * Returns the streetNr
   *
   * @return \string $streetNr
   */
  public function getStreetNr() {
    return $this->streetNr;
  }

  /**
   * Sets the zip
   *
   * @param \string $zip
   * @return void
   */
  public function setZip($zip) {
    $this->zip = $zip;
  }

  /**
   * Returns the zip
   *
   * @return \string $zip
   */
  public function getZip() {
    return $this->zip;
  }

  /**
   * Sets the city
   *
   * @param \string $city
   * @return void
   */
  public function setCity($city) {
    $this->city = $city;
  }

  /**
   * Returns the city
   *
   * @return \string $city
   */
  public function getCity() {
    return $this->city;
  }

	// END: -------------- ADDRESS ----------------

	// START: -------------- BILLING ----------------

  /**
   * bill_mode
   *
   * @var \integer
   */
  protected $billMode;


  /**
   * Sets the billMode
   *
   * @param \integer $billMode
   * @return void
   */
  public function setBillMode($billMode) {
    $this->billMode = $billMode;
  }

  /**
   * Returns the billMode
   *
   * @return \integer $billMode
   */
  public function getBillMode() {
    return $this->billMode;
  }

	// END: -------------- BILLING ----------------

}
?>