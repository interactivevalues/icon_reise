<?php
namespace NEXT\IconNeuklientenanlage\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_neuklientenanlage
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FormCompany extends \NEXT\IconNeuklientenanlage\Domain\Model\Form {

	// START: -------------- COMPANY ----------------

  /**
   * comName
   *
   * @var \string
   */
  protected $comName;

  /**
   * comStreet
   *
   * @var \string
   */
  protected $comStreet;

  /**
   * comStreetNr
   *
   * @var \string
   */
  protected $comStreetNr;

  /**
   * comZip
   *
   * @var \string
   */
  protected $comZip;

   /**
   * comCity
   *
   * @var \string
   */
  protected $comCity;

  /**
   * comCountry
   *
   * @var \string
   */
  protected $comCountry;


  /**
   * Sets the comName
   *
   * @param \integer $comName
   * @return void
   */
  public function setComName($comName) {
    $this->comName = $comName;
  }

  /**
   * Returns the comName
   *
   * @return \integer $comName
   */
  public function getComName() {
    return $this->comName;
  }

  /**
   * Sets the comStreet
   *
   * @param \string $comStreet
   * @return void
   */
  public function setComStreet($comStreet) {
    $this->comStreet = $comStreet;
  }

  /**
   * Returns the comStreet
   *
   * @return \string $comStreet
   */
  public function getComStreet() {
    return $this->comStreet;
  }

  /**
   * Sets the comStreetNr
   *
   * @param \string $comStreetNr
   * @return void
   */
  public function setComStreetNr($comStreetNr) {
    $this->comStreetNr = $comStreetNr;
  }

  /**
   * Returns the comStreetNr
   *
   * @return \string $comStreetNr
   */
  public function getComStreetNr() {
    return $this->comStreetNr;
  }

  /**
   * Sets the comZip
   *
   * @param \string $comZip
   * @return void
   */
  public function setComZip($comZip) {
    $this->comZip = $comZip;
  }

  /**
   * Returns the comZip
   *
   * @return \string $comZip
   */
  public function getComZip() {
    return $this->comZip;
  }
 
  /**
   * Sets the comCity
   *
   * @param \string $comCity
   * @return void
   */
  public function setComCity($comCity) {
    $this->comCity = $comCity;
  }

  /**
   * Returns the comCity
   *
   * @return \string $comCity
   */
  public function getComCity() {
    return $this->comCity;
  }

  /**
   * Sets the comCountry
   *
   * @param \string $comCountry
   * @return void
   */
  public function setComCountry($comCountry) {
    $this->comCountry = $comCountry;
  }

  /**
   * Returns the comCountry
   *
   * @return \string $comCountry
   */
  public function getComCountry() {
    return $this->comCountry;
  }

	// END: -------------- COMPANY ----------------


	// START: -------------- BILLING ----------------

  /**
   * billAddr
   *
   * @var \boolean
   */
  protected $billAddr;

  /**
   * billCareOf
   *
   * @var \string
   */
  protected $billCareOf;

  /**
   * billCompanyname
   *
   * @var \string
   */
  protected $billCompanyname;

  /**
   * billGender
   *
   * @var \string
   */
  protected $billGender;

  /**
   * billTitle
   *
   * @var \string
   */
  protected $billTitle;
  
  /**
   * billFirstname
   *
   * @var \string
   * @- validate NotEmpty
   */
  protected $billFirstname;

  /**
   * billLastname
   *
   * @var \string
   * @- validate NotEmpty
   */
  protected $billLastname;

  /**
   * billTitleAfter
   *
   * @var \string
   */
  protected $billTitleAfter;

  /**
   * billFunction
   *
   * @var \string
   */
  protected $billFunction;

  /**
   * billStreet
   *
   * @var \string
   */
  protected $billStreet;

  /**
   * billStreetNr
   *
   * @var \string
   */
  protected $billStreetNr;

  /**
   * billZip
   *
   * @var \string
   */
  protected $billZip;

   /**
   * billCity
   *
   * @var \string
   */
  protected $billCity;

  /**
   * billCountry
   *
   * @var \string
   */
  protected $billCountry;


  /**
   * Sets the billAddr
   *
   * @param \boolean $billAddr
   * @return void
   */
  public function setBillAddr($billAddr) {
    $this->billAddr = $billAddr;
  }

  /**
   * Returns the billAddr
   *
   * @return \boolean $billAddr
   */
  public function getBillAddr() {
    return $this->billAddr;
  }

  /**
   * Sets the billCompanyname
   *
   * @param \string $billCompanyname
   * @return void
   */
  public function setBillCompanyname($billCompanyname) {
    $this->billCompanyname = $billCompanyname;
  }

  /**
   * Returns the billCompanyname
   *
   * @return \string $billCompanyname
   */
  public function getBillCompanyname() {
    return $this->billCompanyname;
  }


  /**
   * Sets the billCareOf
   *
   * @param \string $billCareOf
   * @return void
   */
  public function setBillCareOf($billCareOf) {
    $this->billCareOf = $billCareOf;
  }

  /**
   * Returns the billCareOf
   *
   * @return \string $billCareOf
   */
  public function getBillCareOf() {
    return $this->billCareOf;
  }


  /**
   * Sets the billGender
   *
   * @param \string $billGender
   * @return void
   */
  public function setBillGender($billGender) {
    $this->billGender = $billGender;
  }

  /**
   * Returns the billGender
   *
   * @return \string $billGender
   */
  public function getBillGender() {
    return $this->billGender;
  }

  /**
   * Sets the billTitle
   *
   * @param \string $billTitle
   * @return void
   */
  public function setBillTitle($billTitle) {
    $this->billTitle = $billTitle;
  }

  /**
   * Returns the billTitle
   *
   * @return \string $billTitle
   */
  public function getBillTitle() {
    return $this->billTitle;
  }

  /**
   * Sets the billFirstname
   *
   * @param \string $billFirstname
   * @return void
   */
  public function setBillFirstname($billFirstname) {
    $this->billFirstname = $billFirstname;
  }

  /**
   * Returns the billFirstname
   *
   * @return \string $billFirstname
   */
  public function getBillFirstname() {
    return $this->billFirstname;
  }

  /**
   * Sets the billLastname
   *
   * @param \string $billLastname
   * @return void
   */
  public function setBillLastname($billLastname) {
    $this->billLastname = $billLastname;
  }

  /**
   * Returns the billLastname
   *
   * @return \string $billLastname
   */
  public function getBillLastname() {
    return $this->billLastname;
  }

  /**
   * Sets the billTitleAfter
   *
   * @param \string $billTitleAfter
   * @return void
   */
  public function setBillTitleAfter($billTitleAfter) {
    $this->billTitleAfter = $billTitleAfter;
  }

  /**
   * Returns the billTitleAfter
   *
   * @return \string $billTitleAfter
   */
  public function getBillTitleAfter() {
    return $this->billTitleAfter;
  }

  /**
   * Sets the billFunction
   *
   * @param \string $billFunction
   * @return void
   */
  public function setBillFunction($billFunction) {
    $this->billFunction = $billFunction;
  }

  /**
   * Returns the billFunction
   *
   * @return \string $billFunction
   */
  public function getBillFunction() {
    return $this->billFunction;
  }

  /**
   * Sets the billStreet
   *
   * @param \string $billStreet
   * @return void
   */
  public function setBillStreet($billStreet) {
    $this->billStreet = $billStreet;
  }

  /**
   * Returns the billStreet
   *
   * @return \string $billStreet
   */
  public function getBillStreet() {
    return $this->billStreet;
  }

  /**
   * Sets the billStreetNr
   *
   * @param \string $billStreetNr
   * @return void
   */
  public function setBillStreetNr($billStreetNr) {
    $this->billStreetNr = $billStreetNr;
  }

  /**
   * Returns the billStreetNr
   *
   * @return \string $billStreetNr
   */
  public function getBillStreetNr() {
    return $this->billStreetNr;
  }

  /**
   * Sets the billZip
   *
   * @param \string $billZip
   * @return void
   */
  public function setBillZip($billZip) {
    $this->billZip = $billZip;
  }

  /**
   * Returns the billZip
   *
   * @return \string $billZip
   */
  public function getBillZip() {
    return $this->billZip;
  }
 
  /**
   * Sets the billCity
   *
   * @param \string $billCity
   * @return void
   */
  public function setBillCity($billCity) {
    $this->billCity = $billCity;
  }

  /**
   * Returns the billCity
   *
   * @return \string $billCity
   */
  public function getBillCity() {
    return $this->billCity;
  }

  /**
   * Sets the billCountry
   *
   * @param \string $billCountry
   * @return void
   */
  public function setBillCountry($billCountry) {
    $this->billCountry = $billCountry;
  }

  /**
   * Returns the billCountry
   *
   * @return \string $billCountry
   */
  public function getBillCountry() {
    return $this->billCountry;
  }

	// END: -------------- BILLING ----------------


	// START: -------------- BILLING EMAIL ----------------

  /**
   * billEmailMode
   *
   * @var \integer
   */
  protected $billEmailMode;

  /**
   * billEmailGender
   *
   * @var \string
   */
  protected $billEmailGender;

  /**
   * billEmailTitle
   *
   * @var \string
   */
  protected $billEmailTitle;
  
  /**
   * billEmailFirstname
   *
   * @var \string
   * @- validate NotEmpty
   */
  protected $billEmailFirstname;

  /**
   * billEmailLastname
   *
   * @var \string
   * @- validate NotEmpty
   */
  protected $billEmailLastname;

  /**
   * billEmailTitleAfter
   *
   * @var \string
   */
  protected $billEmailTitleAfter;

  /**
   * billEmailFunction
   *
   * @var \string
   */
  protected $billEmailFunction;

  /**
   * billEmailEmail
   *
   * @var \string
   * @- validate EmailAddress
   */
  protected $billEmailEmail;

  /**
   * billEmailPhone
   *
   * @var \string
   */
  protected $billEmailPhone;

  /**
   * billEmailPhoneCc
   *
   * @var \string
   */
  protected $billEmailPhoneCc;

  /**
   * billEmailPhoneRc
   *
   * @var \string
   */
  protected $billEmailPhoneRc;

  /**
   * billEmailPhoneNr
   *
   * @var \string
   */
  protected $billEmailPhoneNr;

  /**
   * billEmailPhoneEx
   *
   * @var \string
   */
  protected $billEmailPhoneEx;

  /**
   * billEmailMobile
   *
   * @var \string
   */
  protected $billEmailMobile;

  /**
   * billEmailMobileCc
   *
   * @var \string
   */
  protected $billEmailMobileCc;

  /**
   * billEmailMobileRc
   *
   * @var \string
   */
  protected $billEmailMobileRc;

  /**
   * billEmailMobileNr
   *
   * @var \string
   */
  protected $billEmailMobileNr;

  /**
   * billEmailMobileEx
   *
   * @var \string
   */
  protected $billEmailMobileEx;

  /**
   * billEmailFax
   *
   * @var \string
   */
  protected $billEmailFax;

  /**
   * billEmailFaxCc
   *
   * @var \string
   */
  protected $billEmailFaxCc;

  /**
   * billEmailFaxRc
   *
   * @var \string
   */
  protected $billEmailFaxRc;

  /**
   * billEmailFaxNr
   *
   * @var \string
   */
  protected $billEmailFaxNr;

  /**
   * billEmailFaxEx
   *
   * @var \string
   */
  protected $billEmailFaxEx;


  /**
   * Sets the billEmailMode
   *
   * @param \integer $billEmailMode
   * @return void
   */
  public function setBillEmailMode($billEmailMode) {
    $this->billEmailMode = $billEmailMode;
  }

  /**
   * Returns the billEmailMode
   *
   * @return \integer $billEmailMode
   */
  public function getBillEmailMode() {
    return $this->billEmailMode;
  }

  /**
   * Sets the billEmailGender
   *
   * @param \string $billEmailGender
   * @return void
   */
  public function setBillEmailGender($billEmailGender) {
    $this->billEmailGender = $billEmailGender;
  }

  /**
   * Returns the billEmailGender
   *
   * @return \string $billEmailGender
   */
  public function getBillEmailGender() {
    return $this->billEmailGender;
  }

  /**
   * Sets the billEmailTitle
   *
   * @param \string $billEmailTitle
   * @return void
   */
  public function setBillEmailTitle($billEmailTitle) {
    $this->billEmailTitle = $billEmailTitle;
  }

  /**
   * Returns the billEmailTitle
   *
   * @return \string $billEmailTitle
   */
  public function getBillEmailTitle() {
    return $this->billEmailTitle;
  }

  /**
   * Sets the billEmailFirstname
   *
   * @param \string $billEmailFirstname
   * @return void
   */
  public function setBillEmailFirstname($billEmailFirstname) {
    $this->billEmailFirstname = $billEmailFirstname;
  }

  /**
   * Returns the billEmailFirstname
   *
   * @return \string $billEmailFirstname
   */
  public function getBillEmailFirstname() {
    return $this->billEmailFirstname;
  }

  /**
   * Sets the billEmailLastname
   *
   * @param \string $billEmailLastname
   * @return void
   */
  public function setBillEmailLastname($billEmailLastname) {
    $this->billEmailLastname = $billEmailLastname;
  }

  /**
   * Returns the billEmailLastname
   *
   * @return \string $billEmailLastname
   */
  public function getBillEmailLastname() {
    return $this->billEmailLastname;
  }

  /**
   * Sets the billEmailTitleAfter
   *
   * @param \string $billEmailTitleAfter
   * @return void
   */
  public function setBillEmailTitleAfter($billEmailTitleAfter) {
    $this->billEmailTitleAfter = $billEmailTitleAfter;
  }

  /**
   * Returns the billEmailTitleAfter
   *
   * @return \string $billEmailTitleAfter
   */
  public function getBillEmailTitleAfter() {
    return $this->billEmailTitleAfter;
  }

  /**
   * Sets the billEmailFunction
   *
   * @param \string $billEmailFunction
   * @return void
   */
  public function setBillEmailFunction($billEmailFunction) {
    $this->billEmailFunction = $billEmailFunction;
  }

  /**
   * Returns the billEmailFunction
   *
   * @return \string $billEmailFunction
   */
  public function getBillEmailFunction() {
    return $this->billEmailFunction;
  }

  /**
   * Sets the billEmailEmail
   *
   * @param \string $billEmailEmail
   * @return void
   */
  public function setBillEmailEmail($billEmailEmail) {
    $this->billEmailEmail = $billEmailEmail;
  }

  /**
   * Returns the billEmailEmail
   *
   * @return \string $billEmailEmail
   */
  public function getBillEmailEmail() {
    return $this->billEmailEmail;
  }

  /**
   * Sets the billEmailPhone
   *
   * @param \string $billEmailPhone
   * @return void
   */
  public function setBillEmailPhone($billEmailPhone) {
    $this->billEmailPhone = $billEmailPhone;
  }

  /**
   * Returns the billEmailPhone
   *
   * @return \string $billEmailPhone
   */
  public function getBillEmailPhone() {
    return $this->billEmailPhone;
  }

  /**
   * Sets the billEmailPhoneCc
   *
   * @param \string $billEmailPhoneCc
   * @return void
   */
  public function setBillEmailPhoneCc($billEmailPhoneCc) {
    $this->billEmailPhoneCc = $billEmailPhoneCc;
  }

  /**
   * Returns the billEmailPhoneCc
   *
   * @return \string $billEmailPhoneCc
   */
  public function getBillEmailPhoneCc() {
    return $this->billEmailPhoneCc;
  }

  /**
   * Sets the billEmailPhoneRc
   *
   * @param \string $billEmailPhoneRc
   * @return void
   */
  public function setBillEmailPhoneRc($billEmailPhoneRc) {
    $this->billEmailPhoneRc = $billEmailPhoneRc;
  }

  /**
   * Returns the billEmailPhoneRc
   *
   * @return \string $billEmailPhoneRc
   */
  public function getBillEmailPhoneRc() {
    return $this->billEmailPhoneRc;
  }

  /**
   * Sets the billEmailPhoneNr
   *
   * @param \string $billEmailPhoneNr
   * @return void
   */
  public function setBillEmailPhoneNr($billEmailPhoneNr) {
    $this->billEmailPhoneNr = $billEmailPhoneNr;
  }

  /**
   * Returns the billEmailPhoneNr
   *
   * @return \string $billEmailPhoneNr
   */
  public function getBillEmailPhoneNr() {
    return $this->billEmailPhoneNr;
  }

  /**
   * Sets the billEmailPhoneEx
   *
   * @param \string $billEmailPhoneEx
   * @return void
   */
  public function setBillEmailPhoneEx($billEmailPhoneEx) {
    $this->billEmailPhoneEx = $billEmailPhoneEx;
  }

  /**
   * Returns the billEmailPhoneEx
   *
   * @return \string $billEmailPhoneEx
   */
  public function getBillEmailPhoneEx() {
    return $this->billEmailPhoneEx;
  }


  /**
   * Sets the billEmailMobile
   *
   * @param \string $billEmailMobile
   * @return void
   */
  public function setBillEmailMobile($billEmailMobile) {
    $this->billEmailMobile = $billEmailMobile;
  }

  /**
   * Returns the billEmailMobile
   *
   * @return \string $billEmailMobile
   */
  public function getBillEmailMobile() {
    return $this->billEmailMobile;
  }

  /**
   * Sets the billEmailMobileCc
   *
   * @param \string $billEmailMobileCc
   * @return void
   */
  public function setBillEmailMobileCc($billEmailMobileCc) {
    $this->billEmailMobileCc = $billEmailMobileCc;
  }

  /**
   * Returns the billEmailMobileCc
   *
   * @return \string $billEmailMobileCc
   */
  public function getBillEmailMobileCc() {
    return $this->billEmailMobileCc;
  }

  /**
   * Sets the billEmailMobileRc
   *
   * @param \string $billEmailMobileRc
   * @return void
   */
  public function setBillEmailMobileRc($billEmailMobileRc) {
    $this->billEmailMobileRc = $billEmailMobileRc;
  }

  /**
   * Returns the billEmailMobileRc
   *
   * @return \string $billEmailMobileRc
   */
  public function getBillEmailMobileRc() {
    return $this->billEmailMobileRc;
  }

  /**
   * Sets the billEmailMobileNr
   *
   * @param \string $billEmailMobileNr
   * @return void
   */
  public function setBillEmailMobileNr($billEmailMobileNr) {
    $this->billEmailMobileNr = $billEmailMobileNr;
  }

  /**
   * Returns the billEmailMobileNr
   *
   * @return \string $billEmailMobileNr
   */
  public function getBillEmailMobileNr() {
    return $this->billEmailMobileNr;
  }

  /**
   * Sets the billEmailMobileEx
   *
   * @param \string $billEmailMobileEx
   * @return void
   */
  public function setBillEmailMobileEx($billEmailMobileEx) {
    $this->billEmailMobileEx = $billEmailMobileEx;
  }

  /**
   * Returns the billEmailMobileEx
   *
   * @return \string $billEmailMobileEx
   */
  public function getBillEmailMobileEx() {
    return $this->billEmailMobileEx;
  }


  /**
   * Sets the billEmailFax
   *
   * @param \string $billEmailFax
   * @return void
   */
  public function setBillEmailFax($billEmailFax) {
    $this->billEmailFax = $billEmailFax;
  }

  /**
   * Returns the billEmailFax
   *
   * @return \string $billEmailFax
   */
  public function getBillEmailFax() {
    return $this->billEmailFax;
  }

  /**
   * Sets the billEmailFaxCc
   *
   * @param \string $billEmailFaxCc
   * @return void
   */
  public function setBillEmailFaxCc($billEmailFaxCc) {
    $this->billEmailFaxCc = $billEmailFaxCc;
  }

  /**
   * Returns the billEmailFaxCc
   *
   * @return \string $billEmailFaxCc
   */
  public function getBillEmailFaxCc() {
    return $this->billEmailFaxCc;
  }

  /**
   * Sets the billEmailFaxRc
   *
   * @param \string $billEmailFaxRc
   * @return void
   */
  public function setBillEmailFaxRc($billEmailFaxRc) {
    $this->billEmailFaxRc = $billEmailFaxRc;
  }

  /**
   * Returns the billEmailFaxRc
   *
   * @return \string $billEmailFaxRc
   */
  public function getBillEmailFaxRc() {
    return $this->billEmailFaxRc;
  }

  /**
   * Sets the billEmailFaxNr
   *
   * @param \string $billEmailFaxNr
   * @return void
   */
  public function setBillEmailFaxNr($billEmailFaxNr) {
    $this->billEmailFaxNr = $billEmailFaxNr;
  }

  /**
   * Returns the billEmailFaxNr
   *
   * @return \string $billEmailFaxNr
   */
  public function getBillEmailFaxNr() {
    return $this->billEmailFaxNr;
  }

  /**
   * Sets the billEmailFaxEx
   *
   * @param \string $billEmailFaxEx
   * @return void
   */
  public function setBillEmailFaxEx($billEmailFaxEx) {
    $this->billEmailFaxEx = $billEmailFaxEx;
  }

  /**
   * Returns the billEmailFaxEx
   *
   * @return \string $billEmailFaxEx
   */
  public function getBillEmailFaxEx() {
    return $this->billEmailFaxEx;
  }


	// END: -------------- BILLING EMAIL ----------------

	// START: -------------- TAX-OFFICE - 2 ----------------

  /**
   * taxOfficeUsed2
   *
   * @var \boolean
   */
  protected $taxOfficeUsed2;

  /**
   * taxOfficeNr2
   *
   * @var \string
   */
  protected $taxOfficeNr2;

  /**
   * taxOffice2
   *
   * @var \string
   */
  protected $taxOffice2;

  /**
   * taxId2
   *
   * @var \string
   */
  protected $taxId2;

  /**
   * taxUid2
   *
   * @var \string
   */
  protected $taxUid2;


  /**
   * Sets the taxOfficeUsed2
   *
   * @param \boolean $taxOfficeUsed2
   * @return void
   */
  public function setTaxOfficeUsed2($taxOfficeUsed2) {
    $this->taxOfficeUsed2 = $taxOfficeUsed2;
  }

  /**
   * Returns the taxOfficeUsed2
   *
   * @return \boolean $taxOfficeUsed2
   */
  public function getTaxOfficeUsed2() {
    return $this->taxOfficeUsed2;
  }

  /**
   * Sets the taxOfficeNr2
   *
   * @param \string $taxOfficeNr2
   * @return void
   */
  public function setTaxOfficeNr2($taxOfficeNr2) {
    $this->taxOfficeNr2 = $taxOfficeNr2;
  }

  /**
   * Returns the taxOfficeNr2
   *
   * @return \string $taxOfficeNr2
   */
  public function getTaxOfficeNr2() {
    return $this->taxOfficeNr2;
  }

  /**
   * Sets the taxOffice2
   *
   * @param \string $taxOffice2
   * @return void
   */
  public function setTaxOffice2($taxOffice2) {
    $this->taxOffice2 = $taxOffice2;
  }

  /**
   * Returns the taxOffice2
   *
   * @return \string $taxOffice2
   */
  public function getTaxOffice2() {
    return $this->taxOffice2;
  }

  /**
   * Sets the taxId2
   *
   * @param \string $taxId2
   * @return void
   */
  public function setTaxId2($taxId2) {
    $this->taxId2 = $taxId2;
  }

  /**
   * Returns the taxId2
   *
   * @return \string $taxId2
   */
  public function getTaxId2() {
    return $this->taxId2;
  }

  /**
   * Sets the taxUid2
   *
   * @param \string $taxUid2
   * @return void
   */
  public function setTaxUid2($taxUid2) {
    $this->taxUid2 = $taxUid2;
  }

  /**
   * Returns the taxUid2
   *
   * @return \string $taxUid2
   */
  public function getTaxUid2() {
    return $this->taxUid2;
  }

	// END: -------------- TAX-OFFICE - 2 ----------------

	// START: -------------- TAX-OFFICE - 3 ----------------

  /**
   * taxOfficeUsed3
   *
   * @var \boolean
   */
  protected $taxOfficeUsed3;

  /**
   * taxOfficeNr3
   *
   * @var \string
   */
  protected $taxOfficeNr3;

  /**
   * taxOffice3
   *
   * @var \string
   */
  protected $taxOffice3;

  /**
   * taxId3
   *
   * @var \string
   */
  protected $taxId3;

  /**
   * taxUid3
   *
   * @var \string
   */
  protected $taxUid3;


  /**
   * Sets the taxOfficeUsed3
   *
   * @param \boolean $taxOfficeUsed3
   * @return void
   */
  public function setTaxOfficeUsed3($taxOfficeUsed3) {
    $this->taxOfficeUsed3 = $taxOfficeUsed3;
  }

  /**
   * Returns the taxOfficeUsed3
   *
   * @return \boolean $taxOfficeUsed3
   */
  public function getTaxOfficeUsed3() {
    return $this->taxOfficeUsed3;
  }

  /**
   * Sets the taxOfficeNr3
   *
   * @param \string $taxOfficeNr3
   * @return void
   */
  public function setTaxOfficeNr3($taxOfficeNr3) {
    $this->taxOfficeNr3 = $taxOfficeNr3;
  }

  /**
   * Returns the taxOfficeNr3
   *
   * @return \string $taxOfficeNr3
   */
  public function getTaxOfficeNr3() {
    return $this->taxOfficeNr3;
  }

  /**
   * Sets the taxOffice3
   *
   * @param \string $taxOffice3
   * @return void
   */
  public function setTaxOffice3($taxOffice3) {
    $this->taxOffice3 = $taxOffice3;
  }

  /**
   * Returns the taxOffice3
   *
   * @return \string $taxOffice3
   */
  public function getTaxOffice3() {
    return $this->taxOffice3;
  }

  /**
   * Sets the taxId3
   *
   * @param \string $taxId3
   * @return void
   */
  public function setTaxId3($taxId3) {
    $this->taxId3 = $taxId3;
  }

  /**
   * Returns the taxId3
   *
   * @return \string $taxId3
   */
  public function getTaxId3() {
    return $this->taxId3;
  }

  /**
   * Sets the taxUid3
   *
   * @param \string $taxUid3
   * @return void
   */
  public function setTaxUid3($taxUid3) {
    $this->taxUid3 = $taxUid3;
  }

  /**
   * Returns the taxUid3
   *
   * @return \string $taxUid3
   */
  public function getTaxUid3() {
    return $this->taxUid3;
  }

	// END: -------------- TAX-OFFICE - 3 ----------------

	// START: -------------- TAX-OFFICE - 4 ----------------

  /**
   * taxOfficeUsed4
   *
   * @var \boolean
   */
  protected $taxOfficeUsed4;

  /**
   * taxOfficeNr4
   *
   * @var \string
   */
  protected $taxOfficeNr4;

  /**
   * taxOffice4
   *
   * @var \string
   */
  protected $taxOffice4;

  /**
   * taxId4
   *
   * @var \string
   */
  protected $taxId4;

  /**
   * taxUid4
   *
   * @var \string
   */
  protected $taxUid4;


  /**
   * Sets the taxOfficeUsed4
   *
   * @param \boolean $taxOfficeUsed4
   * @return void
   */
  public function setTaxOfficeUsed4($taxOfficeUsed4) {
    $this->taxOfficeUsed4 = $taxOfficeUsed4;
  }

  /**
   * Returns the taxOfficeUsed4
   *
   * @return \boolean $taxOfficeUsed4
   */
  public function getTaxOfficeUsed4() {
    return $this->taxOfficeUsed4;
  }

  /**
   * Sets the taxOfficeNr4
   *
   * @param \string $taxOfficeNr4
   * @return void
   */
  public function setTaxOfficeNr4($taxOfficeNr4) {
    $this->taxOfficeNr4 = $taxOfficeNr4;
  }

  /**
   * Returns the taxOfficeNr4
   *
   * @return \string $taxOfficeNr4
   */
  public function getTaxOfficeNr4() {
    return $this->taxOfficeNr4;
  }

  /**
   * Sets the taxOffice4
   *
   * @param \string $taxOffice4
   * @return void
   */
  public function setTaxOffice4($taxOffice4) {
    $this->taxOffice4 = $taxOffice4;
  }

  /**
   * Returns the taxOffice4
   *
   * @return \string $taxOffice4
   */
  public function getTaxOffice4() {
    return $this->taxOffice4;
  }

  /**
   * Sets the taxId4
   *
   * @param \string $taxId4
   * @return void
   */
  public function setTaxId4($taxId4) {
    $this->taxId4 = $taxId4;
  }

  /**
   * Returns the taxId4
   *
   * @return \integer $taxId4
   */
  public function getTaxId4() {
    return $this->taxId4;
  }

  /**
   * Sets the taxUid4
   *
   * @param \string $taxUid4
   * @return void
   */
  public function setTaxUid4($taxUid4) {
    $this->taxUid4 = $taxUid4;
  }

  /**
   * Returns the taxUid4
   *
   * @return \string $taxUid4
   */
  public function getTaxUid4() {
    return $this->taxUid4;
  }

	// END: -------------- TAX-OFFICE - 4 ----------------

	// START: -------------- TAX-OFFICE - 5 ----------------

  /**
   * taxOfficeUsed5
   *
   * @var \boolean
   */
  protected $taxOfficeUsed5;

  /**
   * taxOfficeNr5
   *
   * @var \string
   */
  protected $taxOfficeNr5;

  /**
   * taxOffice5
   *
   * @var \string
   */
  protected $taxOffice5;

  /**
   * taxId5
   *
   * @var \string
   */
  protected $taxId5;

  /**
   * taxUid5
   *
   * @var \string
   */
  protected $taxUid5;


  /**
   * Sets the taxOfficeUsed5
   *
   * @param \boolean $taxOfficeUsed5
   * @return void
   */
  public function setTaxOfficeUsed5($taxOfficeUsed5) {
    $this->taxOfficeUsed5 = $taxOfficeUsed5;
  }

  /**
   * Returns the taxOfficeUsed5
   *
   * @return \boolean $taxOfficeUsed5
   */
  public function getTaxOfficeUsed5() {
    return $this->taxOfficeUsed5;
  }

  /**
   * Sets the taxOfficeNr5
   *
   * @param \string $taxOfficeNr5
   * @return void
   */
  public function setTaxOfficeNr5($taxOfficeNr5) {
    $this->taxOfficeNr5 = $taxOfficeNr5;
  }

  /**
   * Returns the taxOfficeNr5
   *
   * @return \string $taxOfficeNr5
   */
  public function getTaxOfficeNr5() {
    return $this->taxOfficeNr5;
  }

  /**
   * Sets the taxOffice5
   *
   * @param \string $taxOffice5
   * @return void
   */
  public function setTaxOffice5($taxOffice5) {
    $this->taxOffice5 = $taxOffice5;
  }

  /**
   * Returns the taxOffice5
   *
   * @return \string $taxOffice5
   */
  public function getTaxOffice5() {
    return $this->taxOffice5;
  }

  /**
   * Sets the taxId5
   *
   * @param \string $taxId5
   * @return void
   */
  public function setTaxId5($taxId5) {
    $this->taxId5 = $taxId5;
  }

  /**
   * Returns the taxId5
   *
   * @return \string $taxId5
   */
  public function getTaxId5() {
    return $this->taxId5;
  }

  /**
   * Sets the taxUid5
   *
   * @param \string $taxUid5
   * @return void
   */
  public function setTaxUid5($taxUid5) {
    $this->taxUid5 = $taxUid5;
  }

  /**
   * Returns the taxUid5
   *
   * @return \string $taxUid5
   */
  public function getTaxUid5() {
    return $this->taxUid5;
  }

	// END: -------------- TAX-OFFICE - 5 ----------------


	// START: -------------- PERSON - 0 ----------------

  /**
   * gender0
   *
   * @var \string
   */
  protected $gender0;

  /**
   * title0
   *
   * @var \string
   */
  protected $title0;
  
  /**
   * firstname0
   *
   * @var \string
   */
  protected $firstname0;

  /**
   * lastname0
   *
   * @var \string
   */
  protected $lastname0;

  /**
   * titleAfter0
   *
   * @var \string
   */
  protected $titleAfter0;

  /**
   * function0
   *
   * @var \string
   */
  protected $function0;

  /**
   * email0
   *
   * @var \string
   */
  protected $email0;

  /**
   * phone0
   *
   * @var \string
   */
  protected $phone0;

  /**
   * phoneCc0
   *
   * @var \string
   */
  protected $phoneCc0;

  /**
   * phoneRc0
   *
   * @var \string
   */
  protected $phoneRc0;

  /**
   * phoneNr0
   *
   * @var \string
   */
  protected $phoneNr0;

  /**
   * phoneEx0
   *
   * @var \string
   */
  protected $phoneEx0;

  /**
   * mobile0
   *
   * @var \string
   */
  protected $mobile0;

  /**
   * mobileCc0
   *
   * @var \string
   */
  protected $mobileCc0;

  /**
   * mobileRc0
   *
   * @var \string
   */
  protected $mobileRc0;

  /**
   * mobileNr0
   *
   * @var \string
   */
  protected $mobileNr0;

  /**
   * mobileEx0
   *
   * @var \string
   */
  protected $mobileEx0;

  /**
   * fax0
   *
   * @var \string
   */
  protected $fax0;

  /**
   * faxCc0
   *
   * @var \string
   */
  protected $faxCc0;

  /**
   * faxRc0
   *
   * @var \string
   */
  protected $faxRc0;

  /**
   * faxNr0
   *
   * @var \string
   */
  protected $faxNr0;

  /**
   * faxEx0
   *
   * @var \string
   */
  protected $faxEx0;


  /**
   * Sets the gender0
   *
   * @param \string $gender0
   * @return void
   */
  public function setGender0($gender0) {
    $this->gender0 = $gender0;
  }

  /**
   * Returns the gender0
   *
   * @return \string $gender0
   */
  public function getGender0() {
    return $this->gender0;
  }

  /**
   * Sets the title0
   *
   * @param \string $title0
   * @return void
   */
  public function setTitle0($title0) {
    $this->title0 = $title0;
  }

  /**
   * Returns the title0
   *
   * @return \string $title0
   */
  public function getTitle0() {
    return $this->title0;
  }

  /**
   * Sets the firstname0
   *
   * @param \string $firstname0
   * @return void
   */
  public function setFirstname0($firstname0) {
    $this->firstname0 = $firstname0;
  }

  /**
   * Returns the firstname0
   *
   * @return \string $firstname0
   */
  public function getFirstname0() {
    return $this->firstname0;
  }

  /**
   * Sets the lastname0
   *
   * @param \string $lastname0
   * @return void
   */
  public function setLastname0($lastname0) {
    $this->lastname0 = $lastname0;
  }

  /**
   * Returns the lastname0
   *
   * @return \string $lastname0
   */
  public function getLastname0() {
    return $this->lastname0;
  }

  /**
   * Sets the titleAfter0
   *
   * @param \string $titleAfter0
   * @return void
   */
  public function setTitleAfter0($titleAfter0) {
    $this->titleAfter0 = $titleAfter0;
  }

  /**
   * Returns the titleAfter0
   *
   * @return \string $titleAfter0
   */
  public function getTitleAfter0() {
    return $this->titleAfter0;
  }

  /**
   * Sets the function0
   *
   * @param \string $function0
   * @return void
   */
  public function setFunction0($function0) {
    $this->function0 = $function0;
  }

  /**
   * Returns the function0
   *
   * @return \string $function0
   */
  public function getFunction0() {
    return $this->function0;
  }

  /**
   * Sets the email0
   *
   * @param \string $email0
   * @return void
   */
  public function setEmail0($email0) {
    $this->email0 = $email0;
  }

  /**
   * Returns the email0
   *
   * @return \string $email0
   */
  public function getEmail0() {
    return $this->email0;
  }


  /**
   * Sets the phone
   *
   * @param \string $phone0
   * @return void
   */
  public function setPhone0($phone0) {
    $this->phone0 = $phone0;
  }

  /**
   * Returns the phone0
   *
   * @return \string $phone0
   */
  public function getPhone0() {
    return $this->phone0;
  }

  /**
   * Sets the phoneCc0
   *
   * @param \string $phoneCc0
   * @return void
   */
  public function setPhoneCc0($phoneCc0) {
    $this->phoneCc0 = $phoneCc0;
  }

  /**
   * Returns the phoneCc0
   *
   * @return \string $phoneCc0
   */
  public function getPhoneCc0() {
    return $this->phoneCc0;
  }

  /**
   * Sets the phoneRc0
   *
   * @param \string $phoneRc0
   * @return void
   */
  public function setPhoneRc0($phoneRc0) {
    $this->phoneRc0 = $phoneRc0;
  }

  /**
   * Returns the phoneRc0
   *
   * @return \string $phoneRc0
   */
  public function getPhoneRc0() {
    return $this->phoneRc0;
  }

  /**
   * Sets the phoneNr0
   *
   * @param \string $phoneNr0
   * @return void
   */
  public function setPhoneNr0($phoneNr0) {
    $this->phoneNr0 = $phoneNr0;
  }

  /**
   * Returns the phoneNr0
   *
   * @return \string $phoneNr0
   */
  public function getPhoneNr0() {
    return $this->phoneNr0;
  }

  /**
   * Sets the phoneEx0
   *
   * @param \string $phoneEx0
   * @return void
   */
  public function setPhoneEx0($phoneEx0) {
    $this->phoneEx0 = $phoneEx0;
  }

  /**
   * Returns the phoneEx0
   *
   * @return \string $phoneEx0
   */
  public function getPhoneEx0() {
    return $this->phoneEx0;
  }


  /**
   * Sets the mobile0
   *
   * @param \string $mobile0
   * @return void
   */
  public function setMobile0($mobile0) {
    $this->mobile0 = $mobile0;
  }

  /**
   * Returns the mobile0
   *
   * @return \string $mobile0
   */
  public function getMobile0() {
    return $this->mobile0;
  }

  /**
   * Sets the mobileCc0
   *
   * @param \string $mobileCc0
   * @return void
   */
  public function setMobileCc0($mobileCc0) {
    $this->mobileCc0 = $mobileCc0;
  }

  /**
   * Returns the mobileCc0
   *
   * @return \string $mobileCc0
   */
  public function getMobileCc0() {
    return $this->mobileCc0;
  }

  /**
   * Sets the mobileRc0
   *
   * @param \string $mobileRc0
   * @return void
   */
  public function setMobileRc0($mobileRc0) {
    $this->mobileRc0 = $mobileRc0;
  }

  /**
   * Returns the mobileRc0
   *
   * @return \string $mobileRc0
   */
  public function getMobileRc0() {
    return $this->mobileRc0;
  }

  /**
   * Sets the mobileNr0
   *
   * @param \string $mobileNr0
   * @return void
   */
  public function setMobileNr0($mobileNr0) {
    $this->mobileNr0 = $mobileNr0;
  }

  /**
   * Returns the mobileNr0
   *
   * @return \string $mobileNr0
   */
  public function getMobileNr0() {
    return $this->mobileNr0;
  }

  /**
   * Sets the mobileEx0
   *
   * @param \string $mobileEx0
   * @return void
   */
  public function setMobileEx0($mobileEx0) {
    $this->mobileEx0 = $mobileEx0;
  }

  /**
   * Returns the mobileEx0
   *
   * @return \string $mobileEx0
   */
  public function getMobileEx0() {
    return $this->mobileEx0;
  }


  /**
   * Sets the fax0
   *
   * @param \string $fax0
   * @return void
   */
  public function setFax0($fax0) {
    $this->fax0 = $fax0;
  }

  /**
   * Returns the fax0
   *
   * @return \string $fax0
   */
  public function getFax0() {
    return $this->fax0;
  }

  /**
   * Sets the faxCc0
   *
   * @param \string $faxCc0
   * @return void
   */
  public function setFaxCc0($faxCc0) {
    $this->faxCc0 = $faxCc0;
  }

  /**
   * Returns the faxCc0
   *
   * @return \string $faxCc0
   */
  public function getFaxCc0() {
    return $this->faxCc0;
  }

  /**
   * Sets the faxRc0
   *
   * @param \string $faxRc0
   * @return void
   */
  public function setFaxRc0($faxRc0) {
    $this->faxRc0 = $faxRc0;
  }

  /**
   * Returns the faxRc0
   *
   * @return \string $faxRc0
   */
  public function getFaxRc0() {
    return $this->faxRc0;
  }

  /**
   * Sets the faxNr0
   *
   * @param \string $faxNr0
   * @return void
   */
  public function setFaxNr0($faxNr0) {
    $this->faxNr0 = $faxNr0;
  }

  /**
   * Returns the faxNr0
   *
   * @return \string $faxNr0
   */
  public function getFaxNr0() {
    return $this->faxNr0;
  }

  /**
   * Sets the faxEx0
   *
   * @param \string $faxEx0
   * @return void
   */
  public function setFaxEx0($faxEx0) {
    $this->faxEx0 = $faxEx0;
  }

  /**
   * Returns the faxEx0
   *
   * @return \string $faxEx0
   */
  public function getFaxEx0() {
    return $this->faxEx0;
  }

  
	// END: -------------- PERSON - 0 ----------------

	// START: -------------- PERSON - 1 ----------------

  /**
   * gender1
   *
   * @var \string
   */
  protected $gender1;

  /**
   * title1
   *
   * @var \string
   */
  protected $title1;
  
  /**
   * firstname1
   *
   * @var \string
   */
  protected $firstname1;

  /**
   * lastname1
   *
   * @var \string
   */
  protected $lastname1;

  /**
   * titleAfter1
   *
   * @var \string
   */
  protected $titleAfter1;

  /**
   * function1
   *
   * @var \string
   */
  protected $function1;

  /**
   * email1
   *
   * @var \string
   */
  protected $email1;

  /**
   * phone1
   *
   * @var \string
   */
  protected $phone1;

  /**
   * phoneCc1
   *
   * @var \string
   */
  protected $phoneCc1;

  /**
   * phoneRc1
   *
   * @var \string
   */
  protected $phoneRc1;

  /**
   * phoneNr1
   *
   * @var \string
   */
  protected $phoneNr1;

  /**
   * phoneEx1
   *
   * @var \string
   */
  protected $phoneEx1;

  /**
   * mobile1
   *
   * @var \string
   */
  protected $mobile1;

  /**
   * mobileCc1
   *
   * @var \string
   */
  protected $mobileCc1;

  /**
   * mobileRc1
   *
   * @var \string
   */
  protected $mobileRc1;

  /**
   * mobileNr1
   *
   * @var \string
   */
  protected $mobileNr1;

  /**
   * mobileEx1
   *
   * @var \string
   */
  protected $mobileEx1;

  /**
   * fax1
   *
   * @var \string
   */
  protected $fax1;

  /**
   * faxCc1
   *
   * @var \string
   */
  protected $faxCc1;

  /**
   * faxRc1
   *
   * @var \string
   */
  protected $faxRc1;

  /**
   * faxNr1
   *
   * @var \string
   */
  protected $faxNr1;

  /**
   * faxEx1
   *
   * @var \string
   */
  protected $faxEx1;

  /**
   * used1
   *
   * @var \integer
   */
  protected $used1;


  /**
   * Sets the gender1
   *
   * @param \string $gender1
   * @return void
   */
  public function setGender1($gender1) {
    $this->gender1 = $gender1;
  }

  /**
   * Returns the gender1
   *
   * @return \string $gender1
   */
  public function getGender1() {
    return $this->gender1;
  }

  /**
   * Sets the title1
   *
   * @param \string $title1
   * @return void
   */
  public function setTitle1($title1) {
    $this->title1 = $title1;
  }

  /**
   * Returns the title1
   *
   * @return \string $title1
   */
  public function getTitle1() {
    return $this->title1;
  }

  /**
   * Sets the firstname1
   *
   * @param \string $firstname1
   * @return void
   */
  public function setFirstname1($firstname1) {
    $this->firstname1 = $firstname1;
  }

  /**
   * Returns the firstname1
   *
   * @return \string $firstname1
   */
  public function getFirstname1() {
    return $this->firstname1;
  }

  /**
   * Sets the lastname1
   *
   * @param \string $lastname1
   * @return void
   */
  public function setLastname1($lastname1) {
    $this->lastname1 = $lastname1;
  }

  /**
   * Returns the lastname1
   *
   * @return \string $lastname1
   */
  public function getLastname1() {
    return $this->lastname1;
  }

  /**
   * Sets the titleAfter1
   *
   * @param \string $titleAfter1
   * @return void
   */
  public function setTitleAfter1($titleAfter1) {
    $this->titleAfter1 = $titleAfter1;
  }

  /**
   * Returns the titleAfter1
   *
   * @return \string $titleAfter1
   */
  public function getTitleAfter1() {
    return $this->titleAfter1;
  }

  /**
   * Sets the function1
   *
   * @param \string $function1
   * @return void
   */
  public function setFunction1($function1) {
    $this->function1 = $function1;
  }

  /**
   * Returns the function1
   *
   * @return \string $function1
   */
  public function getFunction1() {
    return $this->function1;
  }

  /**
   * Sets the email1
   *
   * @param \string $email1
   * @return void
   */
  public function setEmail1($email1) {
    $this->email1 = $email1;
  }

  /**
   * Returns the email1
   *
   * @return \string $email1
   */
  public function getEmail1() {
    return $this->email1;
  }


  /**
   * Sets the phone
   *
   * @param \string $phone1
   * @return void
   */
  public function setPhone1($phone1) {
    $this->phone1 = $phone1;
  }

  /**
   * Returns the phone1
   *
   * @return \string $phone1
   */
  public function getPhone1() {
    return $this->phone1;
  }

  /**
   * Sets the phoneCc1
   *
   * @param \string $phoneCc1
   * @return void
   */
  public function setPhoneCc1($phoneCc1) {
    $this->phoneCc1 = $phoneCc1;
  }

  /**
   * Returns the phoneCc1
   *
   * @return \string $phoneCc1
   */
  public function getPhoneCc1() {
    return $this->phoneCc1;
  }

  /**
   * Sets the phoneRc1
   *
   * @param \string $phoneRc1
   * @return void
   */
  public function setPhoneRc1($phoneRc1) {
    $this->phoneRc1 = $phoneRc1;
  }

  /**
   * Returns the phoneRc1
   *
   * @return \string $phoneRc1
   */
  public function getPhoneRc1() {
    return $this->phoneRc1;
  }

  /**
   * Sets the phoneNr1
   *
   * @param \string $phoneNr1
   * @return void
   */
  public function setPhoneNr1($phoneNr1) {
    $this->phoneNr1 = $phoneNr1;
  }

  /**
   * Returns the phoneNr1
   *
   * @return \string $phoneNr1
   */
  public function getPhoneNr1() {
    return $this->phoneNr1;
  }

  /**
   * Sets the phoneEx1
   *
   * @param \string $phoneEx1
   * @return void
   */
  public function setPhoneEx1($phoneEx1) {
    $this->phoneEx1 = $phoneEx1;
  }

  /**
   * Returns the phoneEx1
   *
   * @return \string $phoneEx1
   */
  public function getPhoneEx1() {
    return $this->phoneEx1;
  }


  /**
   * Sets the mobile1
   *
   * @param \string $mobile1
   * @return void
   */
  public function setMobile1($mobile1) {
    $this->mobile1 = $mobile1;
  }

  /**
   * Returns the mobile1
   *
   * @return \string $mobile1
   */
  public function getMobile1() {
    return $this->mobile1;
  }

  /**
   * Sets the mobileCc1
   *
   * @param \string $mobileCc1
   * @return void
   */
  public function setMobileCc1($mobileCc1) {
    $this->mobileCc1 = $mobileCc1;
  }

  /**
   * Returns the mobileCc1
   *
   * @return \string $mobileCc1
   */
  public function getMobileCc1() {
    return $this->mobileCc1;
  }

  /**
   * Sets the mobileRc1
   *
   * @param \string $mobileRc1
   * @return void
   */
  public function setMobileRc1($mobileRc1) {
    $this->mobileRc1 = $mobileRc1;
  }

  /**
   * Returns the mobileRc1
   *
   * @return \string $mobileRc1
   */
  public function getMobileRc1() {
    return $this->mobileRc1;
  }

  /**
   * Sets the mobileNr1
   *
   * @param \string $mobileNr1
   * @return void
   */
  public function setMobileNr1($mobileNr1) {
    $this->mobileNr1 = $mobileNr1;
  }

  /**
   * Returns the mobileNr1
   *
   * @return \string $mobileNr1
   */
  public function getMobileNr1() {
    return $this->mobileNr1;
  }

  /**
   * Sets the mobileEx1
   *
   * @param \string $mobileEx1
   * @return void
   */
  public function setMobileEx1($mobileEx1) {
    $this->mobileEx1 = $mobileEx1;
  }

  /**
   * Returns the mobileEx1
   *
   * @return \string $mobileEx1
   */
  public function getMobileEx1() {
    return $this->mobileEx1;
  }


  /**
   * Sets the fax1
   *
   * @param \string $fax1
   * @return void
   */
  public function setFax1($fax1) {
    $this->fax1 = $fax1;
  }

  /**
   * Returns the fax1
   *
   * @return \string $fax1
   */
  public function getFax1() {
    return $this->fax1;
  }

  /**
   * Sets the faxCc1
   *
   * @param \string $faxCc1
   * @return void
   */
  public function setFaxCc1($faxCc1) {
    $this->faxCc1 = $faxCc1;
  }

  /**
   * Returns the faxCc1
   *
   * @return \string $faxCc1
   */
  public function getFaxCc1() {
    return $this->faxCc1;
  }

  /**
   * Sets the faxRc1
   *
   * @param \string $faxRc1
   * @return void
   */
  public function setFaxRc1($faxRc1) {
    $this->faxRc1 = $faxRc1;
  }

  /**
   * Returns the faxRc1
   *
   * @return \string $faxRc1
   */
  public function getFaxRc1() {
    return $this->faxRc1;
  }

  /**
   * Sets the faxNr1
   *
   * @param \string $faxNr1
   * @return void
   */
  public function setFaxNr1($faxNr1) {
    $this->faxNr1 = $faxNr1;
  }

  /**
   * Returns the faxNr1
   *
   * @return \string $faxNr1
   */
  public function getFaxNr1() {
    return $this->faxNr1;
  }

  /**
   * Sets the faxEx1
   *
   * @param \string $faxEx1
   * @return void
   */
  public function setFaxEx1($faxEx1) {
    $this->faxEx1 = $faxEx1;
  }

  /**
   * Returns the faxEx1
   *
   * @return \string $faxEx1
   */
  public function getFaxEx1() {
    return $this->faxEx1;
  }

  
  /**
   * Sets the used1
   *
   * @param \integer $used1
   * @return void
   */
  public function setUsed1($used1) {
    $this->used1 = $used1;
  }

  /**
   * Returns the used1
   *
   * @return \integer $used1
   */
  public function getUsed1() {
    return $this->used1;
  }
  
  
	// END: -------------- PERSON - 1 ----------------

	// START: -------------- PERSON - 2 ----------------

  /**
   * gender2
   *
   * @var \string
   */
  protected $gender2;

  /**
   * title2
   *
   * @var \string
   */
  protected $title2;
  
  /**
   * firstname2
   *
   * @var \string
   */
  protected $firstname2;

  /**
   * lastname2
   *
   * @var \string
   */
  protected $lastname2;

  /**
   * titleAfter2
   *
   * @var \string
   */
  protected $titleAfter2;

  /**
   * function2
   *
   * @var \string
   */
  protected $function2;

  /**
   * email2
   *
   * @var \string
   */
  protected $email2;

  /**
   * phone2
   *
   * @var \string
   */
  protected $phone2;

  /**
   * phoneCc2
   *
   * @var \string
   */
  protected $phoneCc2;

  /**
   * phoneRc2
   *
   * @var \string
   */
  protected $phoneRc2;

  /**
   * phoneNr2
   *
   * @var \string
   */
  protected $phoneNr2;

  /**
   * phoneEx2
   *
   * @var \string
   */
  protected $phoneEx2;

  /**
   * mobile2
   *
   * @var \string
   */
  protected $mobile2;

  /**
   * mobileCc2
   *
   * @var \string
   */
  protected $mobileCc2;

  /**
   * mobileRc2
   *
   * @var \string
   */
  protected $mobileRc2;

  /**
   * mobileNr2
   *
   * @var \string
   */
  protected $mobileNr2;

  /**
   * mobileEx2
   *
   * @var \string
   */
  protected $mobileEx2;

  /**
   * fax2
   *
   * @var \string
   */
  protected $fax2;

  /**
   * faxCc2
   *
   * @var \string
   */
  protected $faxCc2;

  /**
   * faxRc2
   *
   * @var \string
   */
  protected $faxRc2;

  /**
   * faxNr2
   *
   * @var \string
   */
  protected $faxNr2;

  /**
   * faxEx2
   *
   * @var \string
   */
  protected $faxEx2;

  /**
   * used2
   *
   * @var \integer
   */
  protected $used2;


  /**
   * Sets the gender2
   *
   * @param \string $gender2
   * @return void
   */
  public function setGender2($gender2) {
    $this->gender2 = $gender2;
  }

  /**
   * Returns the gender2
   *
   * @return \string $gender2
   */
  public function getGender2() {
    return $this->gender2;
  }

  /**
   * Sets the title2
   *
   * @param \string $title2
   * @return void
   */
  public function setTitle2($title2) {
    $this->title2 = $title2;
  }

  /**
   * Returns the title2
   *
   * @return \string $title2
   */
  public function getTitle2() {
    return $this->title2;
  }

  /**
   * Sets the firstname2
   *
   * @param \string $firstname2
   * @return void
   */
  public function setFirstname2($firstname2) {
    $this->firstname2 = $firstname2;
  }

  /**
   * Returns the firstname2
   *
   * @return \string $firstname2
   */
  public function getFirstname2() {
    return $this->firstname2;
  }

  /**
   * Sets the lastname2
   *
   * @param \string $lastname2
   * @return void
   */
  public function setLastname2($lastname2) {
    $this->lastname2 = $lastname2;
  }

  /**
   * Returns the lastname2
   *
   * @return \string $lastname2
   */
  public function getLastname2() {
    return $this->lastname2;
  }

  /**
   * Sets the titleAfter2
   *
   * @param \string $titleAfter2
   * @return void
   */
  public function setTitleAfter2($titleAfter2) {
    $this->titleAfter2 = $titleAfter2;
  }

  /**
   * Returns the titleAfter2
   *
   * @return \string $titleAfter2
   */
  public function getTitleAfter2() {
    return $this->titleAfter2;
  }

  /**
   * Sets the function2
   *
   * @param \string $function2
   * @return void
   */
  public function setFunction2($function2) {
    $this->function2 = $function2;
  }

  /**
   * Returns the function2
   *
   * @return \string $function2
   */
  public function getFunction2() {
    return $this->function2;
  }

  /**
   * Sets the email2
   *
   * @param \string $email2
   * @return void
   */
  public function setEmail2($email2) {
    $this->email2 = $email2;
  }

  /**
   * Returns the email2
   *
   * @return \string $email2
   */
  public function getEmail2() {
    return $this->email2;
  }


  /**
   * Sets the phone
   *
   * @param \string $phone2
   * @return void
   */
  public function setPhone2($phone2) {
    $this->phone2 = $phone2;
  }

  /**
   * Returns the phone2
   *
   * @return \string $phone2
   */
  public function getPhone2() {
    return $this->phone2;
  }

  /**
   * Sets the phoneCc2
   *
   * @param \string $phoneCc2
   * @return void
   */
  public function setPhoneCc2($phoneCc2) {
    $this->phoneCc2 = $phoneCc2;
  }

  /**
   * Returns the phoneCc2
   *
   * @return \string $phoneCc2
   */
  public function getPhoneCc2() {
    return $this->phoneCc2;
  }

  /**
   * Sets the phoneRc2
   *
   * @param \string $phoneRc2
   * @return void
   */
  public function setPhoneRc2($phoneRc2) {
    $this->phoneRc2 = $phoneRc2;
  }

  /**
   * Returns the phoneRc2
   *
   * @return \string $phoneRc2
   */
  public function getPhoneRc2() {
    return $this->phoneRc2;
  }

  /**
   * Sets the phoneNr2
   *
   * @param \string $phoneNr2
   * @return void
   */
  public function setPhoneNr2($phoneNr2) {
    $this->phoneNr2 = $phoneNr2;
  }

  /**
   * Returns the phoneNr2
   *
   * @return \string $phoneNr2
   */
  public function getPhoneNr2() {
    return $this->phoneNr2;
  }

  /**
   * Sets the phoneEx2
   *
   * @param \string $phoneEx2
   * @return void
   */
  public function setPhoneEx2($phoneEx2) {
    $this->phoneEx2 = $phoneEx2;
  }

  /**
   * Returns the phoneEx2
   *
   * @return \string $phoneEx2
   */
  public function getPhoneEx2() {
    return $this->phoneEx2;
  }


  /**
   * Sets the mobile2
   *
   * @param \string $mobile2
   * @return void
   */
  public function setMobile2($mobile2) {
    $this->mobile2 = $mobile2;
  }

  /**
   * Returns the mobile2
   *
   * @return \string $mobile2
   */
  public function getMobile2() {
    return $this->mobile2;
  }

  /**
   * Sets the mobileCc2
   *
   * @param \string $mobileCc2
   * @return void
   */
  public function setMobileCc2($mobileCc2) {
    $this->mobileCc2 = $mobileCc2;
  }

  /**
   * Returns the mobileCc2
   *
   * @return \string $mobileCc2
   */
  public function getMobileCc2() {
    return $this->mobileCc2;
  }

  /**
   * Sets the mobileRc2
   *
   * @param \string $mobileRc2
   * @return void
   */
  public function setMobileRc2($mobileRc2) {
    $this->mobileRc2 = $mobileRc2;
  }

  /**
   * Returns the mobileRc2
   *
   * @return \string $mobileRc2
   */
  public function getMobileRc2() {
    return $this->mobileRc2;
  }

  /**
   * Sets the mobileNr2
   *
   * @param \string $mobileNr2
   * @return void
   */
  public function setMobileNr2($mobileNr2) {
    $this->mobileNr2 = $mobileNr2;
  }

  /**
   * Returns the mobileNr2
   *
   * @return \string $mobileNr2
   */
  public function getMobileNr2() {
    return $this->mobileNr2;
  }

  /**
   * Sets the mobileEx2
   *
   * @param \string $mobileEx2
   * @return void
   */
  public function setMobileEx2($mobileEx2) {
    $this->mobileEx2 = $mobileEx2;
  }

  /**
   * Returns the mobileEx2
   *
   * @return \string $mobileEx2
   */
  public function getMobileEx2() {
    return $this->mobileEx2;
  }


  /**
   * Sets the fax2
   *
   * @param \string $fax2
   * @return void
   */
  public function setFax2($fax2) {
    $this->fax2 = $fax2;
  }

  /**
   * Returns the fax2
   *
   * @return \string $fax2
   */
  public function getFax2() {
    return $this->fax2;
  }

  /**
   * Sets the faxCc2
   *
   * @param \string $faxCc2
   * @return void
   */
  public function setFaxCc2($faxCc2) {
    $this->faxCc2 = $faxCc2;
  }

  /**
   * Returns the faxCc2
   *
   * @return \string $faxCc2
   */
  public function getFaxCc2() {
    return $this->faxCc2;
  }

  /**
   * Sets the faxRc2
   *
   * @param \string $faxRc2
   * @return void
   */
  public function setFaxRc2($faxRc2) {
    $this->faxRc2 = $faxRc2;
  }

  /**
   * Returns the faxRc2
   *
   * @return \string $faxRc2
   */
  public function getFaxRc2() {
    return $this->faxRc2;
  }

  /**
   * Sets the faxNr2
   *
   * @param \string $faxNr2
   * @return void
   */
  public function setFaxNr2($faxNr2) {
    $this->faxNr2 = $faxNr2;
  }

  /**
   * Returns the faxNr2
   *
   * @return \string $faxNr2
   */
  public function getFaxNr2() {
    return $this->faxNr2;
  }

  /**
   * Sets the faxEx2
   *
   * @param \string $faxEx2
   * @return void
   */
  public function setFaxEx2($faxEx2) {
    $this->faxEx2 = $faxEx2;
  }

  /**
   * Returns the faxEx2
   *
   * @return \string $faxEx2
   */
  public function getFaxEx2() {
    return $this->faxEx2;
  }

  
  /**
   * Sets the used2
   *
   * @param \integer $used2
   * @return void
   */
  public function setUsed2($used2) {
    $this->used2 = $used2;
  }

  /**
   * Returns the used2
   *
   * @return \integer $used2
   */
  public function getUsed2() {
    return $this->used2;
  }
  
	// END: -------------- PERSON - 2 ----------------

	// START: -------------- PERSON - 3 ----------------

  /**
   * gender3
   *
   * @var \string
   */
  protected $gender3;

  /**
   * title3
   *
   * @var \string
   */
  protected $title3;
  
  /**
   * firstname3
   *
   * @var \string
   */
  protected $firstname3;

  /**
   * lastname3
   *
   * @var \string
   */
  protected $lastname3;

  /**
   * titleAfter3
   *
   * @var \string
   */
  protected $titleAfter3;

  /**
   * function3
   *
   * @var \string
   */
  protected $function3;

  /**
   * email3
   *
   * @var \string
   */
  protected $email3;

  /**
   * phone3
   *
   * @var \string
   */
  protected $phone3;

  /**
   * phoneCc3
   *
   * @var \string
   */
  protected $phoneCc3;

  /**
   * phoneRc3
   *
   * @var \string
   */
  protected $phoneRc3;

  /**
   * phoneNr3
   *
   * @var \string
   */
  protected $phoneNr3;

  /**
   * phoneEx3
   *
   * @var \string
   */
  protected $phoneEx3;

  /**
   * mobile3
   *
   * @var \string
   */
  protected $mobile3;

  /**
   * mobileCc3
   *
   * @var \string
   */
  protected $mobileCc3;

  /**
   * mobileRc3
   *
   * @var \string
   */
  protected $mobileRc3;

  /**
   * mobileNr3
   *
   * @var \string
   */
  protected $mobileNr3;

  /**
   * mobileEx3
   *
   * @var \string
   */
  protected $mobileEx3;

  /**
   * fax3
   *
   * @var \string
   */
  protected $fax3;

  /**
   * faxCc3
   *
   * @var \string
   */
  protected $faxCc3;

  /**
   * faxRc3
   *
   * @var \string
   */
  protected $faxRc3;

  /**
   * faxNr3
   *
   * @var \string
   */
  protected $faxNr3;

  /**
   * faxEx3
   *
   * @var \string
   */
  protected $faxEx3;

  /**
   * used3
   *
   * @var \integer
   */
  protected $used3;


  /**
   * Sets the gender3
   *
   * @param \string $gender3
   * @return void
   */
  public function setGender3($gender3) {
    $this->gender3 = $gender3;
  }

  /**
   * Returns the gender3
   *
   * @return \string $gender3
   */
  public function getGender3() {
    return $this->gender3;
  }

  /**
   * Sets the title3
   *
   * @param \string $title3
   * @return void
   */
  public function setTitle3($title3) {
    $this->title3 = $title3;
  }

  /**
   * Returns the title3
   *
   * @return \string $title3
   */
  public function getTitle3() {
    return $this->title3;
  }

  /**
   * Sets the firstname3
   *
   * @param \string $firstname3
   * @return void
   */
  public function setFirstname3($firstname3) {
    $this->firstname3 = $firstname3;
  }

  /**
   * Returns the firstname3
   *
   * @return \string $firstname3
   */
  public function getFirstname3() {
    return $this->firstname3;
  }

  /**
   * Sets the lastname3
   *
   * @param \string $lastname3
   * @return void
   */
  public function setLastname3($lastname3) {
    $this->lastname3 = $lastname3;
  }

  /**
   * Returns the lastname3
   *
   * @return \string $lastname3
   */
  public function getLastname3() {
    return $this->lastname3;
  }

  /**
   * Sets the titleAfter3
   *
   * @param \string $titleAfter3
   * @return void
   */
  public function setTitleAfter3($titleAfter3) {
    $this->titleAfter3 = $titleAfter3;
  }

  /**
   * Returns the titleAfter3
   *
   * @return \string $titleAfter3
   */
  public function getTitleAfter3() {
    return $this->titleAfter3;
  }

  /**
   * Sets the function3
   *
   * @param \string $function3
   * @return void
   */
  public function setFunction3($function3) {
    $this->function3 = $function3;
  }

  /**
   * Returns the function3
   *
   * @return \string $function3
   */
  public function getFunction3() {
    return $this->function3;
  }

  /**
   * Sets the email3
   *
   * @param \string $email3
   * @return void
   */
  public function setEmail3($email3) {
    $this->email3 = $email3;
  }

  /**
   * Returns the email3
   *
   * @return \string $email3
   */
  public function getEmail3() {
    return $this->email3;
  }


  /**
   * Sets the phone
   *
   * @param \string $phone3
   * @return void
   */
  public function setPhone3($phone3) {
    $this->phone3 = $phone3;
  }

  /**
   * Returns the phone3
   *
   * @return \string $phone3
   */
  public function getPhone3() {
    return $this->phone3;
  }

  /**
   * Sets the phoneCc3
   *
   * @param \string $phoneCc3
   * @return void
   */
  public function setPhoneCc3($phoneCc3) {
    $this->phoneCc3 = $phoneCc3;
  }

  /**
   * Returns the phoneCc3
   *
   * @return \string $phoneCc3
   */
  public function getPhoneCc3() {
    return $this->phoneCc3;
  }

  /**
   * Sets the phoneRc3
   *
   * @param \string $phoneRc3
   * @return void
   */
  public function setPhoneRc3($phoneRc3) {
    $this->phoneRc3 = $phoneRc3;
  }

  /**
   * Returns the phoneRc3
   *
   * @return \string $phoneRc3
   */
  public function getPhoneRc3() {
    return $this->phoneRc3;
  }

  /**
   * Sets the phoneNr3
   *
   * @param \string $phoneNr3
   * @return void
   */
  public function setPhoneNr3($phoneNr3) {
    $this->phoneNr3 = $phoneNr3;
  }

  /**
   * Returns the phoneNr3
   *
   * @return \string $phoneNr3
   */
  public function getPhoneNr3() {
    return $this->phoneNr3;
  }

  /**
   * Sets the phoneEx3
   *
   * @param \string $phoneEx3
   * @return void
   */
  public function setPhoneEx3($phoneEx3) {
    $this->phoneEx3 = $phoneEx3;
  }

  /**
   * Returns the phoneEx3
   *
   * @return \string $phoneEx3
   */
  public function getPhoneEx3() {
    return $this->phoneEx3;
  }


  /**
   * Sets the mobile3
   *
   * @param \string $mobile3
   * @return void
   */
  public function setMobile3($mobile3) {
    $this->mobile3 = $mobile3;
  }

  /**
   * Returns the mobile3
   *
   * @return \string $mobile3
   */
  public function getMobile3() {
    return $this->mobile3;
  }

  /**
   * Sets the mobileCc3
   *
   * @param \string $mobileCc3
   * @return void
   */
  public function setMobileCc3($mobileCc3) {
    $this->mobileCc3 = $mobileCc3;
  }

  /**
   * Returns the mobileCc3
   *
   * @return \string $mobileCc3
   */
  public function getMobileCc3() {
    return $this->mobileCc3;
  }

  /**
   * Sets the mobileRc3
   *
   * @param \string $mobileRc3
   * @return void
   */
  public function setMobileRc3($mobileRc3) {
    $this->mobileRc3 = $mobileRc3;
  }

  /**
   * Returns the mobileRc3
   *
   * @return \string $mobileRc3
   */
  public function getMobileRc3() {
    return $this->mobileRc3;
  }

  /**
   * Sets the mobileNr3
   *
   * @param \string $mobileNr3
   * @return void
   */
  public function setMobileNr3($mobileNr3) {
    $this->mobileNr3 = $mobileNr3;
  }

  /**
   * Returns the mobileNr3
   *
   * @return \string $mobileNr3
   */
  public function getMobileNr3() {
    return $this->mobileNr3;
  }

  /**
   * Sets the mobileEx3
   *
   * @param \string $mobileEx3
   * @return void
   */
  public function setMobileEx3($mobileEx3) {
    $this->mobileEx3 = $mobileEx3;
  }

  /**
   * Returns the mobileEx3
   *
   * @return \string $mobileEx3
   */
  public function getMobileEx3() {
    return $this->mobileEx3;
  }


  /**
   * Sets the fax3
   *
   * @param \string $fax3
   * @return void
   */
  public function setFax3($fax3) {
    $this->fax3 = $fax3;
  }

  /**
   * Returns the fax3
   *
   * @return \string $fax3
   */
  public function getFax3() {
    return $this->fax3;
  }

  /**
   * Sets the faxCc3
   *
   * @param \string $faxCc3
   * @return void
   */
  public function setFaxCc3($faxCc3) {
    $this->faxCc3 = $faxCc3;
  }

  /**
   * Returns the faxCc3
   *
   * @return \string $faxCc3
   */
  public function getFaxCc3() {
    return $this->faxCc3;
  }

  /**
   * Sets the faxRc3
   *
   * @param \string $faxRc3
   * @return void
   */
  public function setFaxRc3($faxRc3) {
    $this->faxRc3 = $faxRc3;
  }

  /**
   * Returns the faxRc3
   *
   * @return \string $faxRc3
   */
  public function getFaxRc3() {
    return $this->faxRc3;
  }

  /**
   * Sets the faxNr3
   *
   * @param \string $faxNr3
   * @return void
   */
  public function setFaxNr3($faxNr3) {
    $this->faxNr3 = $faxNr3;
  }

  /**
   * Returns the faxNr3
   *
   * @return \string $faxNr3
   */
  public function getFaxNr3() {
    return $this->faxNr3;
  }

  /**
   * Sets the faxEx3
   *
   * @param \string $faxEx3
   * @return void
   */
  public function setFaxEx3($faxEx3) {
    $this->faxEx3 = $faxEx3;
  }

  /**
   * Returns the faxEx3
   *
   * @return \string $faxEx3
   */
  public function getFaxEx3() {
    return $this->faxEx3;
  }


  /**
   * Sets the used3
   *
   * @param \integer $used3
   * @return void
   */
  public function setUsed3($used3) {
    $this->used3 = $used3;
  }

  /**
   * Returns the used3
   *
   * @return \integer $used3
   */
  public function getUsed3() {
    return $this->used3;
  }
  
	// END: -------------- PERSON - 3 ----------------

	// START: -------------- PERSON - 4 ----------------

  /**
   * gender4
   *
   * @var \string
   */
  protected $gender4;

  /**
   * title4
   *
   * @var \string
   */
  protected $title4;
  
  /**
   * firstname4
   *
   * @var \string
   */
  protected $firstname4;

  /**
   * lastname4
   *
   * @var \string
   */
  protected $lastname4;

  /**
   * titleAfter4
   *
   * @var \string
   */
  protected $titleAfter4;

  /**
   * function4
   *
   * @var \string
   */
  protected $function4;

  /**
   * email4
   *
   * @var \string
   */
  protected $email4;

  /**
   * phone4
   *
   * @var \string
   */
  protected $phone4;

  /**
   * phoneCc4
   *
   * @var \string
   */
  protected $phoneCc4;

  /**
   * phoneRc4
   *
   * @var \string
   */
  protected $phoneRc4;

  /**
   * phoneNr4
   *
   * @var \string
   */
  protected $phoneNr4;

  /**
   * phoneEx4
   *
   * @var \string
   */
  protected $phoneEx4;

  /**
   * mobile4
   *
   * @var \string
   */
  protected $mobile4;

  /**
   * mobileCc4
   *
   * @var \string
   */
  protected $mobileCc4;

  /**
   * mobileRc4
   *
   * @var \string
   */
  protected $mobileRc4;

  /**
   * mobileNr4
   *
   * @var \string
   */
  protected $mobileNr4;

  /**
   * mobileEx4
   *
   * @var \string
   */
  protected $mobileEx4;

  /**
   * fax4
   *
   * @var \string
   */
  protected $fax4;

  /**
   * faxCc4
   *
   * @var \string
   */
  protected $faxCc4;

  /**
   * faxRc4
   *
   * @var \string
   */
  protected $faxRc4;

  /**
   * faxNr4
   *
   * @var \string
   */
  protected $faxNr4;

  /**
   * faxEx4
   *
   * @var \string
   */
  protected $faxEx4;

  /**
   * used4
   *
   * @var \integer
   */
  protected $used4;


  /**
   * Sets the gender4
   *
   * @param \string $gender4
   * @return void
   */
  public function setGender4($gender4) {
    $this->gender4 = $gender4;
  }

  /**
   * Returns the gender4
   *
   * @return \string $gender4
   */
  public function getGender4() {
    return $this->gender4;
  }

  /**
   * Sets the title4
   *
   * @param \string $title4
   * @return void
   */
  public function setTitle4($title4) {
    $this->title4 = $title4;
  }

  /**
   * Returns the title4
   *
   * @return \string $title4
   */
  public function getTitle4() {
    return $this->title4;
  }

  /**
   * Sets the firstname4
   *
   * @param \string $firstname4
   * @return void
   */
  public function setFirstname4($firstname4) {
    $this->firstname4 = $firstname4;
  }

  /**
   * Returns the firstname4
   *
   * @return \string $firstname4
   */
  public function getFirstname4() {
    return $this->firstname4;
  }

  /**
   * Sets the lastname4
   *
   * @param \string $lastname4
   * @return void
   */
  public function setLastname4($lastname4) {
    $this->lastname4 = $lastname4;
  }

  /**
   * Returns the lastname4
   *
   * @return \string $lastname4
   */
  public function getLastname4() {
    return $this->lastname4;
  }

  /**
   * Sets the titleAfter4
   *
   * @param \string $titleAfter4
   * @return void
   */
  public function setTitleAfter4($titleAfter4) {
    $this->titleAfter4 = $titleAfter4;
  }

  /**
   * Returns the titleAfter4
   *
   * @return \string $titleAfter4
   */
  public function getTitleAfter4() {
    return $this->titleAfter4;
  }

  /**
   * Sets the function4
   *
   * @param \string $function4
   * @return void
   */
  public function setFunction4($function4) {
    $this->function4 = $function4;
  }

  /**
   * Returns the function4
   *
   * @return \string $function4
   */
  public function getFunction4() {
    return $this->function4;
  }

  /**
   * Sets the email4
   *
   * @param \string $email4
   * @return void
   */
  public function setEmail4($email4) {
    $this->email4 = $email4;
  }

  /**
   * Returns the email4
   *
   * @return \string $email4
   */
  public function getEmail4() {
    return $this->email4;
  }


  /**
   * Sets the phone
   *
   * @param \string $phone4
   * @return void
   */
  public function setPhone4($phone4) {
    $this->phone4 = $phone4;
  }

  /**
   * Returns the phone4
   *
   * @return \string $phone4
   */
  public function getPhone4() {
    return $this->phone4;
  }

  /**
   * Sets the phoneCc4
   *
   * @param \string $phoneCc4
   * @return void
   */
  public function setPhoneCc4($phoneCc4) {
    $this->phoneCc4 = $phoneCc4;
  }

  /**
   * Returns the phoneCc4
   *
   * @return \string $phoneCc4
   */
  public function getPhoneCc4() {
    return $this->phoneCc4;
  }

  /**
   * Sets the phoneRc4
   *
   * @param \string $phoneRc4
   * @return void
   */
  public function setPhoneRc4($phoneRc4) {
    $this->phoneRc4 = $phoneRc4;
  }

  /**
   * Returns the phoneRc4
   *
   * @return \string $phoneRc4
   */
  public function getPhoneRc4() {
    return $this->phoneRc4;
  }

  /**
   * Sets the phoneNr4
   *
   * @param \string $phoneNr4
   * @return void
   */
  public function setPhoneNr4($phoneNr4) {
    $this->phoneNr4 = $phoneNr4;
  }

  /**
   * Returns the phoneNr4
   *
   * @return \string $phoneNr4
   */
  public function getPhoneNr4() {
    return $this->phoneNr4;
  }

  /**
   * Sets the phoneEx4
   *
   * @param \string $phoneEx4
   * @return void
   */
  public function setPhoneEx4($phoneEx4) {
    $this->phoneEx4 = $phoneEx4;
  }

  /**
   * Returns the phoneEx4
   *
   * @return \string $phoneEx4
   */
  public function getPhoneEx4() {
    return $this->phoneEx4;
  }


  /**
   * Sets the mobile4
   *
   * @param \string $mobile4
   * @return void
   */
  public function setMobile4($mobile4) {
    $this->mobile4 = $mobile4;
  }

  /**
   * Returns the mobile4
   *
   * @return \string $mobile4
   */
  public function getMobile4() {
    return $this->mobile4;
  }

  /**
   * Sets the mobileCc4
   *
   * @param \string $mobileCc4
   * @return void
   */
  public function setMobileCc4($mobileCc4) {
    $this->mobileCc4 = $mobileCc4;
  }

  /**
   * Returns the mobileCc4
   *
   * @return \string $mobileCc4
   */
  public function getMobileCc4() {
    return $this->mobileCc4;
  }

  /**
   * Sets the mobileRc4
   *
   * @param \string $mobileRc4
   * @return void
   */
  public function setMobileRc4($mobileRc4) {
    $this->mobileRc4 = $mobileRc4;
  }

  /**
   * Returns the mobileRc4
   *
   * @return \string $mobileRc4
   */
  public function getMobileRc4() {
    return $this->mobileRc4;
  }

  /**
   * Sets the mobileNr4
   *
   * @param \string $mobileNr4
   * @return void
   */
  public function setMobileNr4($mobileNr4) {
    $this->mobileNr4 = $mobileNr4;
  }

  /**
   * Returns the mobileNr4
   *
   * @return \string $mobileNr4
   */
  public function getMobileNr4() {
    return $this->mobileNr4;
  }

  /**
   * Sets the mobileEx4
   *
   * @param \string $mobileEx4
   * @return void
   */
  public function setMobileEx4($mobileEx4) {
    $this->mobileEx4 = $mobileEx4;
  }

  /**
   * Returns the mobileEx4
   *
   * @return \string $mobileEx4
   */
  public function getMobileEx4() {
    return $this->mobileEx4;
  }


  /**
   * Sets the fax4
   *
   * @param \string $fax4
   * @return void
   */
  public function setFax4($fax4) {
    $this->fax4 = $fax4;
  }

  /**
   * Returns the fax4
   *
   * @return \string $fax4
   */
  public function getFax4() {
    return $this->fax4;
  }

  /**
   * Sets the faxCc4
   *
   * @param \string $faxCc4
   * @return void
   */
  public function setFaxCc4($faxCc4) {
    $this->faxCc4 = $faxCc4;
  }

  /**
   * Returns the faxCc4
   *
   * @return \string $faxCc4
   */
  public function getFaxCc4() {
    return $this->faxCc4;
  }

  /**
   * Sets the faxRc4
   *
   * @param \string $faxRc4
   * @return void
   */
  public function setFaxRc4($faxRc4) {
    $this->faxRc4 = $faxRc4;
  }

  /**
   * Returns the faxRc4
   *
   * @return \string $faxRc4
   */
  public function getFaxRc4() {
    return $this->faxRc4;
  }

  /**
   * Sets the faxNr4
   *
   * @param \string $faxNr4
   * @return void
   */
  public function setFaxNr4($faxNr4) {
    $this->faxNr4 = $faxNr4;
  }

  /**
   * Returns the faxNr4
   *
   * @return \string $faxNr4
   */
  public function getFaxNr4() {
    return $this->faxNr4;
  }

  /**
   * Sets the faxEx4
   *
   * @param \string $faxEx4
   * @return void
   */
  public function setFaxEx4($faxEx4) {
    $this->faxEx4 = $faxEx4;
  }

  /**
   * Returns the faxEx4
   *
   * @return \string $faxEx4
   */
  public function getFaxEx4() {
    return $this->faxEx4;
  }


  /**
   * Sets the used4
   *
   * @param \integer $used4
   * @return void
   */
  public function setUsed4($used4) {
    $this->used4 = $used4;
  }

  /**
   * Returns the used4
   *
   * @return \integer $used4
   */
  public function getUsed4() {
    return $this->used4;
  }
  
	// END: -------------- PERSON - 4 ----------------
	
	// START: -------------- PERSON - 5 ----------------

  /**
   * gender5
   *
   * @var \string
   */
  protected $gender5;

  /**
   * title5
   *
   * @var \string
   */
  protected $title5;
  
  /**
   * firstname5
   *
   * @var \string
   */
  protected $firstname5;

  /**
   * lastname5
   *
   * @var \string
   */
  protected $lastname5;

  /**
   * titleAfter5
   *
   * @var \string
   */
  protected $titleAfter5;

  /**
   * function5
   *
   * @var \string
   */
  protected $function5;

  /**
   * email5
   *
   * @var \string
   */
  protected $email5;

  /**
   * phone5
   *
   * @var \string
   */
  protected $phone5;

  /**
   * phoneCc5
   *
   * @var \string
   */
  protected $phoneCc5;

  /**
   * phoneRc5
   *
   * @var \string
   */
  protected $phoneRc5;

  /**
   * phoneNr5
   *
   * @var \string
   */
  protected $phoneNr5;

  /**
   * phoneEx5
   *
   * @var \string
   */
  protected $phoneEx5;

  /**
   * mobile5
   *
   * @var \string
   */
  protected $mobile5;

  /**
   * mobileCc5
   *
   * @var \string
   */
  protected $mobileCc5;

  /**
   * mobileRc5
   *
   * @var \string
   */
  protected $mobileRc5;

  /**
   * mobileNr5
   *
   * @var \string
   */
  protected $mobileNr5;

  /**
   * mobileEx5
   *
   * @var \string
   */
  protected $mobileEx5;

  /**
   * fax5
   *
   * @var \string
   */
  protected $fax5;

  /**
   * faxCc5
   *
   * @var \string
   */
  protected $faxCc5;

  /**
   * faxRc5
   *
   * @var \string
   */
  protected $faxRc5;

  /**
   * faxNr5
   *
   * @var \string
   */
  protected $faxNr5;

  /**
   * faxEx5
   *
   * @var \string
   */
  protected $faxEx5;

  /**
   * used5
   *
   * @var \integer
   */
  protected $used5;


  /**
   * Sets the gender5
   *
   * @param \string $gender5
   * @return void
   */
  public function setGender5($gender5) {
    $this->gender5 = $gender5;
  }

  /**
   * Returns the gender5
   *
   * @return \string $gender5
   */
  public function getGender5() {
    return $this->gender5;
  }

  /**
   * Sets the title5
   *
   * @param \string $title5
   * @return void
   */
  public function setTitle5($title5) {
    $this->title5 = $title5;
  }

  /**
   * Returns the title5
   *
   * @return \string $title5
   */
  public function getTitle5() {
    return $this->title5;
  }

  /**
   * Sets the firstname5
   *
   * @param \string $firstname5
   * @return void
   */
  public function setFirstname5($firstname5) {
    $this->firstname5 = $firstname5;
  }

  /**
   * Returns the firstname5
   *
   * @return \string $firstname5
   */
  public function getFirstname5() {
    return $this->firstname5;
  }

  /**
   * Sets the lastname5
   *
   * @param \string $lastname5
   * @return void
   */
  public function setLastname5($lastname5) {
    $this->lastname5 = $lastname5;
  }

  /**
   * Returns the lastname5
   *
   * @return \string $lastname5
   */
  public function getLastname5() {
    return $this->lastname5;
  }

  /**
   * Sets the titleAfter5
   *
   * @param \string $titleAfter5
   * @return void
   */
  public function setTitleAfter5($titleAfter5) {
    $this->titleAfter5 = $titleAfter5;
  }

  /**
   * Returns the titleAfter5
   *
   * @return \string $titleAfter5
   */
  public function getTitleAfter5() {
    return $this->titleAfter5;
  }

  /**
   * Sets the function5
   *
   * @param \string $function5
   * @return void
   */
  public function setFunction5($function5) {
    $this->function5 = $function5;
  }

  /**
   * Returns the function5
   *
   * @return \string $function5
   */
  public function getFunction5() {
    return $this->function5;
  }

  /**
   * Sets the email5
   *
   * @param \string $email5
   * @return void
   */
  public function setEmail5($email5) {
    $this->email5 = $email5;
  }

  /**
   * Returns the email5
   *
   * @return \string $email5
   */
  public function getEmail5() {
    return $this->email5;
  }


  /**
   * Sets the phone
   *
   * @param \string $phone5
   * @return void
   */
  public function setPhone5($phone5) {
    $this->phone5 = $phone5;
  }

  /**
   * Returns the phone5
   *
   * @return \string $phone5
   */
  public function getPhone5() {
    return $this->phone5;
  }

  /**
   * Sets the phoneCc5
   *
   * @param \string $phoneCc5
   * @return void
   */
  public function setPhoneCc5($phoneCc5) {
    $this->phoneCc5 = $phoneCc5;
  }

  /**
   * Returns the phoneCc5
   *
   * @return \string $phoneCc5
   */
  public function getPhoneCc5() {
    return $this->phoneCc5;
  }

  /**
   * Sets the phoneRc5
   *
   * @param \string $phoneRc5
   * @return void
   */
  public function setPhoneRc5($phoneRc5) {
    $this->phoneRc5 = $phoneRc5;
  }

  /**
   * Returns the phoneRc5
   *
   * @return \string $phoneRc5
   */
  public function getPhoneRc5() {
    return $this->phoneRc5;
  }

  /**
   * Sets the phoneNr5
   *
   * @param \string $phoneNr5
   * @return void
   */
  public function setPhoneNr5($phoneNr5) {
    $this->phoneNr5 = $phoneNr5;
  }

  /**
   * Returns the phoneNr5
   *
   * @return \string $phoneNr5
   */
  public function getPhoneNr5() {
    return $this->phoneNr5;
  }

  /**
   * Sets the phoneEx5
   *
   * @param \string $phoneEx5
   * @return void
   */
  public function setPhoneEx5($phoneEx5) {
    $this->phoneEx5 = $phoneEx5;
  }

  /**
   * Returns the phoneEx5
   *
   * @return \string $phoneEx5
   */
  public function getPhoneEx5() {
    return $this->phoneEx5;
  }


  /**
   * Sets the mobile5
   *
   * @param \string $mobile5
   * @return void
   */
  public function setMobile5($mobile5) {
    $this->mobile5 = $mobile5;
  }

  /**
   * Returns the mobile5
   *
   * @return \string $mobile5
   */
  public function getMobile5() {
    return $this->mobile5;
  }

  /**
   * Sets the mobileCc5
   *
   * @param \string $mobileCc5
   * @return void
   */
  public function setMobileCc5($mobileCc5) {
    $this->mobileCc5 = $mobileCc5;
  }

  /**
   * Returns the mobileCc5
   *
   * @return \string $mobileCc5
   */
  public function getMobileCc5() {
    return $this->mobileCc5;
  }

  /**
   * Sets the mobileRc5
   *
   * @param \string $mobileRc5
   * @return void
   */
  public function setMobileRc5($mobileRc5) {
    $this->mobileRc5 = $mobileRc5;
  }

  /**
   * Returns the mobileRc5
   *
   * @return \string $mobileRc5
   */
  public function getMobileRc5() {
    return $this->mobileRc5;
  }

  /**
   * Sets the mobileNr5
   *
   * @param \string $mobileNr5
   * @return void
   */
  public function setMobileNr5($mobileNr5) {
    $this->mobileNr5 = $mobileNr5;
  }

  /**
   * Returns the mobileNr5
   *
   * @return \string $mobileNr5
   */
  public function getMobileNr5() {
    return $this->mobileNr5;
  }

  /**
   * Sets the mobileEx5
   *
   * @param \string $mobileEx5
   * @return void
   */
  public function setMobileEx5($mobileEx5) {
    $this->mobileEx5 = $mobileEx5;
  }

  /**
   * Returns the mobileEx5
   *
   * @return \string $mobileEx5
   */
  public function getMobileEx5() {
    return $this->mobileEx5;
  }


  /**
   * Sets the fax5
   *
   * @param \string $fax5
   * @return void
   */
  public function setFax5($fax5) {
    $this->fax5 = $fax5;
  }

  /**
   * Returns the fax5
   *
   * @return \string $fax5
   */
  public function getFax5() {
    return $this->fax5;
  }

  /**
   * Sets the faxCc5
   *
   * @param \string $faxCc5
   * @return void
   */
  public function setFaxCc5($faxCc5) {
    $this->faxCc5 = $faxCc5;
  }

  /**
   * Returns the faxCc5
   *
   * @return \string $faxCc5
   */
  public function getFaxCc5() {
    return $this->faxCc5;
  }

  /**
   * Sets the faxRc5
   *
   * @param \string $faxRc5
   * @return void
   */
  public function setFaxRc5($faxRc5) {
    $this->faxRc5 = $faxRc5;
  }

  /**
   * Returns the faxRc5
   *
   * @return \string $faxRc5
   */
  public function getFaxRc5() {
    return $this->faxRc5;
  }

  /**
   * Sets the faxNr5
   *
   * @param \string $faxNr5
   * @return void
   */
  public function setFaxNr5($faxNr5) {
    $this->faxNr5 = $faxNr5;
  }

  /**
   * Returns the faxNr5
   *
   * @return \string $faxNr5
   */
  public function getFaxNr5() {
    return $this->faxNr5;
  }

  /**
   * Sets the faxEx5
   *
   * @param \string $faxEx5
   * @return void
   */
  public function setFaxEx5($faxEx5) {
    $this->faxEx5 = $faxEx5;
  }

  /**
   * Returns the faxEx5
   *
   * @return \string $faxEx5
   */
  public function getFaxEx5() {
    return $this->faxEx5;
  }

  
  /**
   * Sets the used5
   *
   * @param \integer $used5
   * @return void
   */
  public function setUsed5($used5) {
    $this->used5 = $used5;
  }

  /**
   * Returns the used5
   *
   * @return \integer $used5
   */
  public function getUsed5() {
    return $this->used5;
  }
  
	// END: -------------- PERSON - 5 ----------------
	
}
?>