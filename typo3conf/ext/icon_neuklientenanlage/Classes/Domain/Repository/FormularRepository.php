<?php
namespace NEXT\IconNeuklientenanlage\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_neuklientenanlage
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FormularRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {


  /**
   * getDataArrayCompany
   *
   * @param \string $process
   * @param \NEXT\IconNeuklientenanlage\Domain\Model\FormCompany $data
   * @return \mixed
   */
  public function getDataArrayCompany (\NEXT\IconNeuklientenanlage\Domain\Model\FormCompany $data) {
	//
	$dataarray = array();

	//	COMPANY
	$f_com = array();
	$f_com[] = array('ID' => 'Company_Name', 'data' => htmlspecialchars( $data->getComName() ) );
	$f_com[] = array('ID' => 'Street', 'data' => htmlspecialchars( $data->getComStreet() ) );
	$f_com[] = array('ID' => 'Street_Nr', 'data' => htmlspecialchars( $data->getComStreetNr() ) );
	$f_com[] = array('ID' => 'Zip', 'data' => htmlspecialchars( $data->getComZip() ) );
	$f_com[] = array('ID' => 'City', 'data' => htmlspecialchars( $data->getComCity() ) );
	$f_com[] = array('ID' => 'Country', 'data' => htmlspecialchars( $data->getComCountry() ) );
	//
	$dataarray[] = array('ID' => 'COMPANY', 'type' => 'ADDRESS', 'fields' => $f_com);

	//	BILLING:
	$f_bd = array();
	$f_bd[] = array('ID' => 'Billing_To_Address', 'data' => $data->getBillAddr());
	$f_bd[] = array('ID' => 'Delivery_By_Email', 'data' => $data->getBillEmailMode());
	//
	$dataarray[] = array('ID' => 'BILLING', 'type' => 'BILLING_INFO', 'fields' => $f_bd);
	
	//	BILLING ADDRESS:
	if( $data->getBillAddr() ){
		$f_bil = array();
//		$f_bil[] = array('ID' => 'Delivery', 'data' => $data->getBillAddr());
		$f_bil[] = array('ID' => 'Company_Name', 'data' => htmlspecialchars( $data->getBillCompanyname() ) );
		$f_bil[] = array('ID' => 'Care_Of', 'data' => htmlspecialchars( $data->getBillCareOf() ) );
/*
		$f_bil[] = array('ID' => 'Salutation', 'data' => $data->getBillGender() );
		$f_bil[] = array('ID' => 'Title', 'data' => $data->getBillTitle() );
		$f_bil[] = array('ID' => 'Firstname', 'data' => $data->getBillFirstname() );
		$f_bil[] = array('ID' => 'Lastname', 'data' => $data->getBillLastname() );
		$f_bil[] = array('ID' => 'Title_After', 'data' => $data->getBillTitleAfter() );
		$f_bil[] = array('ID' => 'Position_Function', 'data' => $data->getBillFunction() );
*/
		$f_bil[] = array('ID' => 'Street', 'data' => htmlspecialchars( $data->getBillStreet() ) );
		$f_bil[] = array('ID' => 'Street_Nr', 'data' => htmlspecialchars( $data->getBillStreetNr() ) );
		$f_bil[] = array('ID' => 'Zip', 'data' => htmlspecialchars( $data->getBillZip() ) );
		$f_bil[] = array('ID' => 'City', 'data' => htmlspecialchars( $data->getBillCity() ) );
		$f_bil[] = array('ID' => 'Country', 'data' => htmlspecialchars( $data->getBillCountry() ) );
		//
		$dataarray[] = array('ID' => 'BILLING_ADDRESS', 'type' => 'ADDRESS', 'fields' => $f_bil);
	}
	
	//	BILLING DELIVERY EMAIL:
	if( $data->getBillEmailMode() == 1 ){
		$f_bde = array();
//		$f_bde[] = array('ID' => 'Delivery', 'data' => $data->getBillEmailMode());
		$f_bde[] = array('ID' => 'Salutation', 'data' => htmlspecialchars( $data->getBillEmailGender() ) );
		$f_bde[] = array('ID' => 'Title', 'data' => htmlspecialchars( $data->getBillEmailTitle() ) );
		$f_bde[] = array('ID' => 'Firstname', 'data' => htmlspecialchars( $data->getBillEmailFirstname() ) );
		$f_bde[] = array('ID' => 'Lastname', 'data' => htmlspecialchars( $data->getBillEmailLastname() ) );
		$f_bde[] = array('ID' => 'Title_After', 'data' => htmlspecialchars( $data->getBillEmailTitleAfter() ) );
		$f_bde[] = array('ID' => 'Position_Function', 'data' => htmlspecialchars( $data->getBillEmailFunction() ) );
		$f_bde[] = array('ID' => 'Email', 'data' => htmlspecialchars( $data->getBillEmailEmail() ) );
//		$f_bde[] = array('ID' => 'Phone', 'data' => htmlspecialchars( $data->getBillEmailPhone() ) );
		$f_bde[] = array('ID' => 'Phone_CC', 'data' => htmlspecialchars( $data->getBillEmailPhoneCc() ) );
		$f_bde[] = array('ID' => 'Phone_RC', 'data' => htmlspecialchars( $data->getBillEmailPhoneRc() ) );
		$f_bde[] = array('ID' => 'Phone_NR', 'data' => htmlspecialchars( $data->getBillEmailPhoneNr() ) );
		$f_bde[] = array('ID' => 'Phone_EX', 'data' => htmlspecialchars( $data->getBillEmailPhoneEx() ) );
//		$f_bde[] = array('ID' => 'Mobile', 'data' => htmlspecialchars( $data->getBillEmailMobile() ) );
		$f_bde[] = array('ID' => 'Mobile_CC', 'data' => htmlspecialchars( $data->getBillEmailMobileCc() ) );
		$f_bde[] = array('ID' => 'Mobile_RC', 'data' => htmlspecialchars( $data->getBillEmailMobileRc() ) );
		$f_bde[] = array('ID' => 'Mobile_NR', 'data' => htmlspecialchars( $data->getBillEmailMobileNr() ) );
		$f_bde[] = array('ID' => 'Mobile_EX', 'data' => htmlspecialchars( $data->getBillEmailMobileEx() ) );
//		$f_bde[] = array('ID' => 'Fax', 'data' => htmlspecialchars( $data->getBillEmailFax() ) );
		$f_bde[] = array('ID' => 'Fax_CC', 'data' => htmlspecialchars( $data->getBillEmailFaxCc() ) );
		$f_bde[] = array('ID' => 'Fax_RC', 'data' => htmlspecialchars( $data->getBillEmailFaxRc() ) );
		$f_bde[] = array('ID' => 'Fax_NR', 'data' => htmlspecialchars( $data->getBillEmailFaxNr() ) );
		$f_bde[] = array('ID' => 'Fax_EX', 'data' => htmlspecialchars( $data->getBillEmailFaxEx() ) );
		//
		$dataarray[] = array('ID' => 'BILLING_DEVILVERY_EMAIL', 'type' => 'ADDRESS', 'fields' => $f_bde);
	}
	
	//	TAX 1:
	$f_ta1 = array();
	$f_ta1[] = array('ID' => 'INST_NR', 'data' => $data->getTaxOfficeNr1());
	$f_ta1[] = array('ID' => 'Tax_Office', 'data' => htmlspecialchars( $data->getTaxOffice1() ) );
	$f_ta1[] = array('ID' => 'Tax_ID', 'data' => htmlspecialchars( $data->getTaxId1() ) );
	$f_ta1[] = array('ID' => 'UID_No', 'data' => htmlspecialchars( $data->getTaxUid1() ) );
	//
	$dataarray[] = array('ID' => 'TAX_1', 'type' => 'TAXINFO', 'fields' => $f_ta1);
	//	TAX 2:
	$isTax2 = trim( $data->getTaxOffice2() . $data->getTaxId2() . $data->getTaxUid2() );
	if( $isTax2 != '' ){
		$f_ta2 = array();
		$f_ta2[] = array('ID' => 'INST_NR', 'data' => $data->getTaxOfficeNr2());
		$f_ta2[] = array('ID' => 'Tax_Office', 'data' => htmlspecialchars( $data->getTaxOffice2() ) );
		$f_ta2[] = array('ID' => 'Tax_ID', 'data' => htmlspecialchars( $data->getTaxId2() ) );
		$f_ta2[] = array('ID' => 'UID_No', 'data' => htmlspecialchars( $data->getTaxUid2() ) );
		//
		$dataarray[] = array('ID' => 'TAX_2', 'type' => 'TAXINFO', 'fields' => $f_ta2);
  }
	//	TAX 3:
	$isTax3 = trim( $data->getTaxOffice3() . $data->getTaxId3() . $data->getTaxUid3() );
	if( $isTax3 != '' ){
		$f_ta3 = array();
		$f_ta3[] = array('ID' => 'INST_NR', 'data' => $data->getTaxOfficeNr3());
		$f_ta3[] = array('ID' => 'Tax_Office', 'data' => htmlspecialchars( $data->getTaxOffice3() ) );
		$f_ta3[] = array('ID' => 'Tax_ID', 'data' => htmlspecialchars( $data->getTaxId3() ) );
		$f_ta3[] = array('ID' => 'UID_No', 'data' => htmlspecialchars( $data->getTaxUid3() ) );
		//
		$dataarray[] = array('ID' => 'TAX_3', 'type' => 'TAXINFO', 'fields' => $f_ta3);
  }
	//	TAX 4:
	$isTax4 = trim( $data->getTaxOffice4() . $data->getTaxId4() . $data->getTaxUid4() );
	if( $isTax4 != '' ){
		$f_ta4 = array();
		$f_ta4[] = array('ID' => 'INST_NR', 'data' => $data->getTaxOfficeNr4());
		$f_ta4[] = array('ID' => 'Tax_Office', 'data' => htmlspecialchars( $data->getTaxOffice4() ) );
		$f_ta4[] = array('ID' => 'Tax_ID', 'data' => htmlspecialchars( $data->getTaxId4() ) );
		$f_ta4[] = array('ID' => 'UID_No', 'data' => htmlspecialchars( $data->getTaxUid4() ) );
		//
		$dataarray[] = array('ID' => 'TAX_4', 'type' => 'TAXINFO', 'fields' => $f_ta4);
	}
	//	TAX 5:
	$isTax5 = trim( $data->getTaxOffice5() . $data->getTaxId5() . $data->getTaxUid5() );
	if( $isTax5 != '' ){
		$f_ta5 = array();
		$f_ta5[] = array('ID' => 'INST_NR', 'data' => $data->getTaxOfficeNr5());
		$f_ta5[] = array('ID' => 'Tax_Office', 'data' => htmlspecialchars( $data->getTaxOffice5() ) );
		$f_ta5[] = array('ID' => 'Tax_ID', 'data' => htmlspecialchars( $data->getTaxId5() ) );
		$f_ta5[] = array('ID' => 'UID_No', 'data' => htmlspecialchars( $data->getTaxUid5() ) );
		//
		$dataarray[] = array('ID' => 'TAX_5', 'type' => 'TAXINFO', 'fields' => $f_ta5);
	}

	
	//	PERSON - CFO:
	$f_p_0 = array();
	$f_p_0[] = array('ID' => 'Salutation', 'data' => htmlspecialchars( $data->getGender0() ) );
	$f_p_0[] = array('ID' => 'Title', 'data' => htmlspecialchars( $data->getTitle0() ) );
	$f_p_0[] = array('ID' => 'Firstname', 'data' => htmlspecialchars( $data->getFirstname0() ) );
	$f_p_0[] = array('ID' => 'Lastname', 'data' => htmlspecialchars( $data->getLastname0() ) );
	$f_p_0[] = array('ID' => 'Title_After', 'data' => htmlspecialchars( $data->getTitleAfter0() ) );
	$f_p_0[] = array('ID' => 'Position_Function', 'data' => htmlspecialchars( $data->getFunction0() ) );
	$f_p_0[] = array('ID' => 'Email', 'data' => htmlspecialchars( $data->getEmail0() ) );
//	$f_p_0[] = array('ID' => 'Phone', 'data' => htmlspecialchars( $data->getPhone0() ) );
	$f_p_0[] = array('ID' => 'Phone_CC', 'data' => htmlspecialchars( $data->getPhoneCc0() ) );
	$f_p_0[] = array('ID' => 'Phone_RC', 'data' => htmlspecialchars( $data->getPhoneRc0() ) );
	$f_p_0[] = array('ID' => 'Phone_NR', 'data' => htmlspecialchars( $data->getPhoneNr0() ) );
	$f_p_0[] = array('ID' => 'Phone_EX', 'data' => htmlspecialchars( $data->getPhoneEx0() ) );
//	$f_p_0[] = array('ID' => 'Mobile', 'data' => htmlspecialchars( $data->getMobile0() ) );
	$f_p_0[] = array('ID' => 'Mobile_CC', 'data' => htmlspecialchars( $data->getMobileCc0() ) );
	$f_p_0[] = array('ID' => 'Mobile_RC', 'data' => htmlspecialchars( $data->getMobileRc0() ) );
	$f_p_0[] = array('ID' => 'Mobile_NR', 'data' => htmlspecialchars( $data->getMobileNr0() ) );
	$f_p_0[] = array('ID' => 'Mobile_EX', 'data' => htmlspecialchars( $data->getMobileEx0() ) );
//	$f_p_0[] = array('ID' => 'Fax', 'data' => htmlspecialchars( $data->getFax0() ) );
	$f_p_0[] = array('ID' => 'Fax_CC', 'data' => htmlspecialchars( $data->getFaxCc0() ) );
	$f_p_0[] = array('ID' => 'Fax_RC', 'data' => htmlspecialchars( $data->getFaxRc0() ) );
	$f_p_0[] = array('ID' => 'Fax_NR', 'data' => htmlspecialchars( $data->getFaxNr0() ) );
	$f_p_0[] = array('ID' => 'Fax_EX', 'data' => htmlspecialchars( $data->getFaxEx0() ) );
	//
	$dataarray[] = array('ID' => 'PERSON_CFO', 'type' => 'ADDRESS', 'fields' => $f_p_0);

	//	PERSON - 1:
	if( $data->getUsed1() ){
		$f_p_1 = array();
		$f_p_1[] = array('ID' => 'Salutation', 'data' => htmlspecialchars( $data->getGender1() ) );
		$f_p_1[] = array('ID' => 'Title', 'data' => htmlspecialchars( $data->getTitle1() ) );
		$f_p_1[] = array('ID' => 'Firstname', 'data' => htmlspecialchars( $data->getFirstname1() ) );
		$f_p_1[] = array('ID' => 'Lastname', 'data' => htmlspecialchars( $data->getLastname1() ) );
		$f_p_1[] = array('ID' => 'Title_After', 'data' => htmlspecialchars( $data->getTitleAfter1() ) );
		$f_p_1[] = array('ID' => 'Position_Function', 'data' => htmlspecialchars( $data->getFunction1() ) );
		$f_p_1[] = array('ID' => 'Email', 'data' => htmlspecialchars( $data->getEmail1() ) );
//		$f_p_1[] = array('ID' => 'Phone', 'data' => htmlspecialchars( $data->getPhone1() ) );
		$f_p_1[] = array('ID' => 'Phone_CC', 'data' => htmlspecialchars( $data->getPhoneCc1() ) );
		$f_p_1[] = array('ID' => 'Phone_RC', 'data' => htmlspecialchars( $data->getPhoneRc1() ) );
		$f_p_1[] = array('ID' => 'Phone_NR', 'data' => htmlspecialchars( $data->getPhoneNr1() ) );
		$f_p_1[] = array('ID' => 'Phone_EX', 'data' => htmlspecialchars( $data->getPhoneEx1() ) );
//		$f_p_1[] = array('ID' => 'Mobile', 'data' => htmlspecialchars( $data->getMobile1() ) );
		$f_p_1[] = array('ID' => 'Mobile_CC', 'data' => htmlspecialchars( $data->getMobileCc1() ) );
		$f_p_1[] = array('ID' => 'Mobile_RC', 'data' => htmlspecialchars( $data->getMobileRc1() ) );
		$f_p_1[] = array('ID' => 'Mobile_NR', 'data' => htmlspecialchars( $data->getMobileNr1() ) );
		$f_p_1[] = array('ID' => 'Mobile_EX', 'data' => htmlspecialchars( $data->getMobileEx1() ) );
//		$f_p_1[] = array('ID' => 'Fax', 'data' => htmlspecialchars( $data->getFax1() ) );
		$f_p_1[] = array('ID' => 'Fax_CC', 'data' => htmlspecialchars( $data->getFaxCc1() ) );
		$f_p_1[] = array('ID' => 'Fax_RC', 'data' => htmlspecialchars( $data->getFaxRc1() ) );
		$f_p_1[] = array('ID' => 'Fax_NR', 'data' => htmlspecialchars( $data->getFaxNr1() ) );
		$f_p_1[] = array('ID' => 'Fax_EX', 'data' => htmlspecialchars( $data->getFaxEx1() ) );
		//
		$dataarray[] = array('ID' => 'PERSON_1', 'type' => 'ADDRESS', 'fields' => $f_p_1);
	}
	//	PERSON - 2:
	if( $data->getUsed2() ){
		$f_p_2 = array();
		$f_p_2[] = array('ID' => 'Salutation', 'data' => htmlspecialchars( $data->getGender2() ) );
		$f_p_2[] = array('ID' => 'Title', 'data' => htmlspecialchars( $data->getTitle2() ) );
		$f_p_2[] = array('ID' => 'Firstname', 'data' => htmlspecialchars( $data->getFirstname2() ) );
		$f_p_2[] = array('ID' => 'Lastname', 'data' => htmlspecialchars( $data->getLastname2() ) );
		$f_p_2[] = array('ID' => 'Title_After', 'data' => htmlspecialchars( $data->getTitleAfter2() ) );
		$f_p_2[] = array('ID' => 'Position_Function', 'data' => htmlspecialchars( $data->getFunction2() ) );
		$f_p_2[] = array('ID' => 'Email', 'data' => htmlspecialchars( $data->getEmail2() ) );
//		$f_p_2[] = array('ID' => 'Phone', 'data' => htmlspecialchars( $data->getPhone2() ) );
		$f_p_2[] = array('ID' => 'Phone_CC', 'data' => htmlspecialchars( $data->getPhoneCc2() ) );
		$f_p_2[] = array('ID' => 'Phone_RC', 'data' => htmlspecialchars( $data->getPhoneRc2() ) );
		$f_p_2[] = array('ID' => 'Phone_NR', 'data' => htmlspecialchars( $data->getPhoneNr2() ) );
		$f_p_2[] = array('ID' => 'Phone_EX', 'data' => htmlspecialchars( $data->getPhoneEx2() ) );
//		$f_p_2[] = array('ID' => 'Mobile', 'data' => htmlspecialchars( $data->getMobile2() ) );
		$f_p_2[] = array('ID' => 'Mobile_CC', 'data' => htmlspecialchars( $data->getMobileCc2() ) );
		$f_p_2[] = array('ID' => 'Mobile_RC', 'data' => htmlspecialchars( $data->getMobileRc2() ) );
		$f_p_2[] = array('ID' => 'Mobile_NR', 'data' => htmlspecialchars( $data->getMobileNr2() ) );
		$f_p_2[] = array('ID' => 'Mobile_EX', 'data' => htmlspecialchars( $data->getMobileEx2() ) );
//		$f_p_2[] = array('ID' => 'Fax', 'data' => htmlspecialchars( $data->getFax2() ) );
		$f_p_2[] = array('ID' => 'Fax_CC', 'data' => htmlspecialchars( $data->getFaxCc2() ) );
		$f_p_2[] = array('ID' => 'Fax_RC', 'data' => htmlspecialchars( $data->getFaxRc2() ) );
		$f_p_2[] = array('ID' => 'Fax_NR', 'data' => htmlspecialchars( $data->getFaxNr2() ) );
		$f_p_2[] = array('ID' => 'Fax_EX', 'data' => htmlspecialchars( $data->getFaxEx2() ) );
		//
		$dataarray[] = array('ID' => 'PERSON_2', 'type' => 'ADDRESS', 'fields' => $f_p_2);
	}
	//	PERSON - 3:
	if( $data->getUsed3() ){
		$f_p_3 = array();
		$f_p_3[] = array('ID' => 'Salutation', 'data' => htmlspecialchars( $data->getGender3() ) );
		$f_p_3[] = array('ID' => 'Title', 'data' => htmlspecialchars( $data->getTitle3() ) );
		$f_p_3[] = array('ID' => 'Firstname', 'data' => htmlspecialchars( $data->getFirstname3() ) );
		$f_p_3[] = array('ID' => 'Lastname', 'data' => htmlspecialchars( $data->getLastname3() ) );
		$f_p_3[] = array('ID' => 'Title_After', 'data' => htmlspecialchars( $data->getTitleAfter3() ) );
		$f_p_3[] = array('ID' => 'Position_Function', 'data' => htmlspecialchars( $data->getFunction3() ) );
		$f_p_3[] = array('ID' => 'Email', 'data' => htmlspecialchars( $data->getEmail3() ) );
//		$f_p_3[] = array('ID' => 'Phone', 'data' => htmlspecialchars( $data->getPhone3() ) );
		$f_p_3[] = array('ID' => 'Phone_CC', 'data' => htmlspecialchars( $data->getPhoneCc3() ) );
		$f_p_3[] = array('ID' => 'Phone_RC', 'data' => htmlspecialchars( $data->getPhoneRc3() ) );
		$f_p_3[] = array('ID' => 'Phone_NR', 'data' => htmlspecialchars( $data->getPhoneNr3() ) );
		$f_p_3[] = array('ID' => 'Phone_EX', 'data' => htmlspecialchars( $data->getPhoneEx3() ) );
//		$f_p_3[] = array('ID' => 'Mobile', 'data' => htmlspecialchars( $data->getMobile3() ) );
		$f_p_3[] = array('ID' => 'Mobile_CC', 'data' => htmlspecialchars( $data->getMobileCc3() ) );
		$f_p_3[] = array('ID' => 'Mobile_RC', 'data' => htmlspecialchars( $data->getMobileRc3() ) );
		$f_p_3[] = array('ID' => 'Mobile_NR', 'data' => htmlspecialchars( $data->getMobileNr3() ) );
		$f_p_3[] = array('ID' => 'Mobile_EX', 'data' => htmlspecialchars( $data->getMobileEx3() ) );
//		$f_p_3[] = array('ID' => 'Fax', 'data' => htmlspecialchars( $data->getFax3() ) );
		$f_p_3[] = array('ID' => 'Fax_CC', 'data' => htmlspecialchars( $data->getFaxCc3() ) );
		$f_p_3[] = array('ID' => 'Fax_RC', 'data' => htmlspecialchars( $data->getFaxRc3() ) );
		$f_p_3[] = array('ID' => 'Fax_NR', 'data' => htmlspecialchars( $data->getFaxNr3() ) );
		$f_p_3[] = array('ID' => 'Fax_EX', 'data' => htmlspecialchars( $data->getFaxEx3() ) );
		//
		$dataarray[] = array('ID' => 'PERSON_3', 'type' => 'ADDRESS', 'fields' => $f_p_3);
	}
	//	PERSON - 4:
	if( $data->getUsed4() ){
		$f_p_4 = array();
		$f_p_4[] = array('ID' => 'Salutation', 'data' => htmlspecialchars( $data->getGender4() ) );
		$f_p_4[] = array('ID' => 'Title', 'data' => htmlspecialchars( $data->getTitle4() ) );
		$f_p_4[] = array('ID' => 'Firstname', 'data' => htmlspecialchars( $data->getFirstname4() ) );
		$f_p_4[] = array('ID' => 'Lastname', 'data' => htmlspecialchars( $data->getLastname4() ) );
		$f_p_4[] = array('ID' => 'Title_After', 'data' => htmlspecialchars( $data->getTitleAfter4() ) );
		$f_p_4[] = array('ID' => 'Position_Function', 'data' => htmlspecialchars( $data->getFunction4() ) );
		$f_p_4[] = array('ID' => 'Email', 'data' => htmlspecialchars( $data->getEmail4() ) );
//		$f_p_4[] = array('ID' => 'Phone', 'data' => htmlspecialchars( $data->getPhone4() ) );
		$f_p_4[] = array('ID' => 'Phone_CC', 'data' => htmlspecialchars( $data->getPhoneCc4() ) );
		$f_p_4[] = array('ID' => 'Phone_RC', 'data' => htmlspecialchars( $data->getPhoneRc4() ) );
		$f_p_4[] = array('ID' => 'Phone_NR', 'data' => htmlspecialchars( $data->getPhoneNr4() ) );
		$f_p_4[] = array('ID' => 'Phone_EX', 'data' => htmlspecialchars( $data->getPhoneEx4() ) );
//		$f_p_4[] = array('ID' => 'Mobile', 'data' => htmlspecialchars( $data->getMobile4() ) );
		$f_p_4[] = array('ID' => 'Mobile_CC', 'data' => htmlspecialchars( $data->getMobileCc4() ) );
		$f_p_4[] = array('ID' => 'Mobile_RC', 'data' => htmlspecialchars( $data->getMobileRc4() ) );
		$f_p_4[] = array('ID' => 'Mobile_NR', 'data' => htmlspecialchars( $data->getMobileNr4() ) );
		$f_p_4[] = array('ID' => 'Mobile_EX', 'data' => htmlspecialchars( $data->getMobileEx4() ) );
//		$f_p_4[] = array('ID' => 'Fax', 'data' => htmlspecialchars( $data->getFax4() ) );
		$f_p_4[] = array('ID' => 'Fax_CC', 'data' => htmlspecialchars( $data->getFaxCc4() ) );
		$f_p_4[] = array('ID' => 'Fax_RC', 'data' => htmlspecialchars( $data->getFaxRc4() ) );
		$f_p_4[] = array('ID' => 'Fax_NR', 'data' => htmlspecialchars( $data->getFaxNr4() ) );
		$f_p_4[] = array('ID' => 'Fax_EX', 'data' => htmlspecialchars( $data->getFaxEx4() ) );
		//
		$dataarray[] = array('ID' => 'PERSON_4', 'type' => 'ADDRESS', 'fields' => $f_p_4);
	}
	//	PERSON - 5:
	if( $data->getUsed5() ){
		$f_p_5 = array();
		$f_p_5[] = array('ID' => 'Salutation', 'data' => htmlspecialchars( $data->getGender5() ) );
		$f_p_5[] = array('ID' => 'Title', 'data' => htmlspecialchars( $data->getTitle5() ) );
		$f_p_5[] = array('ID' => 'Firstname', 'data' => htmlspecialchars( $data->getFirstname5() ) );
		$f_p_5[] = array('ID' => 'Lastname', 'data' => htmlspecialchars( $data->getLastname5() ) );
		$f_p_5[] = array('ID' => 'Title_After', 'data' => htmlspecialchars( $data->getTitleAfter5() ) );
		$f_p_5[] = array('ID' => 'Position_Function', 'data' => htmlspecialchars( $data->getFunction5() ) );
		$f_p_5[] = array('ID' => 'Email', 'data' => htmlspecialchars( $data->getEmail5() ) );
//		$f_p_5[] = array('ID' => 'Phone', 'data' => htmlspecialchars( $data->getPhone5() ) );
		$f_p_5[] = array('ID' => 'Phone_CC', 'data' => htmlspecialchars( $data->getPhoneCc5() ) );
		$f_p_5[] = array('ID' => 'Phone_RC', 'data' => htmlspecialchars( $data->getPhoneRc5() ) );
		$f_p_5[] = array('ID' => 'Phone_NR', 'data' => htmlspecialchars( $data->getPhoneNr5() ) );
		$f_p_5[] = array('ID' => 'Phone_EX', 'data' => htmlspecialchars( $data->getPhoneEx5() ) );
//		$f_p_5[] = array('ID' => 'Mobile', 'data' => htmlspecialchars( $data->getMobile5() ) );
		$f_p_5[] = array('ID' => 'Mobile_CC', 'data' => htmlspecialchars( $data->getMobileCc5() ) );
		$f_p_5[] = array('ID' => 'Mobile_RC', 'data' => htmlspecialchars( $data->getMobileRc5() ) );
		$f_p_5[] = array('ID' => 'Mobile_NR', 'data' => htmlspecialchars( $data->getMobileNr5() ) );
		$f_p_5[] = array('ID' => 'Mobile_EX', 'data' => htmlspecialchars( $data->getMobileEx5() ) );
//		$f_p_5[] = array('ID' => 'Fax', 'data' => htmlspecialchars( $data->getFax5() ) );
		$f_p_5[] = array('ID' => 'Fax_CC', 'data' => htmlspecialchars( $data->getFaxCc5() ) );
		$f_p_5[] = array('ID' => 'Fax_RC', 'data' => htmlspecialchars( $data->getFaxRc5() ) );
		$f_p_5[] = array('ID' => 'Fax_NR', 'data' => htmlspecialchars( $data->getFaxNr5() ) );
		$f_p_5[] = array('ID' => 'Fax_EX', 'data' => htmlspecialchars( $data->getFaxEx5() ) );
		//
		$dataarray[] = array('ID' => 'PERSON_5', 'type' => 'ADDRESS', 'fields' => $f_p_5);
	}
	
	//	ADDITIONALS:
	$f_add = array();
	$f_add[] = array('ID' => 'Additional', 'data' => $data->getAdditional() );
	//
	$dataarray[] = array('ID' => 'ADDITIONALS', 'type' => 'INFO', 'fields' => $f_add);
	
	//	ADDITIONALY:
	$f_att = array();
	$f_att[] = array('ID' => 'Homepage', 'data' => $data->getAttentivelyWebsite());	
	$f_att[] = array('ID' => 'Tax_Academy_Seminare', 'data' => $data->getAttentivelyTaxAcademy());	
	$f_att[] = array('ID' => 'Inserat', 'data' => $data->getAttentivelyInserat());	
	$f_att[] = array('ID' => 'Empfehlung', 'data' => $data->getAttentivelyEmpfehlung());	
	$f_att[] = array('ID' => 'Empfehlung_Name', 'data' => htmlspecialchars( $data->getAttentivelyEmpfehlungInfo() ) );	
	$f_att[] = array('ID' => 'Suchmaschinen', 'data' => $data->getAttentivelySearchengine());	
	$f_att[] = array('ID' => 'Sonstiges', 'data' => $data->getAttentivelySonstiges());	
	$f_att[] = array('ID' => 'Sonstiges_Text', 'data' => htmlspecialchars( $data->getAttentivelySonstigesInfo() ) );
	//
	$dataarray[] = array('ID' => 'ATTENTIVELY', 'type' => 'OTHERS', 'fields' => $f_att);
	//
	return $dataarray;
  }


  /**
   * getDataArrayPrivate
   *
   * @param \string $process
   * @param \NEXT\IconNeuklientenanlage\Domain\Model\FormPrivate $data
   * @return \mixed
   */
  public function getDataArrayPrivate (\NEXT\IconNeuklientenanlage\Domain\Model\FormPrivate $data) {
	//
	$dataarray = array();

	//	PRIVATE PERSON
	$f_pp = array();
	$f_pp[] = array('ID' => 'Salutation', 'data' => htmlspecialchars( $data->getGender() ) );
	$f_pp[] = array('ID' => 'Title', 'data' => htmlspecialchars( $data->getTitle() ) );
	$f_pp[] = array('ID' => 'Firstname', 'data' =>htmlspecialchars( $data->getFirstname() ) );
	$f_pp[] = array('ID' => 'Lastname', 'data' =>  htmlspecialchars( $data->getLastname() ) );
	$f_pp[] = array('ID' => 'Title_After', 'data' => htmlspecialchars( $data->getTitleAfter() ) );
	$f_pp[] = array('ID' => 'Position_Function', 'data' => htmlspecialchars( $data->getFunction() ) );
	$f_pp[] = array('ID' => 'Email', 'data' => htmlspecialchars( $data->getEmail() ) );
//	$f_pp[] = array('ID' => 'Phone', 'data' => htmlspecialchars( $data->getPhone() ) );
	$f_pp[] = array('ID' => 'Phone_CC', 'data' => htmlspecialchars( $data->getPhoneCc() ) );
	$f_pp[] = array('ID' => 'Phone_RC', 'data' => htmlspecialchars( $data->getPhoneRc() ) );
	$f_pp[] = array('ID' => 'Phone_NR', 'data' => htmlspecialchars( $data->getPhoneNr() ) );
	$f_pp[] = array('ID' => 'Phone_EX', 'data' => htmlspecialchars( $data->getPhoneEx() ) );
//	$f_pp[] = array('ID' => 'Mobile', 'data' => htmlspecialchars( $data->getMobile() ) );
	$f_pp[] = array('ID' => 'Mobile_CC', 'data' => htmlspecialchars( $data->getMobileCc() ) );
	$f_pp[] = array('ID' => 'Mobile_RC', 'data' => htmlspecialchars( $data->getMobileRc() ) );
	$f_pp[] = array('ID' => 'Mobile_NR', 'data' => htmlspecialchars( $data->getMobileNr() ) );
	$f_pp[] = array('ID' => 'Mobile_EX', 'data' => htmlspecialchars( $data->getMobileEx() ) );
//	$f_pp[] = array('ID' => 'Fax', 'data' => htmlspecialchars( $data->getFax() ) );
	$f_pp[] = array('ID' => 'Fax_CC', 'data' => htmlspecialchars( $data->getFaxCc() ) );
	$f_pp[] = array('ID' => 'Fax_RC', 'data' => htmlspecialchars( $data->getFaxRc() ) );
	$f_pp[] = array('ID' => 'Fax_NR', 'data' => htmlspecialchars( $data->getFaxNr() ) );
	$f_pp[] = array('ID' => 'Fax_EX', 'data' => htmlspecialchars( $data->getFaxEx() ) );
	$f_pp[] = array('ID' => 'Street', 'data' => htmlspecialchars( $data->getStreet() ) );
	$f_pp[] = array('ID' => 'Street_Nr', 'data' => htmlspecialchars( $data->getStreetNr() ) );
	$f_pp[] = array('ID' => 'Zip', 'data' => htmlspecialchars( $data->getZip() ) );
	$f_pp[] = array('ID' => 'City', 'data' => htmlspecialchars( $data->getCity() ) );
	$f_pp[] = array('ID' => 'Country', 'data' => htmlspecialchars( $data->getCountry() ) );
	//
	$dataarray[] = array('ID' => 'PRIVATPERSON', 'type' => 'ADDRESS', 'fields' => $f_pp);

	//	BILLING:
	$f_bil = array();
	$f_bil[] = array('ID' => 'Delivery_By_Email', 'data' => $data->getBillMode());
	//
	$dataarray[] = array('ID' => 'BILLING', 'type' => 'BILLING_INFO', 'fields' => $f_bil);
	
	//	TAX:
	$isTax1 = trim( $data->getTaxOffice1() . $data->getTaxId1() . $data->getTaxUid1() );
	if( $isTax1 != '' ){
		$f_tax = array();
		$f_tax[] = array('ID' => 'INST_NR', 'data' => $data->getTaxOfficeNr1());
		$f_tax[] = array('ID' => 'Tax_Office', 'data' => htmlspecialchars( $data->getTaxOffice1() ) );
		$f_tax[] = array('ID' => 'Tax_ID', 'data' => htmlspecialchars( $data->getTaxId1() ) );
		$f_tax[] = array('ID' => 'UID_No', 'data' => htmlspecialchars( $data->getTaxUid1() ) );
		//
		$dataarray[] = array('ID' => 'TAX_1', 'type' => 'TAXINFO', 'fields' => $f_tax);
	}
	
	//	ADDITIONALS:
	$f_add = array();
	$f_add[] = array('ID' => 'Additional', 'data' => htmlspecialchars( $data->getAdditional() ) );
	//
	$dataarray[] = array('ID' => 'ADDITIONALS', 'type' => 'INFO', 'fields' => $f_add);
	
	//	ATTENTIVLEY:
	$f_att = array();
	$f_att[] = array('ID' => 'Homepage', 'data' => $data->getAttentivelyWebsite());	
	$f_att[] = array('ID' => 'Tax_Academy_Seminare', 'data' => $data->getAttentivelyTaxAcademy());	
	$f_att[] = array('ID' => 'Inserat', 'data' => $data->getAttentivelyInserat());	
	$f_att[] = array('ID' => 'Empfehlung', 'data' => $data->getAttentivelyEmpfehlung());	
	$f_att[] = array('ID' => 'Empfehlung_Name', 'data' => htmlspecialchars( $data->getAttentivelyEmpfehlungInfo() ) );	
	$f_att[] = array('ID' => 'Suchmaschinen', 'data' => $data->getAttentivelySearchengine());	
	$f_att[] = array('ID' => 'Sonstiges', 'data' => $data->getAttentivelySonstiges());	
	$f_att[] = array('ID' => 'Sonstiges_Text', 'data' => htmlspecialchars( $data->getAttentivelySonstigesInfo() ) );
	//
	$dataarray[] = array('ID' => 'ATTENTIVELY', 'type' => 'OTHERS', 'fields' => $f_att);
	//
	return $dataarray;
  }

  /**
   * checkIsUsed
   *
   * @param \string $process
   * @param \string $interested
   * @return \boolean
   */
  public function checkIsUsed ($process, $interested) {
	//
	return $interested == 'USED';
  }
  
  
  /**
   * sends Email to user 
   *
   * @param array @mailCFG
   * @param string $mailBody
   * @return
   */
  public function deliverMail ($mailCFG, $mailBody) {
	$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
	$mail->setTo(array( $mailCFG['toEmail'] => $mailCFG['toName'] ))
		->setFrom(array( $mailCFG['fromEmail'] => $mailCFG['fromName'] ))
		->setSubject( $mailCFG['subject'] )
		->setCharset( $mailCFG['charset'] );
	//
	//$mail->setCc($m_ccArray);
	//$mail->setBcc($m_bcc);
	//$message->setReturnPath($cObj->cObjGetSingle($conf[$type . '.']['overwrite.']['returnPath'], $conf[$type . '.']['overwrite.']['returnPath.']));
	//$mail->setReplyTo($replyArray);
	$mail->setBody($mailBody, $mailCFG['contentType']);
	$mail->send();
	return $mail->isSent();
  }
  
}
?>