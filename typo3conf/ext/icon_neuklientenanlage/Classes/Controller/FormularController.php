<?php
namespace NEXT\IconNeuklientenanlage\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_neuklientenanlage
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FormularController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

  /**
   * webserviceRepository
   *
   * @var \NEXT\IconIgelconnector\Domain\Repository\WebserviceRepository
   * @inject
   */
  protected $webserviceRepository;
    
  /**
   * formularRepository
   *
   * @var \NEXT\IconNeuklientenanlage\Domain\Repository\FormularRepository
   * @inject
   */
  protected $formularRepository;

  /**
   * loggingRepository
   *
   * @var \NEXT\IconNeuklientenanlage\Domain\Repository\LoggingRepository
   * @inject
   */
  protected $loggingRepository;

    
	/*
		AUFRUF-SZENARIEN:
	
	NO FORM | NO PROCESS | NO INTERESTED
	http://www.icon.at/de/neuklientenanlage/

	FORM: UNKNOWN | NO PROCESS | NO INTERESTED
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=laszy

	FORM: UNKNOWN | PROCESS: EMPTY | NO INTERESTED
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=laszy&tx_iconneuklientenanlage_pi1[aufruf][process]=

	FORM: UNKNOWN | PROCESS: SOME | NO INTERESTED
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=laszy&tx_iconneuklientenanlage_pi1[aufruf][process]=12345

	FORM: UNKNOWN | PROCESS: SOME | INTERESTED: EMPTY
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=laszy&tx_iconneuklientenanlage_pi1[aufruf][process]=lazy&tx_iconneuklientenanlage_pi1[aufruf][interested]=

	FORM: UNKNOWN | PROCESS: SOME | INTERESTED: SOME
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=laszy&tx_iconneuklientenanlage_pi1[aufruf][process]=lazy&tx_iconneuklientenanlage_pi1[aufruf][interested]=lazy


	COMPANY | CHECK: USED
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=company&tx_iconneuklientenanlage_pi1[aufruf][process]=12345&tx_iconneuklientenanlage_pi1[aufruf][interested]=USED

	COMPANY | CHECK: OK | RESULT: OK
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=company&tx_iconneuklientenanlage_pi1[aufruf][process]=12346&tx_iconneuklientenanlage_pi1[aufruf][interested]=OK&tx_iconneuklientenanlage_pi1[aufruf][test]=1

	COMPANY | CHECK: OK | RESULT: ERROR
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=company&tx_iconneuklientenanlage_pi1[aufruf][process]=12346&tx_iconneuklientenanlage_pi1[aufruf][interested]=ERROR


	PRIVATE | CHECK: USED
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=private&tx_iconneuklientenanlage_pi1[aufruf][process]=67891&tx_iconneuklientenanlage_pi1[aufruf][interested]=USED

	PRIVAT | CHECK: OK | RESULT: OK
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=private&tx_iconneuklientenanlage_pi1[aufruf][process]=67890&tx_iconneuklientenanlage_pi1[aufruf][interested]=OK&tx_iconneuklientenanlage_pi1[aufruf][test]=1

	PRIVATE | CHECK: OK | RESULT: ERROR
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=private&tx_iconneuklientenanlage_pi1[aufruf][process]=67890&tx_iconneuklientenanlage_pi1[aufruf][interested]=ERROR

	*/

/*
FORMULAR BEREITS GESPERRT:
----------------------------------------------------------------------------------------------------------------------------------------------------------------
<process>701</process>
<interests_nr>10008738</interests_nr>

URL:   http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=company&tx_iconneuklientenanlage_pi1[aufruf][process]=701&tx_iconneuklientenanlage_pi1[aufruf][interested]=10008738

-> Statusprüfung zeigt User meldung!
-> siehe screenshot



FORMULAR ZUR ÜBERMITTLUNG
----------------------------------------------------------------------------------------------------------------------------------------------------------------
<process>730</process>
<interests_nr>10008738</interests_nr>

URL:   http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=company&tx_iconneuklientenanlage_pi1[aufruf][process]=730&tx_iconneuklientenanlage_pi1[aufruf][interested]=10008738

-> Statusprüfung leitet User zu Formular!
-> siehe screenshot
-> Formular wurde nicht abgesendet um FALL zum testen zu nutzen

*/

/*
		IGEL WEBSERVICE verfügbar über:
		
		https://83.164.146.178	=> https://portal.icon.at
		
		WERTELISTE:		POSITION/FUNKTION
		https://portal.icon.at/igel/wsi/lov/beziehungen
		
		WERTELISTE:		FINANZAMT
		https://portal.icon.at/igel/wsi/lov/institute/fa
		
		WERTELISTE:		LÄNDER
		https://portal.icon.at/igel/wsi/lov/laender
		
*/

/*
	IGEL-AUFRUFE / 17.05.2017	
		
	http://www.icon.at/de/neuklientenanlage/?tx_iconneuklientenanlage_pi1[aufruf][form]=company&tx_iconneuklientenanlage_pi1[aufruf][process]=7988&tx_iconneuklientenanlage_pi1[aufruf][interested]=10008738

	http://www.icon.at/de/neuklientenanlage/
		?tx_iconneuklientenanlage_pi1[aufruf][form]=private
		&tx_iconneuklientenanlage_pi1[aufruf][process]=7988
		&tx_iconneuklientenanlage_pi1[aufruf][interested]=10008738
*/


	protected function initializeIndexAction(){
		$propertyMappingConfiguration = $this->arguments['aufruf']->getPropertyMappingConfiguration();
		// allow all properties:
		$propertyMappingConfiguration->allowAllProperties();
		// or just allow certain properties
//		$propertyMappingConfiguration->allowProperties('firstname');
		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
	}

  /**
   * action index
   *
   * @param \NEXT\IconNeuklientenanlage\Domain\Model\Aufruf $aufruf
   * @ v a l i d a t e $aufruf
   *  @ d o n t v a l i d a t e $aufruf
   * @return void
   */
  public function indexAction(\NEXT\IconNeuklientenanlage\Domain\Model\Aufruf $aufruf = NULL) {

	//	-> show Result-Screen
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$error = 0;

	//	
	$url = 'https://portal.icon.at/igel/wsi/formular';

	//
	$igel = $this->webserviceRepository->getIgelLogin();
	$igel['action'] = 'KLIENTENNEUANLAGE-STATUSCHECK';
	$igel['language'] = $LANG==1 ? 'EN' : 'DE';
//	$igel['process'] = '7890';			//	gerade Nr. produziert error-ID = 0
//	$igel['process'] = '7891';			//	ungerade Nr. produziert error-ID = 1
//	$igel['process'] = '7891as';		//	nicht nummerisch produziert error-ID = 1
//	$igel['interests_nr'] = '234';
	
	//
//	$aufruf = $this->objectManager->getEmptyObject('\NEXT\IconNeuklientenanlage\Domain\Model\Aufruf');
//	$aufruf = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP("form");
//	$args = $this->request->getArguments();
   	$aufruf = $this->request->getArgument('aufruf');
	//

	if( $aufruf == NULL ){
		$error = 1;
	} else {
//		$form = $aufruf->getForm();
		$test = $aufruf['test'];
		$form = $aufruf['form'];
		$process = $aufruf['process'];
		$interested = $aufruf['interested'];
		//
		$t_log =  'AUFRUF:'.PHP_EOL;
		$t_log .= 'test:          ' . $test . PHP_EOL;
		$t_log .= 'form:          ' . $form . PHP_EOL;
		$t_log .= 'process:       ' . $process . PHP_EOL;
		$t_log .= 'interested:    ' . $interested . PHP_EOL; 
		$resLog = $this->loggingRepository->doLog('STATUS-CHECK - REQUEST', $process.'-'.$interested, $t_log);
		//
/*
		DO THE PRE-CHECK
*/
		$igel['process'] = $process;
		$igel['interests_nr'] = $interested;

		//
		$data = $this->webserviceRepository->buildXML( array('igel' => $igel) );
		//
		$ws = $this->webserviceRepository->init($url);
		//
		$ws = $this->webserviceRepository->setPostFields($ws, $data);
		//
		$resLog = $this->loggingRepository->doLog('STATUS-CHECK - SUBMIT-DATA', $process.'-'.$interested, $this->webserviceRepository->anonymizeXML($data));
		//
		$res = $this->webserviceRepository->call($ws);
		//
		$this->webserviceRepository->close($ws);
		//
		$resLog = $this->loggingRepository->doLogResultIGEL('STATUS-CHECK - RESULT', $process.'-'.$interested, $res);
		//
		$res_error_ID = $res['error']['ID'];
		$res_error_info = $res['error']['info'];
		$res_message  = $res['message'];

		/*
			PROTOKOLLIERE FORMULAR_DATEN UND ANTWORT
		*/
/*
		$m_sender = array( 'webserver@icon.at' => 'icon.at' );
		$m_recipients = array( 'r.schmoller@next-linz.com' => 'smo' );
	//		$m_recipients = array( 'r.schmoller@next-linz.com' => 'smo' );
		$m_subject = 'ICON-KLIENTENANLAGE: STATUS - CHECK!';
		$m_charset = $GLOBALS['TSFE']->metaCharset;
		//
		$mailBody = '';
		$mailBody .= $m_subject . PHP_EOL;
		$mailBody .= PHP_EOL;
		$mailBody .= 'TIMESTAMP:     ' . date('Y-m-d H:i:s') . PHP_EOL;
		$mailBody .= PHP_EOL;
		$mailBody .= 'ERROR-ID:      ' . $res['error']['ID'] . PHP_EOL;
		$mailBody .= 'ERROR-INFO:    ' . $res['error']['info'] . PHP_EOL;
		$mailBody .= 'ERROR-ID:    ' . $res['message'] . PHP_EOL;
		$mailBody .= PHP_EOL;
		$mailBody .= 'IGEL - XML:' . PHP_EOL . $res['RAW']['data'] . PHP_EOL;
		$mailBody .= PHP_EOL;
		$mailBody .= PHP_EOL;
		$mailBody .= 'DATA-XML' . PHP_EOL;
		$mailBody .= $data;
		$mailBody .= PHP_EOL;
		//
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		$mail
		  ->setTo( $m_recipients )
		  ->setFrom( $m_sender )
		  ->setSubject( $m_subject )
		  ->setCharset( $m_charset );
		//
		//$mail->setCc($m_ccArray);
		//$mail->setBcc($m_bccArray);
		//      $message->setReturnPath($cObj->cObjGetSingle($conf[$type . '.']['overwrite.']['returnPath'], $conf[$type . '.']['overwrite.']['returnPath.']));
		//      $mail->setReplyTo($replyArray);
		$mail->setBody($mailBody, 'text/plain');
		$mail->send();
		$m_result = $mail->isSent();
*/

		//
		if( $res_error_ID > 0 ){
			//	SHOW ALREADY USED INFO
			$this->redirect('used');
		} else if( $res_error_ID < 0 && ($test!=1 || $test!='1') ){
			//	ERROR FROM IGEL
			$error = 3;
			$this->view->assignMultiple(array(
				'LANG' => $LANG,
				'aufruf' => $aufruf,
				'error' => $error,
				'res' => $res,
			));
		} else {
			//
			if( $form == 'company' || $form == 'private' ){
				$actionName = $form;
				$controllerName = NULL;
				$extensionName = NULL;
				$arguments = array(
					'process' => $process,
					'interested' => $interested,
	/*
					'process' => $aufruf->getProcess(),
					'interested' => $aufruf->getInterested(),
	*/
				);
				$this->redirect($actionName, $controllerName, $extensioName, $arguments);
			} else {
				//
				$error = 2;
				$this->view->assignMultiple(array(
					'LANG' => $LANG,
					'aufruf' => $aufruf,
					'error' => $error,
					'res' => $res,
				));
			}
		}
	}
	$this->view->assignMultiple(array(
		'LANG' => $LANG,
		'aufruf' => $aufruf,
		'error' => $error,
		'args' => $args,
		'res' => $res,
	));
  }

  /**
   * action company
   *
   * @param \NEXT\IconNeuklientenanlage\Domain\Model\FormCompany $formdata
   * @return void
   */
  public function companyAction(\NEXT\IconNeuklientenanlage\Domain\Model\FormCompany $formdata = NULL) {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$LANG_str = $LANG == 1 ? 'EN' : 'DE';
	//
	$GLOBALS['TSFE']->fe_user->setKey('ses', 'new-client-result', '');
	$GLOBALS['TSFE']->fe_user->storeSessionData();
	//
	if( $formdata == NULL ){
		$formdata = $this->objectManager->getEmptyObject('\NEXT\IconNeuklientenanlage\Domain\Model\FormCompany');
	}
	//
   	$process = $this->request->getArgument('process');
   	$interested = $this->request->getArgument('interested');
	//
	$formdata->setProcess($process);
	$formdata->setInterested($interested);
	//
	//	BILL ADDRESS
	$hideBillAddr = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['billAddr'];
	$hideBillAddr = ($hideBillAddr==1 || $hideBillAddr=='1' ) ? FALSE : TRUE;
//	var_dump($hideBillAddr);
	//	BILL EMAIL MODE
	$hideBillEmailMode = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['billEmailMode'];
	$hideBillEmailMode = ($hideBillEmailMode==1 || $hideBillEmailMode=='1' ) ? FALSE : TRUE;
//	var_dump($hideBillEmailMode);
	//	TAX OFFICES 1 - 5
	$hideTO1 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['taxOfficeUsed1'];
	$hideTO1 = ($hideTO1==1 || $hideTO1=='1' ) ? FALSE : TRUE;
//	var_dump($hideTO1);
	$hideTO2 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['taxOfficeUsed2'];
	$hideTO2 = ($hideTO2==1 || $hideTO2=='1' ) ? FALSE : TRUE;
//	var_dump($hideTO2);
	$hideTO3 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['taxOfficeUsed3'];
	$hideTO3 = ($hideTO3==1 || $hideTO3=='1' ) ? FALSE : TRUE;
//	var_dump($hideTO3);
	$hideTO4 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['taxOfficeUsed4'];
	$hideTO4 = ($hideTO4==1 || $hideTO4=='1' ) ? FALSE : TRUE;
//	var_dump($hideTO4);
	$hideTO5 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['taxOfficeUsed5'];
	$hideTO5 = ($hideTO5==1 || $hideTO5=='1' ) ? FALSE : TRUE;
//	var_dump($hideTO5);
	//	PERSONS 1 - 5
	$hideP1 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['used1'];
	$hideP1 = ($hideP1==1 || $hideP1=='1' ) ? FALSE : TRUE;
//	var_dump($hideP1);
	$hideP2 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['used2'];
	$hideP2 = ($hideP2==1 || $hideP2=='1' ) ? FALSE : TRUE;
//	var_dump($hideP2);
	$hideP3 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['used3'];
	$hideP3 = ($hideP3==1 || $hideP3=='1' ) ? FALSE : TRUE;
//	var_dump($hideP3);
	$hideP4 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['used4'];
	$hideP4 = ($hideP4==1 || $hideP4=='1' ) ? FALSE : TRUE;
//	var_dump($hideP4);
	$hideP5 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['used5'];
	$hideP5 = ($hideP5==1 || $hideP5=='1' ) ? FALSE : TRUE;
//	var_dump($hideP5);

	//
//	var_dump($formdata);
//	var_dump($_POST);

	if( $LANG == 1 ){
		$pf_data = '<option value=""> -- please choose --</option>';
	} else {
		$pf_data = '<option value=""> -- Bitte wählen --</option>';
	}
	$pf_data .= $this->webserviceRepository->getFileData('beziehungen-data', $LANG_str);
	//
	$lst_laender = unserialize($this->webserviceRepository->getFileData('laender', $LANG_str));
	//
	$lst_finanzamt = unserialize($this->webserviceRepository->getFileData('finanzamt', 'DE'));
//	$lst_finanzamt = unserialize($this->webserviceRepository->getFileData('finanzamt', $LANG_str));
	//
	$lst_titel = unserialize($this->webserviceRepository->getFileData('titel', $LANG_str));
	$lst_titel_nach = unserialize($this->webserviceRepository->getFileData('titel_nach', $LANG_str));

	//
	$this->view->assignMultiple(array(
		'LANG' => $LANG,
		'formdata' => $formdata,
		'hideBillAddr' => $hideBillAddr,
		'hideBillEmailMode' => $hideBillEmailMode,
		'hideTO1' => $hideTO1,
		'hideTO2' => $hideTO2,
		'hideTO3' => $hideTO3,
		'hideTO4' => $hideTO4,
		'hideTO5' => $hideTO5,
		'hideP1' => $hideP1,
		'hideP2' => $hideP2,
		'hideP3' => $hideP3,
		'hideP4' => $hideP4,
		'hideP5' => $hideP5,
		'pf_data' => $pf_data,
		'lst_laender' => $lst_laender,
		'lst_finanzamt' => $lst_finanzamt,
		'lst_titel' => $lst_titel,
		'lst_titel_nach' => $lst_titel_nach,
	));
  }

  /**
   * action private
   *
   * @param \NEXT\IconNeuklientenanlage\Domain\Model\FormPrivate $formdata
   * @return void
   */
  public function privateAction(\NEXT\IconNeuklientenanlage\Domain\Model\FormPrivate $formdata = NULL) {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$LANG_str = $LANG == 1 ? 'EN' : 'DE';
	//
	$GLOBALS['TSFE']->fe_user->setKey('ses', 'new-client-result', '');
	$GLOBALS['TSFE']->fe_user->storeSessionData();
	//
	if( $formdata == NULL ){
		$formdata = $this->objectManager->getEmptyObject('\NEXT\IconNeuklientenanlage\Domain\Model\FormPrivate');
	}
	//
   	$process = $this->request->getArgument('process');
   	$interested = $this->request->getArgument('interested');
	//
	$formdata->setProcess($process);
	$formdata->setInterested($interested);

	//	TAX OFFICE
	$hideTO1 = $_POST['tx_iconneuklientenanlage_pi1']['formdata']['taxOfficeUsed1'];
	$hideTO1 = ($hideTO1==1 || $hideTO1=='1' ) ? FALSE : TRUE;
//	var_dump($hideTO1);

	if( $LANG == 1 ){
		$pf_data = '<option value=""> -- please choose --</option>';
	} else {
		$pf_data = '<option value=""> -- Bitte wählen --</option>';
	}
	$pf_data .= $this->webserviceRepository->getFileData('beziehungen-data', $LANG_str);

	//
	$lst_laender = unserialize($this->webserviceRepository->getFileData('laender', $LANG_str));
	//
	$lst_finanzamt = unserialize($this->webserviceRepository->getFileData('finanzamt', 'DE'));
//	$lst_finanzamt = unserialize($this->webserviceRepository->getFileData('finanzamt', $LANG_str));

	$lst_titel = unserialize($this->webserviceRepository->getFileData('titel', $LANG_str));
	$lst_titel_nach = unserialize($this->webserviceRepository->getFileData('titel_nach', $LANG_str));

	//
	/*
		zum TESTEN
	*/
/*
	$formdata->setGender('Herr');
	$formdata->setTitle('Dr.Dr. Prof.');
	$formdata->setFirstname('Vorname');
	$formdata->setLastname('Nachname');
	$formdata->setTitleAfter('Msc.');
	$formdata->setFunction('Leitung');
	$formdata->setPhone('+43 732 12345');
	$formdata->setEmail('email@test.com');
	$formdata->setFax('+43 732 12345 6789');
	$formdata->setMobile('+43 676 123456789');
	$formdata->setStreet('Straße');
	$formdata->setStreetNr('Hausnummer');
	$formdata->setZip('PLZ');
	$formdata->setCity('Stadt');
	$formdata->setCountry('Austria');
	//
	$formdata->setBillMode(0);
	$formdata->setAdditional('weitere Infos hier...');
	//
	$formdata->setAttentivelyWebsite(1);
	$formdata->setAttentivelyTaxAcademy(0);
	$formdata->setAttentivelyInserat(1);
	$formdata->setAttentivelyEmpfehlung(1);
	$formdata->setAttentivelyEmpfehlungInfo('von nem Freund...');
	$formdata->setAttentivelySearchengine(0);
	$formdata->setAttentivelySonstiges(1);
	$formdata->setAttentivelySonstigesInfo('ja von wo anders halt...');
*/
	//
	$this->view->assignMultiple(array(
		'LANG' => $LANG,
		'formdata' => $formdata,
		'hideTO1' => $hideTO1,
		'pf_data' => $pf_data,
		'lst_laender' => $lst_laender,
		'lst_finanzamt' => $lst_finanzamt,
		'lst_titel' => $lst_titel,
		'lst_titel_nach' => $lst_titel_nach,
	));
  }

  /**
   * action FormCompany
   *
   * @param \NEXT\IconNeuklientenanlage\Domain\Model\FormCompany $formdata
   * @validate $formdata \NEXT\IconNeuklientenanlage\Validation\Validator\FormCompanyValidator
   * @return void
   */
  public function saveCompanyAction(\NEXT\IconNeuklientenanlage\Domain\Model\FormCompany $formdata = NULL) {
	//
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	//	-> check formdata
    if( $formdata==NULL ){
		//	-> show Form-Screen
/*		$actionName = 'index';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array();
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
*/
//		$this->redirect();
	}
	//
	//
	$url = 'https://portal.icon.at/igel/wsi/formular';

	//
	$igel = $this->webserviceRepository->getIgelLogin();
	$igel['action'] = 'KLIENTENNEUANLAGE';
	$igel['language'] = $LANG==1 ? 'EN' : 'DE';
	$igel['process'] = $formdata->getProcess();
	$igel['interests_nr'] = $formdata->getInterested();
	//
	$dataarray = $this->formularRepository->getDataArrayCompany($formdata);
	
	$extName = 'tx_iconneuklientenanlage_pi1';
	$uploadPath = $this->settings['upload']['path'];    //    'uploads/tx_nextgenesungswunsch/';
	$uploadPath = 'uploads/';
	$uploadSizeMax = $this->settings['upload']['maxSizeMB'];    //    2 MB;
	$uploadSizeMax = 2;
	$allowedFileTypes = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', 'application/pdf;application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/msword;application/vnd.openxmlformats-officedocument.wordprocessingml.document;image/jpg;image/jpeg;image/pjpeg;');
	//
	$files = array();
	//
	if( $_FILES[$extName] ){
		for( $f=1; $f<=5; $f++ ){
			$fparam = 'file' . $f;
			//    get FILENAME from FORM
			$fileName = trim($_FILES[$extName]['name'][$fparam]);
			//    get FILESIZE from FORM
			$fSize = $_FILES[$extName]['size'][$fparam];
			//    get FILETYPE from FORM
			$fType = trim($_FILES[$extName]['type'][$fparam]);
			//    get TMP-FILENAME to FILE in FORM
			$tmpName = trim($_FILES[$extName]['tmp_name'][$fparam]);
			//
			$isFileName = $fileName != '';
			$isFileSize = $fSize > 0 && $fSize <= ($uploadSizeMax *1024*1024);
			$isFileType = in_array($fType, $allowedFileTypes);
//			$isFileType = true;
			$isFileTemp = $tmpName !='';
			//
			if( $isFileName && $isFileSize && $isFileType && $isFileTemp ){
				$fileData = file_get_contents($tmpName, FILE_USE_INCLUDE_PATH);
//				$fileData = 'fType: ' . $fType . ' mime-type: ' . mime_content_type($tmpName);
				$files[$fparam] = array('name' => $fileName, 'data' => $fileData);
			}
		}
	}
	
	//
	$data = $this->webserviceRepository->buildXML( array('igel' => $igel, 'formular' => $dataarray, 'files' => $files) );
	$data_NoFiles = $this->webserviceRepository->buildXML( array('igel' => $igel, 'formular' => $dataarray) );
	//
	$resLog = $this->loggingRepository->doLog('COMPANY-SAVE - SUBMIT-DATA', $formdata->getProcess().'-'.$formdata->getInterested(), $this->webserviceRepository->anonymizeXML($data_NoFiles));
	//
	$ws = $this->webserviceRepository->init($url);
	//
	$ws = $this->webserviceRepository->setPostFields($ws, $data);
	//
	$res = $this->webserviceRepository->call($ws);
	//
	$this->webserviceRepository->close($ws);
	//
	$resLog = $this->loggingRepository->doLogResultIGEL('COMPANY-SAVE - RESULT', $formdata->getProcess().'-'.$formdata->getInterested(), $res);

/**/
	//
	$res_error_ID = $res['error']['ID'];
	$res_error_info = $res['error']['info'];
	$res_message  = $res['message'];
/**/

	//
	$GLOBALS['TSFE']->fe_user->setKey('ses', 'new-client-result', serialize($res));
	$GLOBALS['TSFE']->fe_user->storeSessionData();

	/*
		PROTOKOLLIERE FORMULAR_DATEN UND ANTWORT
	*/
/*
	$m_sender = array( 'webserver@icon.at' => 'icon.at' );
	$m_recipients = array( 'r.schmoller@next-linz.com' => 'smo' );
//		$m_recipients = array( 'r.schmoller@next-linz.com' => 'smo' );
	$m_subject = 'ICON-KLIENTENANLAGE: FEHLER bei Datenübergabe an IGEL!';
	$m_charset = $GLOBALS['TSFE']->metaCharset;
	//
	$mailBody = '';
	$mailBody .= $m_subject . PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= 'TIMESTAMP:     ' . date('Y-m-d H:i:s') . PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= 'ERROR-ID:      ' . $res['error']['ID'] . PHP_EOL;
	$mailBody .= 'ERROR-INFO:    ' . $res['error']['info'] . PHP_EOL;
	$mailBody .= 'ERROR-ID:    ' . $res['message'] . PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= 'IGEL - XML:' . PHP_EOL . $res['RAW']['data'] . PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= 'DATA-XML' . PHP_EOL;
	$mailBody .= $data;
	$mailBody .= PHP_EOL;
	//
	$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
	$mail
	  ->setTo( $m_recipients )
	  ->setFrom( $m_sender )
	  ->setSubject( $m_subject )
	  ->setCharset( $m_charset );
	//
	//$mail->setCc($m_ccArray);
	//$mail->setBcc($m_bccArray);
	//      $message->setReturnPath($cObj->cObjGetSingle($conf[$type . '.']['overwrite.']['returnPath'], $conf[$type . '.']['overwrite.']['returnPath.']));
	//      $mail->setReplyTo($replyArray);
	$mail->setBody($mailBody, 'text/plain');
	$mail->send();
	$m_result = $mail->isSent();

*/
	//
/* */
	if( $res_error_ID == 0 ){
		$this->redirect('done');
	} else {
		//
		$this->loggingRepository->sendErrorInfo('COMPANY-SAVE - RESULT', $formdata->getProcess().'-'.$formdata->getInterested(), $res);
		//
		$this->redirect('fail');
	}
/* */
/* 
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'url' => $url,
		'data' => $data,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'xmldata' => $res,
		'error_ID' => $res_error_ID,
		'error_info' => $res_error_info,
		'message' => $res_message,
	));
 */
  }
  
  /**
   * action savePrivate
   *
   * @param \NEXT\IconNeuklientenanlage\Domain\Model\FormPrivate $formdata
   * @validate $formdata \NEXT\IconNeuklientenanlage\Validation\Validator\FormPrivateValidator
   * @return void
   */
  public function savePrivateAction(\NEXT\IconNeuklientenanlage\Domain\Model\FormPrivate $formdata = NULL) {
	//
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	//	-> check formdata
    if( $formdata==NULL ){
		//	-> show Form-Screen
/*		$actionName = 'index';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array();
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
*/
//		$this->redirect('done');
	}

	//
	$url = 'https://portal.icon.at/igel/wsi/formular';

	//
	$igel = $this->webserviceRepository->getIgelLogin();
	$igel['action'] = 'KLIENTENNEUANLAGE';
	$igel['language'] = $LANG==1 ? 'EN' : 'DE';
	$igel['process'] = $formdata->getProcess();
	$igel['interests_nr'] = $formdata->getInterested();
	//
	$dataarray = $this->formularRepository->getDataArrayPrivate($formdata);

	$extName = 'tx_iconneuklientenanlage_pi1';
	$uploadPath = $this->settings['upload']['path'];    //    'uploads/tx_nextgenesungswunsch/';
	$uploadPath = 'uploads/';
	$uploadSizeMax = $this->settings['upload']['maxSizeMB'];    //    2 MB;
	$uploadSizeMax = 2;
	$allowedFileTypes = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', 'application/pdf;application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/msword;application/vnd.openxmlformats-officedocument.wordprocessingml.document;image/jpg;image/jpeg;image/pjpeg;');
	//
	$files = array();
	//
	if( $_FILES[$extName] ){
		for( $f=1; $f<=5; $f++ ){
			$fparam = 'file' . $f;
			//    get FILENAME from FORM
			$fileName = trim($_FILES[$extName]['name'][$fparam]);
			//    get FILESIZE from FORM
			$fSize = $_FILES[$extName]['size'][$fparam];
			//    get FILETYPE from FORM
			$fType = trim($_FILES[$extName]['type'][$fparam]);
			//    get TMP-FILENAME to FILE in FORM
			$tmpName = trim($_FILES[$extName]['tmp_name'][$fparam]);
			//
			$isFileName = $fileName != '';
			$isFileSize = $fSize > 0 && $fSize <= ($uploadSizeMax *1024*1024);
			$isFileType = in_array($fType, $allowedFileTypes);
			$isFileTemp = $tmpName !='';
			//
			if( $isFileName && $isFileSize && $isFileType && $isFileTemp ){
				$fileData = file_get_contents($tmpName, FILE_USE_INCLUDE_PATH);
				$files[$fparam] = array('name' => $fileName, 'data' => $fileData);
			}
		}
	}

	//
	$data = $this->webserviceRepository->buildXML( array('igel' => $igel, 'formular' => $dataarray, 'files' => $files) );
	$data_NoFiles = $this->webserviceRepository->buildXML( array('igel' => $igel, 'formular' => $dataarray) );
	//
	$resLog = $this->loggingRepository->doLog('PRIVATE-SAVE - SUBMIT-DATA', $formdata->getProcess().'-'.$formdata->getInterested(), $this->webserviceRepository->anonymizeXML($data_NoFiles));
	//
	$ws = $this->webserviceRepository->init($url);
	//
	$ws = $this->webserviceRepository->setPostFields($ws, $data);
	//
	$res = $this->webserviceRepository->call($ws);
	//
	$this->webserviceRepository->close($ws);
	//
	$resLog = $this->loggingRepository->doLogResultIGEL('PRIVATE-SAVE - RESULT', $formdata->getProcess().'-'.$formdata->getInterested(), $res);

/**/
	//
	$res_error_ID = $res['error']['ID'];
	$res_error_info = $res['error']['info'];
	$res_message  = $res['message'];
/**/

	//
	$GLOBALS['TSFE']->fe_user->setKey('ses', 'new-client-result', serialize($res));
	$GLOBALS['TSFE']->fe_user->storeSessionData();
/* */

	/*
		PROTOKOLLIERE FORMULAR_DATEN UND ANTWORT
	*/
/*
	$m_sender = array( 'webserver@icon.at' => 'icon.at' );
	$m_recipients = array( 'r.schmoller@next-linz.com' => 'smo' );
//		$m_recipients = array( 'r.schmoller@next-linz.com' => 'smo' );
	$m_subject = 'ICON-KLIENTENANLAGE: FEHLER bei Datenübergabe an IGEL! (PRIVATEPERSON!)';
	$m_charset = $GLOBALS['TSFE']->metaCharset;
	//
	$mailBody = '';
	$mailBody .= $m_subject . PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= 'TIMESTAMP:     ' . date('Y-m-d H:i:s') . PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= 'ERROR-ID:      ' . $res['error']['ID'] . PHP_EOL;
	$mailBody .= 'ERROR-INFO:    ' . $res['error']['info'] . PHP_EOL;
	$mailBody .= 'ERROR-ID:    ' . $res['message'] . PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= 'IGEL - XML:' . PHP_EOL . $res['RAW']['data'] . PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= PHP_EOL;
	$mailBody .= 'DATA-XML' . PHP_EOL;
	$mailBody .= $data;
	$mailBody .= PHP_EOL;
	//
	$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
	$mail
	  ->setTo( $m_recipients )
	  ->setFrom( $m_sender )
	  ->setSubject( $m_subject )
	  ->setCharset( $m_charset );
	//
	//$mail->setCc($m_ccArray);
	//$mail->setBcc($m_bccArray);
	//      $message->setReturnPath($cObj->cObjGetSingle($conf[$type . '.']['overwrite.']['returnPath'], $conf[$type . '.']['overwrite.']['returnPath.']));
	//      $mail->setReplyTo($replyArray);
	$mail->setBody($mailBody, 'text/plain');
	$mail->send();
	$m_result = $mail->isSent();
*/
	//
	if( $res_error_ID == 0 ){
		$this->redirect('done');
	} else {
		//
		$this->loggingRepository->sendErrorInfo('PRIVATE-SAVE - RESULT', $formdata->getProcess().'-'.$formdata->getInterested(), $res);
		//
		$this->redirect('fail');
	}
/* */
/*
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'data' => $data,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'xmldata' => '',
		'error_ID' => $res_error_ID,
		'error_info' => $res_error_info,
		'message' => $res_message,
	));
*/
  }
  
  /**
   * action used
   *
   * @return void
   */
  public function usedAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
	));
  }
  
  /**
   * action done
   *
   * @return void
   */
  public function doneAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	//
    if( $GLOBALS['TSFE']->fe_user->getKey('ses', 'new-client-result') ){
      $res = unserialize($GLOBALS['TSFE']->fe_user->getKey('ses', 'new-client-result'));
      $GLOBALS['TSFE']->fe_user->setKey('ses', 'new-client-result', '');
      $GLOBALS['TSFE']->fe_user->storeSessionData();
	}
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'error_ID' => $res['error']['ID'],
		'error_info' => $res['error']['info'],
		'message' => $res['message'],
	));
  }
  
  /**
   * action fail
   *
   * @return void
   */
  public function failAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	//
    if( $GLOBALS['TSFE']->fe_user->getKey('ses', 'new-client-result') ){
      $res = unserialize($GLOBALS['TSFE']->fe_user->getKey('ses', 'new-client-result'));
      $GLOBALS['TSFE']->fe_user->setKey('ses', 'new-client-result', '');
      $GLOBALS['TSFE']->fe_user->storeSessionData();
	}
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'error_ID' => $res['error']['ID'],
		'error_info' => $res['error']['info'],
		'message' => $res['message'],
	));
  }
  
  /**
   * action testformular
   *
   * @return void
   */
  public function testformularAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	//
	$url = 'https://portal.icon.at/igel/wsi/formular';

	$data = '
<?xml version="1.0" encoding="UTF-8"?>
<xml>
	<igel>
		<action>KLIENTENNEUANLAGE</action>
		<!-- action = Text -->
		<user>wsnext</user>
		<!-- user = Text -->
		<password>20ce$c497b#585</password>
		<!-- password = Text -->
		<process>7890</process>
		<!-- process = ZAHL -->
		<interests_nr>234</interests_nr>
		<!-- interests_nr = ZAHL des Interesenten aus IGEL -->
	</igel>
	<data>
		<language>DE</language>
		<!-- language = DE / EN -->
		<formular>
			<fieldset>
				<ID>COMPANY</ID>
				<!-- ID = beliebiger Text -->
				<type>ADDRESS</type>
				<!-- type = beliebiger Text -->
				<fields>
					<field>
						<ID>Company_Name</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Street</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Street_Nr</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Zip</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>City</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Country</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Firstname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Lastname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Department</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
				</fields>
			</fieldset>

			<fieldset>
				<ID>BILLING</ID>
				<!-- ID = beliebiger Text -->
				<type>ADDRESS</type>
				<!-- type = beliebiger Text -->
				<fields>
					<field>
						<ID>Street</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Street_Nr</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Zip</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>City</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Country</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Firstname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Lastname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Department</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
				</fields>
			</fieldset>

			<fieldset>
				<ID>BILLING_DELIVERY</ID>
				<!-- ID = beliebiger Text -->
				<type>DELIVERY</type>
				<!-- type = beliebiger Text -->
				<fields>
					<field>
						<ID>Delivery</ID>
						<data></data>
						<!-- data = EMAIL / POST -->
					</field>
					<field>
						<ID>Email</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Firstname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Lastname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Additional</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
				</fields>
			</fieldset>

			<fieldset>
				<ID>TAX_1</ID>
				<!-- ID = beliebiger Text -->
				<type>TAXINFO</type>
				<!-- type = beliebiger Text -->
				<fields>
					<field>
						<ID>Tax_Office</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Tax_ID</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>UID_No</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
				</fields>
			</fieldset>
			<fieldset>
				<ID>TAX_2</ID>
				<!-- ID = beliebiger Text -->
				<type>TAXINFO</type>
				<!-- type = beliebiger Text -->
				<fields>
					<field>
						<ID>Tax_Office</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Tax_ID</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>UID_No</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
				</fields>
			</fieldset>
			
			<fieldset>
				<ID>PERSON_CFO</ID>
				<!-- ID = beliebiger Text -->
				<type>PERSON</type>
				<!-- type = beliebiger Text -->
				<fields>
					<field>
						<ID>Salutation</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Title</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Firstname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Lastname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Title_After</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Position_Function</ID>
						<data></data>
						<!-- data = ID aus zugehöriger IGEL-Werteliste -->
					</field>
					<field>
						<ID>Phone</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Email</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Fax</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Mobile</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
				</fields>
			</fieldset>
			<fieldset>
				<ID>PERSON_CAO</ID>
				<!-- ID = beliebiger Text -->
				<type>PERSON</type>
				<!-- type = beliebiger Text -->
				<fields>
					<field>
						<ID>Salutation</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Title</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Firstname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Lastname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Title_After</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Position_Function</ID>
						<data></data>
						<!-- data = ID aus zugehöriger IGEL-Werteliste -->
					</field>
					<field>
						<ID>Phone</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Email</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Fax</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Mobile</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
				</fields>
			</fieldset>
			<fieldset>
				<ID>PERSON_1</ID>
				<!-- ID = beliebiger Text -->
				<type>PERSON</type>
				<!-- type = beliebiger Text -->
				<fields>
					<field>
						<ID>Salutation</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Title</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Firstname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Lastname</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Title_After</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Position_Function</ID>
						<data></data>
						<!-- data = ID aus zugehöriger IGEL-Werteliste -->
					</field>
					<field>
						<ID>Phone</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Email</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Fax</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Mobile</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
				</fields>
			</fieldset>
			<fieldset>
				<ID>ATTENTIVELY</ID>
				<!-- ID = beliebiger Text -->
				<type>OTHERS</type>
				<!-- type = beliebiger Text -->
				<fields>
					<field>
						<ID>Homepage</ID>
						<data></data>
						<!-- data = 0 / 1 Zahl -->
					</field>
					<field>
						<ID>Tax_Academy_Seminare</ID>
						<data></data>
						<!-- data = 0 / 1 Zahl -->
					</field>
					<field>
						<ID>Inserat</ID>
						<data></data>
						<!-- data = 0 / 1 Zahl -->
					<field>
						<ID>Empfehlung</ID>
						<data></data>
						<!-- data = 0 / 1 Zahl -->
					</field>
					<field>
						<ID>Empfehlung_Name</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
					<field>
						<ID>Suchmaschinen</ID>
						<data></data>
						<!-- data = 0 / 1 Zahl -->
					</field>
					<field>
						<ID>Sonstiges</ID>
						<data></data>
						<!-- data = 0 / 1 Zahl -->
					</field>
					<field>
						<ID>Sonstiges_Text</ID>
						<data></data>
						<!-- data = beliebiger Text -->
					</field>
				</fields>
			</fieldset>
		</formular>
		<files>
			<file>
				<name>logo.gif</name>
				<!-- Filename = original Dateiname, beliebiger Text -->
				<data><![CDATA[R0lGODlh+gD6AMoAAAFTxwBSx8fHxwFTyW35pQBUxwBTxW76pboYI8fd2235Z8fH36kdtwBTfwBRxtvH5ysZI9/bxwBS0Xupv231Z9/HuG77tTV+/AFV0QFUxLoYATd9/AFRxwBS3trHx/gcIW76Z9/H29/d2t/d3QJTxzd9sHvfvzR+stvH2eYYtwFV3ABSftu43QBWyXyuofgdOgFR0Th+on0io8e4u6kYGgNT09rHuLodA+AZAaEYAeYZOlPnIn0i+AJTxDJ+/G36AgBUfjR9+/gZGgBWxQNRxwJQxABTfQBR1uelqzR+owBRfCdx887Z2fWloWv8Ef8BwqoZGjfEo2v7ZW/8AgNUybodHmv7wCBy8zN+Isfar275sQNRfx9y9r/KIW76Am38AgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACPYBAAAAAAALAAAAAD6APoAAAfHxE0EydHW3Bbc4uTj5efRB+rr6unu7+7t7eMU8vrtFBRNIAYFBaMiBQGpIqkAYMbYHrNkZdi0tmcCAiIRZafm58CopbGhImYrAK5ebmgm5joTsSYdOB34+DkZZXgYHq3KU6uhFPgYpzoawA4BPyCqD6oB8mgeyNm6kzPYK9/AA8cAx7eiV7RfLgkALFggB21AqAHVz7djSHHyP8S0JivZdy/7q8XaOmAjPh4iSOLa6vRcyOcgpWbaWNbu8nEQS3IoW+orJnP7PloGJxZsZQy/APU9X2nZKRhVUqcnXVocFzMmE+oGUiHHKxQhFPwFd3JgKsa0Hu4GQQZTOBZtcxFn56xGqrvSYE5+HmeiMTjEYFzfYAHz3Tt1YnNlYRlWNe8R33ciTwW0JHw1Mjmo42kiOkpAsTr16OcSM9hOqb+icSvH+fR12aoA+j3fZjlEtMJ5ZlXOfmu1dEyn87D3RnyvHmc6bVkLGlo1AmepIO9+1Qt7yHQcu1cLBi0T/v+qSCsO3x4PITrOeMLa3tg97zDLG+IfAtsxffplOm05D/t0uG0YZXir7HJVJ2UfeNv0MB9pE2GSHHQCJv8BOAdGE1BAZEEYt33J1bVYxEQdExllEW0IVeIa6uHj4SKwaBrExW0iGjEs7RijURda3GJh16l0Hi41EnZb6TrT8iFZPwYQJsc5RNzKXnhGZDCiNwwCtad6P0fAHO61V+Aa4EJmqSXtHeYwJecEcCll7R/teB4wyvTxo3hEAgVc67Q64FlpDAGusOJNZPO28jDxKR/8feY5tmYRBDDg9lvNScvsCTLiJ1fHdu86f+wyWqnW89N23+xhGtn2ou99SlHsog5qzeJ6UtwnS8AtSveu9yZE+UnqGmki5zs5Cmau4xLAH0NxeioaQEMdM0DqBmoZLSLARzFqOrUS2+8e+ETko/VQJnrMP3h6Z2C2AT4YzOsgGTUu71ghWsPAYExrp6nadazqqq4ec7fAE8ZbqL+rNkQvM8dS+//mc0ThL3MiOhipa8C3QtbHEXD3yDChCM0La2bYUgroTED5qs78BWZWc6kEDyk6eSfd9RGrJuKvH8upKxwfdlvZ3MvDESrIIqIpQxwxtX/pwT4PdgEtIBMfUlqloNrqZmFSMSJ2U05OrAcx5Qg3anwZD7Fbx1JLWysVBn6sqCtmExv52cC3tvYdGjIrbe0Y+wKupWS66kgzrMgJTR6gOF837LFY4zY472RE1ndP3AcddUWSTsYg3CstSFy7MV30azDUZLox1c0qS1DCT/8BqcobesY4TKIjd/pEaKP1bs0Uq9rC/hFbZjXNUXr7ZMyqwF46t6rOHlRWuxGuuKMrIwfaIuh9LnfLYHo9PS/0+wLTxzG7Zsc83tjF8ubYGe0fZf/Vf7gYtGIZLiFAzz8He7vS+wJV6X4CPDMgdOO0GUhCcawRHq4aAqsiknEFrHoY2RcpXv9vT6nHYKtE8SoecRNDDxIg5ApoAvPi43rcOltwIBZCuAG20bZTezpSrzGqYUVvqhJH2yLLPiAkLF1WoxV36ifvo0nlxMCh6ObbVjgaDKcxGyQQtiTWPzsPZhZMw1UOSEHtDEkRVi10C30wdzo5FQ7JWKdASS7bRQTcUS18UlUWbcGSeRi/Avv/Gjg6GDGvWesWczlNHWUxADXNsVzuQ1VZeAMnE3pv42REoHNE8rupMe3rxHQwGSFJDGtkc8fo1kJkemQlR01uauoP0mQlVTcaVfQcKcejpw9g2DwgKCB7ErpWK+sxoBTqTPFzASUcbOLA+hQnQXHSGC/V/yJfKh1MolQdMWlj5jc2Cz/dyMKpXttMH7EG3LcPwjYa7EwOK+305kwnJmBFwAjcZfJrIt9e7ianXuYGKs+SSTfCNk0+Iuv7wMP2+BpVIHbNM/a4GtY4Uwn0JORs8zFEJzfA9VMjGnTyCsweROcZT6ltGGOpGrt3PgoqNx0xMyL2J0lFOjTO4m8i5ysfPiDsQC4CVDcPtFtIGSYRWD2g4MygwkNXoLR+r9m2IuU6P9U58aUgcKdASPtQR0ZU7Pc0H1LHBa5Ue9n4Czk0ecLlIuhEHk222VOotSQ5WbVq7tZWE6hvYM0JoeBCIg8xp/WoK+h2K1exLiHGINEraBSgRqxaLx5cTalcMw+q+KwT2DNdLBw5p8p6IfFHeV0sygzjFFVdNmTkwmxiB8BWq9m2N1rZamVeaD4rNmV/GOLVwQQrPScCU7ZsBcPb+iKs6BP3RkzFZ2DtOVLc4VMiqTU58m42XO5gdXG3a+3suuy29y8iRM5R+vsCHEPxbcpc6mTTchvCUcJo38BlPBFnVMot2sZ2dlckLdF2S+8x5He6ciJSc7Ep/wFREyLOHUcBIFLEBuogAwVQQQNaHg4OVC0ABXYwx9Nex+oGFAADQ/8FKSDtxQJIr3woSQUHKh4CB0AEdBMm5AFUxcV1/8ixIjBAMlQByQGuIAASy8TgVTAeVnAeq0C23AN4qNEjv0YCAXAAFe3rxVQAGZI3bgDtKhMBA64AItq7AAYAMDAuZ6MADstABhwAygy6GBcKv8QxD8TJFLrEAVD/oRBQAcQlIUAHVUDEDhi/wT7bGQHEDqUgB+fED3RBBxBYAisGHgMhMCEDQkihCrjPZwhY+RzyUjQEGMQDBuqiBwrLABJu6gIEMyEGNCg1sFfVakDOAAkQNkADAMQQXOq1XisoRTI4cMcHKCXKEepBAwLEISB4YAEReEACfsfVz2Y723lSRkEWBHDH6gxABqsWvwNEYANoe/qsKx5BAmDqZCMEIQXm6cQA86nPb6qseDghACAYDPUXF0MFpwjE1mtwxXfaWgAycMQECurFfrQbBR54Eay13c4Q+iAmqTDFAFAgxxks+wILx+ZkI1DRCwNgBTSuvwIEHsQFJGAwGQfcCzgAEQwUQAvGDgbtU1HEFgRgAyIId6xNHuQT5XwEFUrYqksuxygEYMAB8kAFbCEBfxFby1M+2AwGUOQWFMQBGBdAKmQOMWbfIAMOIAH0LfodXKU4FQ67/wl+Om3E4awBSRDEK/8BMWfzfAQL/0ERZizJdicgAlzYOyFREALH4AcAzs1OAKrO288bE7pSrEQGCSw8xCarXdIJ4MQJ92EADFwA1ONnfmVRXm0HpWICCXgAZFEu2V/jYKzUy7gLA1JsFqXmeBowQtUBIMIzHtul0st3ZXke7Qo49QFic+vBJiHPElxldvoT5mWxMgAETH8GRXfGB1ARPh/b+x8BED5l+wIKHjcb+CEf83XyAs8YKhFiBup+ZQFj3WYHAtpIHD7cK8J95tk2uVJGfgBgfuMGAFjKVMTuPc+rbVLBLHoGMw8hf8PKEzGrbtwXK9nhKt/cexghfvgmxASvawZIXmJkf1LKfvscWtLBbBR4fRj/bKzf5xHZff8xAB4oACB4x3wH8jIHY8fM3HxjE30dV18Vr2sX8n0wD3/AGlg0r3838snBEckd6zFOscT498QpEtEAIMVGSNEu4NHDFuk6N8vbBz5PKAwhKADLR8c9GBpWKAAo8tZfSNYCvxxFWOGvFxUZOPwZSMUdGMRoeHwmV8cK/3wW28QnAsfbEQp3eNPY89EyOGEiUCA5ZMlSygIOIQ9pD9wkAypX29xYSC6hFQB34BvnD2IHVEapIH2hZWvtSHPveMnyrw714Bfxxysgy0UyUeYGcOZIEETSv+TkVHYCQOIFIuP0OMTHy0cWx+MnruMxoeOoIeOpSDcBIOUwROW0xOTj3OWlD+dgISPAKsfh2LELHCgPGK5LygjJYHgMRDjiTLfn9gjp+Ug5bxjhLesbWXHpAOrp/2Tkzzjl2Hh/5yLooHh+Aq5CczHp1hXnLP/jyuESOgLqYf/p+v/p6GcLBlnjw2gafazqUaXjDznnEakPE8DpFSnqSL/nIh7r2eBa2kMzSWfgQ5IPuMvor3Dt/wh5EQYAJMQK7g8CIRNrJhEAKDABHGAEZRYDKSEBfycA/P8COMQAL1BqNCBpWHlpOb8AbnYFHEBmA78DKFABwsYAT79qGurxavTxp0YDGuoDbCB6+/9rJRMBrxYAOUByJxcC2HZfLcUKAr8CG/MDPRMKKyErN2QL/EXqZBbHAScQAhG/bhNgYUBQA6sGAg9QASJAcT4gbkTK9BkXAsA2Aj5QeGYmA+4CdeQndCEwciYCbCEh7iseCz8pASkgehFA8QSrfjoTYXLBazPEfO44ZlLbWSATAtsAAxtQciYRADIAG+7LAxbz9QIAeO4GAiPcdAvBdAIQBCAgYwW/A7smADPqACEgdOsmAiLcek5Z+SRgdQRnfXNZchg0KxomY/oCbD4gAQgBYcASAPxFZgYgAfv/dA8wAT1gcyEhAxVQARFgcuqpeCJQAXMpdDIrACUQADUQABy/fKBWAR7EAl03Ag9QsM52cRBmehJAAyPEAicnIDnB80phYs6vbwzEx2CpEwFEE0+SIWIWNu7FKfUTHmKucAFB3/MCd3ILAHjFwW7ByO4ZBrAcvwPiJwIPYHspq/rYOfNOwACpAAQ+JgBy3Gte9uWgFQsD8QrmdvtqGBMY/7Y8CgsYE3krAgtYIe7BOQITHg6pUALKwGzC2HTHCXUoEAEiIQFBIQBCGQA8E/vGJvbYwKci4SBlCmEAIAQeYANLKwBIBnwoHWBWRHApEwoFIAEkcATZMyBJNmAmIO4SADMaVgMFJmVAUGV1yHIivxFKaBAc6gEwEwAOYGZGsa7SMHNT22Fj8wpbAGapHqO2SgQOJgE9IGNlNQw1HmLOEwMQcWMOVgAkEN/YxscK/f/5DlADHioBCjAAtmd6FgYDBdxj22kMcwpiZRMC0wp2JMRkGvEKGDBrQPcLo8Qf49h4IkoKXPHlGSoLBGdh7t9vNGYALvFRWlMfNKwlce427Rww92UmylRcK/9AakhfLzDhAiUX2iNZJeM0qSAiuKIRt2JkE+NTq9ZNwTFAZe8LIAt9HNFGMfVUM0uxNQszfbUKORvROxs8+kgTJyta7FVYNWIYqDRRKHvjU+8nzUMPRQu1w8dPU1tS6xyxdTGxJDNkWCYCpUQgPhoq+ftlPapKxfZlNCEuZc4szmNUE9rjJw89Z2XJUng+FEUPAPca3ARi/BEAUjAioTJbEbfHUXMdImW2CunjpTEeJyDcG+l7wPPOTMdLD9nEZOBQCqNQCu7LDKc6YUTbJyFwDsFgCrDjCgZLKyZA+wF1Iak7YMXjZU8GN/aq7bS0K6k7ZgRAA9k1Nj4jteZTRcOixxy7D/rjW8cxS8fIXKFAAmpUHfUUEfVREdVbDwoCG3VrQAERpboVKhFLyfGsC/r/A19gCvcAKz/cXbIZc6xLOqxrEd9SwEZbFDwTAQpgBbsQ+hEDfMIRW/87wD4eqffjvwWwAthwqgW4+wEbDy6huvUhASYgADtAAaFrCCoToT45oflwAPUgrCO4CSQxCBpMCR1sCCn6rOMgrPsQrP//oR8cG6wvOQkg6ho6uho8DAKhrKwabhoeuANaHgPFAAA7]]></data>
				<!-- data = Dateiinhalt base64-kodiert als string eingebunden / via php: string base64_encode ( string $str ) -->
			</file>
		</files>
	</data>
</xml>
';

	$igel = $this->webserviceRepository->getIgelLogin();
	$igel['action'] = 'KLIENTENNEUANLAGE';
	$igel['language'] = $LANG==1 ? 'EN' : 'DE';
	$igel['process'] = '7890';			//	gerade Nr. produziert error-ID = 0
//	$igel['process'] = '7891';			//	ungerade Nr. produziert error-ID = 1
//	$igel['process'] = '7891as';		//	nicht nummerisch produziert error-ID = 1
	$igel['interests_nr'] = '234';
	
	//
	$dataarray = array();
	$dataarray[] = array('ID' => 'COMPANY', 'type' => 'ADDRESS', 'fields' => array( array('ID' => 'firstname', 'data' => 'Vorname'), array('ID' => 'lastname', 'data' => 'Nachname') ));

	//
//	$data = $this->webserviceRepository->buildXML( array('igel' => $igel, 'formular' => $dataarray) );
	//
	$ws = $this->webserviceRepository->init($url);
	//
	$ws = $this->webserviceRepository->setPostFields($ws, $data);
	//
	$res = $this->webserviceRepository->call($ws);
	//
	$this->webserviceRepository->close($ws);

	//
	$res_error_ID = $res['error']['ID'];
	$res_error_info = $res['error']['info'];
	$res_message  = $res['message'];
	
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'url' => $url,
		'data' => $data,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'xmldata' => $res,
		'error_ID' => $res_error_ID,
		'error_info' => $res_error_info,
		'message' => $res_message,
	));
  }


  /**
   * action teststatus
   *
   * @return void
   */
  public function teststatusAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	//
	$url = 'https://portal.icon.at/igel/wsi/formular';

	//
	$igel = $this->webserviceRepository->getIgelLogin();
	$igel['action'] = 'KLIENTENNEUANLAGE-STATUSCHECK';
	$igel['language'] = $LANG==1 ? 'EN' : 'DE';
	$igel['process'] = '7890';			//	gerade Nr. produziert error-ID = 0
//	$igel['process'] = '7891';			//	ungerade Nr. produziert error-ID = 1
//	$igel['process'] = '7891as';		//	nicht nummerisch produziert error-ID = 1
	$igel['interests_nr'] = '234';

	//
	$data = $this->webserviceRepository->buildXML( array('igel' => $igel) );
	//
	$ws = $this->webserviceRepository->init($url);
	//
	$ws = $this->webserviceRepository->setPostFields($ws, $data);
	//
	$res = $this->webserviceRepository->call($ws);
	//
	$this->webserviceRepository->close($ws);

	//
	$res_error_ID = $res['error']['ID'];
	$res_error_info = $res['error']['info'];
	$res_message  = $res['message'];
	
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'url' => $url,
		'data' => $data,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'xmldata' => $res,
		'error_ID' => $res_error_ID,
		'error_info' => $res_error_info,
		'message' => $res_message,
	));
  }
  

}

?>