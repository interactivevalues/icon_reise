<?php

if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1',
  array(
    'Formular' => 'index, used, company, private, saveCompany, savePrivate, done, fail, testformular, teststatus',
  ),
  // non-cacheable actions
  array(
    'Formular' => 'index, used, company, private, saveCompany, savePrivate, done, fail, testformular, teststatus',
  )
);

?>
