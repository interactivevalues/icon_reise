<?php
defined('TYPO3_MODE') or die();

$_EXTKEY = 'icon_neuklientenanlage';
$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);

/*
  INCLUDE PLUGINS
*/
  //  Pi1
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
  $_EXTKEY,
  'Pi1',
  'ICON - Neuklientenanlage'
);

$pluginSignature = strtolower($extensionName) . '_pi1';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,recursive,select_key,pages';

?>