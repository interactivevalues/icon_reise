<?php
namespace NEXT\IconMailing\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_mailing
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FormUpdate extends \NEXT\IconMailing\Domain\Model\Form {

	// START: -------------- IGEL-ID ----------------

  /**
   * personNr
   *
   * @var \string
   */
  protected $personNr;


  /**
   * Sets the personNr
   *
   * @param \string $personNr
   * @return void
   */
  public function setPersonNr($personNr) {
    $this->personNr = $personNr;
  }

  /**
   * Returns the personNr
   *
   * @return \string $personNr
   */
  public function getPersonNr() {
    return $this->personNr;
  }

	// END: -------------- IGEL-ID ----------------

	// START: -------------- MAILING - INTERESTS ----------------
	
  /**
   * interestA1
   *
   * @var \string
   */
  protected $interestA1;

  /**
   * interestA2
   *
   * @var \string
   */
  protected $interestA2;

  /**
   * interestA3
   *
   * @var \string
   */
  protected $interestA3;

  /**
   * interestA4
   *
   * @var \string
   */
  protected $interestA4;

  /**
   * interestA5
   *
   * @var \string
   */
  protected $interestA5;


  /**
   * Sets the interestA1
   *
   * @param \string $interestA1
   * @return void
   */
  public function setInterestA1 ($interestA1) {
    $this->interestA1 = $interestA1;
  }

  /**
   * Returns the interestA1
   *
   * @return \string $interestA1
   */
  public function getInterestA1 () {
    return $this->interestA1=='1' ? 1 : 0;
  }

  /**
   * Sets the interestA2
   *
   * @param \string $interestA2
   * @return void
   */
  public function setInterestA2 ($interestA2) {
    $this->interestA2 = $interestA2;
  }

  /**
   * Returns the interestA2
   *
   * @return \string $interestA2
   */
  public function getInterestA2 () {
    return $this->interestA2=='1' ? 1 : 0;
  }

  /**
   * Sets the interestA3
   *
   * @param \string $interestA3
   * @return void
   */
  public function setInterestA3 ($interestA3) {
    $this->interestA3 = $interestA3;
  }

  /**
   * Returns the interestA3
   *
   * @return \string $interestA3
   */
  public function getInterestA3 () {
    return $this->interestA3=='1' ? 1 : 0;
  }

  /**
   * Sets the interestA4
   *
   * @param \string $interestA4
   * @return void
   */
  public function setInterestA4 ($interestA4) {
    $this->interestA4 = $interestA4;
  }

  /**
   * Returns the interestA4
   *
   * @return \string $interestA4
   */
  public function getInterestA4 () {
    return $this->interestA4=='1' ? 1 : 0;
  }

  /**
   * Sets the interestA5
   *
   * @param \string $interestA5
   * @return void
   */
  public function setInterestA5 ($interestA5) {
    $this->interestA5 = $interestA5;
  }

  /**
   * Returns the interestA5
   *
   * @return \string $interestA5
   */
  public function getInterestA5 () {
    return $this->interestA5=='1' ? 1 : 0;
  }


  /**
   * interestB1
   *
   * @var \string
   */
  protected $interestB1;

  /**
   * interestB2
   *
   * @var \string
   */
  protected $interestB2;


  /**
   * Sets the interestB1
   *
   * @param \string $interestB1
   * @return void
   */
  public function setInterestB1 ($interestB1) {
    $this->interestB1 = $interestB1;
  }

  /**
   * Returns the interestB1
   *
   * @return \string $interestB1
   */
  public function getInterestB1 () {
    return $this->interestB1=='1' ? 1 : 0;
  }

  /**
   * Sets the interestB2
   *
   * @param \string $interestB2
   * @return void
   */
  public function setInterestB2 ($interestB2) {
    $this->interestB2 = $interestB2;
  }

  /**
   * Returns the interestB2
   *
   * @return \string $interestB2
   */
  public function getInterestB2 () {
    return $this->interestB2=='1' ? 1 : 0;
  }


  /**
   * interestC1
   *
   * @var \string
   */
  protected $interestC1;


  /**
   * Sets the interestC1
   *
   * @param \string $interestC1
   * @return void
   */
  public function setInterestC1 ($interestC1) {
    $this->interestC1 = $interestC1;
  }

  /**
   * Returns the interestC1
   *
   * @return \string $interestC1
   */
  public function getInterestC1 () {
    return $this->interestC1=='1' ? 1 : 0;
  }


  /**
   * interestD1
   *
   * @var \string
   */
  protected $interestD1;


  /**
   * Sets the interestD1
   *
   * @param \string $interestD1
   * @return void
   */
  public function setInterestD1 ($interestD1) {
    $this->interestD1 = $interestD1;
  }

  /**
   * Returns the interestD1
   *
   * @return \string $interestD1
   */
  public function getInterestD1 () {
    return $this->interestD1=='1' ? 1 : 0;
  }


  /**
   * interestE1
   *
   * @var \string
   */
  protected $interestE1;


  /**
   * Sets the interestE1
   *
   * @param \string $interestE1
   * @return void
   */
  public function setInterestE1 ($interestE1) {
    $this->interestE1 = $interestE1;
  }

  /**
   * Returns the interestE1
   *
   * @return \string $interestE1
   */
  public function getInterestE1 () {
    return $this->interestE1=='1' ? 1 : 0;
  }


  /**
   * interestF1
   *
   * @var \string
   */
  protected $interestF1;

  /**
   * interestF2
   *
   * @var \string
   */
  protected $interestF2;


  /**
   * Sets the interestF1
   *
   * @param \string $interestF1
   * @return void
   */
  public function setInterestF1 ($interestF1) {
    $this->interestF1 = $interestF1;
  }

  /**
   * Returns the interestF1
   *
   * @return \string $interestF1
   */
  public function getInterestF1 () {
    return $this->interestF1=='1' ? 1 : 0;
  }

  /**
   * Sets the interestF2
   *
   * @param \string $interestF2
   * @return void
   */
  public function setInterestF2 ($interestF2) {
    $this->interestF2 = $interestF2;
  }

  /**
   * Returns the interestF2
   *
   * @return \string $interestF2
   */
  public function getInterestF2 () {
    return $this->interestF2=='1' ? 1 : 0;
  }

	// END: -------------- MAILING - INTERESTS ----------------

}
?>