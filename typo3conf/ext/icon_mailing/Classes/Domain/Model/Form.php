<?php
namespace NEXT\IconMailing\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_mailing
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Form extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	// START: -------------- PERSON ----------------

  /**
   * gender
   *
   * @var \string
   */
  protected $gender;

  /**
   * title
   *
   * @var \string
   */
  protected $title;
  
  /**
   * firstname
   *
   * @var \string
   * @ v alidate NotEmpty
   */
  protected $firstname;

  /**
   * lastname
   *
   * @var \string
   * @ v alidate NotEmpty
   */
  protected $lastname;

  /**
   * titleAfter
   *
   * @var \string
   */
  protected $titleAfter;

  /**
   * function
   *
   * @var \string
   */
  protected $function;

  /**
   * email
   *
   * @var \string
   */
  protected $email;


  /**
   * Sets the gender
   *
   * @param \string $gender
   * @return void
   */
  public function setGender($gender) {
    $this->gender = $gender;
  }

  /**
   * Returns the gender
   *
   * @return \string $gender
   */
  public function getGender() {
    return $this->gender;
  }

  /**
   * Sets the title
   *
   * @param \string $title
   * @return void
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Returns the title
   *
   * @return \string $title
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Sets the firstname
   *
   * @param \string $firstname
   * @return void
   */
  public function setFirstname($firstname) {
    $this->firstname = $firstname;
  }

  /**
   * Returns the firstname
   *
   * @return \string $firstname
   */
  public function getFirstname() {
    return $this->firstname;
  }

  /**
   * Sets the lastname
   *
   * @param \string $lastname
   * @return void
   */
  public function setLastname($lastname) {
    $this->lastname = $lastname;
  }

  /**
   * Returns the lastname
   *
   * @return \string $lastname
   */
  public function getLastname() {
    return $this->lastname;
  }

  /**
   * Sets the titleAfter
   *
   * @param \string $titleAfter
   * @return void
   */
  public function setTitleAfter($titleAfter) {
    $this->titleAfter = $titleAfter;
  }

  /**
   * Returns the titleAfter
   *
   * @return \string $titleAfter
   */
  public function getTitleAfter() {
    return $this->titleAfter;
  }

  /**
   * Sets the function
   *
   * @param \string $function
   * @return void
   */
  public function setFunction($function) {
    $this->function = $function;
  }

  /**
   * Returns the function
   *
   * @return \string $function
   */
  public function getFunction() {
    return $this->function;
  }

  /**
   * Sets the email
   *
   * @param \string $email
   * @return void
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * Returns the email
   *
   * @return \string $email
   */
  public function getEmail() {
    return $this->email;
  }

	// END: -------------- PERSON ----------------

	// START: -------------- COMPANY ----------------

  /**
   * companyName
   *
   * @var \string
   */
  protected $companyName;

  /**
   * Sets the companyName
   *
   * @param \string $companyName
   * @return void
   */
  public function setCompanyName($companyName) {
    $this->companyName = $companyName;
  }

  /**
   * Returns the companyName
   *
   * @return \string $companyName
   */
  public function getCompanyName() {
    return $this->companyName;
  }


	// END: -------------- COMPANY ----------------

	// START: -------------- ADDRESS ----------------

  /**
   * street
   *
   * @var \string
   */
  protected $street;

  /**
   * streetNr
   *
   * @var \string
   */
  protected $streetNr;

  /**
   * zip
   *
   * @var \string
   */
  protected $zip;

  /**
   * city
   *
   * @var \string
   */
  protected $city;


  /**
   * Sets the street
   *
   * @param \string $street
   * @return void
   */
  public function setStreet($street) {
    $this->street = $street;
  }

  /**
   * Returns the street
   *
   * @return \string $street
   */
  public function getStreet() {
    return $this->street;
  }

  /**
   * Sets the streetNr
   *
   * @param \string $streetNr
   * @return void
   */
  public function setStreetNr($streetNr) {
    $this->streetNr = $streetNr;
  }

  /**
   * Returns the streetNr
   *
   * @return \string $streetNr
   */
  public function getStreetNr() {
    return $this->streetNr;
  }

  /**
   * Sets the zip
   *
   * @param \string $zip
   * @return void
   */
  public function setZip($zip) {
    $this->zip = $zip;
  }

  /**
   * Returns the zip
   *
   * @return \string $zip
   */
  public function getZip() {
    return $this->zip;
  }

  /**
   * Sets the city
   *
   * @param \string $city
   * @return void
   */
  public function setCity($city) {
    $this->city = $city;
  }

  /**
   * Returns the city
   *
   * @return \string $city
   */
  public function getCity() {
    return $this->city;
  }

	// END: -------------- ADDRESS ----------------

}
?>