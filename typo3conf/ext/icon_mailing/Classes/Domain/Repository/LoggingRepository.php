<?php
namespace NEXT\IconMailing\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_mailing
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class LoggingRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * @var string
	 */
	protected $logFolder = 'fileadmin/IGEL/mailing-service/';


	/**
	 * doLog
	 *
	 * @param string $action
	 * @param string $fName
	 * @param mixed $fData
	 * @return void
	 */
	public function doLog ($action, $fName, $fData) {
		//
		$t_linie = $this->getTrennlinie();
		$fBody = $t_linie;
		$fBody .= 'TIMESTAMP:        ' . time() . ' | ' . date('Y-m-d H:i:s') . PHP_EOL;
		$fBody .= 'ACTION:           ' . $action . PHP_EOL;
//		$fBody .= $this->getTrennlinie(40);
		$fBody .= 'DATA:' . PHP_EOL;
		$fBody .= $this->getTrennlinie(5);
		$fBody .= PHP_EOL;
		$fBody .= $fData;
		$fBody .= PHP_EOL . PHP_EOL;
		//
		$fp_file = $this->logFolder;
		$fp_file .= $fName .'_'. date('Y-m-d') .  '.txt';
		$fp = fopen($fp_file, 'a+');
		fwrite($fp, $fBody);
		fclose($fp);
		//
		return;
	}
	
	
	/**
	 * doLogResultIGEL
	 *
	 * @param string $action
	 * @param string $fName
	 * @param mixed $fData
	 * @return void
	 */
	public function doLogResultIGEL ($action, $fName, $fData) {
		//
		$t  = 'ERROR-ID:      ' . $fData['error']['ID'] . PHP_EOL;
		$t .= 'ERROR-INFO:    ' . $fData['error']['info'] . PHP_EOL;
		$t .= 'MESSAGE:       ' . $fData['message'] . PHP_EOL;
		$t .= PHP_EOL;
		$t .= 'IGEL - XML:' . PHP_EOL . $fData['RAW']['data'] . PHP_EOL;
		
		//
		return $this->doLog($action, $fName, $t);
	}
	
	
	/**
	 * sendErrorInfo
	 *
	 * @param string $action
	 * @param mixed $data
	 * @param string $personNr
	 * return void
	 */
	public function sendErrorInfo ($action, $personNr, $data) {
		//
		$m_sender = array( 'webserver@icon.at' => 'icon.at' );
		$m_recipients = array( 'claudia.hois@icon.at' => 'Claudia Hois', 'r.schmoller@next-linz.com' => 'smo' );
//		$m_recipients = array( 'r.schmoller@next-linz.com' => 'smo' );
		$m_subject = 'ICON-MAILING: FEHLER bei Datenübergabe an IGEL!';
		$m_charset = $GLOBALS['TSFE']->metaCharset;
		//
		$mailBody = '';
		$mailBody .= $m_subject . PHP_EOL;
		$mailBody .= PHP_EOL;
		$mailBody .= 'TIMESTAMP:     ' . date('Y-m-d H:i:s') . PHP_EOL;
		$mailBody .= 'ACTION:        ' . $action . PHP_EOL;
		$mailBody .= 'PERSON-NR:     ' . $personNr . PHP_EOL;
		$mailBody .= PHP_EOL;
		$mailBody .= 'ERROR-ID:      ' . $data['error']['ID'] . PHP_EOL;
		$mailBody .= 'ERROR-INFO:    ' . $data['error']['info'] . PHP_EOL;
		$mailBody .= 'ERROR-ID:    ' . $data['message'] . PHP_EOL;
		$mailBody .= PHP_EOL;
		$mailBody .= 'IGEL - XML:' . PHP_EOL . $data['RAW']['data'] . PHP_EOL;
		//
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		$mail
		  ->setTo( $m_recipients )
		  ->setFrom( $m_sender )
		  ->setSubject( $m_subject )
		  ->setCharset( $m_charset );
		//
		//$mail->setCc($m_ccArray);
		//$mail->setBcc($m_bccArray);
		//      $message->setReturnPath($cObj->cObjGetSingle($conf[$type . '.']['overwrite.']['returnPath'], $conf[$type . '.']['overwrite.']['returnPath.']));
		//      $mail->setReplyTo($replyArray);
		$mail->setBody($mailBody, 'text/plain');
		$mail->send();
		$m_result = $mail->isSent();
		//
		return;
	}
	
	
	/**
	 * getTrennlinie
	 *
	 * @param integer maxChars
	 * @param string sign
	 * return string
	 */
	private function getTrennlinie ($maxChars=0, $sign='-') {
		//
		if( $maxChars < 1 || $maxChars == false || $maxChars === false ){
			$maxChars = 80;
		}
		//
		$t = '';
		for($i=0; $i<$maxChars; $i++ ){ $t.=$sign; }
		$t .= PHP_EOL;
		return $t;
	}
}
?>