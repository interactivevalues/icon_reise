<?php
namespace NEXT\IconMailing\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_mailing
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FormularRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {


  /**
   * getDataArrayRegister
   *
   * @param \string $process
   * @param \NEXT\IconMailing\Domain\Model\FormSubscribe $data
   * @return \mixed
   */
  public function getDataArraySubscribe (\NEXT\IconMailing\Domain\Model\FormSubscribe $data) {
	//
	$dataarray = array();

	//	SUBSCRIBER
	$f_sub = array();
	$f_sub[] = array('ID' => 'Salutation', 'data' => $data->getGender() );
	$f_sub[] = array('ID' => 'Title', 'data' => $data->getTitle() );
	$f_sub[] = array('ID' => 'Firstname', 'data' => $data->getFirstname() );
	$f_sub[] = array('ID' => 'Lastname', 'data' => $data->getLastname() );
	$f_sub[] = array('ID' => 'Title_After', 'data' => $data->getTitleAfter() );
	$f_sub[] = array('ID' => 'Position_Function', 'data' => $data->getFunction() );
	$f_sub[] = array('ID' => 'Email', 'data' => $data->getEmail() );
	$f_sub[] = array('ID' => 'Street', 'data' => $data->getStreet() );
	$f_sub[] = array('ID' => 'Street_Nr', 'data' => $data->getStreetNr() );
	$f_sub[] = array('ID' => 'Zip', 'data' => $data->getZip() );
	$f_sub[] = array('ID' => 'City', 'data' => $data->getCity() );
	//
	$dataarray[] = array('ID' => 'MAILING-SUBSCRIBER', 'type' => 'ADDRESS', 'fields' => $f_sub);

	//
	return $dataarray;
  }
  
  /**
   * getDataArrayUpdate
   *
   * @param \string $process
   * @param \NEXT\IconMailing\Domain\Model\FormUpdate $data
   * @return \mixed
   */
  public function getDataArrayUpdate (\NEXT\IconMailing\Domain\Model\FormUpdate $data) {
	//
	$dataarray = array();

	//	convert special HTML-Characters: & < > ' "
	$t_firstname = htmlspecialchars( $data->getFirstname() );
	$t_lastname = htmlspecialchars( $data->getLastname() );
	$t_company = htmlspecialchars( $data->getCompanyName() );
	$t_street = htmlspecialchars( $data->getStreet() );
	$t_streetNr = htmlspecialchars( $data->getStreetNr() );
	$t_zip = htmlspecialchars( $data->getZip() );
	$t_city = htmlspecialchars( $data->getCity() );
	
	//	SUBSCRIBER
	$f_sub = array();
	$f_sub[] = array('ID' => 'Salutation', 'data' => $data->getGender() );
	$f_sub[] = array('ID' => 'Title', 'data' => $data->getTitle() );
	$f_sub[] = array('ID' => 'Firstname', 'data' => $t_firstname );
	$f_sub[] = array('ID' => 'Lastname', 'data' => $t_lastname );
	$f_sub[] = array('ID' => 'Title_After', 'data' => $data->getTitleAfter() );
	$f_sub[] = array('ID' => 'Company_Name', 'data' =>  $t_company );
	$f_sub[] = array('ID' => 'Position_Function', 'data' => $data->getFunction() );
	$f_sub[] = array('ID' => 'Email', 'data' => $data->getEmail() );
	$f_sub[] = array('ID' => 'Street', 'data' => $t_street );
	$f_sub[] = array('ID' => 'Street_Nr', 'data' => $t_streetNr );
	$f_sub[] = array('ID' => 'Zip', 'data' => $t_zip );
	$f_sub[] = array('ID' => 'City', 'data' => $t_city );
	//
	$dataarray[] = array('ID' => 'MAILING-SUBSCRIBER', 'type' => 'ADDRESS', 'fields' => $f_sub);

	//	INTERESTS
	$f_int = array();
	//
/*  */
	$f_int[] = array('ID' => 'internationalesteuerthemen_auslandsentsendungen', 'data' => $data->getInterestA1() );
	$f_int[] = array('ID' => 'internationalesteuerthemen_internationaleprojekte', 'data' => $data->getInterestA2() );
	$f_int[] = array('ID' => 'internationalesteuerthemen_laenderaz', 'data' => $data->getInterestA3() );
	$f_int[] = array('ID' => 'internationalesteuerthemen_verrechnungspreise', 'data' => $data->getInterestA4() );
	$f_int[] = array('ID' => 'internationalesteuerthemen_quellensteuern', 'data' => $data->getInterestA5() );

	$f_int[] = array('ID' => 'nationalessteuerrecht_unternehmensbesteuerunginoesterreich', 'data' => $data->getInterestB1() );
	$f_int[] = array('ID' => 'nationalessteuerrecht_forschungspraemie', 'data' => $data->getInterestB2() );

	$f_int[] = array('ID' => 'umsatzsteuer_nationalundinternational', 'data' => $data->getInterestC1() );

	$f_int[] = array('ID' => 'mergersaquisitions_unternehmenstransaktionen', 'data' => $data->getInterestD1() );

	$f_int[] = array('ID' => 'wirtschaftspruefungbilanzierung_wirtschaftspruefungbilanzierung', 'data' => $data->getInterestE1() );

	$f_int[] = array('ID' => 'einladungen_taxacademy', 'data' => $data->getInterestF1() );
	$f_int[] = array('ID' => 'einladungen_galerie', 'data' => $data->getInterestF2() );
/* */
	//
	$dataarray[] = array('ID' => 'MAILING-INTERESTS', 'type' => 'OTHERS', 'fields' => $f_int);

	//
	return $dataarray;
  }
  

  /**
   * importDataUpdate
   *
   * @param \string $xml
   * @param \NEXT\IconMailing\Domain\Model\FormUpdate $formdata
   * @return \NEXT\IconMailing\Domain\Model\FormUpdate $formdata
   */
  public function importDataUpdate ($xml, \NEXT\IconMailing\Domain\Model\FormUpdate $formdata) {
	//
	$xmldata = simplexml_load_string($xml);
//	print_r($xmldata);
	//
	$xmlarray = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($res['RAW']['data']);
	//
	$fieldsets = $xmldata->data->formular->fieldset;
	//	MAILING-SUBSCRIBER
	foreach( $fieldsets[0]->fields->field as $field ){
		$fID = $field->ID;
		$fdata = $field->data;
//		  print_r('fID:_' . $fID . '_| data: ' . $fdata);
		switch ($fID) {
			case 'Salutation':			$formdata->setGender($fdata); break;
			case 'Title': 				$formdata->setTitle( utf8_decode($fdata) ); break;
			case 'Firstname':			$formdata->setFirstname($fdata); break;
			case 'Lastname': 			$formdata->setLastname($fdata); break;
			case 'Title_After': 		$formdata->setTitleAfter( utf8_decode($fdata) ); break;
			case 'Company_Name': 		$formdata->setCompanyName($fdata); break;
			case 'Position_Function':	$formdata->setFunction( utf8_decode($fdata) ); break;
			case 'Street': 				$formdata->setStreet($fdata); break;
			case 'Street_Nr': 			$formdata->setStreetNr($fdata); break;
			case 'Zip': 				$formdata->setZip($fdata); break;
			case 'City': 				$formdata->setCity($fdata); break;
			case 'Email': 				$formdata->setEmail($fdata); break;
		}
	}
	//	INTERESTS
/* */
	foreach( $fieldsets[1]->fields->field as $field ){
		$fID = $field->ID;
		$fdata = $field->data;
		$fdata = utf8_decode($fdata);
//		  print_r('fID:_' . $fID . '_| data: ' . $fdata);
		switch ($fID) {
			//
			case 'internationalesteuerthemen_auslandsentsendungen':					$formdata->setInterestA1($fdata); break;
			case 'internationalesteuerthemen_internationaleprojekte': 				$formdata->setInterestA2($fdata); break;
			case 'internationalesteuerthemen_laenderaz':							$formdata->setInterestA3($fdata); break;
			case 'internationalesteuerthemen_verrechnungspreise': 					$formdata->setInterestA4($fdata); break;
			case 'internationalesteuerthemen_quellensteuern': 						$formdata->setInterestA5($fdata); break;
			//
			case 'nationalessteuerrecht_unternehmensbesteuerunginoesterreich': 		$formdata->setInterestB1($fdata); break;
			case 'nationalessteuerrecht_forschungspraemie': 						$formdata->setInterestB2($fdata); break;
			//
			case 'umsatzsteuer_nationalundinternational': 							$formdata->setInterestC1($fdata); break;
			//
			case 'mergersaquisitions_unternehmenstransaktionen': 					$formdata->setInterestD1($fdata); break;
			//
			case 'wirtschaftspruefungbilanzierung_wirtschaftspruefungbilanzierung': $formdata->setInterestE1($fdata); break;
			//
			case 'einladungen_taxacademy': 											$formdata->setInterestF1($fdata); break;
			case 'einladungen_galerie': 											$formdata->setInterestF2($fdata); break;
		}
	}
/* */
	//
	return $formdata;
  }


  /**
   * checkIsUsed
   *
   * @param \string $process
   * @param \string $interested
   * @return \boolean
   */
  public function checkIsUsed ($process, $interested) {
	//
	return $interested == 'USED';
  }
  
  
  /**
   * sends Email to user 
   *
   * @param array @mailCFG
   * @param string $mailBody
   * @return
   */
  public function deliverMail ($mailCFG, $mailBody) {
	$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
	$mail->setTo(array( $mailCFG['toEmail'] => $mailCFG['toName'] ))
		->setFrom(array( $mailCFG['fromEmail'] => $mailCFG['fromName'] ))
		->setSubject( $mailCFG['subject'] )
		->setCharset( $mailCFG['charset'] );
	//
	//$mail->setCc($m_ccArray);
	//$mail->setBcc($m_bcc);
	//$message->setReturnPath($cObj->cObjGetSingle($conf[$type . '.']['overwrite.']['returnPath'], $conf[$type . '.']['overwrite.']['returnPath.']));
	//$mail->setReplyTo($replyArray);
	$mail->setBody($mailBody, $mailCFG['contentType']);
	$mail->send();
	return $mail->isSent();
  }
  
}
?>