<?php
namespace NEXT\IconMailing\Validation\Validator;

/***************************************************************
* Copyright notice
*
* (c) 2016 robert`smo´ schmoller <r.schmoller@next-linz.com>
*
* All rights reserved
*
* This script is part of the TYPO3 project. The TYPO3 project is
* free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* The GNU General Public License can be found at
* http://www.gnu.org/copyleft/gpl.html.
*
* This script is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
* Formdata validator
*
* @package icon_mailing
* @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
*
*/
class FormSubscribeValidator extends \NEXT\IconMailing\Validation\Validator\FormValidator {

	/**
	 * Returns TRUE, if the given property ($value) is a valid.
	 *
	 * Otherwise, it is FALSE.
	 *
	 * @param mixed $formdata The value that should be validated
	 * @return boolean TRUE if the value is valid, FALSE if an error occured
	 * @api
	 */
	protected function isValid($formdata) {
		$success = TRUE;
		//	validate SUBSCRIBER
		$success = $this->validateSubscriber($formdata, $success);
		//
		return $success;	
	}
}
?>