<?php
namespace NEXT\IconMailing\Validation\Validator;

/***************************************************************
* Copyright notice
*
* (c) 2016 robert`smo´ schmoller <r.schmoller@next-linz.com>
*
* All rights reserved
*
* This script is part of the TYPO3 project. The TYPO3 project is
* free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* The GNU General Public License can be found at
* http://www.gnu.org/copyleft/gpl.html.
*
* This script is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
* Formdata validator
*
* @package icon_mailing
* @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
*
*/
class FormUnsubscribeValidator extends \NEXT\IconMailing\Validation\Validator\FormValidator {

	/**
	 * Returns TRUE, if the given property ($value) is a valid.
	 *
	 * Otherwise, it is FALSE.
	 *
	 * @param mixed $formdata The value that should be validated
	 * @return boolean TRUE if the value is valid, FALSE if an error occured
	 * @api
	 */
	protected function isValid($formdata) {
		$success = TRUE;
		$success = FALSE;
		//	validate bacis info-set
//		$success = $this->validateBasics($formdata, $success);
		//	validate Tax Office
//		$success = $this->validateTaxOffice($formdata, $succcess);
		//	validate Fileuploads
//		$success = $this->validateFileuploads($succcess);
		//
		return $success;	
	}
	
	/**
	 * validates company, firstname, lastname, email
	 *
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateBasics ($formdata, $success){
		$isGender = strlen($formdata->getGender()) > 1;
		$isFN = strlen($formdata->getFirstname()) > 1;
		$isLN = strlen($formdata->getLastname()) > 1;
//		$isFunction = strlen($formdata->getFunction()) > 1;
		$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail());
		$isStreet = strlen($formdata->getStreet()) > 1;
		$isStreetNr = strlen($formdata->getStreetNr()) > 0;
		$isZip = strlen($formdata->getZip()) > 3;
		$isCity = strlen($formdata->getCity()) > 1;
		$isCountry = strlen($formdata->getCountry()) > 1;
		//
		if( !$isGender ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Anrede nicht gewählt!', time());
			$this->result->forProperty('gender')->addError($error);
			$success = FALSE;
		}
		if( !$isFN ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Vorname nicht ausgefüllt!', time());
			$this->result->forProperty('firstname')->addError($error);
			$success = FALSE;
		}
		if( !$isLN ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Nachname nicht ausgefüllt!', time());
			$this->result->forProperty('lastname')->addError($error);
			$success = FALSE;
		}
/*
		if( !$isFunction ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Position/Funktion nicht ausgefüllt!', time());
			$this->result->forProperty('function')->addError($error);
			$success = FALSE;
		}
*/
		if( !$isEmail ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('E-Mailadresse nicht ausgefüllt!', time());
			$this->result->forProperty('email')->addError($error);
			$success = FALSE;
		}
		if( !$isStreet ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Straße nicht ausgefüllt!', time());
			$this->result->forProperty('street')->addError($error);
			$success = FALSE;
		}
		if( !$isStreetNr ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Hausnummer nicht ausgefüllt!', time());
			$this->result->forProperty('streetNr')->addError($error);
			$success = FALSE;
		}
		if( !$isZip ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('PLZ nicht ausgefüllt!', time());
			$this->result->forProperty('zip')->addError($error);
			$success = FALSE;
		}
		if( !$isCity ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Stadt nicht ausgefüllt!', time());
			$this->result->forProperty('city')->addError($error);
			$success = FALSE;
		}
		if( !$isCountry ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Land nicht ausgefüllt!', time());
			$this->result->forProperty('country')->addError($error);
			$success = FALSE;
		}
		//
		return $success;
	}

	/*
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateTaxOffice ($formdata, $success){
		//
		if( $formdata->getTaxOfficeUsed1() ){
			$isTO = strlen($formdata->getTaxOffice1()) > 1;
			$isId = strlen($formdata->getTaxId1()) > 1;
			$isUid = strlen($formdata->getTaxUid1()) > 1;
			//
			if(!$isTO || !$isId || !$isUid ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error('Finanzamt nicht korret angegeben!', time());
				$this->result->forProperty('taxOfficeUsed1')->addError($error);
				$success = FALSE;
				//
				if( !$isTO ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Finanzamt nicht angegeben!', time());
					$this->result->forProperty('taxOffice1')->addError($error);
				}
				if( !$isId ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('Steuernummer nicht angegeben!', time());
					$this->result->forProperty('taxId1')->addError($error);
				}
				if( !$isUid ){
					$error = new \TYPO3\CMS\Extbase\Validation\Error('UID-Nr. nicht angegeben!', time());
					$this->result->forProperty('taxUid1')->addError($error);
				}
			}
		}
		//
		return $success;
	}


}
?>