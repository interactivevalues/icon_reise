<?php
namespace NEXT\IconMailing\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_mailing
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ManageController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
 
  /**
   * formularRepository
   *
   * @var \NEXT\IconMailing\Domain\Repository\FormularRepository
   * @inject
   */
  protected $formularRepository;
    
  /**
   * webserviceRepository
   *
   * @var \NEXT\IconIgelconnector\Domain\Repository\WebserviceRepository
   * @inject
   */
  protected $webserviceRepository;

  /**
   * loggingRepository
   *
   * @var \NEXT\IconMailing\Domain\Repository\LoggingRepository
   * @inject
   */
  protected $loggingRepository;


  /**
   * action index
   *
   * @return void
   */
  public function indexAction() {
	$LANG = $GLOBALS['TSFE']->sys_language_uid;
	//
//	$personNr = '8E1C4AA43D431380';
	$personNr = NULL;
	if( $this->request->hasArgument('personNr') ){
	   	$personNr = $this->request->getArgument('personNr');
	}

	$resLog = $this->loggingRepository->doLog('INDEX', $personNr, 'aufruf URL/Link!');
	//
	$wsURL = 'https://portal.icon.at/igel/wsi/formular';
	//
	$igel = $this->webserviceRepository->getIgelLogin();
	$igel['action'] = 'MAILING-CHECKSUBSCRIBER';
	$igel['language'] = $LANG==1 ? 'EN' : 'DE';
	$igel['person_nr'] = $personNr;
	//
	$data = $this->webserviceRepository->buildXML( array('igel' => $igel) );
	//
	$ws = $this->webserviceRepository->init($wsURL);
	//
	$ws = $this->webserviceRepository->setPostFields($ws, $data);
	//
	$resLog = $this->loggingRepository->doLog('MAILING-CHECKSUBSCRIBER - SUBMIT-DATA', $personNr, $this->webserviceRepository->anonymizeXML($data));
	//
	$res = $this->webserviceRepository->call($ws);
	//
	$this->webserviceRepository->close($ws);
	//
	$resLog = $this->loggingRepository->doLogResultIGEL('MAILING-CHECKSUBSCRIBER - RESULT', $personNr, $res);
	//
/**/
	//
	$res_error_ID = $res['error']['ID'];
	$res_error_info = $res['error']['info'];
	$res_message  = $res['message'];
/**/
/* */
	//
	if( $res_error_ID == 0 ){
		//
		$GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-check', serialize($res));
		$GLOBALS['TSFE']->fe_user->storeSessionData();
		//
		$this->redirect('form', null, null, array('personNr' => $personNr));
	} else {
/* */
		$this->view->assignMultiple(array(
			'lang' => $LANG,
			'data' => $data,
			'resp_data' => $res['RAW']['data'],
			'resp_error' => $res['RAW']['error'],
			'xmldata' => '',
			'error_ID' => $res_error_ID,
			'error_info' => $res_error_info,
			'message' => $res_message,
		));
/* */
	}
/* */
  }

  
  /**
   * action form
   *
   * @param \NEXT\IconMailing\Domain\Model\FormUpdate $formdata
   * @return void
   */
  public function formAction(\NEXT\IconMailing\Domain\Model\FormUpdate $formdata = NULL) {
	$LANG = $GLOBALS['TSFE']->sys_language_uid;
	$LANG_str = $LANG == 1 ? 'EN' : 'DE';
	//
	$xmldata = '';
	//	GET personNr FROM REQUEST / URL
   	$rqst_personNr = $this->request->getArgument('personNr');
	//
    if( $GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-check') ){
      $res = unserialize($GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-check'));
      $GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-check', '');
      $GLOBALS['TSFE']->fe_user->storeSessionData();
	  //
	  if( $formdata == NULL ){
		  $formdata = $this->objectManager->getEmptyObject('\NEXT\IconMailing\Domain\Model\FormUpdate');
	  }
	  //
	  $formdata = $this->formularRepository->importDataUpdate($res['RAW']['data'], $formdata);
	  //
	  $xmldata = $res['RAW']['data'];
	}
	//
	if( $formdata == NULL ){
/*
		$actionName = 'index';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array( 'personNr' => $personNr, );
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
*/
//		$formdata = $this->objectManager->getEmptyObject('\NEXT\IconMailing\Domain\Model\FormUpdate');
	} else {
		//
		$formdata->setPersonNr($rqst_personNr);
	}
	//
	$lst_titel = unserialize($this->webserviceRepository->getFileData('titel', $LANG_str));
	$lst_titel_nach = unserialize($this->webserviceRepository->getFileData('titel_nach', $LANG_str));
	$lst_function = unserialize($this->webserviceRepository->getFileData('beziehungen', $LANG_str));
	//
	$this->view->assignMultiple(array(
		'LANG' => $LANG,
		'xmldata' => $xmldata,
		'formdata' => $formdata,
		'rqst_personNr' => $rqst_personNr,
		'lst_titel' => $lst_titel,
		'lst_titel_nach' => $lst_titel_nach,
		'lst_function' => $lst_function,
	));
  }

  /**
   * action update
   *
   * @param \NEXT\IconMailing\Domain\Model\FormUpdate $formdata
   * @validate $formdata \NEXT\IconMailing\Validation\Validator\FormUpdateValidator
   * @return void
   */
  public function updateAction(\NEXT\IconMailing\Domain\Model\FormUpdate $formdata = NULL) {
	//
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$LANG_str = $LANG == 1 ? 'EN' : 'DE';

	//	GET personNr FROM REQUEST / URL
   	$rqst_personNr = $this->request->getArgument('personNr');

	//	-> check formdata
    if( $formdata==NULL ){
		//	-> show Form-Screen
/*	*/
		$actionName = 'index';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array( 'personNr' => $rqst_personNr, );
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
/* */
//		$this->redirect('done');
	}

	//
	$wsURL = 'https://portal.icon.at/igel/wsi/formular';
	//
	$igel = $this->webserviceRepository->getIgelLogin();
	$igel['action'] = 'MAILING-UPDATE';
	$igel['language'] = $LANG==1 ? 'EN' : 'DE';
	$igel['person_nr'] = $formdata->getPersonNr();
	//
	$dataarray = $this->formularRepository->getDataArrayUpdate($formdata);
	//
	$data = $this->webserviceRepository->buildXML( array('igel' => $igel, 'formular' => $dataarray) );
	//
	$resLog = $this->loggingRepository->doLog('MAILING-UPDATE - SUBMIT-DATA', $rqst_personNr, $this->webserviceRepository->anonymizeXML($data));
	//
	$ws = $this->webserviceRepository->init($wsURL);
	//
	$ws = $this->webserviceRepository->setPostFields($ws, $data);
	//
	$res = $this->webserviceRepository->call($ws);
	//
	$this->webserviceRepository->close($ws);
	//
	$resLog = $this->loggingRepository->doLogResultIGEL('MAILING-UPDATE - RESULT', $rqst_personNr, $res);
	//
/**/
	//
	$res_error_ID = $res['error']['ID'];
	$res_error_info = $res['error']['info'];
	$res_message  = $res['message'];
/**/

	//
	$GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-result', serialize($res));
	$GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-mode', 'update');
	$GLOBALS['TSFE']->fe_user->storeSessionData();
/* */

	//
	if( $res_error_ID == 0 ){
		$this->redirect('done');
	} else {
		//
		$this->loggingRepository->sendErrorInfo('MAILING-UPDATE - RESULT', $rqst_personNr, $res);
		//
		$this->redirect('fail');
	}
/* */
/* 
	//	ZUM TESTEN
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'data' => $data,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'xmldata' => '',
		'error_ID' => $res_error_ID,
		'error_info' => $res_error_info,
		'message' => $res_message,
	));
*/
  }

  /**
   * action unsubscribe
   *
   * @param \NEXT\IconMailing\Domain\Model\FormUnsubscribe $formunsub
   * @validate $formdata \NEXT\IconMailing\Validation\Validator\FormUnsubscribeValidator
   * @return void
   */
  public function unsubscribeAction(\NEXT\IconMailing\Domain\Model\FormUnsubscribe $formdata = NULL) {
	//
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$LANG_str = $LANG == 1 ? 'EN' : 'DE';
	//	-> check formdata
    if( $formdata==NULL ){
		//	-> show Form-Screen
/*		$actionName = 'index';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array();
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
*/
//		$this->redirect('done');
	}

	//
	$wsURL = 'https://portal.icon.at/igel/wsi/formular';
	//
	$igel = $this->webserviceRepository->getIgelLogin();
	$igel['action'] = 'MAILING-UNSUBSCRIBE';
	$igel['language'] = $LANG==1 ? 'EN' : 'DE';
	$igel['person_nr'] = $formdata->getPersonNr();
	//
	$data = $this->webserviceRepository->buildXML( array('igel' => $igel) );
	//
	$ws = $this->webserviceRepository->init($wsURL);
	//
	$ws = $this->webserviceRepository->setPostFields($ws, $data);
	//
	$resLog = $this->loggingRepository->doLog('MAILING-UNSUBSCRIBE - SUBMIT-DATA', $formdata->getPersonNr(), $this->webserviceRepository->anonymizeXML($data));
	//
	$res = $this->webserviceRepository->call($ws);
	//
	$this->webserviceRepository->close($ws);
	//
	$resLog = $this->loggingRepository->doLogResultIGEL('MAILING-UNSUBSCRIBE - RESULT', $formdata->getPersonNr(), $res);
	//
/**/
	//
	$res_error_ID = $res['error']['ID'];
	$res_error_info = $res['error']['info'];
	$res_message  = $res['message'];
/**/

	//
	$GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-result', serialize($res));
	$GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-mode', 'unsubscribe');
	$GLOBALS['TSFE']->fe_user->storeSessionData();
/* */

	//
	if( $res_error_ID == 0 ){
		$this->redirect('done');
	} else {
		//
		$this->loggingRepository->sendErrorInfo('MAILING-UNSUBSCRIBE - RESULT', $formdata->getPersonNr(), $res);
		//
		$this->redirect('fail');
	}
/* */
/*
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'data' => $data,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'xmldata' => '',
		'error_ID' => $res_error_ID,
		'error_info' => $res_error_info,
		'message' => $res_message,
	));
*/
  }

  /**
   * action done
   *
   * @return void
   */
  public function doneAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$res = array();
	$mode = '';
	//
    if( $GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-result') ){
      $res = unserialize($GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-result'));
	  $mode = $GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-mode');
      $GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-result', '');
      $GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-mode', '');
      $GLOBALS['TSFE']->fe_user->storeSessionData();
	}
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'mode' => $mode,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'error_ID' => $res['error']['ID'],
		'error_info' => $res['error']['info'],
		'message' => $res['message'],
	));
  }
  
  /**
   * action fail
   *
   * @return void
   */
  public function failAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$res = array();
	$mode = '';
	//
    if( $GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-result') ){
      $res = unserialize($GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-result'));
	  $mode = $GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-mode');
      $GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-result', '');
      $GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-mode', '');
      $GLOBALS['TSFE']->fe_user->storeSessionData();
	}
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'mode' => $mode,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'error_ID' => $res['error']['ID'],
		'error_info' => $res['error']['info'],
		'message' => $res['message'],
	));
  }

}
