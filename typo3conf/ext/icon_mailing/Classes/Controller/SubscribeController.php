<?php
namespace NEXT\IconMailing\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_mailing
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SubscribeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

  /**
   * webserviceRepository
   *
   * @var \NEXT\IconIgelconnector\Domain\Repository\WebserviceRepository
   * @inject
   */
  protected $webserviceRepository;

  /**
   * formularRepository
   *
   * @var \NEXT\IconMailing\Domain\Repository\FormularRepository
   * @inject
   */
  protected $formularRepository;

  /**
   * loggingRepository
   *
   * @var \NEXT\IconMailing\Domain\Repository\LoggingRepository
   * @inject
   */
  protected $loggingRepository;
    
   
  /**
   * action index
   *
   * @param \NEXT\IconMailing\Domain\Model\FormSubscribe $formdata
   * @return void
   */
  public function indexAction(\NEXT\IconMailing\Domain\Model\FormSubscribe $formdata = NULL) {
	$LANG = $GLOBALS['TSFE']->sys_language_uid;
	$LANG_str = $LANG == 1 ? 'EN' : 'DE';
	//
/*
	$pageRenderer = $GLOBALS['TSFE']->getPageRenderer();
	$addJsMethod = 'addJs';
	$addJsMethod = 'addJsFooter';
	//	
	if( $this->extConf['footerJS'] == 1 ) {
		$addJsMethod = 'addJsFooter';
	}
	//
	$scripts[] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()) . 'Resources/Public/Scripts/mailworx.js';
	//	
	foreach ($scripts as $script) {
		$pageRenderer->{$addJsMethod . 'File'}($script);
	}
*/
	if( $formdata == NULL ){
		$formdata = $this->objectManager->getEmptyObject('\NEXT\IconMailing\Domain\Model\FormSubscribe');
	}

	$lst_titel = unserialize($this->webserviceRepository->getFileData('titel', $LANG_str));
	$lst_titel_nach = unserialize($this->webserviceRepository->getFileData('titel_nach', $LANG_str));
	$lst_function = unserialize($this->webserviceRepository->getFileData('beziehungen', $LANG_str));
	//
	$this->view->assignMultiple(array(
		'LANG' => $LANG,
		'formdata' => $formdata,
		'lst_titel' => $lst_titel,
		'lst_titel_nach' => $lst_titel_nach,
		'lst_function' => $lst_function,
	));

  }

  /**
   * action save
   *
   * @param \NEXT\IconMailing\Domain\Model\FormSubscribe $formdata
   * @validate $formdata \NEXT\IconMailing\Validation\Validator\FormSubscribeValidator
   * @return void
   */
  public function saveAction(\NEXT\IconMailing\Domain\Model\FormSubscribe $formdata = NULL) {
	//
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$LANG_str = $LANG == 1 ? 'EN' : 'DE';
	//	-> check formdata
    if( $formdata==NULL ){
		//	-> show Form-Screen
/*		$actionName = 'index';
		$controllerName = NULL;
		$extensionName = NULL;
		$arguments = array();
		$this->redirect($actionName, $controllerName, $extensioName, $arguments);
*/
		$this->redirect();
	}

	//
	$wsURL = 'https://portal.icon.at/igel/wsi/formular';
	//
	$igel = $this->webserviceRepository->getIgelLogin();
	$igel['action'] = 'MAILING-REGISTER';
	$igel['language'] = $LANG==1 ? 'EN' : 'DE';
	//
	$dataarray = $this->formularRepository->getDataArraySubscribe($formdata);
	//
	$data = $this->webserviceRepository->buildXML( array('igel' => $igel, 'formular' => $dataarray) );
	//
	$ws = $this->webserviceRepository->init($wsURL);
	//
	$ws = $this->webserviceRepository->setPostFields($ws, $data);
	//
	$resLog = $this->loggingRepository->doLog('MAILING-REGISTER - SUBMIT-DATA', 'REGISTER', $this->webserviceRepository->anonymizeXML($data));
	//
	$res = $this->webserviceRepository->call($ws);
	//
	$this->webserviceRepository->close($ws);
	//
	$resLog = $this->loggingRepository->doLogResultIGEL('MAILING-REGISTER - RESULT', 'REGISTER', $res);
	
	//
	$res_error_ID = $res['error']['ID'];
	$res_error_info = $res['error']['info'];
	$res_message  = $res['message'];

	//
	$GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-result', serialize($res));
	$GLOBALS['TSFE']->fe_user->storeSessionData();
/* */

	//
	if( $res_error_ID == 0 ){
		$this->redirect('done');
	} else {
		//
		$this->loggingRepository->sendErrorInfo('MAILING-REGISTER - RESULT',  '', $res);
		//
		$this->redirect('fail');
	}
/* */
/* 
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'data' => $data,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'xmldata' => '',
		'error_ID' => $res_error_ID,
		'error_info' => $res_error_info,
		'message' => $res_message,
	));
*/
  }

  /**
   * action done
   *
   * @return void
   */
  public function doneAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$res = array();
	//
    if( $GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-result') ){
      $res = unserialize($GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-result'));
      $GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-result', '');
      $GLOBALS['TSFE']->fe_user->storeSessionData();
	}
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'error_ID' => $res['error']['ID'],
		'error_info' => $res['error']['info'],
		'message' => $res['message'],
	));
  }
  
  /**
   * action fail
   *
   * @return void
   */
  public function failAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$res = array();
	//
    if( $GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-result') ){
      $res = unserialize($GLOBALS['TSFE']->fe_user->getKey('ses', 'igel-mailing-result'));
      $GLOBALS['TSFE']->fe_user->setKey('ses', 'igel-mailing-result', '');
      $GLOBALS['TSFE']->fe_user->storeSessionData();
	}
	//
	$this->view->assignMultiple(array(
		'lang' => $LANG,
		'resp_data' => $res['RAW']['data'],
		'resp_error' => $res['RAW']['error'],
		'error_ID' => $res['error']['ID'],
		'error_info' => $res['error']['info'],
		'message' => $res['message'],
	));
  }

}
