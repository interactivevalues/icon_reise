<?php

if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1',
  array(
    'Subscribe' => 'index, save, done, fail',
  ),
  // non-cacheable actions
  array(
    'Subscribe' => 'index, save, done, fail',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi2',
  array(
    'Manage' => 'index, form, update, unsubscribe, done, fail',
  ),
  // non-cacheable actions
  array(
    'Manage' => 'index, form, update, unsubscribe, done, fail',
  )
);

?>

