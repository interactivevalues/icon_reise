plugin.tx_iconmailing {
	view {
		 # cat=plugin.tx_iconmailing/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:icon_mailing/Resources/Private/Templates/
		 # cat=plugin.tx_iconmailing/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:icon_mailing/Resources/Private/Partials/
		 # cat=plugin.tx_iconmailing/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:icon_mailing/Resources/Private/Layouts/
	}
	features {
		requireCHashArgumentForActionArguments = 0
	}
	settings {
	}
}