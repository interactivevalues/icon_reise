<?php
defined ('TYPO3_MODE') or die ('Access denied.');

/*
ADD STARTSITEN-TRENNER für CType-Pulldown:
*/
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array('Startseite', '--div--');

/*
    DEFINE NEW CONTENT-ELEMENT:     next_stageimage / Bühnen-Image
*/
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array('Bühnen-Bild', 'next_stageimage', 'content-image');
$GLOBALS['TCA']['tt_content']['palettes']['next_stageimage'] = array(
        'canNotCollapse' => 1,
        'showitem' => '
header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:header_formlabel,
--linebreak--,
image;Bild,
'
);
$GLOBALS['TCA']['tt_content']['types']['next_stageimage'] = array(
'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
 --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
 --palette--;Bild für Bühne;next_stageimage,
 --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
 --palette--;;hidden,
 --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
 --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,
'
);


/*
    DEFINE NEW CONTENT-ELEMENT:     next_quicklink / Quicklink
*/
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array('Quicklink (Startseite)', 'next_quicklink', 'content-text');
$GLOBALS['TCA']['tt_content']['palettes']['next_quicklink'] = array(
        'canNotCollapse' => 1,
        'showitem' => '
header;Titel,
--linebreak--,
header_link;Link,
'
);
$GLOBALS['TCA']['tt_content']['types']['next_quicklink'] = array(
'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
 --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
 --palette--;Quicklink;next_quicklink,
 --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
 --palette--;;hidden,
 --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
 --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,
'
);

?>