<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "icon_erweiterungen".
 *
 * generated 22014-04-13
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'ICON - Typo3 Erweiterungen',
  'description' => 'extends diffent parts, tt_content, ...',
  'category' => 'plugin',
  'author' => 'smo',
  'author_email' => 'r.schmoller@next-linz.com',
  'shy' => '',
  'dependencies' => '',
  'conflicts' => '',
  'priority' => '',
  'module' => '',
  'state' => 'stable',
  'internal' => '',
  'uploadfolder' => 0,
  'createDirs' => '',
  'modify_tables' => '',
  'clearCacheOnLoad' => 0,
  'lockType' => '',
  'author_company' => '',
  'version' => '0.0.3',
  'constraints' => 
  array (
    'depends' => 
    array (
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  '_md5_values_when_last_written' => '',
  'suggests' => 
  array (
  ),
);

?>
