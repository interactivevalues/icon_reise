<?php
if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
  mod.wizards.newContentElement {
    wizardItems {  
      frontpage.header = Startseite
      frontpage.elements {
        next_quicklink {
          icon = EXT:core/Resources/Public/Icons/T3Icons/content/content-image.svg
          title = Quicklink (Startseite)
          description = Link auf Seite, Datei, ext. URL oder E-Mailadresse
          tt_content_defValues { 
            CType = next_quicklink 
          }
        }
        next_stageimage {
		  icon = EXT:core/Resources/Public/Icons/T3Icons/content/content-text.svg
          title = Bühnenbild (Startseite)
          description = Ein Bild für die Bühne auf der Startseite
          tt_content_defValues { 
            CType = next_stageimage 
          }
        }
      }
      frontpage.show = next_stageimage, next_quicklink
    }
  }
');

?>
