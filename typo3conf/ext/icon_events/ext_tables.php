<?php
defined ('TYPO3_MODE') or die ('Access denied.');


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iconevents_domain_model_event', 'EXT:icon_events/Resources/Private/Language/locallang_csh_tx_iconevents_domain_model_event.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iconevents_domain_model_event');


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iconevents_domain_model_tag', 'EXT:icon_events/Resources/Private/Language/locallang_csh_tx_iconevents_domain_model_tag.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iconevents_domain_model_tag');


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iconevents_domain_model_category', 'EXT:icon_events/Resources/Private/Language/locallang_csh_tx_iconevents_domain_model_category.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iconevents_domain_model_category');


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iconevents_domain_model_registration', 'EXT:icon_events/Resources/Private/Language/locallang_csh_tx_iconevents_domain_model_registration.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iconevents_domain_model_registration');


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iconevents_domain_model_folder', 'EXT:icon_events/Resources/Private/Language/locallang_csh_tx_iconevents_domain_model_folder.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iconevents_domain_model_folder');

?>
