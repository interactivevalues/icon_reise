<?php
namespace NEXT\IconEvents\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Category extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * @var integer
	 */
	protected $uid;

	/**
	 * @var integer
	 */
	protected $sorting;

	/**
	 * @var DateTime
	 */
	protected $crdate;

	/**
	 * @var DateTime
	 */
	protected $tstamp;

	/**
	 * @var DateTime
	 */
	protected $starttime;

	/**
	 * @var integer
	 */
	protected $hidden;

	/**
	 * @var DateTime
	 */
	protected $endtime;

	/**
	 * @var integer
	 */
	protected $sysLanguageUid;

	/**
	 * @var integer
	 */
	protected $l10nParent;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var \NEXT\IconEvents\Domain\Model\Category
	 * @lazy
	 */
	protected $parentcategory;



	/**
	 * Get creation date
	 *
	 * @return DateTime
	 */
	public function getCrdate() {
		return $this->crdate;
	}

	/**
	 * Set Creation Date
	 *
	 * @param DateTime $crdate crdate
	 * @return void
	 */
	public function setCrdate($crdate) {
		$this->crdate = $crdate;
	}

	/**
	 * Get Tstamp
	 *
	 * @return DateTime
	 */
	public function getTstamp() {
		return $this->tstamp;
	}

	/**
	 * Set tstamp
	 *
	 * @param DateTime $tstamp tstamp
	 * @return void
	 */
	public function setTstamp($tstamp) {
		$this->tstamp = $tstamp;
	}

	/**
	 * Get starttime
	 *
	 * @return DateTime
	 */
	public function getStarttime() {
		return $this->starttime;
	}

	/**
	 * Set starttime
	 *
	 * @param DateTime $starttime starttime
	 * @return void
	 */
	public function setStarttime($starttime) {
		$this->starttime = $starttime;
	}

	/**
	 * Get Endtime
	 *
	 * @return DateTime
	 */
	public function getEndtime() {
		return $this->endtime;
	}

	/**
	 * Set Endtime
	 *
	 * @param DateTime $endtime endttime
	 * @return void
	 */
	public function setEndtime($endtime) {
		$this->endtime = $endtime;
	}

	/**
	 * Get Hidden
	 *
	 * @return integer
	 */
	public function getHidden() {
		return $this->hidden;
	}

	/**
	 * Set Hidden
	 *
	 * @param integer $hidden
	 * @return void
	 */
	public function setHidden($hidden) {
		$this->hidden = $hidden;
	}

	/**
	 * Get sys language
	 *
	 * @return integer
	 */
	public function getSysLanguageUid() {
		return $this->_languageUid;
	}

	/**
	 * Set sys language
	 *
	 * @param integer $sysLanguageUid language uid
	 * @return void
	 */
	public function setSysLanguageUid($sysLanguageUid) {
		$this->_languageUid = $sysLanguageUid;
	}

	/**
	 * Get language parent
	 *
	 * @return integer
	 */
	public function getL10nParent() {
		return $this->l10nParent;
	}

	/**
	 * Set language parent
	 *
	 * @param integer $l10nParent l10nparent
	 * @return void
	 */
	public function setL10nParent($l10nParent) {
		$this->l10nParent = $l10nParent;
	}

	/**
	 * Get category title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Set category title
	 *
	 * @param string $title title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Get parent category
	 *
	 * @return \NEXT\IconEvents\Domain\Model\Category
	 */
	public function getParentcategory() {
		return $this->parentcategory;
	}

	/**
	 * Set parent category
	 *
	 * @param \NEXT\IconEvents\Domain\Model\Category $category parent category
	 * @return void
	 */
	public function setParentcategory(\NEXT\IconEvents\Domain\Model\Category $category) {
		$this->parentcategory = $category;
	}

	/**
	 * Get sorting id
	 *
	 * @return integer sorting id
	 */
	public function getSorting() {
		return $this->sorting;
	}

	/**
	 * Set sorting id
	 *
	 * @param integer $sorting sorting id
	 * @return void
	 */
	public function setSorting($sorting) {
		$this->sorting = $sorting;
	}

}

