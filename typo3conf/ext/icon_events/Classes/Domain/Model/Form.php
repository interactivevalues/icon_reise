<?php
namespace NEXT\IconEvents\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Form extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	// START: -------------- SYSTEM ----------------

  /**
   * event
   *
   * @var \string
   */
  protected $event;


	// END: -------------- SYSTEM ----------------

	// START: -------------- GENERALL ----------------

  /**
   * type
   *
   * @var \integer
   */
  protected $type;

  /**
   * message
   *
   * @var \string
   */
  protected $message;

  /**
   * newsletter
   *
   * @var \integer
   */
  protected $newsletter;

   /**
   * participants
   *
   * @var \integer
   */
  protected $participants;

  /**
   * bill_addr
   *
   * @var \integer
   */
  protected $billAddr;


  /**
   * Sets the type
   *
   * @param \integer $type
   * @return void
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Returns the type
   *
   * @return \integer $type
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the message
   *
   * @param \string $message
   * @return void
   */
  public function setMessage($message) {
    $this->message = $message;
  }

  /**
   * Returns the message
   *
   * @return \string $message
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * Sets the newsletter
   *
   * @param \integer $newsletter
   * @return void
   */
  public function setNewsletter($newsletter) {
    $this->newsletter = $newsletter;
  }

  /**
   * Returns the newsletter
   *
   * @return \integer $newsletter
   */
  public function getNewsletter() {
    return $this->newsletter;
  }
 
  /**
   * Sets the participants
   *
   * @param \integer $participants
   * @return void
   */
  public function setParticipants($participants) {
    $this->participants = $participants;
  }

  /**
   * Returns the participants
   *
   * @return \integer $participants
   */
  public function getParticipants() {
    return $this->participants;
  }

  /**
   * Sets the billAddr
   *
   * @param \integer $billAddr
   * @return void
   */
  public function setBillAddr($billAddr) {
    $this->billAddr = $billAddr;
  }

  /**
   * Returns the billAddr
   *
   * @return \integer $billAddr
   */
  public function getBillAddr() {
    return $this->billAddr;
  }

	// END: -------------- GENERALL ----------------

	// START: -------------- TEILNEHMER ----------------

  /**
   * gender
   *
   * @var \string
   */
  protected $gender;

  /**
   * title
   *
   * @var \string
   */
  protected $title;
  
  /**
   * firstname
   *
   * @var \string
   * @- validate NotEmpty
   */
  protected $firstname;

  /**
   * lastname
   *
   * @var \string
   * @- validate NotEmpty
   */
  protected $lastname;

  /**
   * title_after
   *
   * @var \string
   */
  protected $titleAfter;

  /**
   * function
   *
   * @var \string
   */
  protected $function;

  /**
   * phone
   *
   * @var \string
   */
  protected $phone;

  /**
   * email
   *
   * @var \string
   * @- validate EmailAddress
   */
  protected $email;


  /**
   * Sets the gender
   *
   * @param \string $gender
   * @return void
   */
  public function setGender($gender) {
    $this->gender = $gender;
  }

  /**
   * Returns the gender
   *
   * @return \string $gender
   */
  public function getGender() {
    return $this->gender;
  }

  /**
   * Sets the title
   *
   * @param \string $title
   * @return void
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Returns the title
   *
   * @return \string $title
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Sets the firstname
   *
   * @param \string $firstname
   * @return void
   */
  public function setFirstname($firstname) {
    $this->firstname = $firstname;
  }

  /**
   * Returns the firstname
   *
   * @return \string $firstname
   */
  public function getFirstname() {
    return $this->firstname;
  }

  /**
   * Sets the lastname
   *
   * @param \string $lastname
   * @return void
   */
  public function setLastname($lastname) {
    $this->lastname = $lastname;
  }

  /**
   * Returns the lastname
   *
   * @return \string $lastname
   */
  public function getLastname() {
    return $this->lastname;
  }

  /**
   * Sets the titleAfter
   *
   * @param \string $titleAfter
   * @return void
   */
  public function setTitleAfter($titleAfter) {
    $this->titleAfter = $titleAfter;
  }

  /**
   * Returns the titleAfter
   *
   * @return \string $titleAfter
   */
  public function getTitleAfter() {
    return $this->titleAfter;
  }

  /**
   * Sets the function
   *
   * @param \string $function
   * @return void
   */
  public function setFunction($function) {
    $this->function = $function;
  }

  /**
   * Returns the function
   *
   * @return \string $function
   */
  public function getFunction() {
    return $this->function;
  }

  /**
   * Sets the phone
   *
   * @param \string $phone
   * @return void
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * Returns the phone
   *
   * @return \string $phone
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * Sets the email
   *
   * @param \string $email
   * @return void
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * Returns the email
   *
   * @return \string $email
   */
  public function getEmail() {
    return $this->email;
  }

	// END: -------------- TEILNEHMER ----------------

	// START: -------------- ADDRESS ----------------

  /**
   * addr_title
   *
   * @var \string
   */
  protected $addrTitle;

  /**
   * addr_street
   *
   * @var \string
   */
  protected $addrStreet;

  /**
   * addr_zip
   *
   * @var \string
   */
  protected $addrZip;

  /**
   * addr_city
   *
   * @var \string
   */
  protected $addrCity;


  /**
   * Sets the addrTitle
   *
   * @param \string $addrTitle
   * @return void
   */
  public function setAddrTitle($addrTitle) {
    $this->addrTitle = $addrTitle;
  }

  /**
   * Returns the addrTitle
   *
   * @return \string $addrTitle
   */
  public function getAddrTitle() {
    return $this->addrTitle;
  }

  /**
   * Sets the addrStreet
   *
   * @param \string $addrStreet
   * @return void
   */
  public function setAddrStreet($addrStreet) {
    $this->addrStreet = $addrStreet;
  }

  /**
   * Returns the addrStreet
   *
   * @return \string $addrStreet
   */
  public function getAddrStreet() {
    return $this->addrStreet;
  }

  /**
   * Sets the addrZip
   *
   * @param \string $addrZip
   * @return void
   */
  public function setAddrZip($addrZip) {
    $this->addrZip = $addrZip;
  }

  /**
   * Returns the addrZip
   *
   * @return \string $addrZip
   */
  public function getAddrZip() {
    return $this->addrZip;
  }

  /**
   * Sets the addrCity
   *
   * @param \string $addrCity
   * @return void
   */
  public function setAddrCity($addrCity) {
    $this->addrCity = $addrCity;
  }

  /**
   * Returns the addrCity
   *
   * @return \string $addrCity
   */
  public function getAddrCity() {
    return $this->addrCity;
  }

	// END: -------------- ADDRESS ----------------

	// START: -------------- FIRMEN-ADDRESSE ----------------

  /**
   * bill_title
   *
   * @var \string
   */
  protected $billTitle;

  /**
   * bill_street
   *
   * @var \string
   */
  protected $billStreet;

  /**
   * bill_zip
   *
   * @var \string
   */
  protected $billZip;

  /**
   * bill_city
   *
   * @var \string
   */
  protected $billCity;


  /**
   * Sets the billTitle
   *
   * @param \string $billTitle
   * @return void
   */
  public function setBillTitle($billTitle) {
    $this->billTitle = $billTitle;
  }

  /**
   * Returns the billTitle
   *
   * @return \string $billTitle
   */
  public function getBillTitle() {
    return $this->billTitle;
  }

  /**
   * Sets the billStreet
   *
   * @param \string $billStreet
   * @return void
   */
  public function setBillStreet($billStreet) {
    $this->billStreet = $billStreet;
  }

  /**
   * Returns the billStreet
   *
   * @return \string $billStreet
   */
  public function getBillStreet() {
    return $this->billStreet;
  }

  /**
   * Sets the billZip
   *
   * @param \string $billZip
   * @return void
   */
  public function setBillZip($billZip) {
    $this->billZip = $billZip;
  }

  /**
   * Returns the billZip
   *
   * @return \string $billZip
   */
  public function getBillZip() {
    return $this->billZip;
  }

  /**
   * Sets the billCity
   *
   * @param \string $billCity
   * @return void
   */
  public function setBillCity($billCity) {
    $this->billCity = $billCity;
  }

  /**
   * Returns the billCity
   *
   * @return \string $billCity
   */
  public function getBillCity() {
    return $this->billCity;
  }
  
  	// END: -------------- FIRMEN-ADDRESSE ---------------- 

	// START: -------------- TEILNEHMER - 1 ----------------

  /**
   * gender1
   *
   * @var \string
   */
  protected $gender1;

  /**
   * title1
   *
   * @var \string
   */
  protected $title1;
  
  /**
   * firstname1
   *
   * @var \string
   */
  protected $firstname1;

  /**
   * lastname1
   *
   * @var \string
   */
  protected $lastname1;

  /**
   * title_after1
   *
   * @var \string
   */
  protected $titleAfter1;

  /**
   * function1
   *
   * @var \string
   */
  protected $function1;

  /**
   * phone1
   *
   * @var \string
   */
  protected $phone1;

  /**
   * email1
   *
   * @var \string
   */
  protected $email1;


  /**
   * Sets the gender1
   *
   * @param \string $gender1
   * @return void
   */
  public function setGender1($gender1) {
    $this->gender1 = $gender1;
  }

  /**
   * Returns the gender1
   *
   * @return \string $gender1
   */
  public function getGender1() {
    return $this->gender1;
  }

  /**
   * Sets the title1
   *
   * @param \string $title1
   * @return void
   */
  public function setTitle1($title1) {
    $this->title1 = $title1;
  }

  /**
   * Returns the title1
   *
   * @return \string $title1
   */
  public function getTitle1() {
    return $this->title1;
  }

  /**
   * Sets the firstname1
   *
   * @param \string $firstname1
   * @return void
   */
  public function setFirstname1($firstname1) {
    $this->firstname1 = $firstname1;
  }

  /**
   * Returns the firstname1
   *
   * @return \string $firstname1
   */
  public function getFirstname1() {
    return $this->firstname1;
  }

  /**
   * Sets the lastname1
   *
   * @param \string $lastname1
   * @return void
   */
  public function setLastname1($lastname1) {
    $this->lastname1 = $lastname1;
  }

  /**
   * Returns the lastname1
   *
   * @return \string $lastname1
   */
  public function getLastname1() {
    return $this->lastname1;
  }

  /**
   * Sets the titleAfter1
   *
   * @param \string $titleAfter1
   * @return void
   */
  public function setTitleAfter1($titleAfter1) {
    $this->titleAfter1 = $titleAfter1;
  }

  /**
   * Returns the titleAfter1
   *
   * @return \string $titleAfter1
   */
  public function getTitleAfter1() {
    return $this->titleAfter1;
  }

  /**
   * Sets the function1
   *
   * @param \string $function1
   * @return void
   */
  public function setFunction1($function1) {
    $this->function1 = $function1;
  }

  /**
   * Returns the function1
   *
   * @return \string $function1
   */
  public function getFunction1() {
    return $this->function1;
  }

  /**
   * Sets the phone1
   *
   * @param \string $phone1
   * @return void
   */
  public function setPhone1($phone1) {
    $this->phone1 = $phone1;
  }

  /**
   * Returns the phone1
   *
   * @return \string $phone1
   */
  public function getPhone1() {
    return $this->phone1;
  }

  /**
   * Sets the email1
   *
   * @param \string $email1
   * @return void
   */
  public function setEmail1($email1) {
    $this->email1 = $email1;
  }

  /**
   * Returns the email1
   *
   * @return \string $email1
   */
  public function getEmail1() {
    return $this->email1;
  }

	// END: -------------- TEILNEHMER - 1 ----------------

	// START: -------------- TEILNEHMER - 2 ----------------

  /**
   * gender2
   *
   * @var \string
   */
  protected $gender2;

  /**
   * title2
   *
   * @var \string
   */
  protected $title2;
  
  /**
   * firstname2
   *
   * @var \string
   */
  protected $firstname2;

  /**
   * lastname2
   *
   * @var \string
   */
  protected $lastname2;

  /**
   * title_after2
   *
   * @var \string
   */
  protected $titleAfter2;

  /**
   * function2
   *
   * @var \string
   */
  protected $function2;

  /**
   * phone2
   *
   * @var \string
   */
  protected $phone2;

  /**
   * email2
   *
   * @var \string
   */
  protected $email2;


  /**
   * Sets the gender2
   *
   * @param \string $gender2
   * @return void
   */
  public function setGender2($gender2) {
    $this->gender2 = $gender2;
  }

  /**
   * Returns the gender2
   *
   * @return \string $gender2
   */
  public function getGender2() {
    return $this->gender2;
  }

  /**
   * Sets the title2
   *
   * @param \string $title2
   * @return void
   */
  public function setTitle2($title2) {
    $this->title2 = $title2;
  }

  /**
   * Returns the title2
   *
   * @return \string $title2
   */
  public function getTitle2() {
    return $this->title2;
  }

  /**
   * Sets the firstname2
   *
   * @param \string $firstname2
   * @return void
   */
  public function setFirstname2($firstname2) {
    $this->firstname2 = $firstname2;
  }

  /**
   * Returns the firstname2
   *
   * @return \string $firstname2
   */
  public function getFirstname2() {
    return $this->firstname2;
  }

  /**
   * Sets the lastname2
   *
   * @param \string $lastname2
   * @return void
   */
  public function setLastname2($lastname2) {
    $this->lastname2 = $lastname2;
  }

  /**
   * Returns the lastname2
   *
   * @return \string $lastname2
   */
  public function getLastname2() {
    return $this->lastname2;
  }

  /**
   * Sets the titleAfter2
   *
   * @param \string $titleAfter2
   * @return void
   */
  public function setTitleAfter2($titleAfter2) {
    $this->titleAfter2 = $titleAfter2;
  }

  /**
   * Returns the titleAfter2
   *
   * @return \string $titleAfter2
   */
  public function getTitleAfter2() {
    return $this->titleAfter2;
  }

  /**
   * Sets the function2
   *
   * @param \string $function2
   * @return void
   */
  public function setFunction2($function2) {
    $this->function2 = $function2;
  }

  /**
   * Returns the function2
   *
   * @return \string $function2
   */
  public function getFunction2() {
    return $this->function2;
  }

  /**
   * Sets the phone2
   *
   * @param \string $phone2
   * @return void
   */
  public function setPhone2($phone2) {
    $this->phone2 = $phone2;
  }

  /**
   * Returns the phone2
   *
   * @return \string $phone2
   */
  public function getPhone2() {
    return $this->phone2;
  }

  /**
   * Sets the email2
   *
   * @param \string $email2
   * @return void
   */
  public function setEmail2($email2) {
    $this->email2 = $email2;
  }

  /**
   * Returns the email2
   *
   * @return \string $email2
   */
  public function getEmail2() {
    return $this->email2;
  }

	// END: -------------- TEILNEHMER - 2 ----------------

	// START: -------------- TEILNEHMER - 3 ----------------

  /**
   * gender3
   *
   * @var \string
   */
  protected $gender3;

  /**
   * title3
   *
   * @var \string
   */
  protected $title3;
  
  /**
   * firstname3
   *
   * @var \string
   */
  protected $firstname3;

  /**
   * lastname3
   *
   * @var \string
   */
  protected $lastname3;

  /**
   * title_after3
   *
   * @var \string
   */
  protected $titleAfter3;

  /**
   * function3
   *
   * @var \string
   */
  protected $function3;

  /**
   * phone3
   *
   * @var \string
   */
  protected $phone3;

  /**
   * email3
   *
   * @var \string
   */
  protected $email3;


  /**
   * Sets the gender3
   *
   * @param \string $gender3
   * @return void
   */
  public function setGender3($gender3) {
    $this->gender3 = $gender3;
  }

  /**
   * Returns the gender3
   *
   * @return \string $gender3
   */
  public function getGender3() {
    return $this->gender3;
  }

  /**
   * Sets the title3
   *
   * @param \string $title3
   * @return void
   */
  public function setTitle3($title3) {
    $this->title3 = $title3;
  }

  /**
   * Returns the title3
   *
   * @return \string $title3
   */
  public function getTitle3() {
    return $this->title3;
  }

  /**
   * Sets the firstname3
   *
   * @param \string $firstname3
   * @return void
   */
  public function setFirstname3($firstname3) {
    $this->firstname3 = $firstname3;
  }

  /**
   * Returns the firstname3
   *
   * @return \string $firstname3
   */
  public function getFirstname3() {
    return $this->firstname3;
  }

  /**
   * Sets the lastname3
   *
   * @param \string $lastname3
   * @return void
   */
  public function setLastname3($lastname3) {
    $this->lastname3 = $lastname3;
  }

  /**
   * Returns the lastname3
   *
   * @return \string $lastname3
   */
  public function getLastname3() {
    return $this->lastname3;
  }

  /**
   * Sets the titleAfter3
   *
   * @param \string $titleAfter3
   * @return void
   */
  public function setTitleAfter3($titleAfter3) {
    $this->titleAfter3 = $titleAfter3;
  }

  /**
   * Returns the titleAfter3
   *
   * @return \string $titleAfter3
   */
  public function getTitleAfter3() {
    return $this->titleAfter3;
  }

  /**
   * Sets the function3
   *
   * @param \string $function3
   * @return void
   */
  public function setFunction3($function3) {
    $this->function3 = $function3;
  }

  /**
   * Returns the function3
   *
   * @return \string $function3
   */
  public function getFunction3() {
    return $this->function3;
  }

  /**
   * Sets the phone3
   *
   * @param \string $phone3
   * @return void
   */
  public function setPhone3($phone3) {
    $this->phone3 = $phone3;
  }

  /**
   * Returns the phone3
   *
   * @return \string $phone3
   */
  public function getPhone3() {
    return $this->phone3;
  }

  /**
   * Sets the email3
   *
   * @param \string $email3
   * @return void
   */
  public function setEmail3($email3) {
    $this->email3 = $email3;
  }

  /**
   * Returns the email3
   *
   * @return \string $email3
   */
  public function getEmail3() {
    return $this->email3;
  }

	// END: -------------- TEILNEHMER - 3 ----------------

	// START: -------------- TEILNEHMER - 4 ----------------

  /**
   * gender4
   *
   * @var \string
   */
  protected $gender4;

  /**
   * title4
   *
   * @var \string
   */
  protected $title4;
  
  /**
   * firstname4
   *
   * @var \string
   */
  protected $firstname4;

  /**
   * lastname4
   *
   * @var \string
   */
  protected $lastname4;

  /**
   * title_after4
   *
   * @var \string
   */
  protected $titleAfter4;

  /**
   * function4
   *
   * @var \string
   */
  protected $function4;

  /**
   * phone4
   *
   * @var \string
   */
  protected $phone4;

  /**
   * email4
   *
   * @var \string
   */
  protected $email4;


  /**
   * Sets the gender4
   *
   * @param \string $gender4
   * @return void
   */
  public function setGender4($gender4) {
    $this->gender4 = $gender4;
  }

  /**
   * Returns the gender4
   *
   * @return \string $gender4
   */
  public function getGender4() {
    return $this->gender4;
  }

  /**
   * Sets the title4
   *
   * @param \string $title4
   * @return void
   */
  public function setTitle4($title4) {
    $this->title4 = $title4;
  }

  /**
   * Returns the title4
   *
   * @return \string $title4
   */
  public function getTitle4() {
    return $this->title4;
  }

  /**
   * Sets the firstname4
   *
   * @param \string $firstname4
   * @return void
   */
  public function setFirstname4($firstname4) {
    $this->firstname4 = $firstname4;
  }

  /**
   * Returns the firstname4
   *
   * @return \string $firstname4
   */
  public function getFirstname4() {
    return $this->firstname4;
  }

  /**
   * Sets the lastname4
   *
   * @param \string $lastname4
   * @return void
   */
  public function setLastname4($lastname4) {
    $this->lastname4 = $lastname4;
  }

  /**
   * Returns the lastname4
   *
   * @return \string $lastname4
   */
  public function getLastname4() {
    return $this->lastname4;
  }

  /**
   * Sets the titleAfter4
   *
   * @param \string $titleAfter4
   * @return void
   */
  public function setTitleAfter4($titleAfter4) {
    $this->titleAfter4 = $titleAfter4;
  }

  /**
   * Returns the titleAfter4
   *
   * @return \string $titleAfter4
   */
  public function getTitleAfter4() {
    return $this->titleAfter4;
  }

  /**
   * Sets the function4
   *
   * @param \string $function4
   * @return void
   */
  public function setFunction4($function4) {
    $this->function4 = $function4;
  }

  /**
   * Returns the function4
   *
   * @return \string $function4
   */
  public function getFunction4() {
    return $this->function4;
  }

  /**
   * Sets the phone4
   *
   * @param \string $phone4
   * @return void
   */
  public function setPhone4($phone4) {
    $this->phone4 = $phone4;
  }

  /**
   * Returns the phone4
   *
   * @return \string $phone4
   */
  public function getPhone4() {
    return $this->phone4;
  }

  /**
   * Sets the email4
   *
   * @param \string $email4
   * @return void
   */
  public function setEmail4($email4) {
    $this->email4 = $email4;
  }

  /**
   * Returns the email4
   *
   * @return \string $email4
   */
  public function getEmail4() {
    return $this->email4;
  }

	// END: -------------- TEILNEHMER - 4 ----------------

	// START: -------------- TEILNEHMER - 5 ----------------

  /**
   * gender5
   *
   * @var \string
   */
  protected $gender5;

  /**
   * title5
   *
   * @var \string
   */
  protected $title5;
  
  /**
   * firstname5
   *
   * @var \string
   */
  protected $firstname5;

  /**
   * lastname5
   *
   * @var \string
   */
  protected $lastname5;

  /**
   * title_after5
   *
   * @var \string
   */
  protected $titleAfter5;

  /**
   * function5
   *
   * @var \string
   */
  protected $function5;

  /**
   * phone5
   *
   * @var \string
   */
  protected $phone5;

  /**
   * email5
   *
   * @var \string
   */
  protected $email5;


  /**
   * Sets the gender5
   *
   * @param \string $gender5
   * @return void
   */
  public function setGender5($gender5) {
    $this->gender5 = $gender5;
  }

  /**
   * Returns the gender5
   *
   * @return \string $gender5
   */
  public function getGender5() {
    return $this->gender5;
  }

  /**
   * Sets the title5
   *
   * @param \string $title5
   * @return void
   */
  public function setTitle5($title5) {
    $this->title5 = $title5;
  }

  /**
   * Returns the title5
   *
   * @return \string $title5
   */
  public function getTitle5() {
    return $this->title5;
  }

  /**
   * Sets the firstname5
   *
   * @param \string $firstname5
   * @return void
   */
  public function setFirstname5($firstname5) {
    $this->firstname5 = $firstname5;
  }

  /**
   * Returns the firstname5
   *
   * @return \string $firstname5
   */
  public function getFirstname5() {
    return $this->firstname5;
  }

  /**
   * Sets the lastname5
   *
   * @param \string $lastname5
   * @return void
   */
  public function setLastname5($lastname5) {
    $this->lastname5 = $lastname5;
  }

  /**
   * Returns the lastname5
   *
   * @return \string $lastname5
   */
  public function getLastname5() {
    return $this->lastname5;
  }

  /**
   * Sets the titleAfter5
   *
   * @param \string $titleAfter5
   * @return void
   */
  public function setTitleAfter5($titleAfter5) {
    $this->titleAfter5 = $titleAfter5;
  }

  /**
   * Returns the titleAfter5
   *
   * @return \string $titleAfter5
   */
  public function getTitleAfter5() {
    return $this->titleAfter5;
  }

  /**
   * Sets the function5
   *
   * @param \string $function5
   * @return void
   */
  public function setFunction5($function5) {
    $this->function5 = $function5;
  }

  /**
   * Returns the function5
   *
   * @return \string $function5
   */
  public function getFunction5() {
    return $this->function5;
  }

  /**
   * Sets the phone5
   *
   * @param \string $phone5
   * @return void
   */
  public function setPhone5($phone5) {
    $this->phone5 = $phone5;
  }

  /**
   * Returns the phone5
   *
   * @return \string $phone5
   */
  public function getPhone5() {
    return $this->phone5;
  }

  /**
   * Sets the email5
   *
   * @param \string $email5
   * @return void
   */
  public function setEmail5($email5) {
    $this->email5 = $email5;
  }

  /**
   * Returns the email5
   *
   * @return \string $email5
   */
  public function getEmail5() {
    return $this->email5;
  }

	// END: -------------- TEILNEHMER - 5 ----------------

	// START: -------------- TEILNEHMER - 6 ----------------

  /**
   * gender6
   *
   * @var \string
   */
  protected $gender6;

  /**
   * title6
   *
   * @var \string
   */
  protected $title6;
  
  /**
   * firstname6
   *
   * @var \string
   */
  protected $firstname6;

  /**
   * lastname6
   *
   * @var \string
   */
  protected $lastname6;

  /**
   * title_after6
   *
   * @var \string
   */
  protected $titleAfter6;

  /**
   * function6
   *
   * @var \string
   */
  protected $function6;

  /**
   * phone6
   *
   * @var \string
   */
  protected $phone6;

  /**
   * email6
   *
   * @var \string
   */
  protected $email6;


  /**
   * Sets the gender6
   *
   * @param \string $gender6
   * @return void
   */
  public function setGender6($gender6) {
    $this->gender6 = $gender6;
  }

  /**
   * Returns the gender6
   *
   * @return \string $gender6
   */
  public function getGender6() {
    return $this->gender6;
  }

  /**
   * Sets the title6
   *
   * @param \string $title6
   * @return void
   */
  public function setTitle6($title6) {
    $this->title6 = $title6;
  }

  /**
   * Returns the title6
   *
   * @return \string $title6
   */
  public function getTitle6() {
    return $this->title6;
  }

  /**
   * Sets the firstname6
   *
   * @param \string $firstname6
   * @return void
   */
  public function setFirstname6($firstname6) {
    $this->firstname6 = $firstname6;
  }

  /**
   * Returns the firstname6
   *
   * @return \string $firstname6
   */
  public function getFirstname6() {
    return $this->firstname6;
  }

  /**
   * Sets the lastname6
   *
   * @param \string $lastname6
   * @return void
   */
  public function setLastname6($lastname6) {
    $this->lastname6 = $lastname6;
  }

  /**
   * Returns the lastname6
   *
   * @return \string $lastname6
   */
  public function getLastname6() {
    return $this->lastname6;
  }

  /**
   * Sets the titleAfter6
   *
   * @param \string $titleAfter6
   * @return void
   */
  public function setTitleAfter6($titleAfter6) {
    $this->titleAfter6 = $titleAfter6;
  }

  /**
   * Returns the titleAfter6
   *
   * @return \string $titleAfter6
   */
  public function getTitleAfter6() {
    return $this->titleAfter6;
  }

  /**
   * Sets the function6
   *
   * @param \string $function6
   * @return void
   */
  public function setFunction6($function6) {
    $this->function6 = $function6;
  }

  /**
   * Returns the function6
   *
   * @return \string $function6
   */
  public function getFunction6() {
    return $this->function6;
  }

  /**
   * Sets the phone6
   *
   * @param \string $phone6
   * @return void
   */
  public function setPhone6($phone6) {
    $this->phone6 = $phone6;
  }

  /**
   * Returns the phone6
   *
   * @return \string $phone6
   */
  public function getPhone6() {
    return $this->phone6;
  }

  /**
   * Sets the email6
   *
   * @param \string $email6
   * @return void
   */
  public function setEmail6($email6) {
    $this->email6 = $email6;
  }

  /**
   * Returns the email6
   *
   * @return \string $email6
   */
  public function getEmail6() {
    return $this->email6;
  }

	// END: -------------- TEILNEHMER - 6 ----------------

	// START: -------------- TEILNEHMER - 7 ----------------

  /**
   * gender7
   *
   * @var \string
   */
  protected $gender7;

  /**
   * title7
   *
   * @var \string
   */
  protected $title7;
  
  /**
   * firstname7
   *
   * @var \string
   */
  protected $firstname7;

  /**
   * lastname7
   *
   * @var \string
   */
  protected $lastname7;

  /**
   * title_after7
   *
   * @var \string
   */
  protected $titleAfter7;

  /**
   * function7
   *
   * @var \string
   */
  protected $function7;

  /**
   * phone7
   *
   * @var \string
   */
  protected $phone7;

  /**
   * email7
   *
   * @var \string
   */
  protected $email7;


  /**
   * Sets the gender7
   *
   * @param \string $gender7
   * @return void
   */
  public function setGender7($gender7) {
    $this->gender7 = $gender7;
  }

  /**
   * Returns the gender7
   *
   * @return \string $gender7
   */
  public function getGender7() {
    return $this->gender7;
  }

  /**
   * Sets the title7
   *
   * @param \string $title7
   * @return void
   */
  public function setTitle7($title7) {
    $this->title7 = $title7;
  }

  /**
   * Returns the title7
   *
   * @return \string $title7
   */
  public function getTitle7() {
    return $this->title7;
  }

  /**
   * Sets the firstname7
   *
   * @param \string $firstname7
   * @return void
   */
  public function setFirstname7($firstname7) {
    $this->firstname7 = $firstname7;
  }

  /**
   * Returns the firstname7
   *
   * @return \string $firstname7
   */
  public function getFirstname7() {
    return $this->firstname7;
  }

  /**
   * Sets the lastname7
   *
   * @param \string $lastname7
   * @return void
   */
  public function setLastname7($lastname7) {
    $this->lastname7 = $lastname7;
  }

  /**
   * Returns the lastname7
   *
   * @return \string $lastname7
   */
  public function getLastname7() {
    return $this->lastname7;
  }

  /**
   * Sets the titleAfter7
   *
   * @param \string $titleAfter7
   * @return void
   */
  public function setTitleAfter7($titleAfter7) {
    $this->titleAfter7 = $titleAfter7;
  }

  /**
   * Returns the titleAfter7
   *
   * @return \string $titleAfter7
   */
  public function getTitleAfter7() {
    return $this->titleAfter7;
  }

  /**
   * Sets the function7
   *
   * @param \string $function7
   * @return void
   */
  public function setFunction7($function7) {
    $this->function7 = $function7;
  }

  /**
   * Returns the function7
   *
   * @return \string $function7
   */
  public function getFunction7() {
    return $this->function7;
  }

  /**
   * Sets the phone7
   *
   * @param \string $phone7
   * @return void
   */
  public function setPhone7($phone7) {
    $this->phone7 = $phone7;
  }

  /**
   * Returns the phone7
   *
   * @return \string $phone7
   */
  public function getPhone7() {
    return $this->phone7;
  }

  /**
   * Sets the email7
   *
   * @param \string $email7
   * @return void
   */
  public function setEmail7($email7) {
    $this->email7 = $email7;
  }

  /**
   * Returns the email7
   *
   * @return \string $email7
   */
  public function getEmail7() {
    return $this->email7;
  }

	// END: -------------- TEILNEHMER - 7 ----------------

}
?>