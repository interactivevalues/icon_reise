<?php
namespace NEXT\IconEvents\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Registration extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

  /**
   * event
   *
   * @var \integer
   */
  protected $event;

  /**
   * parent_registration
   *
   * @var \integer
   */
  protected $parentRegistration;

  /**
   * type
   *
   * @var \integer
   */
  protected $type;

  /**
   * message
   *
   * @var \string
   */
  protected $message;

  /**
   * newsletter
   *
   * @var \integer
   */
  protected $newsletter;

   /**
   * attendents
   *
   * @var \integer
   */
  protected $attendents;

  /**
   * bill_addr
   *
   * @var \integer
   */
  protected $billAddr;


  /**
   * chash
   *
   * @var \string
   */
  protected $chash;


  /**
   * Sets the event
   *
   * @param \integer $event
   * @return void
   */
  public function setEvent($event) {
    $this->event = $event;
  }

  /**
   * Returns the event
   *
   * @return \integer $event
   */
  public function getEvent() {
    return $this->event;
  }

  /**
   * Sets the parentRegistration
   *
   * @param \integer $parentRegistration
   * @return void
   */
  public function setParentRegistration($parentRegistration) {
    $this->parentRegistration = $parentRegistration;
  }

  /**
   * Returns the parentRegistration
   *
   * @return \integer $parentRegistration
   */
  public function getParentRegistration() {
    return $this->parentRegistration;
  }

  /**
   * Sets the type
   *
   * @param \integer $type
   * @return void
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Returns the type
   *
   * @return \integer $type
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the message
   *
   * @param \string $message
   * @return void
   */
  public function setMessage($message) {
    $this->message = $message;
  }

  /**
   * Returns the message
   *
   * @return \string $message
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * Sets the newsletter
   *
   * @param \integer $newsletter
   * @return void
   */
  public function setNewsletter($newsletter) {
    $this->newsletter = $newsletter;
  }

  /**
   * Returns the newsletter
   *
   * @return \integer $newsletter
   */
  public function getNewsletter() {
    return $this->newsletter;
  }
 
  /**
   * Sets the attendents
   *
   * @param \integer $attendents
   * @return void
   */
  public function setAttendents($attendents) {
    $this->attendents = $attendents;
  }

  /**
   * Returns the attendents
   *
   * @return \integer $attendents
   */
  public function getAttendents() {
    return $this->attendents;
  }

  /**
   * Sets the billAddr
   *
   * @param \integer $billAddr
   * @return void
   */
  public function setBillAddr($billAddr) {
    $this->billAddr = $billAddr;
  }

  /**
   * Returns the billAddr
   *
   * @return \integer $billAddr
   */
  public function getBillAddr() {
    return $this->billAddr;
  }

  /**
   * Sets the chash
   *
   * @param \string $chash
   * @return void
   */
  public function setChash($chash) {
    $this->chash = $chash;
  }

  /**
   * Returns the chash
   *
   * @return \string $chash
   */
  public function getChash() {
    return $this->chash;
  }


	// END: -------------- GENERALL ----------------

	// START: -------------- TEILNEHMER ----------------

  /**
   * gender
   *
   * @var \string
   */
  protected $gender;

  /**
   * title
   *
   * @var \string
   */
  protected $title;
  
  /**
   * firstname
   *
   * @var \string
   * @validate NotEmpty
   */
  protected $firstname;

  /**
   * lastname
   *
   * @var \string
   * @validate NotEmpty
   */
  protected $lastname;

  /**
   * title_after
   *
   * @var \string
   */
  protected $titleAfter;

  /**
   * function
   *
   * @var \string
   */
  protected $function;

  /**
   * phone
   *
   * @var \string
   */
  protected $phone;

  /**
   * email
   *
   * @var \string
   * @validate EmailAddress
   */
  protected $email;


  /**
   * Sets the gender
   *
   * @param \string $gender
   * @return void
   */
  public function setGender($gender) {
    $this->gender = $gender;
  }

  /**
   * Returns the gender
   *
   * @return \string $gender
   */
  public function getGender() {
    return $this->gender;
  }

  /**
   * Sets the title
   *
   * @param \string $title
   * @return void
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Returns the title
   *
   * @return \string $title
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Sets the firstname
   *
   * @param \string $firstname
   * @return void
   */
  public function setFirstname($firstname) {
    $this->firstname = $firstname;
  }

  /**
   * Returns the firstname
   *
   * @return \string $firstname
   */
  public function getFirstname() {
    return $this->firstname;
  }

  /**
   * Sets the lastname
   *
   * @param \string $lastname
   * @return void
   */
  public function setLastname($lastname) {
    $this->lastname = $lastname;
  }

  /**
   * Returns the lastname
   *
   * @return \string $lastname
   */
  public function getLastname() {
    return $this->lastname;
  }

  /**
   * Sets the titleAfter
   *
   * @param \string $titleAfter
   * @return void
   */
  public function setTitleAfter($titleAfter) {
    $this->titleAfter = $titleAfter;
  }

  /**
   * Returns the titleAfter
   *
   * @return \string $titleAfter
   */
  public function getTitleAfter() {
    return $this->titleAfter;
  }

  /**
   * Sets the function
   *
   * @param \string $function
   * @return void
   */
  public function setFunction($function) {
    $this->function = $function;
  }

  /**
   * Returns the function
   *
   * @return \string $function
   */
  public function getFunction() {
    return $this->function;
  }

  /**
   * Sets the phone
   *
   * @param \string $phone
   * @return void
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * Returns the phone
   *
   * @return \string $phone
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * Sets the email
   *
   * @param \string $email
   * @return void
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * Returns the email
   *
   * @return \string $email
   */
  public function getEmail() {
    return $this->email;
  }
  
  /**
   * builds a formal mail Salute
   *
   * @return \string
   */
  public function getBriefAnrede () {
	$t = '';
	if( strtolower($this->getGender()) == 'herr' ){
		$t .= 'Sehr geehrter Herr ';
	} elseif( strtolower($this->getGender()) == 'frau' ){
		$t .= 'Sehr geehrte Frau ';
	} else {
		$t .= 'Sehr geehrte(r) ';
	}
	if( $this->getTitle() ){ $t .= $this->getTitle() . ' '; }
	if( $this->getLastname() ){ $t .= $this->getLastname(); }
	return $t;
  }

  /**
   * builds a complete Formal Salute
   *
   * @return \string
   */
  public function getVollAnrede () {
	  $t = '';
	  if( $this->getGender() ){ $t .= $this->getGender() . ', '; }
	  if( $this->getTitle() ){ $t .= $this->getTitle() . ' '; }
	  if( $this->getFirstname() ){ $t .= $this->getFirstname() . ' '; }
	  if( $this->getLastname() ){ $t .= $this->getLastname(); }
	  if( $this->getTitleAfter() ){ $t .= ', ' . $this->getTitleAfter(); }
	  return $t;
  }

	// END: -------------- TEILNEHMER ----------------

	// START: -------------- ADDRESS ----------------

  /**
   * addr_title
   *
   * @var \string
   */
  protected $addrTitle;

  /**
   * addr_street
   *
   * @var \string
   */
  protected $addrStreet;

  /**
   * addr_zip
   *
   * @var \string
   */
  protected $addrZip;

  /**
   * addr_city
   *
   * @var \string
   */
  protected $addrCity;


  /**
   * Sets the addrTitle
   *
   * @param \string $addrTitle
   * @return void
   */
  public function setAddrTitle($addrTitle) {
    $this->addrTitle = $addrTitle;
  }

  /**
   * Returns the addrTitle
   *
   * @return \string $addrTitle
   */
  public function getAddrTitle() {
    return $this->addrTitle;
  }

  /**
   * Sets the addrStreet
   *
   * @param \string $addrStreet
   * @return void
   */
  public function setAddrStreet($addrStreet) {
    $this->addrStreet = $addrStreet;
  }

  /**
   * Returns the addrStreet
   *
   * @return \string $addrStreet
   */
  public function getAddrStreet() {
    return $this->addrStreet;
  }

  /**
   * Sets the addrZip
   *
   * @param \string $addrZip
   * @return void
   */
  public function setAddrZip($addrZip) {
    $this->addrZip = $addrZip;
  }

  /**
   * Returns the addrZip
   *
   * @return \string $addrZip
   */
  public function getAddrZip() {
    return $this->addrZip;
  }

  /**
   * Sets the addrCity
   *
   * @param \string $addrCity
   * @return void
   */
  public function setAddrCity($addrCity) {
    $this->addrCity = $addrCity;
  }

  /**
   * Returns the addrCity
   *
   * @return \string $addrCity
   */
  public function getAddrCity() {
    return $this->addrCity;
  }

  /**
   * has any Addess-Infos ?
   *
   * @return \boolean
   */
  public function getHasAddress() {
	$t = trim($this->getAddrTitle() . $this->getAddrStreet() . $this->getAddrZip() . $this->getAddrCity());
    return $t != '';
  }

	// END: -------------- ADDRESS ----------------

	// START: -------------- FIRMEN-ADDRESSE ----------------

  /**
   * bill_title
   *
   * @var \string
   */
  protected $billTitle;

  /**
   * bill_street
   *
   * @var \string
   */
  protected $billStreet;

  /**
   * bill_zip
   *
   * @var \string
   */
  protected $billZip;

  /**
   * bill_city
   *
   * @var \string
   */
  protected $billCity;


  /**
   * Sets the billTitle
   *
   * @param \string $billTitle
   * @return void
   */
  public function setBillTitle($billTitle) {
    $this->billTitle = $billTitle;
  }

  /**
   * Returns the billTitle
   *
   * @return \string $billTitle
   */
  public function getBillTitle() {
    return $this->billTitle;
  }

  /**
   * Sets the billStreet
   *
   * @param \string $billStreet
   * @return void
   */
  public function setBillStreet($billStreet) {
    $this->billStreet = $billStreet;
  }

  /**
   * Returns the billStreet
   *
   * @return \string $billStreet
   */
  public function getBillStreet() {
    return $this->billStreet;
  }

  /**
   * Sets the billZip
   *
   * @param \string $billZip
   * @return void
   */
  public function setBillZip($billZip) {
    $this->billZip = $billZip;
  }

  /**
   * Returns the billZip
   *
   * @return \string $billZip
   */
  public function getBillZip() {
    return $this->billZip;
  }

  /**
   * Sets the billCity
   *
   * @param \string $billCity
   * @return void
   */
  public function setBillCity($billCity) {
    $this->billCity = $billCity;
  }

  /**
   * Returns the billCity
   *
   * @return \string $billCity
   */
  public function getBillCity() {
    return $this->billCity;
  }
  
  /**
   * has any Bill-Addess-Infos ?
   *
   * @return \boolean
   */
  public function getHasBillAddress() {
	$t = trim($this->getBillTitle() . $this->getBillStreet() . $this->getBillZip() . $this->getBillCity());
    return $t != '';
  }

  	// END: -------------- FIRMEN-ADDRESSE ---------------- 

}
?>