<?php
namespace NEXT\IconEvents\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Speaker extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
  /**
   * ausgetretten
   *
   * @var \boolean
   */
  protected $ausgetretten;

  /**
   * Title
   *
   * @var \string
   */
  protected $title;

  /**
   * first_name
   *
   * @var \string
   */
  protected $firstName;

  /**
   * last_name
   *
   * @var \string
   */
  protected $lastName;

  /**
   * title_after
   *
   * @var \string
   */
  protected $titleAfter;

  /**
   * image
   *
   * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
   */
  protected $image;

  /**
   * page_detail
   *
   * @var \string
   */
  protected $pageDetail;
  

  /**
   * Returns ausgetretten
   *
   * @return \boolean $ausgetretten
   */
  public function getAusgetretten() {
        return $this->ausgetretten;
  }


  /**
   * Returns the title
   *
   * @return \string $title
   */
  public function getTitle() {
        return $this->title;
  }

  /**
   * Sets the title
   *
   * @param \string $title
   * @return void
   */
  public function setTitle($title) {
        $this->title = $title;
  }

  /**
   * Returns the firstName
   *
   * @return \string $firstName
   */
  public function getFirstName() {
        return $this->firstName;
  }

  /**
   * Sets the firstName
   *
   * @param \string $firstName
   * @return void
   */
  public function setFirstName($firstName) {
        $this->firstName = $firstName;
  }

  /**
   * Sets the lastName
   *
   * @param \string $lastName
   * @return void
   */
  public function setLastName($lastName) {
        $this->lastName = $lastName;
  }

  /**
   * Returns the lastName
   *
   * @return \string $lastName
   */
  public function getLastName() {
        return $this->lastName;
  }

  /**
   * Returns the titleAfter
   *
   * @return \string $titleAfter
   */
  public function getTitleAfter() {
        return $this->titleAfter;
  }

  /**
   * Sets the titleAfter
   *
   * @param \string $titleAfter
   * @return void
   */
  public function setTitleAfter($titleAfter) {
        $this->titleAfter = $titleAfter;
  }

  /**
   * Returns the images
   *
   * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
   */
  public function getImage() {
        return $this->image;
  }

  /**
   * Sets the image
   *
   * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
   * @return void
   */
  public function setImage($image) {
        $this->image = $image;
  }

  /**
   * Returns the pageDetail
   *
   * @return \string $pageDetail
   */
  public function getPageDetail() {
        return $this->pageDetail;
  }

  /**
   * Sets the pageDetail
   *
   * @param \string $pageDetail
   * @return void
   */
  public function setPageDetail($pageDetail) {
        $this->pageDetail = $pageDetail;
  }

}
?>