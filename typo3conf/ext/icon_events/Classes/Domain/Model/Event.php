<?php
namespace NEXT\IconEvents\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Event extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
  /**
   * Type
   *
   * @var \string
   */
  protected $type;

  /**
   * Tstamp
   *
   * @var \DateTime
   */
  protected $tstamp;

  /**
   * Pre_Title
   *
   * @var \string
   */
  protected $preTitle;

  /**
   * Pre_Title_Big
   *
   * @var \boolean
   */
  protected $preTitleBig;

  /**
   * Ttitle
   *
   * @var \string
   * @validate NotEmpty
   */
  protected $title;

  /**
   * Datum Start
   *
   * @var \DateTime
   * @validate NotEmpty
   */
  protected $dateStart;

  /**
   * Datum Ende
   *
   * @var \DateTime
   * @validate NotEmpty
   */
  protected $dateEnd;

  /**
   * Bild
   *
   * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
   */
  protected $image;

  /**
   * Bild Mail
   *
   * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
   */
  protected $imageMail;

  /**
   * location
   *
   * @var \string
   */
  protected $location;

  /**
   * building
   *
   * @var \string
   */
  protected $building;

  /**
   * address
   *
   * @var \string
   */
  protected $address;

  /**
   * addressLink
   *
   * @var \string
   */
  protected $addressLink;

  /**
   * link
   *
   * @var \string
   */
  protected $link;

  /**
   * speaker
   *
   * @var \string
   */
  protected $speaker;

  /**
   * speaker1
   *
   * @var \NEXT\IconEvents\Domain\Model\Speaker
   */
  protected $speaker1;

  /**
   * speaker2
   *
   * @var \NEXT\IconEvents\Domain\Model\Speaker
   */
  protected $speaker2;

  /**
   * Anmeldung
   *
   * @var integer
   */
  protected $registration = 1;

  /**
   * Anmeldung endet x Stunden früher
   *
   * @var integer
   */
  protected $registrationEndbefore = 0;

  /**
   * price
   *
   * @var \string
   */
  protected $price;

  /**
   * price_special
   *
   * @var \string
   */
  protected $priceSpecial;

  /**
   * price_info
   *
   * @var \string
   */
  protected $priceInfo;

  /**
   * price_folder
   *
   * @var \string
   */
  protected $priceFolder;

  /**
   * Text Links
   *
   * @var \string
   */
  protected $textLinks;

  /**
   * Text Rechts
   *
   * @var \string
   */
  protected $textRechts;

  /**
   * Benefit
   *
   * @var \string
   */
  protected $benefit;

  /**
   * Related UID
   *
   * @var \NEXT\IconEvents\Domain\Model\Event
   */
  protected $relatedUid;

  /**
   * Related Text Info
   *
   * @var \string
   */
  protected $relatedTextInfo;

  /**
   * Related Text Link
   *
   * @var \string
   */
  protected $relatedTextLink;

  /**
   * tag
   *
   * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconEvents\Domain\Model\Tag>
   */
//  protected $tag;

  /**
   * category
   *
   * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconEvents\Domain\Model\Category>
   * @lazy
   */
  protected $category;

  /**
   * __construct
   *
   * @return Link
   */
  public function __construct() {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
  }

  /**
   * Initializes all ObjectStorage properties.
   *
   * @return void
   */
  protected function initStorageObjects() {
    $this->categories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    $this->tag = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
  }


  /**
   * Returns the type
   *
   * @return boolean $type
   */
  public function getType() {
        return $this->type;
  }

  /**
   * Sets the type
   *
   * @param \integer $type
   * @return void
   */
  public function setType($type) {
        $this->type = $type;
  }
  
  /**
   *
   * @return \boolean
   */
  public function isType1or2() {
	  	return $this->getType() > 0;
  }
  
  /**
   * Returns the tstamp
   *
   * @return \DateTime $tstamp
   */
  public function getTstamp() {
        return $this->tstamp;
  }

  /**
   * Sets the tstamp
   *
   * @param \DateTime $tstamp
   * @return void
   */
  public function setTstamp($tstamp) {
        $this->tstamp = $tstamp;
  }

  /**
   * Returns the preTitle
   *
   * @return \string $preTitle
   */
  public function getPreTitle() {
        return $this->preTitle;
  }

  /**
   * Sets the preTitle
   *
   * @param \string $preTitle
   * @return void
   */
  public function setPreTitle($preTitle) {
        $this->preTitle = $preTitle;
  }

  /**
   * Returns the preTitleBig
   *
   * @return \boolean $preTitleBig
   */
  public function getPreTitleBig() {
        return $this->preTitleBig;
  }

  /**
   * Sets the preTitleBig
   *
   * @param \string $preTitleBig
   * @return void
   */
  public function setPreTitleBig($preTitleBig) {
        $this->preTitleBig = $preTitleBig;
  }

  /**
   * Returns the title
   *
   * @return \string $title
   */
  public function getTitle() {
        return $this->title;
  }

  /**
   * Sets the title
   *
   * @param \string $title
   * @return void
   */
  public function setTitle($title) {
        $this->title = $title;
  }

  /**
   * Returns the dateStart
   *
   * @return \DateTime $dateStart
   */
  public function getDateStart() {
        return $this->dateStart;
  }

  /**
   * Sets the dateStart
   *
   * @param \DateTime $dateStart
   * @return void
   */
  public function setDateStart($dateStart) {
        $this->dateStart = $dateStart;
  }

  /**
   * Returns the dateEnd
   *
   * @return \DateTime $dateEnd
   */
  public function getDateEnd() {
        return $this->dateEnd;
  }

  /**
   * Sets the dateEnd
   *
   * @param \DateTime $dateEnd
   * @return void
   */
  public function setDateEnd($dateEnd) {
        $this->dateEnd = $dateEnd;
  }

  /**
   * Returns the image
   *
   * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
   */
  public function getImage() {
        return $this->image;
  }

  /**
   * Sets the image
   *
   * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
   * @return void
   */
  public function setImage($image) {
        $this->image = $image;
  }
  
  /**
   * Returns the imageMail
   *
   * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $imageMail
   */
  public function getImageMail() {
        return $this->imageMail;
  }

  /**
   * Sets the imageMail
   *
   * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $imageMail
   * @return void
   */
  public function setImageMail($imageMail) {
        $this->imageMail = $imageMail;
  }
  
  /**
   * Returns the location
   *
   * @return \string $location
   */
  public function getLocation() {
        return $this->location;
  }

  /**
   * Sets the location
   *
   * @param \string $location
   * @return void
   */
  public function setLocation($location) {
        $this->location = $location;
  }

  /**
   * Returns the building
   *
   * @return \string $building
   */
  public function getBuilding() {
        return $this->building;
  }

  /**
   * Sets the building
   *
   * @param \string $building
   * @return void
   */
  public function setBuilding($building) {
        $this->building = $building;
  }

  /**
   * Returns the address
   *
   * @return \string $address
   */
  public function getAddress() {
        return $this->address;
  }

  /**
   * Sets the address
   *
   * @param \string $address
   * @return void
   */
  public function setAddress($address) {
        $this->address = $address;
  }

  /**
   * Returns the addressLink
   *
   * @return \string $addressLink
   */
  public function getAddressLink() {
        return $this->addressLink;
  }

  /**
   * Sets the addressLink
   *
   * @param \string $addressLink
   * @return void
   */
  public function setAddressLink($addressLink) {
        $this->addressLink = $addressLink;
  }

  /**
   * Returns the link
   *
   * @return \string $link
   */
  public function getLink() {
        return $this->link;
  }

  /**
   * Sets the link
   *
   * @param \string $link
   * @return void
   */
  public function setLink($link) {
        $this->link = $link;
  }

  /**
   * Returns the speaker
   *
   * @return \string
   */
  public function getSpeaker() {
        return $this->speaker;
  }

  /**
   * Sets the speaker
   *
   * @param \string $speaker
   * @return void
   */
  public function setSpeaker($speaker) {
        $this->speaker = $speaker;
  }

  /**
   * Sets the speaker1
   *
   * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconEvents\Domain\Model\Speaker> $speaker
   * @return void
   */
  public function setSpeaker1 (\NEXT\IconEvents\Domain\Model\Speaker $speaker) {
	  $this->speaker1 = $speaker;
  }

  /**
   * Returns Speaker 1
   *
   * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconEvents\Domain\Model\Speaker> $speaker
   */
  public function getSpeaker1 () {
	  return $this->speaker1;
  }

  /**
   * Sets the speaker2
   *
   * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconEvents\Domain\Model\Speaker> $speaker
   * @return void
   */
  public function setSpeaker2 (\NEXT\IconEvents\Domain\Model\Speaker $speaker) {
	  $this->speaker2 = $speaker;
  }

  /**
   * Returns Speaker 2
   *
   * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\NEXT\IconEvents\Domain\Model\Speaker> $speaker
   */
  public function getSpeaker2 () {
	  return $this->speaker2;
  }

  /**
   * Returns the price
   *
   * @return \string $price
   */
  public function getPrice() {
        return trim($this->price);
  }

  /**
   * Sets the price
   *
   * @param \string $price
   * @return void
   */
  public function setPrice($price) {
        $this->price = $price;
  }

  /**
   * Returns the priceSpecial
   *
   * @return \string $priceSpecial
   */
  public function getPriceSpecial() {
        return trim($this->priceSpecial);
  }

  /**
   * Sets the priceSpecial
   *
   * @param \string $priceSpecial
   * @return void
   */
  public function setPriceSpecial($priceSpecial) {
        $this->priceSpecial = $priceSpecial;
  }

  /**
   * Returns Flag if has Price
   *
   * @return \boolean
   */
  public function getHasPrice() {
	  return ( $this->getPrice()!='' || $this->getPriceSpecial()!='' );
  }

  /**
   * Returns the priceInfo
   *
   * @return \string $priceInfo
   */
  public function getPriceInfo() {
        return $this->priceInfo;
  }

  /**
   * Sets the priceFolder
   *
   * @param \string $priceFolder
   * @return void
   */
  public function setPriceFolder($priceFolder) {
        $this->priceFolder = $priceFolder;
  }

  /**
   * Returns the priceFolder
   *
   * @return \string $priceFolder
   */
  public function getPriceFolder() {
        return $this->priceFolder;
  }

  /**
   * Sets the priceInfo
   *
   * @param \string $priceInfo
   * @return void
   */
  public function setPriceInfo($priceInfo) {
        $this->priceInfo = $priceInfo;
  }

  /**
   * Returns the registration
   *
   * @return integer $registration
   */
  public function getRegistration() {
        return $this->registration;
  }
  
  /**
   * Sets the registration
   *
   * @param integer $registration
   * @return void
   */
  public function setRegistration($registration) {
        $this->registration = $registration;
  }

  /**
   * Returns the boolean state of REGISTRATION IS ACTIVE
   *
   * @return boolean
   */
  public function isRegistration() {
        return $this->getRegistration() == 1;
  }

  /**
   * Returns the boolean state of REGISTRATION IS ABGESAGT
   *
   * @return boolean
   */
/*
  public function isAbgesagt() {
        return $this->getRegistration() == 2;
  }
*/
  /**
   * Returns the boolean state of registraion is abgesagt
   *
   * @return boolean
   */
  public function getIsAbgesagt () {
	  return $this->getRegistration() == 2;
  }
  

  /**
   * Returns the registrationEndbefore
   *
   * @return boolean $registrationEndbefore
   */
  public function getRegistrationEndbefore() {
        return $this->registrationEndbefore;
  }
  
  /**
   * Sets the registrationEndbefore
   *
   * @param boolean $registrationEndbefore
   * @return void
   */
  public function setRegistrationEndbefore($registrationEndbefore) {
        $this->registrationEndbefore = $registrationEndbefore;
  }

  /**
   * Returns the boolean state of registration is possible
   *
   * @return
   */
  public function getOpenRegistration() {
	if( $this->getRegistration()===1 ){
		if( $this->getRegistrationEndbefore() > 0 ){
			$t = $this->getDateStart();
			$t_end = strtotime('-'.$this->getRegistrationEndbefore().' hours', strtotime($this->getDateStart()->format("Y-m-d H:i") ) ); 
			$t_now = time();
			if( $t_end < $t_now ){
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	} else {
		return FALSE;
	}
  }


  /**
   * Returns the boolean state of event already happend
   *
   * @return
   */
  public function getIsPast() {
	$t = $this->getDateStart();
	$t_end = strtotime($this->getDateStart()->format("Y-m-d H:i") ); 
	$t_now = time();
	if( $t_end < $t_now ){
		return TRUE;
	} else {
		return FALSE;
	}
  }


  /**
   * Returns the textLinks
   *
   * @return \string $textLinks
   */
  public function getTextLinks() {
        return $this->textLinks;
  }

  /**
   * Sets the textLinks
   *
   * @param \string $textLinks
   * @return void
   */
  public function setTextLinks($textLinks) {
        $this->textLinks = $textLinks;
  }

  /**
   * Returns the textRechts
   *
   * @return \string $textRechts
   */
  public function getTextRechts() {
        return $this->textRechts;
  }

  /**
   * Sets the textRechts
   *
   * @param \string $textRechts
   * @return void
   */
  public function setTextRechts($textRechts) {
        $this->textRechts = $textRechts;
  }

  /**
   * Returns the benefit
   *
   * @return \string $benefit
   */
  public function getBenefit() {
        return $this->benefit;
  }

  /**
   * Sets the benefit
   *
   * @param \string $benefit
   * @return void
   */
  public function setBenefit($benefit) {
        $this->benefit = $benefit;
  }

  /**
   * Returns the Related UID
   *
   * @return \string $relatedUid
   */
  public function getRelatedUid() {
        return $this->relatedUid;
  }

  /**
   * Sets the Related UID
   *
   * @param \string $relatedUid
   * @return void
   */
  public function setRelatedUid($relatedUid) {
        $this->relatedUid = $relatedUid;
  }

  /**
   * Returns the Related Text Info
   *
   * @return \string $relatedTextInfo
   */
  public function getRelatedTextInfo() {
        return $this->relatedTextInfo;
  }

  /**
   * Sets the Related Text Info
   *
   * @param \string $relatedTextInfo
   * @return void
   */
  public function setRelatedTextInfo($relatedTextInfo) {
        $this->relatedTextInfo = $relatedTextInfo;
  }

  /**
   * Returns the Related Text Link
   *
   * @return \string $relatedTextLink
   */
  public function getRelatedTextLink() {
        return $this->relatedTextLink;
  }

  /**
   * Sets the Related Text Link
   *
   * @param \string $relatedTextLink
   * @return void
   */
  public function setRelatedTextLink($relatedTextLink) {
        $this->relatedTextLink = $relatedTextLink;
  }

  /**
   * Returns the category
   *
   * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<> $category
   */
  public function getCategory() {
        return $this->category;
  }

  /**
   * Sets the category
   *
   * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<> $category
   * @return void
   */
  public function setCategory(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $category) {
        $this->category = $category;
  }

  /**
   * Get first category
   *
   * @return \NEXT\IconEvents\Domain\Model\Category
   */
  public function getFirstCategory() {
    $categories = $this->getCategory();
	if (!is_null($categories)) {
		$categories->rewind();
		return $categories->current();
	} else {
		return NULL;
	}
  }
}
?>