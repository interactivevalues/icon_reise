<?php
namespace NEXT\IconEvents\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CategoryRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Build list for searchform
	 *
	 * @return \array
	 */
	public function buildListForSearch() {
		$t = array();
		//
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$categories = $query->execute();
		//
		foreach ($categories as $category) {
			$k = 'type0|' . $category->getUid();
			$t[$k] = $category->getTitle() . ' (TAX ACADEMY)';
//			$t[$k] = $category->getTitle();
		}
		//
		$t['type1'] = 'Event';
		$t['type2'] = 'Galerie';
		$t['type3'] = 'Extern';
		return $t;
	}

	/**
	 * Find categories by a given pid
	 *
	 * @param integer $pid pid
	 * @return Tx_Extbase_Persistence_QueryInterface
	 */
	public function findParentCategoriesByPid($pid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		return $query->matching(
			$query->logicalAnd(
				$query->equals('pid', (int)$pid),
				$query->equals('parentcategory', 0)
			))->execute();
	}

	/**
	 * Find category tree
	 *
	 * @param array $rootIdList list of id s
	 * @return Tx_Extbase_Persistence_QueryInterface
	 */
	public function findTree(array $rootIdList) {
		$subCategories = Tx_News_Service_CategoryService::getChildrenCategories(implode(',',$rootIdList));
		$ordering = array('sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING);

		$categories = $this->findByIdList(explode(',',$subCategories), $ordering);
		$flatCategories = array();
		foreach ($categories as $category) {
			$flatCategories[$category->getUid()] = Array(
				'item' =>  $category,
				'parent' => ($category->getParentcategory()) ? $category->getParentcategory()->getUid() : NULL
			);
		}

		$tree = array();

		// If leaves are selected without its parents selected, those are shown as parent
		foreach($flatCategories as $id => &$flatCategory) {
			if (!isset($flatCategories[$flatCategory['parent']])) {
				$flatCategory['parent'] = NULL;
			}
		}

		foreach ($flatCategories as $id => &$node) {
			if ($node['parent'] === NULL) {
				$tree[$id] = &$node;
			} else {
				$flatCategories[$node['parent']]['children'][$id] = &$node;
			}
		}

		return $tree;
	}

	/**
	 * Find categories by a given pid
	 *
	 * @param array $idList list of id s
	 * @param array $ordering ordering
	 * @return Tx_Extbase_Persistence_QueryInterface
	 */
/*
	public function findByIdList(array $idList, array $ordering = array()) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);

		if (count($ordering) > 0) {
			$query->setOrderings($ordering);
		}
		$this->overlayTranslatedCategoryIds($idList);

		return $query->matching(
			$query->logicalAnd(
				$query->in('uid', $idList)
			))->execute();
	}
*/
	/**
	 * Find categories by a given parent
	 *
	 * @param integer $parent parent
	 * @return \TYPO3\CMS\Extbase\Persistence_QueryInterface
	 */
	public function findChildren($parent) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		return $query->matching(
			$query->logicalAnd(
				$query->equals('parentcategory', (int)$parent)
			))->execute();
	}

	/**
	 * Overlay the category ids with the ones from current language
	 *
	 * @param array $idList
	 * return void
	 */
	protected function overlayTranslatedCategoryIds(array &$idList) {
		$language = $this->getSysLanguageUid();

		if ($language > 0) {
			if (isset($GLOBALS['TSFE']) && is_object($GLOBALS['TSFE'])) {
				$whereClause = 'sys_language_uid=' . $language .' AND l10n_parent IN(' . implode(',', $idList) .')' . $GLOBALS['TSFE']->sys_page->enableFields('tx_news_domain_model_category');
				$rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('l10n_parent, uid,sys_language_uid', 'tx_news_domain_model_category', $whereClause);

				$idList = $this->replaceCategoryIds($idList, $rows);
			} else {
				// @todo currently only implemented for the frontend
			}
		}
	}

	/**
	 * Get the current sys language uid
	 *
	 * @return integer
	 */
	protected function getSysLanguageUid() {
		$sysLanguage = 0;
		if (isset($GLOBALS['TSFE']) && is_object($GLOBALS['TSFE'])) {
			$sysLanguage = $GLOBALS['TSFE']->sys_language_content;
		} elseif (intval(t3lib_div::_GP('L'))) {
			$sysLanguage = intval(t3lib_div::_GP('L'));
		}
		return $sysLanguage;
	}

}