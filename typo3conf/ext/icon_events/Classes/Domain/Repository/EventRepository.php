<?php
namespace NEXT\IconEvents\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class EventRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * getRecordByUid
	 * 
	 * @param \integer $uid
	 * @return
	 */
	public function getRecordByUid ($uid) {
		//
		$query = $this->createQuery();
		// don't add the pid constraint
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		
		// define the enablecolumn fields to be ignored
		// if nothing else is given, all enableFields are ignored
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);       
		// define single fields to be ignored
		$query->getQuerySettings()->setEnableFieldsToBeIgnored(array('disabled'));

		//
		$query->matching( $query->equals('uid', $uid) );
		
		// the same functions as shown in initializeObject can be applied.
		return $query->execute()->getFirst();
	}

  /**
   * listEvents
   *
   * @param \NEXT\IconEvents\Domain\Model\Search $searchdata
   * @param \integer $offset
   * @param \integer $limit
   * @return
   */
  public function listEvents ($searchdata, $offset=NULL, $limit=NULL) {
    $query = $this->createQuery();
    $query->getQuerySettings()->setRespectStoragePage(FALSE);    

    $dateConstraints = array();
	$dateConstraints[] = $query->greaterThanOrEqual('date_end', time());

	$filterConstraints = array();
	$tag = $searchdata->getTag();
	if( $tag ){
		$t_constraints = array();
		$tags = explode(',', $tag);
		foreach($tags as $tagUid){
			$t_constraints[] = $query->contains('tags', $tagUid );
		}
		$filterConstraints[] = $query->logicalOr($t_constraints);
	}
	$category = $searchdata->getCategory();
    if( $category ){
		//
		$parts = explode('|', $category);
		$typ = str_replace('type', '', $parts[0]);
		$cat = trim($parts[1]);
		//
		if( $cat != '' ){
			$filterConstraints[] = $query->logicalAnd(
				$query->equals('type', $typ),
				$query->contains('category', $cat)
			);
		} else {
			$filterConstraints[] = $query->equals('type', $typ);
		}
		//
    }
	$speaker = $searchdata->getSpeaker();
	if( $speaker ){
		$filterConstraints[] = $query->logicalOr(
			$query->equals('speaker', $speaker),
			$query->like('speaker', $speaker . ',%' ),
			$query->like('speaker', '%,' . $speaker)
		);
	}
	//
	if( count($filterConstraints) ){
		$resultConstraints = $query->logicalAnd( $query->logicalAnd($dateConstraints), $query->logicalAnd($filterConstraints) );
	} else {
			$resultConstraints = $query->logicalAnd( $query->logicalAnd($dateConstraints) );
	}
	$query->matching($resultConstraints);
    //
    $orderings = array();
    $orderings['dateStart'] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;
    $query->setOrderings($orderings);    
	//
	if( $offset >= 0 && $limit >= 1 ){
	    $query->setOffset($offset);
    	$query->setLimit($limit);
	}

    return $query->execute();
  }
  
  /**
   * buildIcsFile
   *
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @param \array $settings
   * @return \string
   */
  public function buildIcsFile (\NEXT\IconEvents\Domain\Model\Event $event, $settings) {
	//
	$LBCR = chr(13).chr(10);
	//
	$dt_start = $event->getDateStart()->format('Ymd') . 'T' . $event->getDateStart()->format('His');	// . 'Z';
	$dt_end = $event->getDateEnd()->format('Ymd') . 'T' . $event->getDateEnd()->format('His');			// . 'Z';
	$dt_tstamp = $event->getTstamp()->format('Ymd') . 'T' . $event->getTstamp()->format('His');			// . 'Z';
	$eventType = $event->getType();
	$UID = 'icon-event_' . $event->getUid() .'@icon.at';
	$organizerType = 'organizerType' . $eventType;
	$organizer = $settings['icsFile'][$organizerType];
	$location = str_replace(',', '\,', ($event->getBuilding() . ', ' . $event->getAddress()) );
	$summary = $event->getPreTitle() . ' ' . $event->getTitle();
	$description = '';
	$url = $settings['icsFile']['URL'];
	$url = str_replace('###UID###', $event->getUid(), $url);
	//
	$t = 'BEGIN:VCALENDAR' . $LBCR;
	$t .= 'VERSION:2.0' . $LBCR;
	$t .= 'PRODID:-//NEXT e-Marketing Gmbh//Typo3 Extension//EN' . $LBCR;
	$t .= 'METHOD:PUBLISH' . $LBCR;
	$t .= 'BEGIN:VEVENT' . $LBCR;
	$t .= 'UID:' . $UID . $LBCR;
	$t .= 'ORGANIZER;CN=' . $organizer . $LBCR;
	$t .= 'LOCATION:' . $location . $LBCR;
	$t .= 'SUMMARY:' . $summary . $LBCR;
	$t .= 'DESCRIPTION:' . $description. $LBCR;
	//$t .= 'URL:' . $url . $LBCR;
	$t .= 'CLASS:PUBLIC' . $LBCR;
	$t .= 'DTSTART:' . $dt_start . $LBCR;
	$t .= 'DTEND:' . $dt_end . $LBCR;
	$t .= 'DTSTAMP:' . $dt_tstamp . $LBCR;
	$t .= 'END:VEVENT' . $LBCR;
	$t .= 'END:VCALENDAR' . $LBCR;
	//
	//$t = utf8_decode($t);
	//
	return $t;
  }
  
  
  /**
   * sends Email to user 
   *
   * @param array @mailCFG
   * @param string $mailBody
   * @return
   */
  public function deliverMail ($mailCFG, $mailBody) {
	$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
	$mail->setTo(array( $mailCFG['toEmail'] => $mailCFG['toName'] ))
		->setFrom(array( $mailCFG['fromEmail'] => $mailCFG['fromName'] ))
		->setSubject( $mailCFG['subject'] )
		->setCharset( $mailCFG['charset'] );
	//
	//$mail->setCc($m_ccArray);
	//$mail->setBcc($m_bcc);
	//$message->setReturnPath($cObj->cObjGetSingle($conf[$type . '.']['overwrite.']['returnPath'], $conf[$type . '.']['overwrite.']['returnPath.']));
	//$mail->setReplyTo($replyArray);
	$mail->setBody($mailBody, $mailCFG['contentType']);
	$mail->send();
	return $mail->isSent();
  }
  
  /**
   * build Mail-Body and return string
   * 
   * @param array $mailData
   * @param array $settings
   * @return string
   */
  public function buildMailBody ($mailData, $settings) {
	$mailBody = $this->getMailBody($settings);
	$mailBody = $this->mergeMailBodyData($mailBody, $mailData);
	//
	return $mailBody;
  }
  
  /**
   * builds TITLE for Mail incl. formatted HTML
   * 
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @return string
   */
  public function buildMailTitle (\NEXT\IconEvents\Domain\Model\Event $event) {
  	$t = $event->getTitle();
	if( $t ){
		$t = '<h2 style="margin-top:0; padding-top:0; color:#004e83; font-family:Arial, Helvetica, sans-serif; font-size:27px; font-weight:normal;">'.$t.'</h2>';
	}
	//
	return $t;
  }
  
  /**
   * builds PRE_TITLE for Mail incl. formatted HTML
   * 
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @return string
   */
  public function buildMailPreTitle (\NEXT\IconEvents\Domain\Model\Event $event) {
	$t = $event->getPreTitle();
	if( $t ){
		if( $event->getPreTitleBig() ){
			$t = '<h3 style="margin:0; padding:0; color:#7f7f7f; font-family:Verdana, Geneva, sans-serif; font-size:22px; font-weight:normal;">'.$t.'</h3>';
		} else {
			$t = '<h3 style="margin:0 0 5px 0; padding:0; color:#7f7f7f; font-family:Verdana, Geneva, sans-serif; font-size:18px; font-weight:normal;">'.$t.'</h3>';
		}
	}
	//
	return $t;
  }
  
  /**
   * builds Event-Details for Mail
   * 
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @param array $settings
   * @param mixed $cObj
   * @return string
   */
  public function buildMailDetails (\NEXT\IconEvents\Domain\Model\Event $event, $settings, $cObj) {
	//
	$t = '';
	$t .= '<strong>';
	$t .= $this->buildMailDayDateTime($event);
	$t .= '</strong><br>';
	if( $event->getBuilding() ){
		$t .= $event->getBuilding() . ',<br>';
	}
	if( $event->getAddress() ){
		$t .= $event->getAddress();
	}
	if( $event->getAddressLink() ){
		$cfgTypoLink = array( 'parameter' => $event->getAddressLink() );
		$eventLink = $cObj->typolink('Anfahrtsplan', $cfgTypoLink);
		$eventLink = str_replace('href="/', 'href="'.$settings['mailURLPrefix'], $eventLink);
		$t .= ' | ' . $eventLink;
	}
	$t .= '<br><br>';
	//
	return $t;
  }

  /**
   * builds PRICE for Mail
   * 
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @param array $settings
   * @return string
   */
  public function buildMailPrice (\NEXT\IconEvents\Domain\Model\Event $event, $settings) {
	$t = '';
	if( $event->getHasPrice() ){
		$t = $this->settings['type' .$eventType. 'MailPriceInfo'];
	}
	//
	return $t;
  }
    
  /**
   * builds FOOTER LINKS for Mail
   * 
   * @param array $settings
   * @param mixed $cObj
   * @return string
   */
  public function buildMailFooterLinks ($settings, $cObj) {
	$ATagParams = 'style="color:#FFFFFF; font-size:12px;"';
	$srch = 'href="/';
	$rplc = 'href="' . $settings['mailURLPrefix'];
	$t = array();
	$contact = $settings['mailFooterContactPage'];
	$imprint = $settings['mailFooterImprintPage'];
	$disclaimer = $settings['mailFooterDisclaimerPage'];
	if( $contact ){
		$cfgTypoLink = array( 'parameter' => $contact, 'ATagParams' => $ATagParams );
		$t[] = str_replace( $srch, $rplc, $cObj->typolink('KONTAKT', $cfgTypoLink) );
	}
	if( $imprint ){
		$cfgTypoLink = array( 'parameter' => $imprint, 'ATagParams' => $ATagParams  );
		$t[] = str_replace( $srch, $rplc, $cObj->typolink('IMPRESSUM', $cfgTypoLink) );
	}
	if( $disclaimer ){
		$cfgTypoLink = array( 'parameter' => $disclaimer, 'ATagParams' => $ATagParams  );
		$t[] = str_replace( $srch, $rplc, $cObj->typolink('DISCLAIMER', $cfgTypoLink) );
	}
	//
	$t = join( ' &middot; ', $t);
	//
	return $t;
  }
    
  /**
   * builds Event - Day-Date-Time for Mail
   * 
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @return string
   */
  public function buildMailDayDateTime (\NEXT\IconEvents\Domain\Model\Event $event) {
	//
	$eventStart = $event->getDateStart();
	$eventEnd = $event->getDateEnd();
	$eventDay = $eventStart->format('N');
	$day_array = array('Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag' );
	$eventDay = $day_array[$eventDay-1];
	//
	$t = $eventDay . ', ';
	$t .= $eventStart->format('d.m.Y') . ' | ' . $eventStart->format('H:i') . ' bis '. $eventEnd->format('H:i') .' Uhr';
	//
	return $t;
  }
  
  /**
   * builds IMAGE for Mail
   * 
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @return string
   */
  public function buildMailImage (\NEXT\IconEvents\Domain\Model\Event $event) {
	$t = '';
	$img = $event->getImageMail();
	if( $img ){
		$url = $img->getOriginalResource()->getPublicUrl();
		if( $url ){
			$url = 'http://www.icon.at/' . $url;
			$t = '<tr><td width="100%" valign="top"><img src="'.$url.'" width="600" border="0" style="display:block" alt=""/></td></tr>';
		}
	}
	//
	return $t;
  }
    
  /**
   * inserts MailData into MailBody and returns new string
   *
   * @param string $mailBody
   * @param array $mailData
   * @return string
   */
  public function mergeMailBodyData ($mailBody, $mailData) {
	//
	$mailBody = str_replace('###TOPIC###', $mailData['topic'], $mailBody);
	$mailBody = str_replace('###LOGO_URL###', $mailData['logo_url'], $mailBody);
	$mailBody = str_replace('###LOGO_TITLE###', $mailData['logo_title'], $mailBody);
	$mailBody = str_replace('###IMAGE###', $mailData['mailImage'], $mailBody);
	$mailBody = str_replace('###INTRO###', $mailData['mailIntro'], $mailBody);
	$mailBody = str_replace('###PRE_TITLE###', $mailData['eventPreTitle'], $mailBody);
	$mailBody = str_replace('###TITLE###', $mailData['eventTitle'], $mailBody);
	$mailBody = str_replace('###DETAILS###', $mailData['eventDetails'], $mailBody);
	$mailBody = str_replace('###INFO###', $mailData['mailInfo'], $mailBody);
	$mailBody = str_replace('###PRICEINFO###', $mailData['eventPrice'], $mailBody);
	$mailBody = str_replace('###FOOTER_LINKS###', $mailData['mailFooterLinks'], $mailBody);
	//
	return $mailBody;
  }
  
  
  /**
   * returns contents of template as MailBody
   *
   * @param array $settings
   * @return string
   */
  public function getMailBody ($settings) {
	$fp_file = $settings['mail']['template'];
	$fp = fopen($fp_file, 'r');
	$mailBody = fread($fp, filesize($fp_file));
	fclose($fp);
	//
	return $mailBody;
  }

}
?>