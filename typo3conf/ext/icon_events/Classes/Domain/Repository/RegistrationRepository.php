<?php
namespace NEXT\IconEvents\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class RegistrationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	
	/**
	 * copyFormdata
	 * 
	 * @param \NEXT\IconEvents\Domain\Model\Form $formdata
	 * @param \NEXT\IconEvents\Domain\Model\Event $event
	 * @return \NEXT\IconEvents\Domain\Model\Registration
	 */
	public function copyFormdata(\NEXT\IconEvents\Domain\Model\Form $formdata, \NEXT\IconEvents\Domain\Model\Event $event){
		//
		$reg = $this->objectManager->getEmptyObject('\NEXT\IconEvents\Domain\Model\Registration');
		//
		$reg->setEvent($event->getUid());
		//
		$reg->setGender($formdata->getGender());
		$reg->setTitle($formdata->getTitle());
		$reg->setFirstname($formdata->getFirstname());
		$reg->setLastname($formdata->getLastname());
		$reg->setTitleAfter($formdata->getTitleAfter());
		$reg->setFunction($formdata->getFunction());
		$reg->setPhone($formdata->getPhone());
		$reg->setEmail($formdata->getEmail());
		//
		$reg->setAttendents($formdata->getParticipants());
		//
		$reg->setAddrTitle($formdata->getAddrTitle());
		$reg->setAddrStreet($formdata->getAddrStreet());
		$reg->setAddrZip($formdata->getAddrZip());
		$reg->setAddrCity($formdata->getAddrCity());
		$reg->setBillAddr($formdata->getBillAddr());
		//
		if( $reg->getBillAddr() ){
			//
			$reg->setBillTitle($formdata->getBillTitle());
			$reg->setBillStreet($formdata->getBillStreet());
			$reg->setBillZip($formdata->getBillZip());
			$reg->setBillCity($formdata->getBillCity());
		}
		//
		$reg->setMessage($formdata->getMessage());
		$reg->setNewsletter($formdata->getNewsletter());
		//
		return $reg;
	}

	/**
	 * copyAttendent
	 * 
	 * @param \integer $nr
	 * @param \NEXT\IconEvents\Domain\Model\Form $formdata
	 * @param \NEXT\IconEvents\Domain\Model\Event $event
	 * @param \NEXT\IconEvents\Domain\Model\Registration $registration
	 * @return \NEXT\IconEvents\Domain\Model\Registration
	 */
	public function copyAttendent($nr, \NEXT\IconEvents\Domain\Model\Form $formdata, \NEXT\IconEvents\Domain\Model\Event $event, \NEXT\IconEvents\Domain\Model\Registration $registration){
		//
		$reg = $this->objectManager->getEmptyObject('\NEXT\IconEvents\Domain\Model\Registration');
		//
		$reg->setEvent($event->getUid());
		$reg->setParentRegistration($registration->getUid());
		//
		if( $nr==1 ){
			$reg->setGender($formdata->getGender1());
			$reg->setTitle($formdata->getTitle1());
			$reg->setFirstname($formdata->getFirstname1());
			$reg->setLastname($formdata->getLastname1());
			$reg->setTitleAfter($formdata->getTitleAfter1());
			$reg->setFunction($formdata->getFunction1());
			$reg->setPhone($formdata->getPhone1());
			$reg->setEmail($formdata->getEmail1());
		} elseif( $nr==2 ){
			$reg->setGender($formdata->getGender2());
			$reg->setTitle($formdata->getTitle2());
			$reg->setFirstname($formdata->getFirstname2());
			$reg->setLastname($formdata->getLastname2());
			$reg->setTitleAfter($formdata->getTitleAfter2());
			$reg->setFunction($formdata->getFunction2());
			$reg->setPhone($formdata->getPhone2());
			$reg->setEmail($formdata->getEmail2());
		} elseif( $nr==3 ){
			$reg->setGender($formdata->getGender3());
			$reg->setTitle($formdata->getTitle3());
			$reg->setFirstname($formdata->getFirstname3());
			$reg->setLastname($formdata->getLastname3());
			$reg->setTitleAfter($formdata->getTitleAfter3());
			$reg->setFunction($formdata->getFunction3());
			$reg->setPhone($formdata->getPhone3());
			$reg->setEmail($formdata->getEmail3());
		} elseif( $nr==4 ){
			$reg->setGender($formdata->getGender4());
			$reg->setTitle($formdata->getTitle4());
			$reg->setFirstname($formdata->getFirstname4());
			$reg->setLastname($formdata->getLastname4());
			$reg->setTitleAfter($formdata->getTitleAfter4());
			$reg->setFunction($formdata->getFunction4());
			$reg->setPhone($formdata->getPhone4());
			$reg->setEmail($formdata->getEmail4());
		} elseif( $nr==5 ){
			$reg->setGender($formdata->getGender5());
			$reg->setTitle($formdata->getTitle5());
			$reg->setFirstname($formdata->getFirstname5());
			$reg->setLastname($formdata->getLastname5());
			$reg->setTitleAfter($formdata->getTitleAfter5());
			$reg->setFunction($formdata->getFunction5());
			$reg->setPhone($formdata->getPhone5());
			$reg->setEmail($formdata->getEmail5());
		} elseif( $nr==6 ){
			$reg->setGender($formdata->getGender6());
			$reg->setTitle($formdata->getTitle6());
			$reg->setFirstname($formdata->getFirstname6());
			$reg->setLastname($formdata->getLastname6());
			$reg->setTitleAfter($formdata->getTitleAfter6());
			$reg->setFunction($formdata->getFunction6());
			$reg->setPhone($formdata->getPhone6());
			$reg->setEmail($formdata->getEmail6());
		} elseif( $nr==7 ){
			$reg->setGender($formdata->getGender7());
			$reg->setTitle($formdata->getTitle7());
			$reg->setFirstname($formdata->getFirstname7());
			$reg->setLastname($formdata->getLastname7());
			$reg->setTitleAfter($formdata->getTitleAfter7());
			$reg->setFunction($formdata->getFunction7());
			$reg->setPhone($formdata->getPhone7());
			$reg->setEmail($formdata->getEmail7());
		}
		//
	//	$reg->setAttendents($formdata->getParticipants());
		//
		$reg->setAddrTitle($formdata->getAddrTitle());
		$reg->setAddrStreet($formdata->getAddrStreet());
		$reg->setAddrZip($formdata->getAddrZip());
		$reg->setAddrCity($formdata->getAddrCity());
		$reg->setBillAddr($formdata->getBillAddr());
		//
		if( $reg->getBillAddr() ){
			//
			$reg->setBillTitle($formdata->getBillTitle());
			$reg->setBillStreet($formdata->getBillStreet());
			$reg->setBillZip($formdata->getBillZip());
			$reg->setBillCity($formdata->getBillCity());
		}
		//
	//	$reg->setMessage($formdata->getMessage());
		$reg->setNewsletter($formdata->getNewsletter());
		//
		return $reg;
	}
	
	/*
	 * @param \integer uid
	 * @return
     */
	public function getAttendentsByParent($uid) {
			$query = $this->createQuery();
			$query->getQuerySettings()->setRespectStoragePage(FALSE);    
			return $query->matching($query->equals('parent_registration', $uid))->execute();
	}

  /**
   * buildCsvFile
   *
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @param \array $settings
   * @return \string
   */
  public function buildCsvFile (\NEXT\IconEvents\Domain\Model\Event $event, $settings) {
	//
	$filename .= 'teilnehmerliste';
	$headers = array('Frau/Herr', 'Titel', 'Vorname', 'Nachname', 'Nachgest. Titel', 'Position', 'Telefon', 'E-Mail', 'Begleitpersonen', 'Firma_Name', 'Firma_Straße', 'Firma_PLZ', 'Firma_Ort', 'Rechungsadresse', 'Rechnung_Name', 'Rechnung_Straße', 'Rechnung_PLZ', 'Rechnung_Ort', 'Anmerkungen', 'Newsletter', 'uid', 'parent', 'Datum/Uhrzeit');
	$delimiter = ';';
	// we use a threshold of 2 MB (2 *1024 * 1024), it's just an example
	$fd = fopen('php://temp/maxmemory:2097152', 'w');
	if($fd === FALSE) {
		die('Failed to open temporary file');
	}
	//
	fputcsv($fd, $headers, $delimiter);
	//
	$uid = $event->getUid();
	if( $uid > 0 ){
		//
		$filename .= '_uid' . $uid . '_' . $event->getDateStart()->format('Y-m-d_Hi');
		//
		$sql = ' SELECT * FROM `tx_iconevents_domain_model_registration` ';
		$sql .= ' WHERE `event` = ' . $uid ;
		$sql .= ' ORDER BY `uid` ASC ';
		//
		$res = $GLOBALS['TYPO3_DB']->sql_query($sql);
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$crdate = $row['crdate'];
			$t = array(
				$row['gender'],
				$row['title'],
				$row['firstname'],
				$row['lastname'],
				$row['title_after'],
				$row['function'],
				$row['phone'],
				$row['email'],
				$row['attendents'],
				$row['addr_title'],
				$row['addr_street'],
				$row['addr_zip'],
				$row['addr_city'],
				$row['bill_addr'],
				$row['bill_title'],
				$row['bill_street'],
				$row['bill_zip'],
				$row['bill_city'],
				$row['message'],
				$row['newsletter'],
				$row['uid'],
				$row['parent_registration'],
				date("d.m.Y H:i:s", $row['crdate'])
			);
			fputcsv($fd, $t, $delimiter);
		}
		$GLOBALS['TYPO3_DB']->sql_free_result($res);
	}
	//
	rewind($fd);
	$csv = stream_get_contents($fd);
	fclose($fd);
	//
	$filename .= '.csv';
	$csv = utf8_decode($csv);
	//
//	header('Content-Type: application/csv');
//	header('Content-Type: text/csv; charset=utf-8');
    // tell the browser we want to save it instead of displaying it
    header('Content-Disposition: attachement; filename="'.$filename.'";');
	//
	return $csv;
  }

}
?>