<?php
namespace NEXT\IconEvents\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FolderRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	
	/**
	 * copyFormdata
	 * 
	 * @param \NEXT\IconEvents\Domain\Model\Folder $folderdata
	 * @param \NEXT\IconEvents\Domain\Model\Event $event
	 * @return \NEXT\IconEvents\Domain\Model\Folder
	 */
	public function copyFormdata(\NEXT\IconEvents\Domain\Model\Folder $folderdata, \NEXT\IconEvents\Domain\Model\Event $event){
		//
		$f = $this->objectManager->getEmptyObject('\NEXT\IconEvents\Domain\Model\Folder');
		//
		$f->setEvent($event->getUid());
		//
		$f->setGender($folderdata->getGender());
		$f->setTitle($folderdata->getTitle());
		$f->setFirstname($folderdata->getFirstname());
		$f->setLastname($folderdata->getLastname());
		$f->setTitleAfter($folderdata->getTitleAfter());
		$f->setFunction($folderdata->getFunction());
		$f->setPhone($folderdata->getPhone());
		$f->setEmail($folderdata->getEmail());
		//
		$f->setBillTitle($folderdata->getBillTitle());
		$f->setBillStreet($folderdata->getBillStreet());
		$f->setBillZip($folderdata->getBillZip());
		$f->setBillCity($folderdata->getBillCity());
		//
		$f->setSendPdf($folderdata->getSendPdf());
		$f->setMessage($folderdata->getMessage());
		$f->setNewsletter($folderdata->getNewsletter());
		//
		return $f;
	}


  /**
   * buildCsvFile
   *
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @param \array $settings
   * @return \string
   */
  public function buildCsvFile (\NEXT\IconEvents\Domain\Model\Event $event, $settings) {
	//
	$filename .= 'seminarunterlagen';
	$headers = array('Frau/Herr', 'Titel', 'Vorname', 'Nachname', 'Nachgest. Titel', 'Position', 'Telefon', 'E-Mail', 'Rechnung_Name', 'Rechnung_Straße', 'Rechnung_PLZ', 'Rechnung_Ort', 'per E-Mail', 'Anmerkungen', 'Newsletter', 'Datum/Uhrzeit');
	$delimiter = ';';
	// we use a threshold of 2 MB (2 *1024 * 1024), it's just an example
	$fd = fopen('php://temp/maxmemory:2097152', 'w');
	if($fd === FALSE) {
		die('Failed to open temporary file');
	}
	//
	fputcsv($fd, $headers, $delimiter);
	//
	$uid = $event->getUid();
	if( $uid > 0 ){
		//
		$filename .= '_uid' . $uid . '_' . $event->getDateStart()->format('Y-m-d_Hi');
		//
		$sql = ' SELECT * FROM `tx_iconevents_domain_model_folder` ';
		$sql .= ' WHERE `event` = ' . $uid ;
		$sql .= ' ORDER BY `uid` ASC ';
		//
		$res = $GLOBALS['TYPO3_DB']->sql_query($sql);
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$crdate = $row['crdate'];
			$t = array(
				$row['gender'],
				$row['title'],
				$row['firstname'],
				$row['lastname'],
				$row['title_after'],
				$row['function'],
				$row['phone'],
				$row['email'],
				$row['bill_title'],
				$row['bill_street'],
				$row['bill_zip'],
				$row['bill_city'],
				$row['send_pdf'],
				$row['message'],
				$row['newsletter'],
				date("d.m.Y H:i:s", $row['crdate'])
			);
			fputcsv($fd, $t, $delimiter);
		}
		$GLOBALS['TYPO3_DB']->sql_free_result($res);
	}
	//
	rewind($fd);
	$csv = stream_get_contents($fd);
	fclose($fd);
	//
	$filename .= '.csv';
	$csv = utf8_decode($csv);
	//
//	header('Content-Type: application/csv');
//	header('Content-Type: text/csv; charset=utf-8');
    // tell the browser we want to save it instead of displaying it
    header('Content-Disposition: attachement; filename="'.$filename.'";');
	//
	return $csv;
  }

}
?>