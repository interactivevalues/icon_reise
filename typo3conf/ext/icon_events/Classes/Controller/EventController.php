<?php
namespace NEXT\IconEvents\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class EventController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

  /**
   * eventRepository
   *
   * @var \NEXT\IconEvents\Domain\Repository\EventRepository
   * @inject
   */
  protected $eventRepository;

  /**
   * tagRepository
   *
   * @var \NEXT\IconEvents\Domain\Repository\TagRepository
   * @inject
   */
  protected $tagRepository;
  
  /**
   * categoryRepository
   *
   * @var \NEXT\IconEvents\Domain\Repository\CategoryRepository
   * @inject
   */
  protected $categoryRepository;
  
  /**
   * speakerRepository
   *
   * @var \NEXT\IconEvents\Domain\Repository\SpeakerRepository
   * @inject
   */
  protected $speakerRepository;
  
  /**
   * registrationRepository
   *
   * @var \NEXT\IconEvents\Domain\Repository\RegistrationRepository
   * @inject
   */
  protected $registrationRepository;
  
  /**
   * folderRepository
   *
   * @var \NEXT\IconEvents\Domain\Repository\FolderRepository
   * @inject
   */
  protected $folderRepository;
  
  /**
   * webserviceRepository
   *
   * @var \NEXT\IconIgelconnector\Domain\Repository\WebserviceRepository
   * @inject
   */
  protected $webserviceRepository;
    
  /**
   * persistence manager
   *
   * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
   * @inject
   */
  protected $persistenceManager;
  

  /**
   * action index
   *
   * @param \NEXT\IconEvents\Domain\Model\Search $searchdata
   * @dontvalidate $searchdata
   * @return void
   */
  public function indexAction(\NEXT\IconEvents\Domain\Model\Search $searchdata = NULL) {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
    if( $searchdata==NULL ){
      $searchdata = $this->objectManager->getEmptyObject('\NEXT\IconEvents\Domain\Model\Search');
		//
		if( $this->request->hasArgument('speaker') ){
			$speaker = $this->request->getArgument('speaker');
			$searchdata->setSpeaker($speaker);
		}
    }
	
	//	GENERLIERE LISTE ALLER VORTRAGENDEN, WELCHE 
	$events_speaker = $this->eventRepository->listEvents( $this->objectManager->getEmptyObject('\NEXT\IconEvents\Domain\Model\Search') );
	$speakers = array();
	$speakers_DELIMITER = '|sort ';
	foreach( $events_speaker as $event ){
		$speaker = explode(',', $event->getSpeaker());
		$sp1_uid = $speaker[0];
		$sp2_uid = $speaker[1];
		$sp1 = $this->speakerRepository->findByUid($sp1_uid);
		$sp2 = $this->speakerRepository->findByUid($sp2_uid);
		if( $sp1 ){
			if( $sp1->getAusgetretten() == FALSE ){
				$t_name = $sp1->getLastName() . $speakers_DELIMITER . $sp1->getTitle() . ' ' . $sp1->getFirstName() . ' ' . $sp1->getLastName();
				$t_name = $sp1->getTitleAfter() ? $t_name .= ', ' . $sp1->getTitleAfter() : $t_name;
				$speakers[ $sp1_uid ] = $t_name;		
			}
		}
		if( $sp2 ){
			if( $sp2->getAusgetretten() == FALSE ){
				$t_name = $sp2->getLastName() . $speakers_DELIMITER . $sp2->getTitle() . ' ' . $sp2->getFirstName() . ' ' . $sp2->getLastName();
				$t_name = $sp2->getTitleAfter() ? $t_name .= ', ' . $sp2->getTitleAfter() : $t_name;
				$speakers[ $sp2_uid ] = $t_name;			
			}
		}
	}
	//	SORTIERE VORTRAGENDE ALPHABETISCH NACH NACHNAMEN
	asort($speakers);
	//	BEREINIGE VORTRAGENDE
	foreach($speakers as $k => $v ){
		$v = explode($speakers_DELIMITER, $v);
		$speakers[$k] = $v[1];
	}

	//
	$tags = $this->tagRepository->listAll();
	$categories = $this->categoryRepository->buildListForSearch();
	$events = $this->eventRepository->listEvents($searchdata);
	//
	$this->view->assignMultiple(array(
		'LANG' => $LANG,
		'tags' => $tags,
		'categories' => $categories,
		'speakers' => $speakers,
		'searchdata' => $searchdata,
		'events' => $events,
	));
  }

  /**
   * action detail
   *
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @return void
   */
  public function detailAction(\NEXT\IconEvents\Domain\Model\Event $event = NULL) {
	
/*
	if (is_null($news) && isset($this->settings['detail']['errorHandling'])) {
		$this->handleNoNewsFoundError($this->settings['detail']['errorHandling']);
	}
*/

	if ( is_null($event) ) {
		/*
			FUNKTIONIERT NUR, WENN DER PATCH EINGEBAUT IST:
			https://forge.typo3.org/issues/55861
			https://review.typo3.org/#/c/27535/2/typo3/sysext/extbase/Classes/Mvc/Controller/AbstractController.php
		*/
		$this->redirect('index');
//		break;
	}
	
	//	CHANGE PAGE - TITLE
	$page_title = $event->getTitle();
	$GLOBALS['TSFE']->page['title'] = $page_title;
	$GLOBALS['TSFE']->indexedDocTitle = $page_title;
	
	if( $this->settings['onlyLogo'] ){
		$t = $event->getType();
		$p = 'type' . $t;
		$file = $this->settings['logo'][$p];
		$alt = $this->settings['logo']['altText'];
		$title = $this->settings['logo']['titleText'];
		$width = $this->settings['logo']['width'];
		$img = '<img src="'.$file.'" alt="'.$alt.'" title="'.$title.'" width="'.$width.'" border="0" />';
		return $img;
	}
	
	$speaker = explode(',', $event->getSpeaker());
	$sp1 = $this->speakerRepository->findByUid($speaker[0]);
	$sp2 = $this->speakerRepository->findByUid($speaker[1]);
	if( $sp1 ){
		if( $sp1->getAusgetretten() == FALSE ){
			$event->setSpeaker1( $sp1 );
		}
	}
	if( $sp2 ){
		if( $sp2->getAusgetretten() == FALSE ){
			$event->setSpeaker2( $sp2 );
		}
	}
    $this->view->assign('eventItem', $event);
  }

  /**
   * action form
   *
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @param \NEXT\IconEvents\Domain\Model\Form $formdata
   * @return void
   */
  public function formAction(\NEXT\IconEvents\Domain\Model\Event $event = NULL, \NEXT\IconEvents\Domain\Model\Form $formdata = NULL) {
	//
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$LANG_str = $LANG == 1 ? 'EN' : 'DE';
	//
	$max_participants = array(0,1,2,3,4,5,6,7);
	$max_participants_gallery = array(0,1,2,3,4,5,6,7,8,9,10);
	//
	if( $formdata == NULL ){
		$formdata = $this->objectManager->getEmptyObject('\NEXT\IconEvents\Domain\Model\Form');
		$formdata->setType($event->getType());
	}
	//
	$lst_titel = unserialize($this->webserviceRepository->getFileData('titel-plain', $LANG_str));
	$lst_titel_nach = unserialize($this->webserviceRepository->getFileData('titel_nach-plain', $LANG_str));
	$lst_pf = unserialize($this->webserviceRepository->getFileData('beziehungen-plain', $LANG_str));
	//
	$this->view->assignMultiple(array(
		'eventItem' => $event,
		'formdata' => $formdata,
		'max_participants' => $max_participants,
		'max_participants_gallery' => $max_participants_gallery,
		'lst_titel' => $lst_titel,
		'lst_titel_nach' => $lst_titel_nach,
		'lst_pf' => $lst_pf,
	));
  }

  /**
   * action folder
   *
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @param \NEXT\IconEvents\Domain\Model\Form $folderdata
   * @return void
   */
  public function folderAction(\NEXT\IconEvents\Domain\Model\Event $event = NULL, \NEXT\IconEvents\Domain\Model\Form $folderdata = NULL) {
	//
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$LANG_str = $LANG == 1 ? 'EN' : 'DE';
	//
	$lst_titel = unserialize($this->webserviceRepository->getFileData('titel-plain', $LANG_str));
	$lst_titel_nach = unserialize($this->webserviceRepository->getFileData('titel_nach-plain', $LANG_str));
	$lst_pf = unserialize($this->webserviceRepository->getFileData('beziehungen-plain', $LANG_str));
	//
	$this->view->assignMultiple(array(
		'eventItem' => $event,
		'formdata' => $folderdata,
		'lst_titel' => $lst_titel,
		'lst_titel_nach' => $lst_titel_nach,
		'lst_pf' => $lst_pf,
	));
  }

  /**
   * action store
   *
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @param \NEXT\IconEvents\Domain\Model\Form $formdata
   * @validate $formdata \NEXT\IconEvents\Validation\Validator\FormdataValidator
   * @return void
   */
  public function storeAction(\NEXT\IconEvents\Domain\Model\Event $event = NULL, \NEXT\IconEvents\Domain\Model\Form $formdata = NULL) {
	//
	if( $event==NULL ){
		$this->redirect();
	}
	//	-> check formdata
    if( $formdata==NULL ){
		if( $event->getUid() > 0 ){
			//	-> show Result-Screen
			$actionName = 'form';
			$controllerName = NULL;
			$extensionName = NULL;
			$arguments = array( 'event' => $event->getUid() );
			$this->redirect($actionName, $controllerName, $extensioName, $arguments);
		} else {
			$this->redirect();
		}
	}
	//
	$registration = $this->registrationRepository->copyFormdata($formdata, $event);
    $this->registrationRepository->add($registration);
    $this->persistenceManager->persistAll();
	//
	$hash = md5($registration->getUid());
	$registration->setChash($hash);
	//
    $this->registrationRepository->update($registration);
    $this->persistenceManager->persistAll();
	
	//
	$eventType = $event->getType();
	$eventDayDateTime = $this->eventRepository->buildMailDayDateTime($event);
	//
	$mailData = array(
		'topic' => $this->settings['mailRegisterTopic'],
		'logo_url' => $this->settings['mailLogoURL'] . $this->settings['type' .$eventType. 'MailLogoPath'],
		'logo_title' => $this->settings['type' .$eventType. 'MailLogoTitle'],
		'mailImage' => $this->eventRepository->buildMailImage($event),
		'mailIntro' => $this->settings['type' .$eventType. 'MailIntro'],
		'mailInfo' => $this->settings['type' .$eventType. 'MailInfo'],
		'eventPreTitle' => $this->eventRepository->buildMailPreTitle($event),
		'eventTitle' => $this->eventRepository->buildMailTitle($event),
		'eventDetails' => $this->eventRepository->buildMailDetails($event, $this->settings, $this->configurationManager->getContentObject()),
		'eventPrice' => $this->eventRepository->buildMailPrice($event, $this->settings),
		'mailFooterLinks' => $this->eventRepository->buildMailFooterLinks($this->settings, $this->configurationManager->getContentObject()),
	);
	$mailBody = $this->eventRepository->buildMailBody($mailData, $this->settings);
	//
	$user_mailBody = str_replace('###ANREDE###', $registration->getBriefAnrede(), $mailBody);
	//
	$mailCFG = array(
		'fromEmail' => $this->settings['type' . $eventType . 'ContactEmail'],
		'fromName' => $this->settings['type' .$eventType. 'MailSenderName'],
		'subject' => $this->settings['type' .$eventType. 'MailSubject'] . ' ' . $eventDayDateTime,
		'contentType' => 'text/html',
		'charset' => $GLOBALS['TSFE']->metaCharset,
		'toEmail' => $registration->getEmail(),
		'toName' => trim($registration->getFirstname() . ' ' . $registration->getLastname()),
	);
	//
	$user_mailRes = $this->eventRepository->deliverMail($mailCFG, $user_mailBody);

	//
	if( $eventType < 2 ){
		$attendents = $registration->getAttendents();
		if( $attendents > 0 ){
			$t_array = array();
			for( $i=1; $i<=$attendents; $i++ ){
				//	-> create Attendent
				$i_attendent = $this->registrationRepository->copyAttendent($i, $formdata, $event, $registration);
				$this->registrationRepository->add($i_attendent);
				//
				$i_mailBody = str_replace('###ANREDE###', $i_attendent->getBriefAnrede(), $mailBody);
				//
				$i_mailCFG = $mailCFG;
				$i_mailCFG['toEmail'] = $i_attendent->getEmail();
				$i_mailCFG['toName'] = trim($i_attendent->getFirstname() . ' ' . $i_attendent->getLastname());
				//	-> submit E-Mail to Attendent
				$i_mailRes = $this->eventRepository->deliverMail($i_mailCFG, $i_mailBody);
			}
			//	-> save all Attendents to DB
			$this->persistenceManager->persistAll();
		}
	}
	//	has MESSAGE?
	if( trim($registration->getMessage()) != '' && $this->settings['mailRegisterInfoSubject'] != ''){
		//	send INFO to central USER
		$m_info = '';
		$m_info .= 'Teilnehmer <strong>';
		$m_info .= '<a href="mailto:' . $registration->getEmail() . '">' . $registration->getFirstname() . ' ' . $registration->getLastname() . '</a>';
		$m_info .= '</strong> schreibt:<br><br>';
		$m_info .= nl2br( strip_tags( $registration->getMessage() ) );
		$m_info .= '<br>';
		//
		$m_mailData = $mailData;
		$m_mailData['topic'] = $this->settings['mailRegisterInfoSubject'];
		$m_mailData['mailIntro'] = '';
		$m_mailData['mailInfo'] = $m_info;
		$m_mailBody = $this->eventRepository->buildMailBody($m_mailData, $this->settings);
		$m_mailBody = str_replace('###ANREDE###!', '', $m_mailBody);
		//
		$m_mailCFG = $mailCFG;
		$m_mailCFG['toEmail'] = $this->settings['type' . $eventType . 'ContactEmail'];
		//$m_mailCFG['toEmail'] = 'r.schmoller@next-linz.com';
		$m_mailCFG['toName'] = $this->settings['type' .$eventType. 'MailSenderName'];
		$m_mailCFG['subject'] = $this->settings['mailRegisterInfoSubject'];
		//
		$m_mailRes = $this->eventRepository->deliverMail($m_mailCFG, $m_mailBody);
	}
	//	-> show Result-Screen
	$actionName = 'done';
	$controllerName = NULL;
	$extensionName = NULL;
	$arguments = array(
		'registration' => $registration->getUid(),
	);
	$this->redirect($actionName, $controllerName, $extensioName, $arguments);
  }
  
  /**
   * action done
   *
   * @param \NEXT\IconEvents\Domain\Model\Registration $registration
   * @return void
   */
  public function doneAction(\NEXT\IconEvents\Domain\Model\Registration $registration = NULL) {
	//
	if( $registration==NULL ){
		$this->redirect();
	}
	//
	$event = $this->eventRepository->findByUid($registration->getEvent());
	$attendents = $this->registrationRepository->getAttendentsByParent($registration->getUid());
	//
	$this->view->assignMultiple(array(
		'eventItem' => $event,
		'registration' => $registration,
		'attendents' => $attendents,
	));
  }

  /**
   * action storeFolder
   *
   * @param \NEXT\IconEvents\Domain\Model\Event $event
   * @param \NEXT\IconEvents\Domain\Model\Form $folderdata
   * @validate $folderdata \NEXT\IconEvents\Validation\Validator\FolderdataValidator
   * @return void
   */
  public function storeFolderAction(\NEXT\IconEvents\Domain\Model\Event $event = NULL, \NEXT\IconEvents\Domain\Model\Folder $folderdata = NULL) {
	//
	if( $event==NULL ){
		$this->redirect();
	}
	//	-> check formdata
    if( $folderdata==NULL ){
		if( $event->getUid() > 0 ){
			//	-> show Result-Screen
			$actionName = 'folder';
			$controllerName = NULL;
			$extensionName = NULL;
			$arguments = array( 'event' => $event->getUid() );
			$this->redirect($actionName, $controllerName, $extensioName, $arguments);
		} else {
			$this->redirect();
		}
	}
	//	save Model-Data to DB
	$folderdata->setEvent($event->getUid());
    $this->folderRepository->add($folderdata);
    $this->persistenceManager->persistAll();
	//
	$hash = md5($folderdata->getUid());
	$folderdata->setChash($hash);
	//
    $this->folderRepository->update($folderdata);
    $this->persistenceManager->persistAll();

	//
	$eventType = 0;		//	$event->getType();
//	$eventDayDateTime = $this->eventRepository->buildMailDayDateTime($event);
	//
	$mailData = array(
		'topic' => $this->settings['mailFolderTopic'],
		'logo_url' => $this->settings['mailLogoURL'] . $this->settings['type' .$eventType. 'MailLogoPath'],
		'logo_title' => $this->settings['type' .$eventType. 'MailLogoTitle'],
		'mailIntro' => $this->settings['mailFolderIntro'],
		'mailInfo' => $this->settings['mailFolderInfo'],
		'eventPreTitle' => $this->eventRepository->buildMailPreTitle($event),
		'eventTitle' => $this->eventRepository->buildMailTitle($event),
		'eventDetails' => $this->eventRepository->buildMailDetails($event, $this->settings, $this->configurationManager->getContentObject()),
		'eventPrice' => $this->eventRepository->buildMailPrice($event, $this->settings),
		'mailFooterLinks' => $this->eventRepository->buildMailFooterLinks($this->settings, $this->configurationManager->getContentObject()),
	);
	$mailBody = $this->eventRepository->buildMailBody($mailData, $this->settings);
	//
	$mailBody = str_replace('###ANREDE###', $folderdata->getBriefAnrede(), $mailBody);
	//
	$mailCFG = array(
		'fromEmail' => $this->settings['type' . $eventType . 'ContactEmail'],
		'fromName' => $this->settings['type' .$eventType. 'MailSenderName'],
		'subject' => $this->settings['mailFolderSubject'],
		'contentType' => 'text/html',
		'charset' => $GLOBALS['TSFE']->metaCharset,
		'toEmail' => $folderdata->getEmail(),
		'toName' => trim($folderdata->getFirstname() . ' ' . $folderdata->getLastname()),
	);
	//
	$mailRes = $this->eventRepository->deliverMail($mailCFG, $mailBody);
	
	//	send FORMDATA to central USER
	//
	$m_info = '';
	$m_info .= '<table border="0" cellpadding="0" cellspacing="5">';
	$m_info .= '<tr><td valign="top" width="170"><strong>Anrede</strong></td><td>'. $folderdata->getGender() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Titel</strong></td><td>'. $folderdata->getTitle() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Vorname</strong></td><td>'. $folderdata->getFirstname() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Nachname</strong></td><td>'. $folderdata->getLastname() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Nachgest. Titel</strong></td><td>'. $folderdata->getTitleAfter() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Position/Funktion</strong></td><td>'. $folderdata->getFunction() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Telefon</strong></td><td>'. $folderdata->getPhone() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>E-Mail</strong></td><td>'. $folderdata->getEmail() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Firma</strong></td><td>'. $folderdata->getBillTitle() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Stra&szlig;e</strong></td><td>'. $folderdata->getBillStreet() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>PLZ</strong></td><td>'. $folderdata->getBillZip() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Ort</strong></td><td>'. $folderdata->getBillCity() .'</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Zustellung</strong></td><td>';
	$m_info .= (string)$folderdata->getSendPdf() == '1' ? 'per E-Mail' : 'per Post';
	$m_info .= '</td></tr>';
	$m_info .= '<tr><td valign="top"><strong>Anmerkungen</strong></td><td>'. nl2br( strip_tags( $folderdata->getMessage() ) ) .'</td></tr>';
	$m_info .= '</table>';
	//
	$mailData['mailIntro'] = '';
	$mailData['mailInfo'] = $m_info;
	$mailBody = $this->eventRepository->buildMailBody($mailData, $this->settings);
	//
	$mailBody = str_replace('###ANREDE###!', '', $mailBody);
	//
	$mailCFG['toEmail'] = $this->settings['type' . $eventType . 'ContactEmail'];
	//$mailCFG['toEmail'] = 'r.schmoller@next-linz.com';
	$mailCFG['toName'] = $this->settings['type' .$eventType. 'MailSenderName'];
	//
	$mailRes = $this->eventRepository->deliverMail($mailCFG, $mailBody);

	//	-> show Result-Screen
	$actionName = 'doneFolder';
	$controllerName = NULL;
	$extensionName = NULL;
	$arguments = array(
		'folder' => $folderdata->getUid(),
	);
	$this->redirect($actionName, $controllerName, $extensioName, $arguments);
  }
  
  /**
   * action doneFolder
   *
   * @param \NEXT\IconEvents\Domain\Model\Folder $folder
   * @return void
   */
  public function doneFolderAction(\NEXT\IconEvents\Domain\Model\Folder $folder = NULL) {
	//
	if( $folder==NULL ){
		$this->redirect();
	}
	//
	$event = $this->eventRepository->findByUid($folder->getEvent());
	//
	$this->view->assignMultiple(array(
		'eventItem' => $event,
		'registration' => $folder,
	));
  }



  /**
   * action logo
   *
   * @return void
   */
  public function logoAction() {
	$pi1 = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP("tx_iconevents_pi1");
	$uidEvent = $pi1['event'];
	$uidRegistration = $pi1['registration'];
/*
	echo \TYPO3\CMS\Core\Utility\DebugUtility::debug($_REQUEST);
	echo 'event: ' . $uidEvent . ' | ' . $pi1['event'];
	echo '<br>';
	echo 'registrtaion: ' . $uidRegistration . ' | ' . $pi1['registration'];
	echo '<br>';
*/
	if ( $uidRegistration > 0 ){
		$registration = $this->registrationRepository->findByUid($uidRegistration);
		$uidEvent = $registration->getEvent();
	}	
	if( $uidEvent > 0 ){
		$event = $this->eventRepository->findByUid($uidEvent);
	}
	if( $event ){
		$t = $event->getType();
		$p = 'type' . $t;
		$file = $this->settings['logo'][$p];
	} else {
		$file = $this->settings['logo']['default'];
	}
	$alt = $this->settings['logo']['altText'];
	$title = $this->settings['logo']['titleText'];
	$width = $this->settings['logo']['width'];
	$img = '<img src="'.$file.'" alt="'.$alt.'" title="'.$title.'" width="'.$width.'" border="0" />';
	return $img;
  }

  /**
   * action shortlist
   *
   * @return void
   */
  public function shortlistAction() {
    $pid = (int)$this->settings['storagePid'];
	$offset = 0;
	$limit = (int)$this->settings['limit'];
	//
    $searchdata = $this->objectManager->getEmptyObject('\NEXT\IconEvents\Domain\Model\Search');
	$searchdata->setCategory('type0');
	//
	$events = $this->eventRepository->listEvents($searchdata, $offset, $limit);
	//
	$this->view->assignMultiple(array(
		'LANG' => $LANG,
		'events' => $events,
	));
  }

  /**
   * action teaser
   *
   * @return void
   */
  public function teaserAction() {
    $pid = (int)$this->settings['storagePid'];
	$offset = 0;
	$limit = (int)$this->settings['limit'];
	//
    $searchdata = $this->objectManager->getEmptyObject('\NEXT\IconEvents\Domain\Model\Search');
	//
	$events = $this->eventRepository->listEvents($searchdata, $offset, $limit);
	//
	$this->view->assignMultiple(array(
		'LANG' => $LANG,
		'events' => $events,
	));
  }

  /**
   * action csvFile
   *
   * @return void
   */
  public function csvFileAction() {
	$csv = '';
	// this will execute only in the backend...
	$uidEvent = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP("registrations");
//	$uidEvent = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP("event");
	$uidFolder = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP("folder");
	if( $uidEvent > 0 ){
		$event = $this->eventRepository->getRecordByUid($uidEvent);
	//	$event = $this->eventRepository->findByUid($uidEvent);
		if( $event ){
			$csv = $this->registrationRepository->buildCsvFile($event, $this->settings);
		}
	} elseif ( $uidFolder > 0 ){
		$event = $this->eventRepository->findByUid($uidFolder);
		if( $event ){
			$csv = $this->folderRepository->buildCsvFile($event, $this->settings);
		}
	} else {
		header('Content-Type: text/plain; charset=utf-8');
		$csv = 'ungültiger Aufruf!;event = ' . $uidEvent;
	}
	//
	return $csv;
  }


  /**
   * action icsFile
   *
   * @param \NEXT\IconEvents\Domain\Model\Event $event event item
   * @return void
   */
  public function icsFileAction(\NEXT\IconEvents\Domain\Model\Event $event = NULL) {
//    $this->view->assign('eventItem', $event);
//    $ics = $this->view->render();
	//
	$ics = $this->eventRepository->buildIcsFile($event, $this->settings);
	//

      header('Content-Description: File Transfer');
      header('Content-Type: text/plain; charset=utf-8');
      header('Content-Disposition: attachment; filename="eintrag.ics"');
	echo $ics;die;
  }


}

