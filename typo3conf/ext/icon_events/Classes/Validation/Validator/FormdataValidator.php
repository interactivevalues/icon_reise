<?php
namespace NEXT\IconEvents\Validation\Validator;

/***************************************************************
* Copyright notice
*
* (c) 2014 robert`smo´ schmoller <r.schmoller@next-linz.com>
*
* All rights reserved
*
* This script is part of the TYPO3 project. The TYPO3 project is
* free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* The GNU General Public License can be found at
* http://www.gnu.org/copyleft/gpl.html.
*
* This script is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
* Formdata validator
*
* @package icon_events
* @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
*
*/
class FormdataValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator {

	/**
	 * Returns TRUE, if the given property ($value) is a valid.
	 *
	 * Otherwise, it is FALSE.
	 *
	 * @param mixed $formdata The value that should be validated
	 * @return boolean TRUE if the value is valid, FALSE if an error occured
	 * @api
	 */
	protected function isValid($formdata) {
		$success = TRUE;
		//	get which Type it is!!!
		$eventType = $formdata->getType();
		//	validate bacis info-set
		$success = $this->validateBasics($formdata, $success);
		//
		if( $eventType==0 ){
			//	-> Type0 - SEMINAR
			//	validate PARTICIPANTS
			if( $formdata->getParticipants() > 0 ){
				$success = $this->validateParticipants($formdata, $success);
			}
			//	validate ADDRESS
			$success = $this->validateAddress($formdata, $success);
		
		} elseif( $eventType==1 ){
			//	-> Type1 - EVENT
			//	validate PARTICIPANTS
			if( $formdata->getParticipants() > 0 ){
				$success = $this->validateParticipants($formdata, $success);
			}
		} elseif( $eventType==2 ){
			//	-> Type2 - GALLERY
			//	-> place additional Validation here
		}
		//
		return $success;	
	}
	
	/**
	 * validates firstname, lastname, email
	 *
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateBasics ($formdata, $success){
		$isFN = strlen($formdata->getFirstname()) > 1;
		$isLN = strlen($formdata->getLastname()) > 1;
		$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail());
		//	\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($value)
		if( !$isFN ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Vorname nicht ausgefüllt!', time());
			$this->result->forProperty('firstname')->addError($error);
			$success = FALSE;
		}
		if( !$isLN ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Nachname nicht ausgefüllt!', time());
			$this->result->forProperty('lastname')->addError($error);
			$success = FALSE;
		}
		if( !$isEmail ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('E-Mailadresse nicht ausgefüllt!', time());
			$this->result->forProperty('email')->addError($error);
			$success = FALSE;
		}
		//
		return $success;
	}
	
	/**
	 * validates addrTitle, addrStreet, addrZip, addrCity
	 *
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateAddress ($formdata, $success){
		$isAddrTitle = strlen($formdata->getAddrTitle()) > 1;
		$isAddrStreet = strlen($formdata->getAddrStreet()) > 1;
		$isAddrZip = strlen($formdata->getAddrZip()) > 1;
		$isAddrCity = strlen($formdata->getAddrCity()) > 3;
		if( !$isAddrTitle ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Firma nicht ausgefüllt!', time());
			$this->result->forProperty('addrTitle')->addError($error);
			$success = FALSE;
		}
		if( !$isAddrStreet ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Straße nicht ausgefüllt!', time());
			$this->result->forProperty('addrStreet')->addError($error);
			$success = FALSE;
		}
		if( !$isAddrZip ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('PLZ nicht ausgefüllt!', time());
			$this->result->forProperty('addrZip')->addError($error);
			$success = FALSE;
		}
		if( !$isAddrCity ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Ort nicht ausgefüllt!', time());
			$this->result->forProperty('addrCity')->addError($error);
			$success = FALSE;
		}
		//
		return $success;
	}
	
	/*
	 * @param mixed $formdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateParticipants ($formdata, $success){
		$participants = $formdata->getParticipants();
		if( $participants >= 1 ){
			$nr = 1;
			$TN = '2. Teilnehmer, ';
			if( strlen($formdata->getFirstname1()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname1()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail1());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $participants >= 2 ){
			$nr = 2;
			$TN = '3. Teilnehmer, ';
			if( strlen($formdata->getFirstname2()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname2()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail2());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $participants >= 3 ){
			$nr = 3;
			$TN = '4. Teilnehmer, ';
			if( strlen($formdata->getFirstname3()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname3()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail3());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $participants >= 4 ){
			$nr = 4;
			$TN = '5. Teilnehmer, ';
			if( strlen($formdata->getFirstname4()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname4()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail4());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $participants >= 5 ){
			$nr = 5;
			$TN = '6. Teilnehmer, ';
			if( strlen($formdata->getFirstname5()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname5()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail5());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $participants >= 6 ){
			$nr = 6;
			$TN = '7. Teilnehmer, ';
			if( strlen($formdata->getFirstname6()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname6()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail6());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		if( $participants >= 7 ){
			$nr = 7;
			$TN = '8. Teilnehmer, ';
			if( strlen($formdata->getFirstname7()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Vorname nicht ausgefüllt!', time());
				$this->result->forProperty('firstname' . $nr)->addError($error);
				$success = FALSE;
			}
			if( strlen($formdata->getLastname7()) < 1 ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'Nachname nicht ausgefüllt!', time());
				$this->result->forProperty('lastname' . $nr)->addError($error);
				$success = FALSE;
			}
			$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($formdata->getEmail7());
			if( !$isEmail ){
				$error = new \TYPO3\CMS\Extbase\Validation\Error($TN . 'E-Mailadresse nicht ausgefüllt!', time());
				$this->result->forProperty('email' . $nr)->addError($error);
				$success = FALSE;
			}
		}
		//
		return $success;
	}
}
?>