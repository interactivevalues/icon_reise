<?php
namespace NEXT\IconEvents\Validation\Validator;

/***************************************************************
* Copyright notice
*
* (c) 2014 robert`smo´ schmoller <r.schmoller@next-linz.com>
*
* All rights reserved
*
* This script is part of the TYPO3 project. The TYPO3 project is
* free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* The GNU General Public License can be found at
* http://www.gnu.org/copyleft/gpl.html.
*
* This script is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
* Folderdata validator
*
* @package icon_events
* @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
*
*/
class FolderdataValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator {

	/**
	 * Returns TRUE, if the given property ($value) is a valid.
	 *
	 * Otherwise, it is FALSE.
	 *
	 * @param mixed $folderdata The value that should be validated
	 * @return boolean TRUE if the value is valid, FALSE if an error occured
	 * @api
	 */
	protected function isValid($folderdata) {
		$success = TRUE;
		//	validate bacis info-set
		$success = $this->validateBasics($folderdata, $success);
		//	validate ADDRESS
		$success = $this->validateAddress($folderdata, $success);
		//
		return $success;	
	}
	
	/**
	 * validates firstname, lastname, email
	 *
	 * @param mixed $folderdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateBasics ($folderdata, $success){
		$isFN = strlen($folderdata->getFirstname()) > 1;
		$isLN = strlen($folderdata->getLastname()) > 1;
		$isEmail = \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($folderdata->getEmail());
		//	\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($value)
		if( !$isFN ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Vorname nicht ausgefüllt!', time());
			$this->result->forProperty('firstname')->addError($error);
			$success = FALSE;
		}
		if( !$isLN ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Nachname nicht ausgefüllt!', time());
			$this->result->forProperty('lastname')->addError($error);
			$success = FALSE;
		}
		if( !$isEmail ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('E-Mailadresse nicht ausgefüllt!', time());
			$this->result->forProperty('email')->addError($error);
			$success = FALSE;
		}
		//
		return $success;
	}
	
	/**
	 * validates billTitle, billStreet, billZip, billCity, sendPdf
	 *
	 * @param mixed $folderdata
	 * @param boolean $success
	 * @return boolean
	 */
	protected function validateAddress ($folderdata, $success){
		$isBillTitle = strlen($folderdata->getBillTitle()) > 1;
		$isBillStreet = strlen($folderdata->getBillStreet()) > 1;
		$isBillZip = strlen($folderdata->getBillZip()) > 1;
		$isBillCity = strlen($folderdata->getBillCity()) > 3;
		//
		$t_sendPdf = (string)$folderdata->getSendPdf();
		$isSendPdf = $t_sendPdf === '0' || $t_sendPdf === '1';
		//
		if( !$isBillTitle ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Firma nicht ausgefüllt!', time());
			$this->result->forProperty('billTitle')->addError($error);
			$success = FALSE;
		}
		if( !$isBillStreet ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Straße nicht ausgefüllt!', time());
			$this->result->forProperty('billStreet')->addError($error);
			$success = FALSE;
		}
		if( !$isBillZip ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('PLZ nicht ausgefüllt!', time());
			$this->result->forProperty('billZip')->addError($error);
			$success = FALSE;
		}
		if( !$isBillCity ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Ort nicht ausgefüllt!', time());
			$this->result->forProperty('billCity')->addError($error);
			$success = FALSE;
		}
		if( !$isSendPdf ){
			$error = new \TYPO3\CMS\Extbase\Validation\Error('Zustellung nicht ausgefüllt!', time());
			$this->result->forProperty('sendPdf')->addError($error);
			$success = FALSE;
		}
		//
		return $success;
	}
}
?>