#
# Table structure for table 'tx_iconevents_domain_model_event'
#
CREATE TABLE tx_iconevents_domain_model_event (

  uid int(11) NOT NULL auto_increment,
  pid int(11) DEFAULT '0' NOT NULL,

  type varchar(100) NOT NULL DEFAULT '0',
  title varchar(255) DEFAULT '' NOT NULL,
  pre_title varchar(255) DEFAULT '' NOT NULL,
  pre_title_big tinyint(4) unsigned DEFAULT '0' NOT NULL,
  date_start int(11) DEFAULT '0' NOT NULL,
  date_end int(11) DEFAULT '0' NOT NULL,
  image text NOT NULL,
  image_mail text NOT NULL,
  link varchar(255) DEFAULT '' NOT NULL,
  registration tinyint(4) unsigned DEFAULT '0' NOT NULL,
  registration_endbefore tinyint(4) unsigned DEFAULT '0' NOT NULL,
  location varchar(255) DEFAULT '' NOT NULL,
  building varchar(255) DEFAULT '' NOT NULL,
  address varchar(255) DEFAULT '' NOT NULL,
  address_link varchar(255) DEFAULT '' NOT NULL,
  price varchar(255) DEFAULT '' NOT NULL,
  price_special varchar(255) DEFAULT '' NOT NULL,
  price_info text NOT NULL,
  price_folder varchar(255) DEFAULT '' NOT NULL,
  speaker varchar(255) DEFAULT '' NOT NULL,
  text_links text NOT NULL,
  text_rechts text NOT NULL,
  benefit text NOT NULL,
  related_uid int(11) DEFAULT '0' NOT NULL,
  related_text_info varchar(255) DEFAULT '' NOT NULL,
  related_text_link varchar(255) DEFAULT '' NOT NULL,

  tstamp int(11) unsigned DEFAULT '0' NOT NULL,
  crdate int(11) unsigned DEFAULT '0' NOT NULL,
  cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
  deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
  hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
  starttime int(11) unsigned DEFAULT '0' NOT NULL,
  endtime int(11) unsigned DEFAULT '0' NOT NULL,
  tags int(11) DEFAULT '0' NOT NULL,
  category int(11) DEFAULT '0' NOT NULL,
	
  t3ver_oid int(11) DEFAULT '0' NOT NULL,
  t3ver_id int(11) DEFAULT '0' NOT NULL,
  t3ver_wsid int(11) DEFAULT '0' NOT NULL,
  t3ver_label varchar(255) DEFAULT '' NOT NULL,
  t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
  t3ver_stage int(11) DEFAULT '0' NOT NULL,
  t3ver_count int(11) DEFAULT '0' NOT NULL,
  t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
  t3ver_move_id int(11) DEFAULT '0' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  t3_origuid int(11) DEFAULT '0' NOT NULL,
  sys_language_uid int(11) DEFAULT '0' NOT NULL,
  l10n_parent int(11) DEFAULT '0' NOT NULL,
  l10n_diffsource mediumblob,

  PRIMARY KEY (uid),
  KEY parent (pid),
  KEY t3ver_oid (t3ver_oid,t3ver_wsid),
  KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_iconevents_domain_model_tag'
#
CREATE TABLE tx_iconevents_domain_model_tag (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	title tinytext,

	PRIMARY KEY (uid),
	KEY parent (pid)
);

#
# Table structure for table 'tx_iconevents_domain_model_event_tag_mm'
#
CREATE TABLE tx_iconevents_domain_model_event_tag_mm (
	uid_local int(11) DEFAULT '0' NOT NULL,
	uid_foreign int(11) DEFAULT '0' NOT NULL,
	tablenames varchar(30) DEFAULT '' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_iconevents_domain_model_category'
#
CREATE TABLE tx_iconevents_domain_model_category (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumtext,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
	fe_group varchar(100) DEFAULT '0' NOT NULL,
	title tinytext,
	parentcategory int(11) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY parentcategory (parentcategory)
);

#
# Table structure for table 'tx_iconevents_domain_model_event_category_mm'
#
#
CREATE TABLE tx_iconevents_domain_model_event_category_mm (
	uid_local int(11) DEFAULT '0' NOT NULL,
	uid_foreign int(11) DEFAULT '0' NOT NULL,
	tablenames varchar(30) DEFAULT '' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_iconevents_domain_model_registration'
#
CREATE TABLE tx_iconevents_domain_model_registration (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumtext,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
	fe_group varchar(100) DEFAULT '0' NOT NULL,
	
	event int(11) DEFAULT '0' NOT NULL,
	parent_registration int(11) DEFAULT '0' NOT NULL,

	gender varchar(4) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	firstname varchar(255) DEFAULT '' NOT NULL,
	lastname varchar(255) DEFAULT '' NOT NULL,
	title_after varchar(255) DEFAULT '' NOT NULL,
	function varchar(255) DEFAULT '' NOT NULL,
	phone varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	attendents int(11) DEFAULT '0' NOT NULL,

	addr_title varchar(255) DEFAULT '' NOT NULL,
	addr_street varchar(255) DEFAULT '' NOT NULL,
	addr_zip varchar(255) DEFAULT '' NOT NULL,
	addr_city varchar(255) DEFAULT '' NOT NULL,

	bill_addr int(11) DEFAULT '0' NOT NULL,

	bill_title varchar(255) DEFAULT '' NOT NULL,
	bill_street varchar(255) DEFAULT '' NOT NULL,
	bill_zip varchar(255) DEFAULT '' NOT NULL,
	bill_city varchar(255) DEFAULT '' NOT NULL,

	message text NOT NULL,
	newsletter int(11) DEFAULT '0' NOT NULL,
	chash varchar(32) DEFAULT '' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY event (event)
);

#
# Table structure for table 'tx_iconevents_domain_model_folder'
#
CREATE TABLE tx_iconevents_domain_model_folder (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumtext,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
	fe_group varchar(100) DEFAULT '0' NOT NULL,
	
	event int(11) DEFAULT '0' NOT NULL,

	gender varchar(4) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	firstname varchar(255) DEFAULT '' NOT NULL,
	lastname varchar(255) DEFAULT '' NOT NULL,
	title_after varchar(255) DEFAULT '' NOT NULL,
	function varchar(255) DEFAULT '' NOT NULL,
	phone varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	attendents int(11) DEFAULT '0' NOT NULL,

	bill_title varchar(255) DEFAULT '' NOT NULL,
	bill_street varchar(255) DEFAULT '' NOT NULL,
	bill_zip varchar(255) DEFAULT '' NOT NULL,
	bill_city varchar(255) DEFAULT '' NOT NULL,

	send_pdf tinyint(4) DEFAULT '0' NOT NULL,
	message text NOT NULL,
	newsletter int(11) DEFAULT '0' NOT NULL,
	chash varchar(32) DEFAULT '' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY event (event)
);

