plugin.tx_iconevents {
	view {
		templateRootPath = {$plugin.tx_iconevents.view.templateRootPath}
		partialRootPath = {$plugin.tx_iconevents.view.partialRootPath}
		layoutRootPath = {$plugin.tx_iconevents.view.layoutRootPath}
	}

	features {
		requireCHashArgumentForActionArguments = {$plugin.tx_iconevents.features.requireCHashArgumentForActionArguments}
	}

	settings {
		cssFile = {$plugin.tx_iconevents.settings.cssFile}
		logo {
			altText = {$plugin.tx_iconevents.settings.logo.altText}
			titleText = {$plugin.tx_iconevents.settings.logo.titleText}
			width = {$plugin.tx_iconevents.settings.logo.width}
			height = {$plugin.tx_iconevents.settings.logo.height}
			type0 = {$plugin.tx_iconevents.settings.logo.type0}
			type0 = {$plugin.tx_iconevents.settings.logo.type0}
			type1 = {$plugin.tx_iconevents.settings.logo.type1}
			type2 = {$plugin.tx_iconevents.settings.logo.type2}
		}
		list {
		}
		detail {
			image {
				width = {$plugin.tx_iconevents.settings.detail.image.width}
				height = {$plugin.tx_iconevents.settings.detail.image.height}
			}
			imageAddress {
				path = {$plugin.tx_iconevents.settings.detail.imageAddress.path}
				width = {$plugin.tx_iconevents.settings.detail.imageAddress.width}
				height = {$plugin.tx_iconevents.settings.detail.imageAddress.height}
			}
		}
		teaser {
		}
		icsFile {
		  URL = {$plugin.tx_iconevents.settings.icsFile.URL}
		  organizerType0 = {$plugin.tx_iconevents.settings.icsFile.organizerType0}
		  organizerType1 = {$plugin.tx_iconevents.settings.icsFile.organizerType1}
		  organizerType2 = {$plugin.tx_iconevents.settings.icsFile.organizerType2}
		}
		mail {
		  senderEmail = {$plugin.tx_iconevents.settings.mail.senderEmail}
		  senderName = {$plugin.tx_iconevents.settings.mail.senderName}
		  subject = {$plugin.tx_iconevents.settings.mail.subject}
		  template = {$plugin.tx_iconevents.settings.mail.template}
		}
	}
}

#Setup the actual ICS file
tx_iconevents = PAGE
tx_iconevents {
  typeNum = 798
  config.disableAllHeaderCode = 1
  #config.renderCharset = utf-8
  config.renderCharset = iso-8859-1
  config.additionalHeaders = Content-type: text/calendar; charset= iso-8859-1
  #config.additionalHeaders = Content-type: text/calendar; charset= utf-8
  config.no_cache = 1
  10 = USER
  10 {
  	userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    pluginName = Ics
    extensionName = IconEvents
    controller = Event
    vendorName = NEXT
    action = icsFile
    switchableControllerActions {
    Event { 
        1 = icsFile
      }
    }
    settings =< plugin.tx_iconevents.settings
    persistence =< plugin.tx_iconevents.persistence
    view =< plugin.tx_iconevents.view
  }
}

#Setup the actual CSV file
tx_iconevents_csv = PAGE
tx_iconevents_csv {
  typeNum = 797
  config.disableAllHeaderCode = 1
  config.renderCharset = utf-8
  config.additionalHeaders = Content-type: text/csv; charset=iso-8859-1
  config.no_cache = 1
  10 = USER
  10 {
  	userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    pluginName = Csv
    extensionName = IconEvents
    controller = Event
    vendorName = NEXT
    action = csvFile
    switchableControllerActions {
    Event { 
        1 = csvFile
      }
    }
    settings =< plugin.tx_iconevents.settings
    persistence =< plugin.tx_iconevents.persistence
    view =< plugin.tx_iconevents.view
  }
}