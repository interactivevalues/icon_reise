<?php
defined ('TYPO3_MODE') or die ('Access denied.');

$_EXTKEY = 'icon_events';
$ll = 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xml:';

$version7 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('7.0');
$version8 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('8.0');

return array(
	'ctrl' => array(
		'title'  => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event',
		'label' => 'title',
		'prependAtCopy' => ' - Kopie',
		'hideAtCopy' => TRUE,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		//'sortby' => 'sorting',
		'default_sortby' => 'ORDER BY date_start DESC',
		'versioningWS' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'type' => 'type',
/*
        'typeicon_column' => 'type',
        'typeicon_classes' => array(
            '0' => 'ext-iconevents-type-0',
            '1' => 'ext-iconevents-type-1',
            '2' => 'ext-iconevents-type-2',
            '3' => 'ext-iconevents-type-3',
        ),
*/
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		
		'searchFields' => 'title,date_start,date_end,image,text,link,registration,',
		'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/tx_iconevents_domain_model_event.gif'
	),
	'feInterface' => $TCA['tx_iconevents_domain_model_event']['feInterface'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, starttime, endtime, type, pre_title, pre_title_big, title, date_start, date_end, location, building, address, address_link, image, link, registration, price, price_special, speaker, text_links, text_rechts,',
	),
  'types' => array(
  	//   TYPE = SEMINAR
    '0' => array('showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource,
		type;Typ, 
		uid,
		pre_title, pre_title_big, title,
		date_start, date_end, 
		building, address, location, address_link, 
		registration, registration_endbefore, tags,
		,--div--;LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tabs.type.I.0,
		category, speaker, image, 
		benefit, 
		text_links, text_rechts, 
		,--div--;LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tabs.type.I.0-related,
		related_uid, related_text_info, related_text_link, 
		,--div--;LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tabs.price,price_special, price, price_info, price_folder, 
		--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, hidden, starttime, endtime, 
	'),
	//	TYPE = EVENT
    '1' => array('showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, 
		type;Typ, 
		uid,
		pre_title, pre_title_big, title, 
		date_start, date_end, 
		building, address, location, address_link, 
		registration, registration_endbefore, tags,
		,--div--;LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tabs.type.I.1,
		image,
		text_links, text_rechts, 
		--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, hidden, starttime, endtime, 
	'),
	//	TYPE = GALLERY
    '2' => array('showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, 
		type;Typ, 
		uid,
		pre_title, pre_title_big, title, 
		date_start, date_end, 
		building, address, location, address_link, 
		registration, registration_endbefore, tags,
		,--div--;LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tabs.type.I.2,
		image, 
		image_mail,
		text_links;Vernisage;, text_rechts;Ausstellung;, 
		--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, hidden, starttime, endtime, 
	'),
	//	TYPE = EXTERN
    '3' => array('showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, 
		type;Typ, 
		uid,
		title, 
		date_start, date_end, location, link,
		speaker, 
		tags,
		--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, hidden, starttime, endtime, 
	'),
/*
	//	TYPE = GALLERY
    '2' => array('showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden;;1, type, pre_title, pre_title_big;;0, title, date_start, date_end, location, building, address, address_link, image, link, registration, price, price_special, speaker, text_links, text_rechts, ,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,starttime, endtime'),
*/
  ),
  'columns' => array(
    'uid' => array(
	  'exclude' => 1,
      'label' => 'Teilnehmerliste',
      'config' => array(
        'type' => 'user',
        'size' => 30,
        'userFunc' => 'EXT:icon_events/Classes/TCA/class.tx_iconevents_tca.php:tx_iconevents_tca->showLinkCSV',
        'parameters' => array(),
      ),
    ),
    'type' => array(
	  'exclude' => 0,
	  'label' => 'LLL:EXT:cms/locallang_tca.xml:pages.doktype_formlabel',
	  'config' => array(
		'type' => 'select',
		'renderType' => 'selectSingle',
		'items' => array(
			array('LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.type.I.0', 0),
			array('LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.type.I.1', 1),
			array('LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.type.I.2', 2),
			array('LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.type.I.3', 3),
		),
		'size' => 1,
		'maxitems' => 1,
	  )
	),
    'sys_language_uid' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
      'config' => array(
        'type' => 'select',
		'renderType' => 'selectSingle',
        'foreign_table' => 'sys_language',
        'foreign_table_where' => 'ORDER BY sys_language.title',
        'items' => array(
          array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
          array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
        ),
      ),
    ),
    'l10n_parent' => array(
      'displayCond' => 'FIELD:sys_language_uid:>:0',
      'exclude' => 1,
      'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
      'config' => array(
        'type' => 'select',
		'renderType' => 'selectSingle',
        'items' => array(
          array('', 0),
        ),
        'foreign_table' => 'tx_iconevents_domain_model_event',
        'foreign_table_where' => 'AND tx_iconevents_domain_model_event.pid=###CURRENT_PID### AND tx_iconevents_domain_model_event.sys_language_uid IN (-1,0)',
      ),
    ),
    'l10n_diffsource' => array(
      'config' => array(
        'type' => 'passthrough',
      ),
    ),
    't3ver_label' => array(
      'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
      'config' => array(
        'type' => 'input',
        'size' => 30,
        'max' => 255,
      )
    ),
	'crdate' => array(
	  'label' => 'crdate',
	  'config' => array(
	    'type' => 'passthrough',
	  )
	),
	'tstamp' => array(
	  'label' => 'tstamp',
	  'config' => array(
	    'type' => 'passthrough',
	  )
	),
    'hidden' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
      'config' => array(
        'type' => 'check',
      ),
    ),
    'starttime' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
      'config' => array(
        'type' => 'input',
		'renderType' => 'inputDateTime',
        'size' => 13,
        'eval' => 'datetime',
        'checkbox' => 0,
        'default' => 0,
        'range' => array(
          'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
        ),
		'behaviour' => array(
			'allowLanguageSynchronization' => true,
		),
      ),
    ),
    'endtime' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
      'config' => array(
        'type' => 'input',
		'renderType' => 'inputDateTime',
        'size' => 13,
        'eval' => 'datetime',
        'checkbox' => 0,
        'default' => 0,
        'range' => array(
          'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
        ),
		'behaviour' => array(
			'allowLanguageSynchronization' => true,
		),
      ),
    ),
    'pre_title' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.pre_title',
      'config' => array(
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
      ),
    ),
    'pre_title_big' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.pre_title_big',
      'config' => array(
        'type' => 'check',
		'default' => 0,
      ),
    ),
    'title' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.title',
      'config' => array(
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim,required'
      ),
    ),
    'date_start' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.date_start',
      'config' => array(
        'type' => 'input',
		'renderType' => 'inputDateTime',
        'size' => 10,
        'eval' => 'datetime,required',
        'checkbox' => 1,
        'default' => time()
      ),
    ),
    'date_end' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.date_end',
      'config' => array(
        'type' => 'input',
		'renderType' => 'inputDateTime',
        'size' => 10,
        'eval' => 'datetime,required',
        'checkbox' => 1,
        'default' => time()
      ),
    ),
    'location' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.location',
      'config' => array(
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
      ),
    ),
    'building' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.building',
      'config' => array(
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
      ),
    ),
    'address' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.address',
      'config' => array(
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
      ),
    ),
    'address_link' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.address_link',
      'config' => array(
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim',
		'renderType' => 'inputLink',
      ),
    ),
    'image' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.image',
      'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', array(
        'appearance' => array(
          'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
        ),
        'minitems' => 0,
        'maxitems' => 1,
      ), $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
    ),
    'image_mail' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.image_mail',
      'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image_mail', array(
        'appearance' => array(
          'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
        ),
        'minitems' => 0,
        'maxitems' => 1,
      ), $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
    ),
    'link' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.link',
      'config' => array(
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim',
        'wizards'  => array(
          '_PADDING' => 2,
          'link'     => array(
            'type'         => 'popup',
            'title'        => 'Link',
            'icon'         => 'actions-wizard-link',
            'module' => array(
				'name' => 'wizard_element_browser',
			),
            'params' => array(
              'blindLinkOptions' => 'spec,folder',
            ),
            'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1'
          )
        )
      ),
    ),
    'registration' => array(
	  'exclude' => 1,
	  'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.registration',
	  'config' => array(
		'type' => 'select',
		'renderType' => 'selectSingle',
		'items' => array(
			array('LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.registration.I.0', 0),
			array('LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.registration.I.1', 1),
			array('LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.registration.I.2', 2),
		),
		'default' => 1,
		'size' => 1,
		'maxitems' => 1,
		'behaviour' => array(
			'allowLanguageSynchronization' => true,
		),
	  )
	),
	'registration_endbefore' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.registration_endbefore',
      'config' => array(
        'type' => 'input',
        'size' => 5,
		'default' => 0,
        'eval' => 'integer'
      ),
    ),
	'price' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.price',
      'config' => array(
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim'
      ),
    ),
    'price_special' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.price_special',
      'config' => array(
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim'
      ),
    ),
    'price_info' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.price_info',
      'config' => array(
		'type' => 'text',
		'cols' => 40,
		'rows' => 5,
        'eval' => 'trim',
		'enableRichtext' => true,
      ),
    ),
    'price_folder' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.price_folder',
      'config' => array(
        'type' => 'input',
        'size' => 5,
        'eval' => 'trim'
      ),
    ),
    'speaker' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.speaker',
	  'config' => array(
	    'type' => 'group',
		'internal_type' => 'db',
		'allowed' => 'tt_address',
		'size' => 5,
		'maxitems' => 2,
		'minitems' => 0,
		'autoSizeMax' => 10,
		'fieldWizard' => array(
			'recordsOverview' => array(
				'disabled' => true,
			),
		),
		'wizards' => array(
		),
	  ),
    ),
    'text_links' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.text_links',
      'config' => array(
        'type' => 'text',
        'cols' => 40,
        'rows' => 15,
        'eval' => 'trim',
		'enableRichtext' => true,
      ),
    ),
    'text_rechts' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.text_rechts',
      'config' => array(
        'type' => 'text',
        'cols' => 40,
        'rows' => 15,
        'eval' => 'trim',
		'enableRichtext' => true,
      ),
    ),
    'benefit' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.benefit',
      'config' => array(
		'type' => 'text',
		'cols' => 40,
		'rows' => 5,
        'eval' => 'trim'
      ),
    ),
    'related_uid' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.related_uid',
	  'config' => array(
	    'type' => 'group',
		'internal_type' => 'db',
		'allowed' => 'tx_iconevents_domain_model_event',
		'size' => 1,
		'maxitems' => 1,
		'minitems' => 0,
		'autoSizeMax' => 1,
		'fieldWizard' => array(
			'recordsOverview' => array(
				'disabled' => true,
			),
		),
		'wizards' => array(
		),
	  ),
    ),
    'related_text_info' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.related_text_info',
      'config' => array(
        'type' => 'input',
        'size' => 255,
        'eval' => 'trim'
      ),
    ),
    'related_text_link' => array(
      'exclude' => 1,
      'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.related_text_link',
      'config' => array(
        'type' => 'input',
        'size' => 255,
        'eval' => 'trim'
      ),
    ),
	'tags' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.tags',
		'config' => array(
			'type' => 'select',
			'renderType' => 'selectCheckBox',
			'allowed' => 'tx_iconevents_domain_model_tag',
			'MM' => 'tx_iconevents_domain_model_event_tag_mm',
			'foreign_table' => 'tx_iconevents_domain_model_tag',
			'foreign_table_where' => 'ORDER BY tx_iconevents_domain_model_tag.title',
			'size' => 10,
			'autoSizeMax' => 20,
			'minitems' => 0,
			'maxitems' => 20,
			'behaviour' => array(
				'allowLanguageSynchronization' => true,
			),
		),
	),
	'category' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_event.category',
		'config' => array(
			'type' => 'group',
			'internal_type' => 'db',
			'allowed' => 'tx_iconevents_domain_model_category',
			'MM' => 'tx_iconevents_domain_model_event_category_mm',
			'foreign_table' => 'tx_iconevents_domain_model_category',
			'foreign_table_where' => ' ORDER BY tx_iconevents_domain_model_category.sorting',
			'size' => 3,
			'autoSizeMax' => 10,
			'minitems' => 0,
			'maxitems' => 1,
			'wizards' => array(
				'_PADDING' => 2,
				'_VERTICAL' => 1,
			),
			'behaviour' => array(
				'allowLanguageSynchronization' => true,
			),
		),
	),
  ),
  'palettes' => array(
	'paletteCore' => array(
      'showitem' => 'sys_language_uid, hidden,',
      'canNotCollapse' => TRUE
    ),
	'paletteTitle' => array(
	  'showItem' => 'pre_title_big, title,',
	  'canNotCollapse' => TRUE
	),
  ),
);

?>