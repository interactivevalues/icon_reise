<?php
defined ('TYPO3_MODE') or die ('Access denied.');

$_EXTKEY = 'icon_events';
$ll = 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xml:';

$version7 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('7.0');
$version8 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('8.0');

return array(
	'ctrl' => array(
		'title'     => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xml:tx_iconevents_domain_model_folder',
		'label'     => 'title',
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'languageField'            => 'sys_language_uid',
		'transOrigPointerField'    => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'default_sortby' => 'ORDER BY crdate',
		'sortby' => 'sorting',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
			'fe_group' => 'fe_group',
		),
		'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/tx_iconevents_domain_model_folder.gif',
		'treeParentField' => '',
		'searchFields' => 'uid,title',
	),
	'feInterface' => $TCA['tx_iconevents_domain_model_folder']['feInterface'],
	'interface' => array(
		'showRecordFieldList' => 'sorting,sys_language_uid,l10n_parent,l10n_diffsource,hidden,starttime,endtime,event,parent_registration,gender,title,firstname,lastname,title_after,function,phone,email,attendents,addr_title,addr_street,addr_zip,addr_city,bill_addr,bill_title,bill_street,bill_zip,bill_city,message,newsletter,'
	),
	'columns' => array(
		'pid' => array(
		'label' => 'pid',
		'config' => array(
				'type' => 'passthrough',
			),
		),
		'sorting' => array(
			'label' => 'sorting',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				)
			)
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_iconevents_domain_model_folder',
				'foreign_table_where' => 'AND tx_iconevents_domain_model_folder.pid=###CURRENT_PID### AND tx_iconevents_domain_model_folder.sys_language_uid IN (-1,0)',
			)
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'starttime' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'renderType' => 'inputDateTime',
				'size' => 8,
				'eval' => 'datetime',
				'default' => 0,
			)
		),
		'endtime' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'renderType' => 'inputDateTime',
				'size' => 8,
				'eval' => 'datetime',
				'default' => 0,
			)
		),
		'event' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.event',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'parent_registration' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.parent_registration',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'gender' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.gender',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'firstname' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.firstname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'lastname' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.lastname',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'title_after' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.title_after',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'function' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.function',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'phone' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.phone',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'email' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'attendents' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.attendents',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'addr_title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.addr_title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'addr_street' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.addr_street',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'addr_zip' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.addr_zip',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'addr_city' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.addr_city',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'bill_addr' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.bill_adrr',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'bill_title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.bill_title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'bill_street' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.bill_street',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'bill_zip' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.bill_zip',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'bill_city' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.bill_city',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'send_pdf' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.send_pdf',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'message' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.message',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'newsletter' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.newsletter',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'tim',
			)
		),
		'chash' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xlf:tx_iconevents_domain_model_folder.chash',
			'config' => array(
				'type' => 'input',
				'size' => 32,
				'eval' => 'tim',
			)
		),
	),
	'types' => array(
		0 => array(
			'showitem' => 'title, ',
		),
	),
	'palettes' => array(
		'paletteCore' => array(
			'showitem' => 'hidden,sys_language_uid, l10n_parent, l10n_diffsource,',
			'canNotCollapse' => TRUE,
		),
		'paletteAccess' => array(
			'showitem' => 'starttime;LLL:EXT:cms/locallang_ttc.xml:starttime_formlabel, endtime;LLL:EXT:cms/locallang_ttc.xml:endtime_formlabel, ',
			'canNotCollapse' => TRUE,
		),
	)
);

?>