<?php
defined ('TYPO3_MODE') or die ('Access denied.');

$_EXTKEY = 'icon_events';
$ll = 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xml:';

$version7 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('7.0');
$version8 = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) >= \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger('8.0');

return array(
	'ctrl' => array(
		'title'     => 'LLL:EXT:icon_events/Resources/Private/Language/locallang_db.xml:tx_iconevents_domain_model_tag',
		'label'     => 'title',
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'default_sortby' => 'ORDER BY title',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => 'uid,title',
		'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/tx_iconevents_domain_model_tag.png',
	),
	'feInterface' => $TCA['tx_iconevets_domain_model_tag']['feInterface'],
	'interface' => array(
		'showRecordFieldList' => 'hidden,title'
	),
	'columns' => array(
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'title' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_iconevents_domain_model_tag.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'required,unique,trim',
				'behaviour' => array(
					'allowLanguageSynchronization' => true,
				),
			)
		),
	),
	'types' => array(
		0 => array(
			'showitem' => 'title, ',
		)
	),
	'palettes' => array(
		'palletteCore' => array(
			'showitem' => 'hidden,',
			'canNotCollapse' => TRUE
		),
	)
);

?>