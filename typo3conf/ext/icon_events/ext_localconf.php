<?php

if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1',
  array(
    'Event' => 'index, detail, form, store, done, folder, storeFolder, doneFolder',
	'Tag' => 'list',
	'Category' => 'list',
  ),
  // non-cacheable actions
  array(
    'Event' => 'index, detail, form, store, done, folder, storeFolder, doneFolder',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1_teaser',
  array(
    'Event' => 'teaser',
  ),
  // non-cacheable actions
  array(
    'Event' => 'teaser',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1_shortlist',
  array(
    'Event' => 'shortlist',
  ),
  // non-cacheable actions
  array(
    'Event' => 'shortlist',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1_logo',
  array(
    'Event' => 'logo',
  ),
  // non-cacheable actions
  array(
    'Event' => 'logo',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Ics',
  array(
    'Event' => 'icsFile',
  ),
  // non-cacheable actions
  array(
    'Event' => 'icsFile',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Csv',
  array(
	'Event' => 'csvFile',
  ),
  // non-cacheable actions
  array(
	'Event' => 'csvFile',
  )
);


/*
	WICHTIG !!!		!!!			!!!			!!!
	
	damit das klappt muß realurl in /typo3conf/PackageStage.php vor dieser extension geladen werden !!!
	-> muss vl. per hand angepasst werden !!!

*/

  // registering contact.vcf for each hierachy of configuration to realurl (meaning to every website in a multisite installation)
$realurl = $TYPO3_CONF_VARS['EXTCONF']['realurl'];
if (is_array($realurl))  {
	foreach ($realurl as $host => $cnf) {
		// we won't do anything with string pointer (e.g. example.org => www.example.org)
		if (!is_array($realurl[$host])) {
			continue;
		}
		if (!isset($realurl[$host]['fileName'])) {
			$realurl[$host]['fileName'] = array();
		}
		$realurl[$host]['fileName']['index']['event.csv']['keyValues']['type'] = 797;
		$realurl[$host]['fileName']['index']['event.ics']['keyValues']['type'] = 798;
	}
  
	if( !isset($realurl['_DEFAULT']['postVarSets']) ) { $realurl['_DEFAULT']['postVarSets'] = array(); }
	if( !isset($realurl['_DEFAULT']['postVarSets']['_DEFAULT']) ) { $realurl['_DEFAULT']['postVarSets']['_DEFAULT'] = array(); }

	//	EVENT
	/*
		?tx_iconevents_pi1[event]=UID&tx_iconevents_pi1[action]=detail&tx_iconevents_pi1[controller]=Event
		?tx_iconevents_pi1[event]=UID&tx_iconevents_pi1[action]=form&tx_iconevents_pi1[controller]=Event
		?tx_iconevents_pi1[event]=UID&tx_iconevents_pi1[action]=store&tx_iconevents_pi1[controller]=Event
		?tx_iconevents_pi1[event]=UID&tx_iconevents_pi1[action]=folder&tx_iconevents_pi1[controller]=Event
		?tx_iconevents_pi1[event]=UID&tx_iconevents_pi1[action]=storeFolder&tx_iconevents_pi1[controller]=Event
	*/
	$realurl['_DEFAULT']['postVarSets']['_DEFAULT']['termin'] = array(
		array(
			'GETvar' => 'tx_iconevents_pi1[controller]',
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_iconevents_pi1[action]',
			'valueMap' => array(
				'detail' => 'detail',
				'register' => 'form',
				'checkRegister' => 'store',
				'folder' => 'folder',
				'checkFolder' => 'storeFolder',
			),
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_iconevents_pi1[event]',
			'lookUpTable' => array(
				'table' => 'tx_iconevents_domain_model_event',
				'id_field' => 'uid',
				'alias_field' => "CONCAT( FROM_UNIXTIME( date_start, '%Y-%m-%d' ), '-', title)",
				'addWhereClause' => ' AND deleted = 0 ',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-',
				),
/*
				'languageGetVar' => 'L',
				'languageExceptionUids' => '',
				'languageField' => 'sys_language_uid',
				'transOrigPointerField' => 'l10n_parent',
*/
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			),
		),
	);
	
	//	EVENT - REGISTRATION CONFIRMATION
	/*
		?tx_iconevents_pi1[registration]=UID&tx_iconevents_pi1[action]=done&tx_iconevents_pi1[controller]=Event
	*/
	$realurl['_DEFAULT']['postVarSets']['_DEFAULT']['registration'] = array(
		array(
			'GETvar' => 'tx_iconevents_pi1[controller]',
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_iconevents_pi1[action]',
			'valueMap' => array(
				'done' => 'done',
			),
			'valueDefault' => 'done',
		),
		array(
			'GETvar' => 'tx_iconevents_pi1[registration]',
			'lookUpTable' => array(
				'table' => 'tx_iconevents_domain_model_registration',
				'id_field' => 'uid',
				'alias_field' => "chash",
				'addWhereClause' => ' AND NOT deleted',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-',
				),
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			),
		),
	);

	//	EVENT - FOLDER REQUEST
	/*
		?tx_iconevents_pi1[folder]=UID&tx_iconevents_pi1[action]=doneFolder&tx_iconevents_pi1[controller]=Event
	*/
	$realurl['_DEFAULT']['postVarSets']['_DEFAULT']['folder'] = array(
		array(
			'GETvar' => 'tx_iconevents_pi1[controller]',
			'noMatch' => 'bypass',
		),
		array(
			'GETvar' => 'tx_iconevents_pi1[action]',
			'valueMap' => array(
				'done' => 'doneFolder',
			),
			'valueDefault' => 'done',
		),
		array(
			'GETvar' => 'tx_iconevents_pi1[folder]',
			'lookUpTable' => array(
				'table' => 'tx_iconevents_domain_model_folder',
				'id_field' => 'uid',
				'alias_field' => "chash",
				'addWhereClause' => ' AND NOT deleted',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-',
				),
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			),
		),
	);
  
	$TYPO3_CONF_VARS['EXTCONF']['realurl'] = $realurl;
}

?>

