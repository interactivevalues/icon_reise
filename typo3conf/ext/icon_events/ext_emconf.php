<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "icon_events".
 *
 * 2014-04-20, r.schmoller@next-linz.com
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'ICON - Termine',
  'description' => '',
  'category' => 'plugin',
  'author' => 'smo',
  'author_email' => 'r.schmoller@next-linz.com',
  'shy' => '',
  'dependencies' => '',
  'conflicts' => '',
  'priority' => '',
  'module' => '',
  'state' => 'stable',
  'internal' => '',
  'uploadfolder' => 0,
  'createDirs' => '',
  'modify_tables' => '',
  'clearCacheOnLoad' => 0,
  'lockType' => '',
  'author_company' => '',
  'version' => '0.0.9',
  'constraints' => array (
    'depends' => array(
      'typo3' => '7.6',
      'fluid' => '7.6',
      'extbase' => '7.6',
      'icon_erweiterungen' => '0.0.2',
/*
	  'realurl' => '1.10',
	  'tt_address' => '2.3.4',
*/
    ),
    'conflicts' => array (
    ),
    'suggests' => array (
    ),
  ),
);

?>