<?php

if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

/*
	TASK FOR SCHEDULER
*/
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['NEXT\IconIgelconnector\Task\Task'] = array(
	'extension' => $_EXTKEY,
	'title' => 'IGEL Wertelisten aktualisieren',
	'description' => 'Aktualisiert die nötigen Listen, welche als Files zwischengespeichert werden.',
);

?>
