plugin.tx_igelconnector {
	view {
		templateRootPath = {$plugin.tx_igelconnector.view.templateRootPath}
		partialRootPath = {$plugin.tx_igelconnector.view.partialRootPath}
		layoutRootPath = {$plugin.tx_igelconnector.view.layoutRootPath}
	}
	settings {
	}
}