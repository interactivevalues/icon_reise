<?php
namespace NEXT\IconIgelconnector\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_igelconnector
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class WebserviceRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * @var string
	 */
	protected $igelUSR = 'wsnext';

	/**
	 * @var string
	 */
	protected $igelPWD = '20ce$c497b#585';

	/**
	 * @var string
	 */
	protected $igelURL_beziehungen = 'https://portal.icon.at/igel/wsi/lov/beziehungen';

	/**
	 * @var string
	 */
	protected $igelURL_finanzamt = 'https://portal.icon.at/igel/wsi/lov/institute/fa';

	/**
	 * @var string
	 */
	protected $igelURL_laender = 'https://portal.icon.at/igel/wsi/lov/laender';
	
	/**
	 * @var string
	 */
	protected $igelURL_titel = 'https://portal.icon.at/igel/wsi/lov/titel';
	
	/**
	 * @var string
	 */
	protected $igelURL_titel_nach = 'https://portal.icon.at/igel/wsi/lov/titel/nach';
	
	/**
	 * @var string
	 */
	protected $extFolder = 'uploads/icon_igelconnector/';


  /**
   * getIgelLogin
   *
   * @return array
   */
  public function getIgelLogin () {
	//
	$igel['user'] = $this->igelUSR;
	$igel['password'] = $this->igelPWD;
	$igel['language'] = 'DE';
	//
	return $igel;
  }

  /**
   * init
   *
   * @param \string $url
   * @return \mixed
   */
  public function init ($url) {
	//
	// Initialize session and set URL.
	$ch = curl_init();
	//
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	// Set so curl_exec returns the result instead of outputting it.
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
//	curl_setopt($ch, CURLOPT_HEADER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml;  charset=UTF-8") );
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
	//
	curl_setopt($ch, CURLOPT_URL, $url);
	//
	return $ch;
  }
  
  
  /**
   * close
   *
   * @param \mixed $ch
   */
  public function close ($ch) {
	//
	curl_close($ch);
  }

  
  /**
   * call
   *
   * @param \mixed $ch
   * @return \mixed
   */
  public function call ($ch) {
	// Get the response and close the channel.
	$d = curl_exec($ch);
	$e = curl_error($ch);
	//
	$res['RAW']['data'] = $d;
	$res['RAW']['error'] = $e;
	//
	$xmlarray = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($d);
/*
	foreach($xmlarray as $k1 => $v1){
		$xmldata .= 'k1: ' . $k1 . ' v1: ' . $v1 . ' --- ' . chr(10).chr(13);
		foreach( $v1 as $k2 => $v2 ) {
			$xmldata .= 'k2: ' . $k2 . ' v2: ' . $v2 . ' --- ' . chr(10).chr(13);
			foreach( $v2 as $k3 => $v3 ) {
				$xmldata .= 'k3: ' . $k3 . ' v3: ' . $v3 . ' --- ' . chr(10).chr(13);
			}
		}
	}
	$xmldata .= 'ID: ' . $xmlarray['response']['error']['ID'];
*/
	//
	$res['error']['ID'] = $xmlarray['response']['error']['ID'];
	$res['error']['info'] = $xmlarray['response']['error']['info'];
	$res['message'] = $xmlarray['response']['message'];
	//
	return $res;
  }

  /**
   * download
   *
   * @param \mixed $ch
   * @return \mixed
   */
  public function download ($ch) {
	// Get the response and close the channel.
	$d = curl_exec($ch);
	$e = curl_error($ch);
	//
	$res['data'] = $d;
	$res['error'] = $e;
	//
	return $res;
  }

  
  /**
   * setPostFields
   *
   * @param \mixed $ch
   * @param \mixed $data
   * @return \mixed
   */
  public function setPostFields ($ch, $data) {
	//
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	return $ch;
  }


  /**
   * buildXML
   *
   * @param \array $data
   * @return \string
   */
  public function buildXML ($data) {
	//
	$xml = '';
	//
/*
	$igel = $data['igel'];
	if( is_array($igel) ){
		$xml .= '	<igel>' . PHP_EOL;
		foreach($igel as $k1 => $v1){
			$xml .= '		<' . $k1 . '>' . $v1 . '</' . $k1 . '>' . PHP_EOL;
		}
		$xml .= '	</igel>' . PHP_EOL;
	}
	//
	$xml = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '<xml>' . PHP_EOL . $xml . '</xml>';
*/
/*
	$xml = \TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($data);
	//
	$xml = str_replace(array('<phparray>', '</phparray>'), array('<xml>', '</xml>'), $xml);
	$xml = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . $xml;
*/

	$igel = $data['igel'];
//	$language = $data['language'];
	$formular = $data['formular'];
	$files = $data['files'];
	//
	if( is_array($igel) ){
		$t = \TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($igel);
		//	clean up igel-xml
		$t = str_replace(array('<phparray>', '</phparray>'), array('<igel>', '</igel>'), $t);
		//	add igel-xml to XML
		$xml .= $t . PHP_EOL;
	}
	//
	if( is_array($formular) ){
		$fs_str = '';
		foreach( $formular as $fieldset ){
			$t_str = '<ID>' . $fieldset['ID'] . '</ID>' . PHP_EOL;
			$t_str .= '<type>' . $fieldset['type'] . '</type>' . PHP_EOL;
			//
			$f_str = '';
			foreach( $fieldset['fields'] as $field ){
				$f_str .= '<field>'.PHP_EOL.'<ID>'.$field['ID'].'</ID>'.PHP_EOL.'<data><![CDATA['.$field['data'].']]></data>'.PHP_EOL.'</field>'.PHP_EOL;
			}
			$f_str = $t_str . '<fields>' . PHP_EOL . $f_str . '</fields>' . PHP_EOL;
			//
			$fs_str .= '<fieldset>' . PHP_EOL . $f_str . '</fieldset>' . PHP_EOL;
		}
		//	set formuar in data-xml
		$fs_str = '<formular>' . PHP_EOL . $fs_str . '</formular>' . PHP_EOL;
		//	add data-xml to XML
//		$xml .= '<data>' . PHP_EOL . '<language>' . $language . '</language>' . PHP_EOL . $fs_str . '</data>' . PHP_EOL;
		$xml .= '<data>' . PHP_EOL . $fs_str . '</data>' . PHP_EOL;
	}
	//
	if( is_array($files) ){
		$t = \TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($files);
		//	cleanup files-xml
		$t = str_replace(array('<phparray>', '</phparray>', ' type="array"', ' base64="1"'), array('<files>', '</files>', '', ''), $t);
		//	add files-xml to XML
		$xml .= $t . PHP_EOL;
	}
	//
	$xml = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '<xml>' . PHP_EOL . $xml . '</xml>';
	//
	return $xml;
  }
  
  /**
   * writeFile
   * 
   * @param \string $filename
   * @param \string $data
   * @return void
   **/
  public function writeFile ($filename, $data) {
	  //
	  $filePath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($this->extFolder) . $filename;
	  $fileData = $data;
	  //
	  \TYPO3\CMS\Core\Utility\GeneralUtility::writeFile($filePath, $fileData);
	  //
	  return;
  }
  
  /**
   * @param \string $file
   * @param \string $lang
   * @return void
   **/
  public function updateFile ($file, $lang='DE') {
	//
	$fileNameXML = $file . '_' . $lang . '.xml';
	$fileNameTXT = $file . '_' . $lang . '.txt';
	//
	$igelURL = '';
	if( $file=='beziehungen' ){ $igelURL = $this->igelURL_beziehungen; }
	elseif( $file == 'beziehungen-data' ){ $igelURL = $this->igelURL_beziehungen; }
	elseif( $file == 'beziehungen-plain' ){ $igelURL = $this->igelURL_beziehungen; }
	elseif( $file == 'finanzamt' ){ $igelURL = $this->igelURL_finanzamt; }
	elseif( $file == 'laender' ){ $igelURL = $this->igelURL_laender; }
	elseif( $file == 'titel' ){ $igelURL = $this->igelURL_titel; }
	elseif( $file == 'titel-plain' ){ $igelURL = $this->igelURL_titel; }
	elseif( $file == 'titel_nach' ){ $igelURL = $this->igelURL_titel_nach; }
	elseif( $file == 'titel_nach-plain' ){ $igelURL = $this->igelURL_titel_nach; }
	
	//
	$igel = $this->getIgelLogin();
	$igel['language'] = $lang;
	//
	$data = $this->buildXML( array('igel' => $igel) );
	//
	$ws = $this->init($igelURL);
	//
	$ws = $this->setPostFields($ws, $data);
	//
	$res = $this->download($ws);
	//
	$this->close($ws);

	$pf_xml = $res['data'];

	//	SAVE RESPONSE DATA
	$this->writeFile($fileNameXML, $pf_xml);

	//
	$pf_xml = str_replace(array('<ROWSET>', '</ROWSET>', '</ROW>'), array('', '', ''), $pf_xml);
	$pf_temp = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('<ROW>', $pf_xml, true);

	$pf_data = '';
	$pf_array = array();
	if( $file == 'beziehungen' ){
		$pf_plain = array();
		$pf_ts = array();
		for( $i=0; $i<count($pf_temp); $i++ ){
			$i_array = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array('<xml>'.$pf_temp[$i] . '</xml>');
			$pf_array[$i_array['BEZ_NR']] = $i_array['BEZ_NAME'];
			$pf_plain[$i_array['BEZ_NAME']] = $i_array['BEZ_NAME'];
			$pf_ts[] = $i_array['BEZ_NAME'];
		}
		$pf_data = serialize($pf_array);
		$pf_plain = serialize($pf_plain);
		$pf_ts = implode('[\n]', $pf_ts);
		//
		$this->writeFile($file . '-plain_' . $lang . '.txt', $pf_plain);
		$this->writeFile($file . '-typoscript_' . $lang . '.txt', $pf_ts);
		//
	} elseif( $file == 'beziehungen-data' ){
		for( $i=0; $i<count($pf_temp); $i++ ){
//			print_r($i);
//			print_r($pf_temp[$i]);
			$i_array = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array('<xml>'.$pf_temp[$i] . '</xml>');
//			print_r($i_array);
			$pf_array[] = $i_array;
//			print_r(' -- ');
			$pf_data .= '<option value="' . $i_array['BEZ_NR'] . '">' . $i_array['BEZ_NAME'] . '</option>';
//			$pf_data .= '<option value="' . $i_array['BEZ_NR'] . '">' . $i_array['BEZ_NAME'] . '</option>' . PHP_EOL;
		}
	} elseif( $file == 'finanzamt' ){
		for( $i=0; $i<count($pf_temp); $i++ ){
			$i_array = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array('<xml>'.$pf_temp[$i] . '</xml>');
//			$pf_array[] = $i_array;
//			$pf_data .= '<option value="' . $i_array['INST_NR'] . '">' . $i_array['INST_NAME'] . '</option>';

//			$pf_array[$i_array['INST_NR']] = $i_array['INST_NAME'] . ' ( ' . $i_array['INST_NR'] . ' )';
			$pf_array[$i_array['INST_NR']] = $i_array['INST_NAME'];
		}
		$pf_data = serialize($pf_array);
	} elseif( $file == 'laender' ){
		for( $i=0; $i<count($pf_temp); $i++ ){
			$i_array = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array('<xml>'.$pf_temp[$i] . '</xml>');
//			$pf_array[$i_array['LD_NR']] = $i_array['LD_NAME'];
			$pf_array[$i_array['LD_NR']] = $i_array['LD_NAME'] . ' ( ' . $i_array['LD_NR'] . ' )';
		}
		$pf_data = serialize($pf_array);
	} elseif( $file == 'titel' ){
		$pf_plain = array();
		$pf_ts = array();
		for( $i=0; $i<count($pf_temp); $i++ ){
			$i_array = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array('<xml>'.$pf_temp[$i] . '</xml>');
			$pf_array[$i_array['TIT_NR']] = $i_array['TIT_NAME'];
//			$pf_array[$i_array['TIT_NR']] = $i_array['TIT_NAME'] . ' ( ' . $i_array['TIT_NR'] . ' )';
			$pf_plain[$i_array['TIT_NAME']] = $i_array['TIT_NAME'];
			$pf_ts[] = $i_array['TIT_NAME'];
		}
		$pf_data = serialize($pf_array);
		$pf_plain = serialize($pf_plain);
		$pf_ts = implode('[\n]', $pf_ts);
		//
		$this->writeFile($file . '-plain_' . $lang . '.txt', $pf_plain);
		$this->writeFile($file . '-typoscript_' . $lang . '.txt', $pf_ts);
		//
	} elseif( $file == 'titel_nach' ){
		$pf_plain = array();
		$pf_ts = array();
		for( $i=0; $i<count($pf_temp); $i++ ){
			$i_array = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array('<xml>'.$pf_temp[$i] . '</xml>');	
			$pf_array[$i_array['TITN_NR']] = $i_array['TITN_NAME'];
//			$pf_array[$i_array['TITN_NR']] = $i_array['TITN_NAME'] . ' ( ' . $i_array['TITN_NR'] . ' )';
			$pf_plain[$i_array['TITN_NAME']] = $i_array['TITN_NAME'];
			$pf_ts[] = $i_array['TITN_NAME'];
		}
		$pf_data = serialize($pf_array);
		$pf_plain = serialize($pf_plain);
		$pf_ts = implode('[\n]', $pf_ts);
		//
		$this->writeFile($file . '-plain_' . $lang . '.txt', $pf_plain);
		$this->writeFile($file . '-typoscript_' . $lang . '.txt', $pf_ts);
		//
	}

	//	SAVE PREPED DATA
	$this->writeFile($fileNameTXT, $pf_data);
  }

  /**
   * @param string $file
   * @param string $lang
   * @param string $type
   * @return mised
   **/
  public function getFileData ($file, $lang='DE', $type='txt') {
	//
	$fileName = $file . '_' . $lang . '.' . $type;
	//
	$filePath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($this->extFolder) . $fileName;
	$fileData = \TYPO3\CMS\Core\Utility\GeneralUtility::getURL($filePath);
	//
	return $fileData;
  }
  
  /**
   *
   * return string
   */
  public function anonymizeXML ($data) {
	  $data = str_replace('<user>'.$this->igelUSR .'</user>', '<user>********</user>', $data);
	  $data = str_replace('<password>'.$this->igelPWD .'</password>', '<password>********</password>', $data);
	  return $data;
  }

}
?>