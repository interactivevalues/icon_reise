<?php
namespace NEXT\IconIgelconnector\Task;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package icon_igelconnector
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Task extends \TYPO3\CMS\Scheduler\Task\AbstractTask {
	
  /**
   * webserviceRepository
   *
   * @var \NEXT\IconIgelconnector\Domain\Repository\WebserviceRepository
   * @inject
   */
//  protected $webserviceRepository;
  
  
  /**
   *
   **/
  public function execute() {
	//
	$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
	$webserviceRepository = $objectManager->get('NEXT\IconIgelconnector\Domain\Repository\WebserviceRepository');

	//
//	$webserviceRepository->writeFile('test.txt', 'hello-1');

	//	update LIST:	BEZIEHUNGEN DE
	$webserviceRepository->updateFile('beziehungen');

	//	update LIST:	BEZIEHUNGEN DE
	$webserviceRepository->updateFile('beziehungen', 'EN');

	//	update LIST:	FINANZAMT DE
	$webserviceRepository->updateFile('finanzamt');

/*
	//	update LIST:	FINANZAMT EN
	$webserviceRepository->updateFile('finanzamt', 'EN');
*/

	//	update LIST:	LAENDER DE
	$webserviceRepository->updateFile('laender');

	//	update LIST:	LAENDER DE
	$webserviceRepository->updateFile('laender', 'EN');

	//	update LIST:	TITEL DE
	$webserviceRepository->updateFile('titel');

	//	update LIST:	TITEL EN
	$webserviceRepository->updateFile('titel', 'EN');

	//	update LIST:	TITEL_NACH DE
	$webserviceRepository->updateFile('titel_nach');

	//	update LIST:	TITEL_NACH DE
	$webserviceRepository->updateFile('titel_nach', 'EN');

	//
	return TRUE; //ende gut, alles gut?
  }

/*
	// BEISPIEL-CODE:
	public function execute() {
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
        $sampleRepository= $objectManager->get('\TYPO3\ExtName\Domain\Repository\SampleRepository');
        $new = new \TYPO3\ExtName\Domain\Model\Sample;
        $new->setName('test');
        $sampleRepository->add($new);
        $objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
    }
*/	

}
?>