<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

unset($GLOBALS['TCA']['tt_address']['columns']['description']);


// Configure new fields:
$fields = array(
	'birthday' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:tt_address/locallang_tca.xml:tt_address.birthday',
		'config' => array(
			'type' => 'input',
			'renderType' => 'inputDateTime',
			'eval' => 'date',
			'size' => '8',
		)
	),
	'www' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:tt_address/locallang_tca.xml:LGL.www',
		'config' => array(
			'type' => 'input',
			'renderType' => 'inputLink',
			'eval' => 'trim',
			'size' => '20',
			'max' => '255',
			'softref' => 'typolink,url',
		)
	),
    'ausgetretten' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.ausgetretten',        
        'config' => array(
            'type' => 'check'
        )
    ),
    'board_member' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.board_member',        
        'config' => array(
            'type' => 'check'
        )
    ),
    'title_after' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.title_after',        
        'config' => array(
            'type' => 'input',    
            'size' => '20',
            'eval' => 'trim',
            'max'  => '255'
        )
    ),
    'funktion' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.funktion',        
        'config' => array(
            'type' => 'input',    
            'size' => '20',
            'eval' => 'trim',
            'max'  => '255'
        )
    ),
    'berufstitel' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.berufstitel',        
        'config' => array(
            'type' => 'input',    
            'size' => '20',
            'eval' => 'trim',
            'max'  => '255'
        )
    ),
    'berufstitel_short' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.berufstitel_short',        
        'config' => array(
            'type' => 'input',    
            'size' => '20',
            'eval' => 'trim',
            'max'  => '255'
        )
    ),
    'page_detail' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.page_detail',        
        'config' => array(
            'type'     => 'input',
			'renderType' => 'inputLink',
            'size'     => '15',
            'max'      => '255',
            'checkbox' => '',
            'eval'     => 'trim',
        )
    ),
    'funktion_EN' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.funktion_EN',        
        'config' => array(
            'type' => 'input',    
            'size' => '20',
            'eval' => 'trim',
            'max'  => '255'
        )
    ),
    'berufstitel_EN' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.berufstitel_EN',        
        'config' => array(
            'type' => 'input',    
            'size' => '20',
            'eval' => 'trim',
            'max'  => '255'
        )
    ),
    'berufstitel_short_EN' => array(        
        'exclude' => 1,        
        'label' => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.berufstitel_short_EN',        
        'config' => array(
            'type' => 'input',    
            'size' => '20',
            'eval' => 'trim',
            'max'  => '255'
        )
    ),
	'description_EN' => array (
		'exclude' => 1,
		'label'   => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.description_EN',
		'config'  => array (
			'type' => 'text',
			'rows' => 15,
			'cols' => 40,
			'eval' => 'trim',
			'enableRichtext' => false,
		),
	),
	'description' => array (
		'exclude' => 1,
		'label'   => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.description',
		'config'  => array (
			'type' => 'text',
			'rows' => 15,
			'cols' => 40,
			'enableRichtext' => true,
		),
	),
	'description_EN' => array (
		'exclude' => 1,
		'label'   => 'LLL:EXT:tt_address_icon/locallang_db.xml:tt_address.description_EN',
		'config'  => array (
			'type' => 'text',
			'rows' => 15,
			'cols' => 40,
			'enableRichtext' => true,
		),
	),
);

// Add new fields to tt_address:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_address', $fields);
  
/*
// Make fields visible in the TCEforms:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_address', 
	'ausgetretten, board_member, title_after, page_detail, funktion, funktion_EN, berufstitel, berufstitel_short, berufstitel_EN, berufstitel_short_EN, description_EN'
);
*/

/*
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
  'tt_address', // Table name
  '--palette--;LLL:EXT:pages_addfields/Resources/Private/Language/locallang_db.xlf:pages.palette_title;tx_pagesaddfields', // Field list to add
  '1', // List of specific types to add the field list to. (If empty, all type entries are affected)
  'after:nav_title' // Insert fields before (default) or after one, or replace a field
);
*/

/*  
// Add the new palette:
$GLOBALS['TCA']['pages']['palettes']['tx_pagesaddfields'] = array(
  'showitem' => 'tx_pagesaddfields_customcheckbox,tx_pagesaddfields_customtext'
);
*/
$GLOBALS['TCA']['tt_address']['interface']['showRecordFieldList'] = 'first_name,middle_name,last_name,address,building,room,city,zip,region,country,phone,fax,email,www,title,company,image,description,description_EN,';

$GLOBALS['TCA']['tt_address']['types'][0] = array(
	'showitem' => 'hidden, 
	    ausgetretten,
	    board_member,
	    gender, 
		title,
		first_name, 
		middle_name, 
		last_name, 
		title_after, 
		company, 
		funktion,
		funktion_EN,
		berufstitel,
		berufstitel_short,
		berufstitel_EN,
		berufstitel_short_EN,
		birthday, 
		address, 
		building, 
		room, 
		zip, 
		city, 
		region, 
		country, 
		email, 
		www,
		phone, 
		mobile, 
		fax, 
		image, 
		page_detail,
		description, 
		description_EN,
		categories,'
);

?>