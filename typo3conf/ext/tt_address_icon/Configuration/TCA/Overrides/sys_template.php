<?php
defined('TYPO3_MODE') or die();

$_EXTKEY = 'tt_address_icon';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Addresses ICON');
