<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// Configure new fields:
$fields = array(
	'configuration_map' => array(
		'exclude' => 0,
		'label' => 'MAP-Configuration',
		'config' => array(
			'type' => 'user',
			'userFunc' => 'Clickstorm\GoMapsExt\Utility\LocationUtility->render',
			'parameters' => array(
				'longitude' => 'longitude',
				'latitude' => 'latitude',
				'address' => 'address',
			),
		),
	),
	'latitude' => array(
		'exclude' => 0,
		'label' => 'MAP-Latitude',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'double,trim'
		),
	),
	'longitude' => array(
		'exclude' => 0,
		'label' => 'MAP-Longitude',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'double,trim'
		),
	),
	'address' => array(
		'exclude' => 0,
		'label' => 'MAP-Address',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
);
  
// Add new fields to tt_address:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $fields);

// Make fields visible in the TCEforms:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'sys_category',
	'--div--;Länder A-Z,
	configuration_map, latitude, longitude, address'
);

?>