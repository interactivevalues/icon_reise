plugin.tx_ttaddressicon {
	view {
		templateRootPath = {$plugin.tx_ttaddressicon.view.templateRootPath}
		partialRootPath = {$plugin.tx_ttaddressicon.view.partialRootPath}
		layoutRootPath = {$plugin.tx_ttaddressicon.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_ttaddressicon.persistence.storagePid}
	}
	settings {
		general {
			dummyImage = {$plugin.tx_ttaddressicon.settings.general.dummyImage}
			phone = {$plugin.tx_ttaddressicon.settings.general.phone}
			fax = {$plugin.tx_ttaddressicon.settings.general.fax}
		}
		overview {
		}
		detail {
			image {
				width = {$plugin.tx_ttaddressicon.settings.detail.image.width}
				height = {$plugin.tx_ttaddressicon.settings.detail.image.height}
			}
		}
		map {
			openByClick = {$plugin.tx_ttaddressicon.settings.map.openByClick}
			closeByClick = {$plugin.tx_ttaddressicon.settings.map.closeByClick}
		}
		person {
			detail {
				pageUid = {$plugin.tx_ttaddressicon.settings.person.detail.pageUid}
				extensionName = {$plugin.tx_ttaddressicon.settings.person.detail.extensionName}
				pluginName = {$plugin.tx_ttaddressicon.settings.person.detail.pluginName}
				controller = {$plugin.tx_ttaddressicon.settings.person.detail.controller}
				action = {$plugin.tx_ttaddressicon.settings.person.detail.action}
			}
			events {
				events {
					detailUid = {$plugin.tx_ttaddressicon.settings.events.events.detailUid}
					storagePid = {$plugin.tx_ttaddressicon.settings.events.events.storagePid}
				}
			}
			publications {
				books {
					detailUid = {$plugin.tx_ttaddressicon.settings.publications.books.detailUid}
					storagePid = {$plugin.tx_ttaddressicon.settings.publications.books.storagePid}
				}
				news {
					detailUid = {$plugin.tx_ttaddressicon.settings.publications.news.detailUid}
					storagePid = {$plugin.tx_ttaddressicon.settings.publications.news.storagePid}
				}
				publications {
					detailUid = {$plugin.tx_ttaddressicon.settings.publications.publications.detailUid}
					storagePid = {$plugin.tx_ttaddressicon.settings.publications.publications.storagePid}
				}
			}
			map {
				image {
					width = {$plugin.tx_ttaddressicon.settings.person.map.image.width}
					height = {$plugin.tx_ttaddressicon.settings.person.map.image.height}
				}
			}
		}
	}
}

#    ADDITIONAL SETTINGS FOR vCard !!!
plugin.tx_ttaddressicon.settings {

  #iconFile = EXT:tt_address_icon/res/vcard.jpg
  #allowedPids = 0

  #vCardLink = <a href="/vcard.vcf?uid=###UID###&amp;pid=###PID###" title="Download vCard for ###NAME###">###VCF_ICON###</a>
  #linkTitleFields = first_name,middle_name,last_name
  #linkTitleFieldsSeparator = " "

  mapping {
    default {
      FN = name
      N = last_name,first_name,middle_name,title
      NICKNAME = name
      PHOTO = image
      BDAY = birthday
    
      #ADR types can be of the following: dom;intl;postal;parcel;home;work;pref
      ADR = work;postal:address
      TEL = WORK.phone,CELL.mobile,FAX.fax
      EMAIL = email
      TITLE = title
      ORG = company
      NOTE = description
      REV = tstamp
      SORT-STRING = last_name
      URL = www
	}
	team {
      N = last_name,first_name
      FN = name
      ORG = company
      URL = www
      EMAIL = email
      TEL = WORK.phone,CELL.mobile,FAX.fax    
      #ADR types can be of the following: dom;intl;postal;parcel;home;work;pref
      ADR = work;postal:address
    }
  }
  
  extraMarkers {
    #put extra markers here
    # for example:
    PHONE_LABEL = Tlf:
  }
}

#Setup the actual CSV file
tx_ttaddressicon_pi5 = PAGE
tx_ttaddressicon_pi5 {
	typeNum = 799
	config.disableAllHeaderCode = 1
	config.renderCharset = iso-8859-1
	config.additionalHeaders = Content-type: text/x-vcard; charset= iso-8859-1
	config.no_cache = 1
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		pluginName = Pi5
		extensionName = TtAddressIcon
		controller = Vcard
		vendorName = NEXT
		action = vcardFile
		switchableControllerActions {
			Vcard {
				1 = vcardFile
			}
		}

		settings =< plugin.tx_ttaddressicon.settings
		persistence =< plugin.tx_ttaddressicon.persistence
		view =< plugin.tx_ttaddressicon.view
	}
}
