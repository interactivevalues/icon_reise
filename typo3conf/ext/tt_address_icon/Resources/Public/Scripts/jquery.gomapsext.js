/**
 * Created by mhirdes on 27.11.13.
 */
(function ($) {
    // jQuery Plugin

    // create a new Google Map
    $.fn.gomapsext = function (gme) {
        var element = $(this);

        var Route = new Array();
        var pointDescriptions = Array();
        var infoWindow = new google.maps.InfoWindow();
        if (gme.mapSettings.CSSClass != '') {
            element.addClass(gme.mapSettings.CSSClass);
        }
        if (gme.mapSettings.tooltipTitle != '') {
            element.attr("title", gme.mapSettings.tooltipTitle);
        }
        element.css("width", gme.mapSettings.width);
        element.css("height", gme.mapSettings.height);
        element.data("markers", Array());

        // styled Map
        if (gme.mapSettings.styledMapCode) {
            var myStyle = gme.mapSettings.styledMapCode;
        }
        if (gme.mapSettings.styledMapName) {
            var styledMapOptions = {
                name: gme.mapSettings.styledMapName,
                alt: gme.mapSettings.tooltipTitle
            };
            var myMapType = new google.maps.StyledMapType(
                myStyle,
                styledMapOptions
            )
        }

        // set map options
        var myOptions = {
            zoom: gme.mapSettings.defaultZoom,
            center: new google.maps.LatLng(gme.mapSettings.centerLAT, gme.mapSettings.centerLNG),
            draggable: gme.mapSettings.draggable,
            disableDoubleClickZoom: gme.mapSettings.doubleClickZoom,
            scrollwheel: gme.mapSettings.scrollZoom,
            panControl: gme.mapSettings.panControl,
            scaleControl: gme.mapSettings.scaleControl,
            streetViewControl: gme.mapSettings.streetviewControl,
            zoomControl: gme.mapSettings.zoomControl,
            zoomControlOptions: {style: gme.zoomTypes[gme.mapSettings.zoomControlType]},
			maxZoom: gme.mapSettings.maxZoom,
			minZoom: gme.mapSettings.minZoom,
            mapTypeId: gme.defaultMapTypes[gme.mapSettings.defaultType],
            mapTypeControl: gme.mapSettings.mapTypeControl,
            mapTypeControlOptions: {mapTypeIds: gme.mapSettings.mapTypes}
        }

        element.data("map", new google.maps.Map(document.getElementById(gme.mapSettings.title), myOptions));
        element.data("bounds", new google.maps.LatLngBounds());

        if (gme.mapSettings.styledMapName) {
            element.data("map").mapTypes.set(gme.mapSettings.styledMapName, myMapType);
        }

        if (gme.mapSettings.defaultType == 3 && gme.mapSettings.styledMapName) {
            element.data("map").setMapTypeId(gme.mapSettings.styledMapName);
        }

        // KML import
        if (gme.mapSettings.kmlUrl != '' && gme.mapSettings.kmlLocal == 0) {
            var kmlLayer = new google.maps.KmlLayer(gme.mapSettings.kmlUrl, {preserveViewport: gme.mapSettings.kmlPreserveViewport});
            kmlLayer.setMap(element.data("map"));
        }

        // KML import local
        if (gme.mapSettings.kmlUrl != '' && gme.mapSettings.kmlLocal == 1) {
            jQuery.get(gme.mapSettings.kmlUrl, function (data) {

                html = "";

                //loop through placemarks tags
                jQuery(data).find("Placemark").each(function (index, value) {
                    //get coordinates and place name
                    var coords = jQuery(this).find("coordinates").text();
                    var place = jQuery(this).find("name").text();
                    var description = jQuery(this).find("description").text();
                    //store as JSON
                    var c = coords.split(",")
                    var address = {
                        title: place,
                        latitude: c[1],
                        longitude: c[0],
                        address: place,
                        marker: '',
                        imageSize: 0,
                        imageWidth: 0,
                        imageHeight: 0,
                        shadow: '',
                        shadowSize: 0,
                        shadowWidth: 0,
                        shadowHeight: 0,
                        infoWindowContent: description,
                        infoWindowLink: 0,
                        openByClick: 1,
                        closeByClick: 1,
                        opened: 0
                    };
                    addMapPoint(address, Route, element, infoWindow, gme);
                    gme.addresses.push(address);
                });
                refreshMap(element, gme);
            });
        }

        // Search
        if (gme.mapSettings.markerSearch == 1) {
            var myForm = jQuery('#' + gme.mapSettings.title + '-search');
            var searchIn = myForm.find('.gme-sword');
            myForm.submit(function () {
                var submitValue = jQuery(searchIn).val().toLowerCase();
                var notFound = true;
                for (var i = 0; i < gme.addresses.length; i++) {
                    jQuery.each(gme.addresses[i], function (index, val) {
                        if (typeof val == "string" && (index == "title" || index == "infoWindowContent") && submitValue != "") {
                            if (val.toLowerCase().indexOf(submitValue) != -1) {
                                infoWindow.setContent(gme.addresses[i].infoWindowContent);
                                infoWindow.open(element.data("map"), element.data("markers")[i]);
                                element.data("map").setCenter(element.data("markers")[i].getPosition());
                                element.data("map").setZoom(7);
                                gme.infoWindow = element.data("markers")[i].getPosition();
                                notFound = false;
                            }
                        }
                    });
                }
                if (notFound) {
                    alert('Die Suche liefert keine Ergebnisse.');
                }
                return false;
            });
        }

        // Add backend addresses
        if (gme.mapSettings.showRoute == 0) {
            element.data("geocoder", new google.maps.Geocoder());
            if (element.data("geocoder")) {
				var adr_max = gme.addresses.length;
				var n_col1 = Math.ceil(adr_max * 0.333);
				var n_col2 = Math.ceil(adr_max * 0.666);
				var l_col1 = '';
				var l_col2 = '';
				var l_col3 = '';
                for (var i = 0; i < adr_max; i++) {
					var i_obj = gme.addresses[i];
                    addMapPoint(i_obj, Route, element, infoWindow, gme);
					//
					var elm = '<li><a href="#" data-id="'+i+'">' + i_obj.title + '</a></li>';
					if( i <= n_col1 ){ l_col1 += elm; }
					else if( i > n_col1 && i <= n_col2 ){ l_col2 += elm; }
					else { l_col3 += elm; }
                }
				//
				l_col1 = '<ul>' + l_col1 + '</ul>';
				l_col2 = '<ul>' + l_col2 + '</ul>';
				l_col3 = '<ul>' + l_col3 + '</ul>';
				//
				$('.map-selector-list .col1').append( l_col1 );
				$('.map-selector-list .col2').append( l_col2 );
				$('.map-selector-list .col3').append( l_col3 );
            }
			//
			$('.map-selector-list').attr('style', 'display:none;');
			$('.map-selector-list a').click(function () {
				showCountry($(this).data('id'), element, gme);
			});
			$('.countryArrow').click(function() {
				toggleCountryList(gme);
			});
			setCountryListButton(gme);
			//
            refreshMap(element, gme);
        }

        // init Route function
        if (gme.mapSettings.showRoute == 1 || gme.mapSettings.calcRoute == 1) {
            var panelHtml = jQuery('<div id="dPanel-' + gme.mapSettings.title + '"><\/div>');
            panelHtml.insertAfter(element);
            var directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer();
            var renderRoute = function ($start, $end, $travelMode, $unitSystem) {
                directionsDisplay.setMap(element.data("map"));
                directionsDisplay.setPanel(document.getElementById("dPanel-" + gme.mapSettings.title));
                var unitSystem = getUnitSystem($unitSystem);
                var request = {
                    origin: $start,
                    destination: $end,
                    travelMode: getTravelMode($travelMode)
                };
                if (unitSystem != 0) {
                    request.unitSystem = unitSystem;
                }
                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                    } else {
                        alert(gme.ll.alert);
                    }
                });
            };
        }

        // show route from backend
        if (gme.mapSettings.showRoute == 1) {
            renderRoute(gme.addresses[0].address, gme.addresses[1].address, gme.mapSettings.travelMode, gme.mapSettings.unitSystem);
        }

        // show route from frontend
        if (gme.mapSettings.showForm == 1) {
            var mapForm = jQuery('#' + gme.mapSettings.title + '-form');

            mapForm.submit(function () {
                var formStartAddress = mapForm.find('.gme-saddress').val();
                var endAddressIndex = mapForm.find('.gme-eaddress option:selected').val();
                var formEndAddress = endAddressIndex ?
                    gme.addresses[parseInt(endAddressIndex)].address :
                    gme.addresses[0].address;
                var formTravelMode = mapForm.find('.gme-travelmode').val();
                var formUnitSystem = mapForm.find('.gme-unitsystem').val();

                if (formStartAddress == null) {
                    formStartAddress = gme.addresses[0].address;
                    formEndAddress = gme.addresses[1].address;
                }

                if (formTravelMode == null) {
                    formTravelMode = gme.mapSettings.travelMode;
                } else {
                    formTravelMode = parseInt(formTravelMode);
                }
                if (formUnitSystem == null) {
                    formUnitSystem = gme.mapSettings.unitSystem;
                } else {
                    formUnitSystem = parseInt(formUnitSystem);
                }

                renderRoute(formStartAddress, formEndAddress, formTravelMode, formUnitSystem);
                return false;
            });
        }

        // eventHandler resize can be used
        element.bind('mapresize', function () {
            google.maps.event.trigger(element.data("map"), 'resize');
            element.data("map").fitBounds(element.data("bounds"));
            if (gme.mapSettings.zoom > 0) {
                element.data("map").setZoom(gme.mapSettings.zoom);
            }
			refreshMap(element, gme);
            google.maps.event.trigger(infoWindow, 'content_changed');
        });

        // open info window
        window.setTimeout(function () {
            element.trigger("openinfo");
        }, 2000);

        // categories checkboxes
        var setCategories = function (selectedCats) {
            jQuery.each(element.data("markers"), function (key, marker) {
                marker.setVisible(false);
                jQuery.each(marker.categories, function (keyM, category) {
                    if (jQuery.inArray(category, selectedCats) != -1) {
                        marker.setVisible(true);
                        return true;
                    }
                });
            });
            if (element.markerCluster) {
                element.markerCluster.repaint();
            }
        }

        // set categories
        var getCats = getURLParameter('tx_gomapsext_show\\[cat\\]');
        if (getCats) {
            getCats = getCats.split(",");
            setCategories(getCats);
            jQuery('.gomapsext-cats INPUT').each(function (key, checkbox) {
                if (jQuery.inArray(jQuery(checkbox).val(), getCats) != -1) {
                    jQuery(checkbox).attr('checked', true);
                    return true;
                }
            });
        }

        jQuery('.gomapsext-cats INPUT').change(function () {
            var selectedCats = jQuery('.gomapsext-cats INPUT:checked').map(function () {
                return this.value;
            });
            setCategories(selectedCats);
        });

        // trigger mapcreated on map
        element.trigger("mapcreated");
    };

    // decode URL Parameter go_maps_ext[cat]
    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
    }

    // add a point
    function addMapPoint(pointDescription, Route, element, infoWindow, gme) {
        Route.push(pointDescription.address);
        var latitude = pointDescription.latitude;
        var longitude = pointDescription.longitude;
        if (Math.round(latitude) == 0 && Math.round(longitude) == 0) {
            element.data("geocoder").geocode({"address": pointDescription.address}, function (point, status) {
                latitude = point[0].geometry.location.lat();
                longitude = point[0].geometry.location.lng();
                var position = new google.maps.LatLng(latitude, longitude);
                setMapPoint(pointDescription, Route, element, infoWindow, position, gme);
            });
            return;
        }
        var position = new google.maps.LatLng(latitude, longitude);
        setMapPoint(pointDescription, Route, element, infoWindow, position, gme);
    };

    // insert the point on the map
    function setMapPoint(pointDescription, Route, element, infoWindow, position, gme) {
        if (pointDescription.marker != "") {
            if (pointDescription.imageSize == 1) {
                var Icon = new google.maps.MarkerImage(pointDescription.marker,
                    new google.maps.Size(pointDescription.imageWidth, pointDescription.imageHeight),
                    new google.maps.Point(0, 0));
                var Shape = {type: 'rectangle', coord: [0, 0, pointDescription.imageWidth, 0, pointDescription.imageWidth, pointDescription.imageHeight, 0, pointDescription.imageHeight, 0, 0]};
            } else {
                var Icon = new google.maps.MarkerImage(pointDescription.marker);
            }
            if (pointDescription.shadow != "") {
                if (pointDescription.shadowSize == 1) {
                    var Shadow = new google.maps.MarkerImage(
                        pointDescription.shadow,
                        new google.maps.Size(pointDescription.shadowWidth, pointDescription.shadowHeight),
                        new google.maps.Point(0, 0),
                        new google.maps.Point((pointDescription.imageWidth / 2), pointDescription.imageHeight)
                    );
                } else {
                    var Shadow = new google.maps.MarkerImage(pointDescription.shadow);
                }
                var marker = new google.maps.Marker({
                    position: position,
                    map: element.data("map"),
                    icon: Icon,
                    shape: Shape,
                    shadow: Shadow
                });
            } else {
                var marker = new google.maps.Marker({
                    position: position,
                    map: element.data("map"),
                    icon: Icon,
                    shape: Shape
                });
            }
        } else {
            var marker = new google.maps.Marker({position: position, map: element.data("map")});
        }
        ;
        if (gme.mapSettings.markerCluster == 1) {
            google.maps.event.addListener(marker, 'visible_changed', function () {
                if (marker.getVisible()) {
                    element.markerCluster.addMarker(marker, true);
                } else {
                    element.markerCluster.removeMarker(marker, true);
                }
            });
        }

        var closeInfoWindows = function () {
            infoWindowsArray.each(function (index, infoWindow) {
                infoWindow.close();
            });
        };
        if (pointDescription.infoWindowContent != "" || pointDescription.infoWindowLink > 0) {
            var infoWindowContent = pointDescription.infoWindowContent;
            if (pointDescription.infoWindowLink > 0) {
                var daddr = (pointDescription.infoWindowLink == 2) ? pointDescription.latitude + ", " + pointDescription.longitude : pointDescription.address;
                daddr += " (" + pointDescription.title + ")";
                infoWindowContent += '<p class="routeLink"><a href="//maps.google.com/maps?daddr=' + encodeURI(daddr) + '" target="_blank">' + gme.ll.infoWindowLinkText + '<\/a><\/p>';
            }
            infoWindowContent = '<div class="gme-info-window">' + infoWindowContent + '</div>';

            if (pointDescription.openByClick) {
                google.maps.event.addListener(marker, "click", function () {
                    if (!infoWindow.getMap() || gme.infoWindow != this.getPosition()) {
                        infoWindow.setContent(infoWindowContent);
                        infoWindow.open(element.data("map"), this);
                        gme.infoWindow = this.getPosition();
                    }
                });
				if( pointDescription.showDetails ){
					google.maps.event.addListener(infoWindow,'closeclick',function(){
						hideDetails(gme);
					});
				}
            } else {
                google.maps.event.addListener(marker, "mouseover", function () {
                    if (!infoWindow.getMap() || gme.infoWindow != this.getPosition()) {
                        infoWindow.setContent(infoWindowContent);
                        infoWindow.open(element.data("map"), this);
                        gme.infoWindow = this.getPosition();
                    }
                });
            }
			if (pointDescription.showDetails) {
				google.maps.event.addListener(marker, "click", function () {
					element.data("map").setCenter(marker.getPosition());
					showDetails(pointDescription, gme);
                });
			}
            if (!pointDescription.closeByClick) {
                google.maps.event.addListener(marker, "mouseout", function () {
                    infoWindow.close();
                });
				hideDetails(gme);
            }
            if (pointDescription.opened) {
                element.off("openinfo").on("openinfo", function () {
                    infoWindow.setContent(infoWindowContent);
                    infoWindow.open(element.data("map"), marker);
                });
                gme.infoWindow = marker.getPosition();
            }
        };
		
        marker.categories = pointDescription.categories.split(",");
        element.data("markers").push(marker);
        element.data("bounds").extend(position);
    };

    function getTravelMode($travelMode) {
        var travelMode = google.maps.TravelMode.DRIVING;
        switch ($travelMode) {
            case 2:
                travelMode = google.maps.TravelMode.BICYCLING;
                break;
            case 3:
                travelMode = google.maps.TravelMode.TRANSIT;
                break;
            case 4:
                travelMode = google.maps.TravelMode.WALKING;
                break;
        }
        return travelMode;
    }

    // get the unit system
    function getUnitSystem($unitSystem) {
        var unitSystem = 0;
        switch ($unitSystem) {
            case 2:
                unitSystem = google.maps.UnitSystem.METRIC;
                break;
            case 3:
                unitSystem = google.maps.UnitSystem.IMPERIAL;
                break;
        }
        return unitSystem;
    }

    // Set zoom, center and cluster
    function refreshMap(element, gme) {
        if (gme.mapSettings.zoom > 0 || gme.addresses.length == 1) {
            google.maps.event.addListener(element.data("map"), "zoom_changed", function () {
                var zoomChangeBoundsListener = google.maps.event.addListener(element.data("map"), "bounds_changed", function (event) {
                    if (this.initZoom == 1) {
                        this.setZoom((gme.mapSettings.zoom > 0) ? gme.mapSettings.zoom : gme.mapSettings.defaultZoom);
                        this.initZoom = 0;
                    }
                    google.maps.event.removeListener(zoomChangeBoundsListener);
                });
            });
            element.data("map").initZoom = 1;
        }

		if( gme.mapSettings.centerFitBounds ){
	        element.data("map").fitBounds(element.data("bounds"));
		}
		
        refreshCluster(element, gme);
    }

    // refresh the cluster
    function refreshCluster(element, gme) {
        if (gme.mapSettings.markerCluster == 1) {
            if (element.markerCluster != null) {
                element.markerCluster.clearMarkers();
            }
            element.markerCluster = new MarkerClusterer(element.data("map"), element.data("markers"), {
                maxZoom: gme.mapSettings.markerClusterZoom,
                gridSize: gme.mapSettings.markerClusterSize
            });
        }
    }
	
	//	show Details
	function showDetails (pointDescription, gme) {
		//
		var html = '';
		var count_max = 2;
		var count = pointDescription.uid_addr.length;
		count = count > count_max ? count_max : count;
		var t_sort = [];
		var t_obj = {};
		for( var i=0; i<count; i++ ){
			var obj = gme.details[pointDescription.uid_addr[i]];
			t = '<div class="address-author address-author-width1">';
			if( obj.image ){
				t += '<div class="address-image">';
				if( obj.page ){ t += '<a href="' + obj.page + '">'; }
				t += '<img width="100" height="134" src="' + obj.image +'" alt="'+ obj.fullname +'">';
				if( obj.page ){ t += '</a>'; }
				t += '</div>';
			}
			t += '<div class="address-name">';
			if( obj.page ){ t += '<a href="' + obj.page + '">'; }
			t += obj.fullname;
			if( obj.page ){ t += '</a>'; }
			t += '</div>';
			t += '<div class="address-data">';
			if( obj.funktion ){ t +=  obj.funktion + '<br>'; }
			if( obj.berufstitel ){ t +=  obj.berufstitel + '<br>'; }
			if( obj.berufstitel || obj.funktion ){ t += '<br>'; }
			if( obj.phone ){ t +=  obj.phone + '<br>'; }
			if( obj.email ){ t +=  obj.email + '<br>'; }
			t += '</div>';
			t += '</div>';
//			html += t;
			//
			t_sort.push(obj.lastname);
			t_obj[obj.lastname] = t;
		}
		//
		t_sort.sort();
		//
		var t_len = t_sort.length;
		for( var i=0; i<t_len; i++ ){
			i_name = t_sort[i];
			html += t_obj[i_name];
		}
		//
		var h = count > 1 ? gme.ll.addressHeader_more : gme.ll.addressHeader_single;
		//
		$('.mapDetailsHeader').html(h);
		$('.mapDetailsAddresses').html(html);
		$('.mapDetailInfo').hide();
	}
	
	//	hide Details
	function hideDetails (gme) {
		$('.mapDetailsHeader').html(gme.ll.infoHeader);
		$('.mapDetailInfo').show();
		$('.mapDetailsAddresses').empty();
	}
	
	//	show COUNTRY
	function showCountry (idx, element, gme) {
		toggleCountryList(gme);
		google.maps.event.trigger(element.data("markers")[idx], 'click');
		return false;
	}
	
	function toggleCountryList (gme) {
		$(".map-selector-list").toggle();
		setCountryListButton(gme);
	}
	
	function setCountryListButton (gme) {
		var isEN = gme.ll.submit == "Calculate Route";
		if( $('.map-selector-list:visible').length ){
			var t = isEN ? 'hide country list' : 'Länderliste verbergen';
		} else {
			var t = isEN ? 'show country list' : 'Länderliste zeigen';
		}
		$('.countryArrow').html(t);
		$('.countryArrow').show();
	}

})(jQuery);