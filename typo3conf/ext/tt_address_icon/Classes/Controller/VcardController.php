<?php
namespace NEXT\TtAddressIcon\Controller;

/***************************************************************
*  Copyright notice
*
*  (c) 2014 smo <r.schmoller@next-linz.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/


//The vCard spec calls for the termination of each property with a carriage return followed by a line feed. Rather than cluttering this file with "chr(10) . chr(13)"
//everywhere, I'll use CRLF in its place by defining it here.
define(CRLF, "\r\n");

/**
 * Library functions for the 'tt_address_icon' extension.
 *
 * @author  smo <r.schmoller@next-linz.com>
 * @package  TYPO3
 * @subpackage  tx_address_icon
 */
 
/**
 *
 *
 * @package tt_address_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class VcardController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

  /**
   * addressRepository
   *
   * @var \NEXT\TtAddressIcon\Domain\Repository\AddressRepository
   * @inject
   */
  protected $addressRepository;


  var $conf;
  var $mapping;
  var $vCardContent;
  var $pid;

  /**
   * Generates a vCard from the specified (URL parameter) address in the tt_address table.
   *
   * @param       string  the content to be filled, usually empty
   * @param       array  additional configuration parameters
   * @return      string  the XML sitemap ready to render
   */

	/**
	 * action vcardFile
	 *
	 * @return void
	 */
	public function vcardFileAction() {
  
		$csv = '';
		// this will execute only in the backend...
		$uidAddress = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP("team");
		if( $uidAddress > 0 ){
			$address = $this->addressRepository->findByUid($uidAddress);
			if( $address ){
				$vcardPath = $this->addressRepository->makeVCard($address, $this->settings);
				//Redirect the user's browser to the file.
				header ('Location: ' . \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL') . $vcardPath);
			}
		} else {
			header('Content-Type: text/plain; charset=utf-8');
			$vcard = 'ungültiger Aufruf!;team = ' . $uidAddress;
		}
		//
		return $vcard;
	}
}
?>