<?php
namespace NEXT\TtAddressIcon\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package tt_address_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AddressController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

  /**
   * addressRepository
   *
   * @var \NEXT\TtAddressIcon\Domain\Repository\AddressRepository
   * @inject
   */
  protected $addressRepository;

  /**
   * addressGroupRepository
   *
   * @var \NEXT\TtAddressIcon\Domain\Repository\AddressGroupRepository
   * @inject
   */
  protected $addressGroupRepository;

  /**
   * newsRepository
   *
   * @var \NEXT\NewsIcon\Domain\Repository\NewsRepository
   * @inject
   */
  protected $newsRepository;

  /**
   * eventRepository
   *
   * @var \NEXT\IconEvents\Domain\Repository\EventRepository
   * @inject
   */
  protected $eventRepository;


  /**
   * action index
   *
   * @param \NEXT\TtAddressIcon\Domain\Model\Search $searchdata
   * @dontvalidate $searchdata
   * @return void
   */
  public function indexAction(\NEXT\TtAddressIcon\Domain\Model\Search $searchdata = NULL) {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$sql = '';
	//
    if( $searchdata==NULL ){
      $searchdata = $this->objectManager->getEmptyObject('\NEXT\TtAddressIcon\Domain\Model\Search');
    }
	//
	$group1 = $this->addressGroupRepository->listChilds($this->settings['filter1Groups']);
	$group2 = $this->addressGroupRepository->listChilds($this->settings['filter2Groups']);
	//
	$showForm = $this->settings['filter1Groups']!='' || $this->settings['filter2Groups']!='';
	//
	$filter1 = $searchdata->getFilter1();
	$filter2 = $searchdata->getFilter2();
	if( $filter1 > 0 && $filter2 == '' ){
		$subGroup = $filter1;
	} elseif ( $filter1 == '' && $filter2 > 0 ){
		$subGroup = $filter2;
	} else {
		$subGroup = 0;
	}
	//
	if( $subGroup > 0 ){
		$list = $this->addressRepository->listAddressesFilteredBySubGroup($this->settings['groupSelection'], $subGroup, $this->settings['berufstitelshort']);
	} else {
		$list = $this->addressRepository->listAddresses($this->settings['groupSelection'], $this->settings['berufstitelshort']);
	}
	$this->view->assignMultiple( array(
		'LANG' => $LANG,
		'sql' => $sql, 
		'list' => $list,
		'group1' => $group1,
		'group2' => $group2,
		'searchdata' => $searchdata,
		'showForm' => $showForm,
	));
  }

  /**
   * action detail
   *
   * @param string $address
   * @return void
   */
  public function detailAction($address=0) {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	if ($this->request->hasArgument('address')) {
		$address = (int)$this->request->getArgument('address');
		if( $address > 0 ){
			$res_address = $this->addressRepository->findByUid($address);
			if( $res_address ){
				//	CHANGE PAGE - TITLE
				$page_title = trim( $res_address->getTitle() . ' ' . $res_address->getFirstname() . ' ' . $res_address->getLastname() );
				if( $res_address->getTitleAfter() ){
					$page_title .= ', ' . $res_address->getTitleAfter();
				}
				$GLOBALS['TSFE']->page['title'] = $page_title;
				$GLOBALS['TSFE']->indexedDocTitle = $page_title;
				//
				$searchdata = $this->objectManager->getEmptyObject('\NEXT\NewsIcon\Domain\Model\Search');
				$searchdata->setAuthor($address);

				//	CHECK, IF HAS BOOKS
				$hasBooks = 0;
				$pidBooks = $this->settings['person']['publications']['books']['storagePid'];
				if( $pidBooks ){
				    $res_books = $this->newsRepository->searchNewsNew($pidBooks, $searchdata);
					if( $res_books ){
						if( count($res_books) > 0 ){
							$hasBooks = TRUE;
						}
					}
				}
				//	CHECK, IF HAS NEWS
				$hasNews = 0;
				$pidNews = $this->settings['person']['publications']['news']['storagePid'];
				if( $pidNews ) {
				    $res_news = $this->newsRepository->searchNewsNew($pidNews, $searchdata);
					if( $res_news ){
						if( count($res_news) > 0 ){
							$hasNews = TRUE;
						}
					}
				}
				//	CHECK, IF HAS PUBLICATIONS
				$hasPublications = 0;
				$pidPublications = $this->settings['person']['publications']['publications']['storagePid'];
				if( $pidPublications ) {
				    $res_publications = $this->newsRepository->searchNewsNew($pidPublications, $searchdata);
					if( $res_publications ){
						if( count($res_publications) > 0 ){
							$hasPublications = TRUE;
						}
					}
				}
				//	CHECK, IF HAS EVENTS
				$hasEvents = 0;
				$searchdata = $this->objectManager->getEmptyObject('\NEXT\IconEvents\Domain\Model\Search');
				$searchdata->setSpeaker($address);
				$pidEvents = $this->settings['person']['events']['events']['storagePid'];
				if( $pidEvents ) {
					$res_events = $this->eventRepository->listEvents($searchdata);
					if( $res_events ){
						if( count($res_events) > 0 ){
							$hasEvents = TRUE;
						}
					}
				}

				//
				$showButtons = $hasBooks || $hasNews || $hasPublications || $hasEvents;
				//
				$this->view->assignMultiple( array(
					'LANG' => $LANG, 
					'address' => $res_address,
					'showButtons' => $showButtons,
					'hasBooks' => $hasBooks,
					'hasPublications' => $hasPublications,
					'hasNews' => $hasNews,
					'hasEvents' => $hasEvents,
				));
			} else {
				return '';
			}
		} else {
			return '';
		}
	} else {
		return '';
	}
  }

  
  /**
   * action list
   *
   * @return void
   */
  public function listAction() {
	$LANG =  $GLOBALS['TSFE']->sys_language_uid;
	$list = $this->addressRepository->listAddresses($this->settings['groupSelection'], $this->settings['berufstitelshort']);
	$this->view->assignMultiple( array(
		'LANG' => $LANG, 
		'list' => $list
	));
  }

}

