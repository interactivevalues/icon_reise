<?php
namespace NEXT\TtAddressIcon\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package tt_address_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CountriesController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

  /**
   * countriesRepository
   *
   * @var \NEXT\TtAddressIcon\Domain\Repository\CountriesRepository
   * @inject
   */
  protected $countriesRepository;
  
  /**
   * action list
   *
   * @return void
   */
  public function listAction() {
	$L =  $GLOBALS['TSFE']->sys_language_uid;
	$group = $this->settings['groupSelection'];
	$list = $this->countriesRepository->listExpertsByCountries($group);
	$this->view->assign('L', $L);
	$this->view->assign('list', $list);
  }
  
  
  /**
   * action map
   *
   * @return void
   */
  public function mapAction() {
	$L =  $GLOBALS['TSFE']->sys_language_uid;
	$group = $this->settings['groupSelection'];
	list($list, $details) = $this->countriesRepository->listExpertsByCountriesForMap($group);

	$ext_key = $this->request->getControllerExtensionKey();
	$ext_key = 'go_maps_ext';
	$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$ext_key]);
	$pageRenderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Page\\PageRenderer');
	$addJsMethod = 'addJs';
	
	if($this->extConf['footerJS'] == 1) {
		$addJsMethod = 'addJsFooter';
	}

	$googleMapsLibrary = $this->settings['googleMapsLibrary'] ?  htmlentities($this->settings['googleMapsLibrary']) : '//maps.google.com/maps/api/js?v=3.18&sensor=false';

	if($this->settings['language']) {
		$googleMapsLibrary .= '&language=' . $this->settings['language'];
	}

	$pageRenderer->{$addJsMethod . 'Library'}('googleMaps', $googleMapsLibrary, 'text/javascript', FALSE, FALSE, '', TRUE);

	$ext_key = $this->request->getControllerExtensionKey();
	$scripts[] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($ext_key) . 'Resources/Public/Scripts/jquery.gomapsext.js';	
	$scripts[] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($ext_key) . 'Resources/Public/Scripts/markerclusterer_compiled.js';
	
	foreach ($scripts as $script) {
//		$pageRenderer->{$addJsMethod . 'File'}($script);
		$pageRenderer->{$addJsMethod . 'File'}($script, 'text/javascript', TRUE, TRUE, '', TRUE);
	}
	
	$zoom = $this->settings['mapZoom'];
	$zoom = $zoom!='' ? $zoom : 1;
	$centerFitBounds = ($this->settings['mapCenterLAT']>0 && $this->settings['mapCenterLNG']>0) ? 0 : 1;

	$map = array(
		'title' => 'address-map',
		'width' => $this->settings['mapWidth'] . 'px',
		'height' => $this->settings['mapHeight'] . 'px',
		'centerLAT' => $this->settings['mapCenterLAT'],
		'centerLNG' => $this->settings['mapCenterLNG'],
		'centerFitBounds' => $centerFitBounds,
		'zoom' => $zoom,
		'scrollZoom' => 1,
		'draggable' => 1,
		'zoomControl' => 1,
		'doubleClickZoom' => 1,
		'mapTypeControl' => 1,
		'defaultType' => 0,
		'zoomControlType' => 0,
		'minZoom' => 1,
		'travelMode' => 0,
		'unitSystem' => 2,
		'mapTypes' => array(0, 2),
	);

	$this->view->assign('lang', $L);
	$this->view->assign('map', $map);
	$this->view->assign('list', $list);
	$this->view->assign('details', $details);
  }


}

