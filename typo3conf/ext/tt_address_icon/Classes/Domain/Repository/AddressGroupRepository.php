<?php
namespace NEXT\TtAddressIcon\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package tt_address_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AddressGroupRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

  /**
   * listChilds
   *
   * @param \integer $uid
   * @return
   */
  public function listChilds($uid) {
    $t = array();
    //
	if( $uid > 0 ){
	    $sql = ' SELECT * FROM `sys_category` ';
		$sql .= ' WHERE `deleted`=0 AND `hidden`=0 ';
		$sql .= ' AND `parent`='.$uid.' ';
	    $sql .= ' ORDER BY `title` ASC ';
		//
	    $res = $GLOBALS['TYPO3_DB']->sql_query($sql);
    	while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
/*
	      	$t[] = array(
				'uid' => $row['uid'],
				'title' => $row['title'],
			);
*/
			$t[ $row['uid'] ] = $row['title'];
    	}
	    $GLOBALS['TYPO3_DB']->sql_free_result($res);
    }
    //
    return $t;
  }

}
?>