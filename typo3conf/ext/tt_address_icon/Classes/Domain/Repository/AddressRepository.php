<?php

namespace NEXT\TtAddressIcon\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package tt_address_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AddressRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * listAddresses
     *
     * @param \integer $uid
     * @param \string $berufstitelshort
     * @return
     */
    public function listAddresses($uid, $berufstitelshort)
    {
        $t = array();
        //
        if ($uid > 0) {
            $sql = ' SELECT `TA`.* FROM `tt_address` AS `TA`, `sys_category_record_mm` AS `TGMM` ';
            $sql .= ' WHERE `TA`.`deleted`=0 AND `TA`.`hidden`=0 ';
            $sql .= ' AND `TA`.`ausgetretten`=0 ';
            $sql .= ' AND `TGMM`.`uid_foreign` = `TA`.`uid` ';
            $sql .= ' AND `TGMM`.`uid_local`=' . $uid . ' ';
            if ($berufstitelshort != '') {
                $sql .= ' AND berufstitel_short like "%' . $berufstitelshort . '%" ';
            }
            $sql .= ' ORDER BY `TA`.`last_name` ASC, `TA`.`first_name` ASC ';
//var_dump($sql);
            //
            $res = $GLOBALS['TYPO3_DB']->sql_query($sql);
            while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                $t[] = array(
                    'uid' => $row['uid'],
                    'title' => $row['title'],
                    'lastname' => $row['last_name'],
                    'firstname' => $row['first_name'],
                    'titleAfter' => $row['title_after'],
                    'berufstitelshort' => $row['berufstitel_short'],
                    'berufstitelshort_EN' => $row['berufstitel_short_EN'],
                    'funktion' => $row['funktion'],
                    'funktion_EN' => $row['funktion_EN'],
                    'phone' => $row['phone'],
                    'mobile' => $row['mobile'],
                    'fax' => $row['fax'],
                    'page' => $row['page_detail'],
                );
            }
            $GLOBALS['TYPO3_DB']->sql_free_result($res);
        }
        //
        return $t;
    }

    /**
     * listAddressesFilteredBySubGroup
     *
     * @param \integer $uid
     * @param \integer $subgroup
     * @param \string $berufstitelshort
     * @return
     */
    public function listAddressesFilteredBySubGroup($uid, $subgroup, $berufstitelshort)
    {
        $t = array();
        $sql = '';
        //
        if ($uid > 0) {
            $sql = ' SELECT `TA1`.* FROM `tt_address` AS `TA1`, `sys_category_record_mm` AS `TGMM1` ';
            $sql .= ' WHERE `TA1`.`deleted`=0 AND `TA1`.`hidden`=0 ';
            $sql .= ' AND `TA1`.`ausgetretten`=0 ';
            $sql .= ' AND `TGMM1`.`uid_local` = `TA1`.`uid` ';
            $sql .= ' AND `TGMM1`.`uid_local` IN ( ';

            $sql .= '   SELECT `TA`.uid FROM `tt_address` AS `TA`, `sys_category_record_mm` AS `TGMM` ';
            $sql .= '   WHERE `TA`.`deleted`=0 AND `TA`.`hidden`=0 ';
            $sql .= '   AND `TA`.`ausgetretten`=0 ';
            $sql .= '   AND `TGMM`.`uid_local` = `TA`.`uid` ';
            $sql .= '   AND `TGMM`.`uid_foreign` = ' . $uid . ' ';
            if ($berufstitelshort != '') {
                $sql .= '   AND berufstitel_short like "%' . $berufstitelshort . '%" ';
            }
            $sql .= ' ) ';
            $sql .= ' AND `TGMM1`.`uid_foreign` = ' . $subgroup . ' ';
            $sql .= ' ORDER BY `TA1`.`last_name` ASC, `TA1`.`first_name` ASC ';
            //
            $res = $GLOBALS['TYPO3_DB']->sql_query($sql);
            while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                $t[] = array(
                    'uid' => $row['uid'],
                    'title' => $row['title'],
                    'lastname' => $row['last_name'],
                    'firstname' => $row['first_name'],
                    'titleAfter' => $row['title_after'],
                    'berufstitelshort' => $row['berufstitel_short'],
                    'funktion' => $row['funktion'],
                    'phone' => $row['phone'],
                    'mobile' => $row['mobile'],
                    'fax' => $row['fax'],
                    'page' => $row['page_detail'],
                );
            }
            $GLOBALS['TYPO3_DB']->sql_free_result($res);
        }
        //
        return $t;
//    return array($t, $sql);
    }


    /*
     *	makeVCard
     *
     * @param mixed $address
     * @return string
     */
    public function makeVCard($address, $settings)
    {
        $this->conf = $settings;
        // for TEAM add predefined values
        $this->contact['phone'] = '+43 732 69412 ' . $this->contact['phone'];
        $this->contact['fax'] = '+43 732 69412 ' . $this->contact['fax'];
        $this->contact['address'] = 'Stahlstrasse 14';
        $this->contact['zip'] = '4020';
        $this->contact['cty'] = 'Linz';
        $this->contact['country'] = 'Austria';
        $this->contact['address'] = 'Stahlstrasse 14;Linz;;4020;Austria';
        $this->contact['company'] = 'ICON';
        $this->contact['www'] = 'http://www.icon.at';

        //Configure the mapping; which fields in the VCF map to which fields in tt_address
        $this->mapping = $this->explodeMapping($this->conf['mapping']['team']);
        if (!$this->mapping)
            return $this->returnError($LL['mapping_error']);

        //Start building the vCard
        $this->vCardContent = "BEGIN:VCARD" . CRLF;
        $this->vCardContent .= "VERSION:3.0" . CRLF;

        //Build the variable content
        $this->vCardContent .= $this->buildVCard();

        //Finish off the vCard
        $this->vCardContent .= "CLASS:PUBLIC" . CRLF;
        $this->vCardContent .= "END:VCARD";

        //Set the target directory for saving the vCard, and set the file name.
        $relPath = 'typo3temp/tt_address_icon/';
        $dname = PATH_site . $relPath;
        $fname = ($this->contact['name'] != '' ? $this->contact['name'] : 'contact') . '.vcf';

        if (!is_dir($dname))
            \TYPO3\CMS\Core\Utility\GeneralUtility::mkdir($dname);

        //Create the file if it doesn't exist and open a handle to it.
        //touch($dname . $fname);
        $fp = fopen($dname . $fname, 'w');

        //Write to the file and close it.

        $this->vCardContent = utf8_decode($this->vCardContent);

        fwrite($fp, $this->vCardContent);
        fclose($fp);

        return $relPath . $fname;
    }


    //Build the various properties that comprise the vCard. Some of the properties have a very specific syntax, and for these properties there is a
    //function specifically to deal with them.
    function buildVCard()
    {
        $vCardData = '';
        foreach ($this->mapping as $property => $value) {
            switch ($property) {
                case 'N' :
                    $vCardData .= $this->buildProperty($property, $value, 1, 'CHARSET=iso-8859-1');
                    break;
                case 'FN' :
                    $vCardData .= $this->buildProperty($property, $value, 0, 'CHARSET=iso-8859-1');
                    break;
                case 'TEL' :
                    $vCardData .= $this->buildTEL($value);
                    break;
                case 'PHOTO' :
                    $vCardData .= $this->buildPHOTO($value);
                    break;
                case 'BDAY' :
                    $vCardData .= $this->buildDATE('BDAY', $value);
                    break;
                case 'ADR' :
                    $vCardData .= $this->buildADR($value);
                    break;
                case 'EMAIL' :
                    $vCardData .= $this->buildEMAIL($value);
                    break;
                case 'WWW' :
                    $vCardData .= $this->buildURL($value);
                    break;
                case 'REV' :
                    $vCardData .= $this->buildDATE('REV', $value);
                    break;
                default :
                    $vCardData .= $this->buildProperty($property, $value);
            }
        }
        return $vCardData;
    }


    //Build the EMAIL property
    function buildEMAIL($value)
    {
        return 'EMAIL;TYPE=internet:' . $this->contact[$value] . CRLF;
    }

    //Build the URL property
    function buildURL($value)
    {
        return 'URL;' . $this->contact[$value] . CRLF;
    }

    //Build the ADR property
    function buildADR($value)
    {

        $arrTemp = explode(":", $value);
        if (count($arrTemp) > 1) {
            $locations = "TYPE=" . join(",", explode(";", $arrTemp[0]));
            $addressField = $arrTemp[1];
        } else
            $addressField = $value;

        if (empty($this->contact[$addressField]))
            return '';

        $address = array('POBOX' => '', 'EXTADD' => '', 'STREET' => '', 'CITY' => '', 'STATE' => '', 'POSTCODE' => '', 'COUNTRY' => '');
        $address['STREET'] = implode(',', explode("\r\n", $this->contact['address']));
        $address['CITY'] = $this->contact['city'];
        $address['STATE'] = $this->contact['zone'];
        $address['POSTCODE'] = $this->contact['zip'];
        $address['COUNTRY'] = $this->contact['country'];

        $label = 'LABEL;ENCODING=QUOTED-PRINTABLE:';
        foreach ($address as $addr_el)
            if (!empty($addr_el))
                $label .= $addr_el . '=0D=0A';
        $label = substr($label, 0, -6);
        return 'ADR;' . $locations . ':' . implode(';', $address) . CRLF . $label . CRLF;
    }


    //Build the DATE property
    function buildDATE($type, $value)
    {
        $date = $this->contact[$value];
        if (!empty($date))
            return $type . ':' . date('Y-m-d', $date) . CRLF;
        return '';
    }


    //Build the PHOTO property
    function buildPHOTO($value)
    {

        if (empty($this->contact[$value]))
            return '';

        $fnameRel = 'uploads/pics/' . $this->contact[$value];
        $fnameAbs = PATH_site . $fnameRel;
        if (file_exists($fnameAbs))
            return 'PHOTO;ENCODING=b;TYPE=JPEG:' . base64_encode(addslashes(fread(fopen($fnameAbs, "r"), filesize($fnameAbs)))) . CRLF;
        return '';
    }


    //Build the TEL property
    function buildTEL($value)
    {

        $propString = '';

        if (is_array($value)) {
            foreach ($value as $phoneType) {
                $arrTmp = explode('\.', $phoneType);
                if (!empty($this->contact[$arrTmp[1]])) {
                    switch ($arrTmp[0]) {
                        case 'WORK' :
                            $propString .= 'TEL;WORK;VOICE:' . $this->contact[$arrTmp[1]] . CRLF;
                            break;
                        case 'CELL' :
                            $propString .= 'TEL;CELL;VOICE:' . $this->contact[$arrTmp[1]] . CRLF;
                            break;
                        case 'FAX' :
                            $propString .= 'TEL;FAX:' . $this->contact[$arrTmp[1]] . CRLF;
                            break;
                        default :
                            $propString .= 'TEL:' . $this->contact[$arrTmp[1]] . CRLF;
                            break;
                    }
                }
            }
        } else {
            if (!empty($this->contact[substr(strstr($value, '.'), 1)]))
                $propString .= 'TEL;VOICE:' . $this->contact[substr(strstr($value, '.'), 1)] . CRLF;
        }
        return $propString;
    }


    //Build the LABEL property
    function buildLABEL($value)
    {
        return 'LABEL:' . $value . 'REFORMATTED' . CRLF;
    }


    //Build a "generic" property that has no special syntax
    function buildProperty($property, $value, $allowEmpty = 0, $encoding = '')
    {

        if ($encoding) {
            $propString = $property . ';' . $encoding . ':';
        } else {
            $propString = $property . ':';
        }

        if (is_array($value)) {
            foreach ($value as $val) {
                if ($allowEmpty) {
                    $propString .= $this->contact[$val] . ';';
                } else {
                    if (!empty($this->contact[$val]))
                        $propString .= $this->contact[$val] . ';';
                    else
                        return '';
                }
            }
            $propString = substr($propString, 0, -1); //Removes trailing ;
        } else {
            if (!empty($this->contact[$value]))
                $propString .= $this->contact[$value];
            else
                return '';
        }
        return $propString . CRLF;
    }


    //Escape commas and semicolons, as they are part of the specification syntax
    function escapeData($data)
    {

        //Even though it's in the spec, I can't find an address book client that supports it! So I'll leave it out.
        //return utf8_decode(str_replace(',','\,', str_replace(';','\;', str_replace(':','\:',$data))));
        //return utf8_decode($data);
        return $data;
    }


    function explodeMapping($arrMapping)
    {

        foreach ($arrMapping as $key => $value) {
            $arrTemp = explode(",", $value);
            if (count($arrTemp) > 1) {
                $arrMapping[$key] = array();
                foreach ($arrTemp as $newvalue)
                    $arrMapping[$key][] = $newvalue;
            }
        }

        return $arrMapping;
    }


}

?>