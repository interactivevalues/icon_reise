<?php
namespace NEXT\TtAddressIcon\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package tt_address_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CountriesRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {


  /**
   * listExpertsByCountries
   *
   * @param \integer $uid
   * @return
   */
  public function listExpertsByCountries($uid) {
    $t = array();
    //
	if( $uid > 0 ){
	    $sql = ' SELECT ';
		$sql .= ' REPLACE( REPLACE( REPLACE( LOWER(`TG`.`title`), "ö", "oz-"), "ü", "uz-" ), "ä", "az-") AS `sorttitle`, ';
		$sql .= ' `TG`.`title` AS `group_title`, `TA`.* ';
		$sql .= ' FROM `sys_category` AS `TG`, `sys_category_record_mm` AS `TGMM`, `tt_address` AS `TA` ';
		$sql .= ' WHERE `TG`.`parent` = ' . $uid;
		$sql .= ' AND `TGMM`.`uid_local` = `TG`.`uid` ';
		$sql .= ' AND `TA`.`uid` = `TGMM`.`uid_foreign` ';
		$sql .= ' AND `TA`.`hidden`=0 AND `TA`.`deleted`=0 ';
		$sql .= ' AND `TA`.`ausgetretten`=0 ';
	    $sql .= ' ORDER BY `sorttitle` ASC ';
		//
//var_dump($sql);
	    $res = $GLOBALS['TYPO3_DB']->sql_query($sql);
    	while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$t_fn = $row['first_name'];
			$t_ln = $row['last_name'];
			$t_title = $row['title'];
			$t_name = $t_fn . ' ' . $t_ln;
			if( $t_title != '' ){
				$t_name .= ', ' . $t_title;
			}
	      	$t[] = array(
				'uid' => $row['uid'],
				'country' => $row['group_title'],
				'name' => $t_name,
				'page' => $row['page_detail'],
			);
    	}
	    $GLOBALS['TYPO3_DB']->sql_free_result($res);
    }
    //
    return $t;    

  }

  /**
   * listExpertsByCountriesForMap
   *
   * @param \integer $uid
   * @return
   */
  public function listExpertsByCountriesForMap($uid) {
    $t = array();
	$a = array();
    //
	if( $uid > 0 ){
	    $sql = ' SELECT ';
		$sql .= ' REPLACE( REPLACE( REPLACE( LOWER(`TG`.`title`), "ö", "oz-"), "ü", "uz-" ), "ä", "az-") AS `sorttitle`, ';
		$sql .= ' `TG`.`uid` AS `group_uid`, `TG`.`title` AS `group_title`, `TG`.`latitude` AS `group_latitude`, `TG`.`longitude` AS `group_longitude`, `TA`.* ';
		$sql .= ' FROM `sys_category` AS `TG`, `sys_category_record_mm` AS `TGMM`, `tt_address` AS `TA` ';
		$sql .= ' WHERE `TG`.`parent` = ' . $uid;
		$sql .= ' AND `TGMM`.`uid_local` = `TG`.`uid` ';
		$sql .= ' AND `TA`.`uid` = `TGMM`.`uid_foreign` ';
		$sql .= ' AND `TA`.`hidden`=0 AND `TA`.`deleted`=0 ';
		$sql .= ' AND `TA`.`ausgetretten`=0 ';
	    $sql .= ' ORDER BY `sorttitle` ASC ';
		//
//var_dump($sql);
	    $res = $GLOBALS['TYPO3_DB']->sql_query($sql);
		$last = '';
    	while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$t_fn = $row['first_name'];
			$t_ln = $row['last_name'];
			$t_title = $row['title'];
			$t_name = $t_fn . ' ' . $t_ln;
			if( $t_title != '' ){
				$t_name .= ', ' . $t_title;
			}
			if( $row['group_latitude'] <> 0.0 and $row['group_longitude'] <> 0.0 ){
				if( $row['group_uid'] != $last ){
			      	$t[] = array(
						'uid_group' => $row['group_uid'],
						'country' => $row['group_title'],
						'latitude' => $row['group_latitude'],
						'longitude' => $row['group_longitude'],
						'title' => $row['group_title'],
						'infoWindowContent' => '<strong>' . $row['group_title'] . '</strong>',
						'uid_addr' => $row['uid'],
					);
					$last = $row['group_uid'];
				} else {
					// add additional uid_address !!!
					$t[ count($t)-1 ]['uid_addr'] .= ',' . $row['uid'];
				}
				//	BUILD ADDRESSES
				$t_name = trim( trim($row['title'] . ' ' . $t_fn . ' ' . $t_ln . ', ' . $row['title_after']), ', ');
				//
				$t_image = '';
				$t_sql = ' SELECT * FROM `sys_file_reference` ';
				$t_sql .= ' WHERE `uid_foreign` = ' . $row['uid'];
				$t_sql .= ' AND `tablenames` = "tt_address" AND `fieldname` = "image" ';
				$t_res = $GLOBALS['TYPO3_DB']->sql_query($t_sql);
				while ($t_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($t_res)) {
					$t_image = $t_row['uid'];
				}
			    $GLOBALS['TYPO3_DB']->sql_free_result($t_res);
				//
				$a[ $row['uid'] ] = array(
					'uid' => $row['uid'],
					'lastname' => $t_ln,
					'fullname' => $t_name,
					'page' => $row['page_detail'],
					'image' => $t_image,
					'funktion' => $row['funktion'],
					'berufstitel' => $row['berufstitel'],
					'phone' => $row['phone'],
					'email' => $row['email'],
				);
			}
    	}
	    $GLOBALS['TYPO3_DB']->sql_free_result($res);
    }
    //
    return array($t, $a);    

  }

}
?>