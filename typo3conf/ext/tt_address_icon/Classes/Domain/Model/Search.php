<?php
namespace NEXT\TtAddressIcon\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package tt_address_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Search extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

  /**
   * filter1
   *
   * @var \integer
   */
  protected $filter1;
  
  /**
   * filter2
   *
   * @var \integer
   */
  protected $filter2;


  /**
   * Sets the filter1
   *
   * @param \integer $filter1
   * @return void
   */
  public function setFilter1($filter1) {
    $this->filter1 = $filter1;
  }

  /**
   * Returns the filter1
   *
   * @return \integer $filter1
   */
  public function getFilter1() {
    return $this->filter1;
  }

  /**
   * Sets the filter2
   *
   * @param \integer $filter2
   * @return void
   */
  public function setFilter2($filter2) {
    $this->filter2 = $filter2;
  }

  /**
   * Returns the filter2
   *
   * @return \integer $filter2
   */
  public function getFilter2() {
    return $this->filter2;
  }

}
?>