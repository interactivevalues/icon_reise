<?php
namespace NEXT\TtAddressIcon\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 smo <r.schmoller@next-linz.com>, next e-Marketing GmbH.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Address model
 *
 * @package tt_address_icon
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Address extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * uid
	 * @var integer
	 */
	protected $uid;

	/**
	 * hidden
	 * @var boolean
	 */
	protected $hidden;

	/**
	 * ausgetretten
	 * @var boolean
	 */ 
	protected $ausgetretten;

	/**
	 * deleted
	 * @var boolean
	 */ 
	protected $deleted;

	/**
	 * gender
	 * @var string
	 */
	protected $geder;

    /**
     * Image
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image = null;

	/**
	 * title
	 * @var string
	 */
	protected $title;

	/**
	 * last_name
	 * @var string
	 */
	protected $lastName;

	/**
	 * middle_name
	 * @var string
	 */
	protected $middleName;

	/**
	 * first_name
	 * @var string
	 */
	protected $firstName;

	/**
	 * title_after
	 * @var string
	 */
	protected $titleAfter;

	/**
	 * funktion
	 * @var string
	 */
	protected $funktion;

	/**
	 * funktion_EN
	 * @var string
	 */
	protected $funktionEn;

	/**
	 * berugfstitel
	 * @var string
	 */
	protected $berufstitel;

	/**
	 * berufstitel_short
	 * @var string
	 */
	protected $berufstitelShort;

	/**
	 * berugfstitel_EN
	 * @var string
	 */
	protected $berufstitelEn;

	/**
	 * berufstitel_short_EN
	 * @var string
	 */
	protected $berufstitelShortEn;

	/**
	 * phone
	 * @var string
	 */
	protected $phone;

	/**
	 * fax
	 * @var string
	 */
	protected $fax;

	/**
	 * mobile
	 * @var string
	 */
	protected $mobile;

	/**
	 * email
	 * @var string
	 */
	protected $email;

	/**
	 * address
	 * @var string
	 */
	protected $address;

	/**
	 * building
	 * @var string
	 */
	protected $building;

	/**
	 * room
	 * @var string
	 */
	protected $room;

	/**
	 * company
	 * @var string
	 */
	protected $company;

	/**
	 * city
	 * @var string
	 */
	protected $city;

	/**
	 * zip
	 * @var string
	 */
	protected $zip;

	/**
	 * region
	 * @var string
	 */
	protected $region;

	/**
	 * country
	 * @var string
	 */
	protected $country;

	/**
	 * description
	 * @var string
	 */
	protected $description;

	/**
	 * descriptionEn
	 * @var string
	 */
	protected $descriptionEn;

	/**
	 * page_info
	 * @var string
	 */
	protected $pageInfo;

	/**
	 * addressgroup
	 * @var integer
	 */
	protected $addressgroup;

	/**
	 * hasBerufstitelFunktion
	 * @var boolean
	 */
	protected $hasBerufstitelFunktion;


	public function getUid () {
		return $this->uid;
	}

	public function getHidden () {
		return $this->hidden;
	}

	public function getAusgetretten () {
		return $this->ausgetretten;
	}

	public function getDeleted () {
		return $this->deleted;
	}

	public function getGender () {
		return $this->gender;
	}

	/**
	 * Returns the images
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
	 */
	public function getImage() {
		return $this->image;
	}

	public function getTitle () {
		return $this->title;
	}

	public function getLastName () {
		return $this->lastName;
	}

	public function getMiddleName () {
		return $this->middleName;
	}

	public function getFirstName () {
		return $this->firstName;
	}

	public function getTitleAfter () {
		return $this->titleAfter;
	}

	public function getFunktion () {
		return $this->funktion;
	}

	public function getFunktionEn () {
		return $this->funktionEn;
	}

	public function getMyFunktionEn () {
		return $this->funktionEn ? $this->funktionEn : $this->funktion;
	}

	public function getBerufstitel () {
		return $this->berufstitel;
	}

	public function getBerufstitelShort () {
		return $this->berufstitelShort;
	}

	public function getBerufstitelEn () {
		return $this->berufstitelEn;
	}

	public function getMyBerufstitelEn () {
		return $this->berufstitelEn ? $this->berufstitelEn : $this->berufstitel;
	}

	public function getBerufstitelShortEn () {
		return $this->berufstitelShortEn;
	}
	
	public function getMyBerufstitelShortEn () {
		return $this->berufstitelShortEn ? $this->berufstitelShortEn : $this->berufstitelShort;
	}

	public function getPhone () {
		return $this->phone;
	}

	public function getFax () {
		return $this->fax;
	}

	public function getMobile () {
		return $this->mobile;
	}

	public function getEmail () {
		return $this->email;
	}

	public function getWww () {
		return $this->www;
	}

	public function getAddress () {
		return $this->address;
	}

	public function getBuilding () {
		return $this->building;
	}
	
	public function getRoom () {
		return $this->room;
	}

	public function getCompany () {
		return $this->company;
	}

	public function getCity () {
		return $this->city;
	}

	public function getZip () {
		return $this->zip;
	}

	public function getRegion () {
		return $this->region;
	}

	public function getCountry () {
		return $this->country;
	}

	public function getDescription () {
		return $this->description;
	}

	public function getDescriptionEn () {
		return $this->descriptionEn;
	}

	public function getMyDescriptionEn () {
		return $this->descriptionEn ? $this->descriptionEn : $this->description;
	}

	public function getPageInfo () {
		return trim($this->pageInfo);
	}

	public function getAddressgroup () {
		return $this->addressgroup;
	}

	/*
	 * get hasBerufstitelFunktion
	 *
	 * return boolean
	 */
	public function getHasBerufstitelFunktion () {
		return $this->berufstitel . $this->funktion;
	}

	/*
	 * get hasBerufstitelFunktionEn
	 *
	 * return boolean
	 */
	public function getHasBerufstitelFunktionEn () {
		return ($this->berufstitelEn ? $this->berufstitelEn : $this->berufstitel) . ($this->funktionEn ? $this->funktionEn : $this->funktion);
	}

}
?>
