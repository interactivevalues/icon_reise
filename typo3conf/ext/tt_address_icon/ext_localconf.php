<?php

if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi1',
  array(
    'Countries' => 'list',
  ),
  // non-cacheable actions
  array(
    'Countries' => '',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi2',
  array(
    'Address' => 'index,detail',
  ),
  // non-cacheable actions
  array(
    'Address' => 'index',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi3',
  array(
    'Address' => 'detail',
  ),
  // non-cacheable actions
  array(
    'Address' => 'detail',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi4',
  array(
    'Countries' => 'map',
  ),
  // non-cacheable actions
  array(
    'Countries' => 'map',
  )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
  'NEXT.' . $_EXTKEY,
  'Pi5',
  array(
	'Vcard' => 'vcardFile',
  ),
  // non-cacheable actions
  array(
	'Vcard' => 'vcardFile',
  )
);


/*
	1:	registering contact.vcf for each hierachy of configuration to realurl (meaning to every website in a multisite installation)
	2:	adding config for RealURL address-access
*/
$realurl = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'];
if (is_array($realurl))  {
	//
	foreach ($realurl as $host => $cnf) {
		// we won't do anything with string pointer (e.g. example.org => www.example.org)
		if (!is_array($realurl[$host])) {
			continue;
		}
		if (!isset($realurl[$host]['fileName'])) {
			$realurl[$host]['fileName'] = array();
		}
		$realurl[$host]['fileName']['index']['contact.vcf']['keyValues']['type'] = 799;
	}
	
	
	//	APPLY fixedPostVars:
	if( !isset($realurl['_DEFAULT']['fixedPostVars']) ) { $realurl['_DEFAULT']['fixedPostVars'] = array(); }
	
	$realurl['_DEFAULT']['fixedPostVars']['mitarbeiterConfiguration'] = array(
		array(
			'GETvar' => 'tx_ttaddressicon_pi2[action]',
/*
			'valueMap' => array(
				'detail' => '',
			),
*/
			'noMatch' => 'bypass'
		),
		array(
			'GETvar' => 'tx_ttaddressicon_pi2[controller]',
/*
			'valueMap' => array(
				'Address' => '',
			),
*/
			'noMatch' => 'bypass'
		),
		array(
			'GETvar' => 'tx_ttaddressicon_pi2[address]',
			'lookUpTable' => array(
				'table' => 'tt_address',
				'id_field' => 'uid',
				'alias_field' => "CONCAT(last_name, '-', first_name)",
				'addWhereClause' => ' AND NOT deleted ',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-'
				),
/*
				'languageGetVar' => 'L',
				'languageExceptionUids' => '',
				'languageField' => 'sys_language_uid',
				'transOrigPointerField' => 'l10n_parent',
*/
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			)
		),
	);
	$realurl['_DEFAULT']['fixedPostVars']['ansprechpartnerConfiguration'] = array(
		array(
			'GETvar' => 'tx_ttaddressicon_pi3[action]',
/*
			'valueMap' => array(
				'detail' => '',
			),
*/
			'noMatch' => 'bypass'
		),
		array(
			'GETvar' => 'tx_ttaddressicon_pi3[controller]',
/*
			'valueMap' => array(
				'Address' => '',
			),
*/
			'noMatch' => 'bypass'
		),
		array(
			'GETvar' => 'tx_ttaddressicon_pi3[address]',
			'lookUpTable' => array(
				'table' => 'tt_address',
				'id_field' => 'uid',
				'alias_field' => "CONCAT(last_name, '-', first_name)",
				'addWhereClause' => ' AND NOT deleted ',
				'useUniqueCache' => 1,
				'useUniqueCache_conf' => array(
					'strtolower' => 1,
					'spaceCharacter' => '-'
				),
/*
				'languageGetVar' => 'L',
				'languageExceptionUids' => '',
				'languageField' => 'sys_language_uid',
				'transOrigPointerField' => 'l10n_parent',
*/
				'enable404forInvalidAlias' => 1,
				'autoUpdate' => 1,
				'expireDays' => 180,
			)
		),
	);
	$realurl['_DEFAULT']['fixedPostVars']['133'] = 'mitarbeiterConfiguration';
	$realurl['_DEFAULT']['fixedPostVars']['134'] = 'mitarbeiterConfiguration';
	$realurl['_DEFAULT']['fixedPostVars']['135'] = 'mitarbeiterConfiguration';
	$realurl['_DEFAULT']['fixedPostVars']['136'] = 'mitarbeiterConfiguration';
	$realurl['_DEFAULT']['fixedPostVars']['418'] = 'ansprechpartnerConfiguration';
	
	//	reapply changes to TYPO3_CONF_VARS
	$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = $realurl;
}


/*
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tt_address']['extraItemMarkerHook'][]='EXT:tt_address_icon/class.tx_ttaddressvg_extramarker.php:tx_ttaddressicon_extramarker';
*/

?>

