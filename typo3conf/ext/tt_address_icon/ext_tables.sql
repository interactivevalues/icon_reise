#
# Table structure for table 'tt_address'
#
CREATE TABLE tt_address (
	title_after varchar(255) DEFAULT '' NOT NULL,
	funktion varchar(255) DEFAULT '' NOT NULL,
	berufstitel varchar(255) DEFAULT '' NOT NULL,
	berufstitel_short varchar(255) DEFAULT '' NOT NULL,
	page_detail tinytext,
	ausgetretten tinyint(4) DEFAULT '0' NOT NULL,
	board_member tinyint(4) DEFAULT '0' NOT NULL,
	description_EN text NOT NULL,
	funktion_EN varchar(255) DEFAULT '' NOT NULL,
	berufstitel_EN varchar(255) DEFAULT '' NOT NULL,
	berufstitel_short_EN varchar(255) DEFAULT '' NOT NULL,
);

CREATE TABLE sys_category (
	configuration_map varchar(255) DEFAULT '' NOT NULL,
	latitude double(11,6) DEFAULT '0.000000' NOT NULL,
	longitude double(11,6) DEFAULT '0.000000' NOT NULL,
	address varchar(255) DEFAULT '' NOT NULL,
);
