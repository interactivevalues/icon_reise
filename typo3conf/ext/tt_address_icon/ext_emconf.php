<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "tt_address_icon".
 *
 * 2014-04-20, r.schmoller@next-linz.com
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Address List - ICON',
  'description' => 'extends tt_address',
  'category' => 'plugin',
  'author' => 'smo',
  'author_email' => 'r.schmoller@next-linz.com',
  'shy' => '',
  'dependencies' => '',
  'conflicts' => '',
  'priority' => '',
  'module' => '',
  'state' => 'stable',
  'internal' => '',
  'uploadfolder' => 0,
  'createDirs' => '',
  'modify_tables' => '',
  'clearCacheOnLoad' => 1,
  'lockType' => '',
  'author_company' => '',
  'version' => '0.0.5',
  'constraints' => array (
    'depends' => array (
      'typo3' => '6.2',
      'fluid' => '6.2',
      'extbase' => '6.2',
      'tt_address' => '2.3.4',
	  'realurl' => '2.0.0',
    ),
    'conflicts' => array (),
    'suggests' => array (),
  ),
);

?>
