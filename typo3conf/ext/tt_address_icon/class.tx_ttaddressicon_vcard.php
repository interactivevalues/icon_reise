<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2014 smo <r.schmoller@next-linz.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/


//The vCard spec calls for the termination of each property with a carriage return followed by a line feed. Rather than cluttering this file with "chr(10) . chr(13)"
//everywhere, I'll use CRLF in its place by defining it here.
define(CRLF, "\r\n");

/**
 * Library functions for the 'tt_address_icon' extension.
 *
 * @author  smo <r.schmoller@next-linz.com>
 * @package  TYPO3
 * @subpackage  tx_address_icon
 */
 
class tx_ttaddressicon_vcard {

  var $conf;
  var $mapping;
  var $vCardContent;
  var $pid;

  /**
   * Generates a vCard from the specified (URL parameter) address in the tt_address table.
   *
   * @param       string  the content to be filled, usually empty
   * @param       array  additional configuration parameters
   * @return      string  the XML sitemap ready to render
   */
  function createVCard($content, $conf) {
  
    //Get the configuration for the plugin.
    $this->conf = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_ttaddressicon.'];

    //Obtain the plugin's locallang file and extract the current language's array.
    $lang = $GLOBALS['TSFE']->tmpl->setup['config.']['language'] != '' ? $GLOBALS['TSFE']->tmpl->setup['config.']['language'] : 'default' ;
//    $LL = \TYPO3\CMS\Core\Utility\GeneralUtility::readLLfile('EXT:tt_address_icon/locallang.xml', $lang);
	$fileToParse = 'EXT:tt_address_icon/locallang.xml';
	
	$languageFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Localization\LocalizationFactory');
	$languageFactory->getParsedData($fileToParse, $lang, $renderCharset, $errorMode);
	$LL = $languageFactory->getParsedData($fileToParse, $lang);
var_dump($LL);
	$LL = $lang != '' ? $LL[$lang] : $LL['default'];
var_dump($LL);

    //Set the error title string for the returnError function
    $this->errorTitle = $LL['error_title'];
    
/*
    //Check the URI for the "pid" parameter, and get its value.
    preg_match('/[\\?&|&amp;]pid=([^&#]*)/', $GLOBALS['TSFE']->siteScript, $matches);
    if(!isset($matches[1]))
      return $this->returnError($LL['pid_parse_error']);

    //Check it's a number, and ONLY a number.
    if(!preg_match('/^\d+$/', $matches[1]))
      return $this->returnError($LL['pid_not_number']);
      
    //If must be a valid number if we get here
    $this->pid = $matches[1];

    $allowedPids = explode(",", $this->conf['allowedPids']);
    if(!in_array($this->pid, $allowedPids))
      return $this->returnError($LL['pid_not_allowed_error']);
            
    //Check if contacts exist for the supplied PID
    $tmpResults = t3lib_BEfunc::getRecordsByField('tt_address', 'pid', $this->pid);
    if(!is_array($tmpResults))
      return $this->returnError($LL['pid_no_addresses']);
*/

/*
    //Check the URI for the "team" parameter, and get its value.
	var_dump($GLOBALS['TSFE']->siteScript);
	var_dump($matches);
    preg_match('/[\\?&|&amp;]team=([^&#]*)/', $GLOBALS['TSFE']->siteScript, $matches);
	var_dump($matches);
    if(!isset($matches[1]))
	return 'smo-1';
      return $this->returnError($LL['id_parse_error']);
	return 'smo-2';
    
    //Check it's a number, and ONLY a number.
    if(!preg_match('/^\d+$/', $matches[1]))
      return $this->returnError($LL['id_not_number']);
*/
	$this->uid = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP("team");
var_dump($this->uid);

    //If must be a valid number if we get here
 //   $this->uid = $matches[1];

    //Check if a record exists for that PID and that UID.
//    $result = t3lib_BEfunc::getRecordsByField('tt_address', 'uid', $this->uid, ' AND hidden <> 1 AND deleted <> 1');
//	$result = TYPO3\CMS\BackendUtility\BackendUtility::getRecordsByField('tt_address', 'uid', $this->uid, ' AND hidden <> 1 AND deleted <> 1');
	$queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tt_address');
/*
    $result = t3lib_BEfunc::getRecordsByField('tt_address', 'pid', $this->pid, ' AND uid = ' . $this->uid . ' AND hidden <> 1 AND deleted <> 1');
*/
    if(!is_array($result))
      return $this->returnError($LL['id_not_found']);

    //If we get here, it's because the PID and UID returned a valid result.
    $this->contact = $result[0];

    //Escape all the returned data.
    foreach($this->contact as &$row) {
      $row = $this->escapeData($row);
    }
	
	// for TEAM add predefined values
	$this->contact['phone'] = '+43 732 69412 ' . $this->contact['phone'];
	$this->contact['fax'] = '+43 732 69412 ' . $this->contact['fax'];
	$this->contact['address'] = 'Stahlstrasse 14';
	$this->contact['zip'] = '4020';
	$this->contact['cty'] = 'Linz';
	$this->contact['country'] = 'Austria';
	$this->contact['address'] = 'Stahlstrasse 14;Linz;;4020;Austria';
	$this->contact['company'] = 'ICON';
	$this->contact['www'] = 'http://www.icon.at';

    //Configure the mapping; which fields in the VCF map to which fields in tt_address
    $this->mapping = $this->explodeMapping($this->conf['mapping.']['team.']);
    if(!$this->mapping)
      return $this->returnError($LL['mapping_error']);
    
    //Start building the vCard
    $this->vCardContent = "BEGIN:VCARD" . CRLF;
    $this->vCardContent .= "VERSION:3.0" . CRLF;

    //Build the variable content
    $this->vCardContent .= $this->buildVCard();
    
    //Finish off the vCard
    $this->vCardContent .= "CLASS:PUBLIC" . CRLF;
    $this->vCardContent .= "END:VCARD";
    
    //Set the target directory for saving the vCard, and set the file name.
    $relPath = 'typo3temp/tt_address_icon/';
    $dname = PATH_site . $relPath;
    $fname = ($this->contact['name'] != '' ? $this->contact['name'] : 'contact') . '.vcf';

    if(!is_dir($dname))
      \TYPO3\CMS\Core\Utility\GeneralUtility::mkdir($dname);
    
    //Delete all existing files in the folder.
//    foreach(glob($dname.'*.*') as $v){unlink($v);}

    //Create the file if it doesn't exist and open a handle to it.
    //touch($dname . $fname);
    $fp = fopen($dname . $fname, 'w');

    //Write to the file and close it.
    
    $this->vCardContent = utf8_decode($this->vCardContent);
    
    fwrite($fp, $this->vCardContent);
    fclose($fp);

    //Redirect the user's browser to the file.
    header ('Location: ' . \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL') . $relPath . $fname);

  }

  //Returns an error to the browser. If you want more meaningful errors, edit the locallang file :)
  function returnError($error) {
    Header('Content-type: text/html; charset=utf-8');
    return '<h1>' . $this->errorTitle . '</h1><p>' . $error . '</p>' . $this->debuggy;
  }


  //Build the various properties that comprise the vCard. Some of the properties have a very specific syntax, and for these properties there is a
  //function specifically to deal with them.
  function buildVCard() {
    $vCardData = '';
    foreach($this->mapping as $property => $value) {
      switch($property) {
        case 'N' : $vCardData .= $this->buildProperty($property, $value, 1, 'CHARSET=iso-8859-1'); break;
        case 'FN' : $vCardData .= $this->buildProperty($property, $value, 0, 'CHARSET=iso-8859-1'); break;
        case 'TEL' : $vCardData .= $this->buildTEL($value); break;
        case 'PHOTO' : $vCardData .= $this->buildPHOTO($value); break;
        case 'BDAY' : $vCardData .= $this->buildDATE('BDAY', $value); break;
        case 'ADR' : $vCardData .= $this->buildADR($value); break;
        case 'EMAIL' : $vCardData .= $this->buildEMAIL($value); break;
        case 'WWW' : $vCardData .= $this->buildURL($value); break;
        case 'REV' : $vCardData .= $this->buildDATE('REV', $value); break;
        default : $vCardData .= $this->buildProperty($property, $value);
      }
    }
    return $vCardData;
  }


  //Build the EMAIL property
  function buildEMAIL($value) {
    return 'EMAIL;TYPE=internet:' . $this->contact[$value] . CRLF;
  }

  //Build the URL property
  function buildURL($value) {
    return 'URL;' . $this->contact[$value] . CRLF;
  }

  //Build the ADR property
  function buildADR($value) {
    
    $arrTemp = split(":", $value);
    if(count($arrTemp) > 1) {
      $locations = "TYPE=" . join(",", split(";", $arrTemp[0]));
      $addressField = $arrTemp[1];
    } else
      $addressField = $value;
    
    if(empty($this->contact[$addressField]))
      return '';
      
    $address = array('POBOX' => '', 'EXTADD' => '', 'STREET' => '', 'CITY' => '', 'STATE' => '', 'POSTCODE' => '', 'COUNTRY' => '');
    $address['STREET'] = implode(',',explode("\r\n",$this->contact['address']));    
    $address['CITY'] = $this->contact['city'];
    $address['STATE'] = $this->contact['zone'];
    $address['POSTCODE'] = $this->contact['zip'];
    $address['COUNTRY'] = $this->contact['country'];
    
    $label = 'LABEL;ENCODING=QUOTED-PRINTABLE:';
    foreach($address as $addr_el)
      if(!empty($addr_el))
        $label .= $addr_el . '=0D=0A';
    $label = substr($label, 0, -6);
    return 'ADR;' . $locations . ':' . implode(';', $address) . CRLF . $label . CRLF;
  }
  

  //Build the DATE property  
  function buildDATE($type, $value) {
    $date = $this->contact[$value];
    if(!empty($date))
      return $type . ':' . date('Y-m-d',$date) . CRLF;
    return '';
  }


  //Build the PHOTO property
  function buildPHOTO($value) {
    
    if(empty($this->contact[$value]))
      return '';
      
    $fnameRel = 'uploads/pics/' . $this->contact[$value];
    $fnameAbs = PATH_site . $fnameRel;
    if(file_exists($fnameAbs))    
      return 'PHOTO;ENCODING=b;TYPE=JPEG:' . base64_encode(addslashes(fread(fopen($fnameAbs, "r"), filesize($fnameAbs)))) . CRLF;
    return '';
  }
  
  
  //Build the TEL property
  function buildTEL($value) {
  
    $propString = '';
    
    if(is_array($value)) {
      foreach($value as $phoneType) {
        $arrTmp = split('\.', $phoneType);
        if(!empty($this->contact[$arrTmp[1]])) {
          switch($arrTmp[0]) {
            case 'WORK' : $propString .= 'TEL;WORK;VOICE:' . $this->contact[$arrTmp[1]] . CRLF; break;
            case 'CELL' : $propString .= 'TEL;CELL;VOICE:' . $this->contact[$arrTmp[1]] . CRLF; break;
            case 'FAX' : $propString .= 'TEL;FAX:' . $this->contact[$arrTmp[1]] . CRLF; break;
            default : $propString .= 'TEL:' . $this->contact[$arrTmp[1]] . CRLF; break;
          }
        }
      }
    } else {
      if(!empty($this->contact[substr(strstr($value, '.'), 1)]))
        $propString .= 'TEL;VOICE:' . $this->contact[substr(strstr($value, '.'), 1)] . CRLF;
    }
    return $propString;
  }
  
  
  //Build the LABEL property
  function buildLABEL($value) {
    return 'LABEL:' . $value . 'REFORMATTED' . CRLF;
  }

  
  //Build a "generic" property that has no special syntax
  function buildProperty($property, $value, $allowEmpty=0, $encoding='') {

	if( $encoding ){
	    $propString = $property . ';' . $encoding . ':';
	} else {
	    $propString = $property . ':';
	}
    
    if(is_array($value)) {
      foreach($value as $val) {
        if($allowEmpty) {
            $propString .= $this->contact[$val] . ';';          
        } else {
          if(!empty($this->contact[$val]))
            $propString .= $this->contact[$val] . ';';
          else
            return '';
        }
      }
      $propString = substr($propString, 0, -1); //Removes trailing ;
    } else {
      if(!empty($this->contact[$value]))
        $propString .= $this->contact[$value];
      else
        return '';
    }
    return $propString . CRLF;
  }


  //Escape commas and semicolons, as they are part of the specification syntax  
  function escapeData ($data) {
    
    //Even though it's in the spec, I can't find an address book client that supports it! So I'll leave it out.
    //return utf8_decode(str_replace(',','\,', str_replace(';','\;', str_replace(':','\:',$data))));
    //return utf8_decode($data);
    return $data;
  }


  function explodeMapping($arrMapping) {

    foreach($arrMapping as $key => $value) {
      $arrTemp = split(",", $value);
      if(count($arrTemp) > 1) {
        $arrMapping[$key] = array();
        foreach($arrTemp as $newvalue)
          $arrMapping[$key][] = $newvalue;
      }
    }
    
    return $arrMapping;
  }
    
}


if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tt_address_icon/class.tx_ttaddressicon_vcard.php']) {
   include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tt_address_icon/class.tx_ttaddressicon_vcard.php']);
}
?>