<?php

/**
 *
 * @author    SMO <r.schmoller@next-linz.com>
 */
class tx_ttaddressvg_extramarker {

    function extraItemMarkerProcessor(&$markerArray, &$address, &$lConf, &$pObj) {
        $lcObj = t3lib_div::makeInstance('tslib_cObj');
        $lcObj->data = $address;
        
        $markerArray['###TITLE_AFTER###']  = $lcObj->stdWrap($address['title_after'],$lConf['title_after.']);
        $markerArray['###LINK_MORE###']  = $lcObj->stdWrap($address['link_more'],$lConf['link_more.']);
        $markerArray['###LINK_MORE_TEXT###']  = $lcObj->stdWrap($address['link_more_text'],$lConf['link_more_text.']);

        return $markerArray;
    } 

}

?>
